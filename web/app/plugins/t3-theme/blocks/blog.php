<?php

add_filter('sage/blocks/blog/data', function (array $block) {
	$homeurl = App::homeurl();
	$listProps = get_field('blog_props');
	$block['type'] = $listProps['type'];
	$block['showDate'] = $listProps['show_date'];
	$block['pagination'] = $listProps['has_pages'];
	$block['pagination_html'] = '';
	$block['postType'] = 'post';
	$orderby = 'publish_date';
	$imgSize = 'card-small';

	if($block['type']  = 'cards') {
		$block['listDesign'] = 'b-cards b-cards--offset grid-x grid-margin-x grid-margin-y';
	}
	else {
		//TODO adding w/o img listtype
		$block['listDesign'] = 'b-list b-list--img';
	}


	/** Blog list */
	if(!is_admin()):
		$posts_per_page = -1;
	else:
		$posts_per_page = 3;
	endif;

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$query_array = array(
		'post_type' => array($block['postType']), //array('product'), //
		'posts_per_page'	=> $posts_per_page,
		'orderby'			=> 'publish_date',
		'order'				=> 'DESC',
		'paged'				=> $paged,
	);

	$loop = new \WP_Query( $query_array );
	if($block['type'] == 'cards'):
		while ( $loop->have_posts() ) : $loop->the_post();
			$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
      $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
      if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }
      $excerpt = get_the_excerpt();
      $imgTitle = App::setImgTitleAttr($thumbnail_id);

			$block['list_items'][] = \App\template('partials.parts.items.card-item-blog', [
				'title' => get_the_title(),
				'content' => $excerpt,
				'linkUrl' => get_the_permalink(),
				'imageSmall' => $homeurl . get_the_post_thumbnail_url(get_the_ID(), $imgSize ),
        'alt' => $alt,
        'imgTitle' => $imgTitle, //App::getImgTitleAttr($thumbnail_id),
				'linkTitle' => __('weiterlesen', 't3-theme'),
				'showDate' => $block['showDate'],
				'date' => get_the_date('d.m.Y'),
			]);
		endwhile;
	endif;

	$big = 999999999;
	$paginate_links = paginate_links(
		array(
			'base'      => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
			'current'   => max( 1, get_query_var( 'paged' ) ),
			'total'     => $loop->max_num_pages,
			'mid_size'  => 5,
			'prev_next' => true,
			'prev_text' => __( '&laquo;', 't3theme' ),
			'next_text' => __( '&raquo;', 't3theme' ),
			'type'      => 'list',
		)
	);

	$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination text-center' role='navigation' aria-label='Pagination'>", $paginate_links );
	$paginate_links = str_replace( '<li><span class="page-numbers dots">', "<li><a href='#'>", $paginate_links );
	$paginate_links = str_replace( '</span>', '</a>', $paginate_links );
	$paginate_links = str_replace( "<li><span class='page-numbers current'>", "<li class='current'>", $paginate_links );
	$paginate_links = str_replace( "<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links );
	$paginate_links = preg_replace( '/\s*page-numbers/', '', $paginate_links );

	// Display the pagination if more than one page is found.
	if ( $paginate_links ):
		$block['pagination_html'] = $paginate_links;
	endif;

	$block['pagination_html'] .= next_posts_link('<span icon="icon-arrow-left"></span>',$loop->max_num_pages);

	// 	while ( $loop->have_posts() ) : $loop->the_post();
	// 		$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
	// 		$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
	//
	// 		$block['list_items'][] = \App\template('partials.parts.items.list-item-blog', [
	// 			'title' => get_the_title(),
	// 			'content' => get_the_excerpt(),
	// 			'linkUrl' => get_the_permalink(),
	// 			'imageSmall' => get_the_post_thumbnail_url(get_the_ID(), $imgSize ),
	// 			'alt' => $alt,
	// 			'moreLinkText' => __('t3theme', 'mehr erfahren'),
	// 		]);
	// 	endwhile;
	// endif;
	//
	// wp_reset_query();

	return $block;
});

add_action('wp_ajax_filterb', 'blog_filter_function'); // wp_ajax_{ACTION HERE}
add_action('wp_ajax_nopriv_filterb', 'blog_filter_function');
function blog_filter_function(){
	if ( isset( $_GET[ 'wpml_lang' ] ) ) {
		do_action( 'wpml_switch_language',  $_GET[ 'wpml_lang' ] );
	}

	// $params = json_decode( stripslashes( $_POST['query'] ), true );
	if(isset($_POST['next'])):
		$params['paged'] = $_POST['current_page'] + 1;
	elseif(isset($_POST['prev'])):
		$params['paged'] = $_POST['current_page'] - 1;
	else:
		$params['paged'] = $_POST['current_page'];
	endif;
	$params['post_type'] = $_POST['postType'];
	$params['post_status'] = 'publish';
	$params['posts_per_page'] = $_POST['postsPerPage'];
	$params['suppress_filters'] = false;
	$params['orderby'] = 'publish_date';
	$params['order'] = 'DESC';
	// if(!isset($_POST['prev']) && isset($_POST['next'])):
	// 	$params['paged'] = $POST['paged'];
	// elseif(isset($_POST['prev'])):
	// 	$_POST['page'] + 1;
	// elseif(isset($_POST['prev'])):
	// 	$_POST['page'] - 1;
	// endif;

	if($_POST['cat'] != '0'):
		$params['cat'] = $_POST['cat'];
	endif;

	// $query_array = array(
	// 	'post_type' => array($block['postType']), //array('product'), //
	// 	'posts_per_page'	=> $posts_per_page,
	// 	'orderby'			=> 'publish_date',
	// 	'order'				=> 'DESC',
	// 	'paged'				=> $paged,
	// );

	// $loop = new \WP_Query( $query_array );
	// if($block['type'] == 'cards'):
	// 	while ( $loop->have_posts() ) : $loop->the_post();
	// 		$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
	// 		$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
	// 		$excerpt = get_the_excerpt();
	//
	// 		$block['list_items'][] = \App\template('partials.parts.items.card-item-blog', [
	// 			'title' => get_the_title(),
	// 			'content' => $excerpt,
	// 			'linkUrl' => get_the_permalink(),
	// 			'imageSmall' => get_the_post_thumbnail_url(get_the_ID(), $imgSize ),
	// 			'alt' => $alt,
	// 			'linkTitle' => __('mehr erfahren', 't3theme'),
	// 			'showDate' => $block['showDate'],
	// 			'date' => get_the_date('d.m.Y'),
	// 		]);
	// 	endwhile;
	// endif;


	query_posts( $params );
	global $wp_query;
	$posts_html;

	if( have_posts() ) :
		while( have_posts() ): the_post();
			$imageUrl = get_the_post_thumbnail_url(get_the_ID(), 'card-small' );
			$thumbnailId = get_post_thumbnail_id( get_the_ID() );
			$alt = get_post_meta($thumbnailId, '_wp_attachment_image_alt', true);
      if(empty($alt)) { $alt = App::getImgFilename($thumbnailId); }
      $imgTitle = App::setImgTitleAttr($thumbnailId);
      
			// Testing
			//$posts_html  .= get_the_title( get_the_ID());

			// Markup

			ob_start();
			echo '
			<li class="b-cards__item cell small-12 medium-6 large-4">
        <div class="b-cards__header">';
			    if(!empty(get_the_permalink())):
			      echo '<a href="' . get_the_permalink() . '" class="hasHover">
			          <img data-src="' . get_the_post_thumbnail_url(get_the_ID(), 'cards-small' ) .'" alt="' . $alt . '" class="lazyload b-cards__img"' . $imgTitle . ' />
			      </a>';
			  	else:
			        echo '<img data-src="' . get_the_post_thumbnail_url(get_the_ID(), 'cards-small' ) .'" alt="' . $alt . '" class="lazyload b-cards__img"' . $imgTitle . ' />';
			    endif;
			  echo '</div>
			  <div class="b-cards__body">';
			    if(!empty(get_the_permalink())):
			      echo '<a href="' . get_the_permalink() . '" class="b-cards__title no-link-style">
			        <h3>' . get_the_title() . '</h3>
			      </a>';
			  	else:
			      echo '<h3 class="b-cards__title">' . get_the_title() . '</h3>';
			  	endif;
			    if(!empty($_POST['showDate'])):
			      echo '<p class="date">' . get_the_date('d.m.Y') . '</p>';
			  	endif;
			    echo '<p>' . get_the_excerpt() . '</p>';
			    if(!empty(get_the_permalink())):
			    echo '<a href="' . get_the_permalink() . '" class="more">' . __('mehr erfahren', 't3theme') . '</a>';
				endif;
			  echo '</div>
			</li>';
			$posts_html .= ob_get_contents();
			ob_end_clean();

		endwhile;
	endif;

	$pagination_html;

	$big = 999999999;
	$paginate_links = paginate_links(
		array(
			'base'      => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
			'current'   => max( 1, get_query_var( 'paged' ) ),
			'total'     => $wp_query->max_num_pages,
			'mid_size'  => 5,
			'prev_next' => true,
			'prev_text' => __( '&laquo;', 't3theme' ),
			'next_text' => __( '&raquo;', 't3theme' ),
			'type'      => 'list',
		)
	);

	$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination text-center' role='navigation' aria-label='Pagination'>", $paginate_links );
	$paginate_links = str_replace( '<li><span class="page-numbers dots">', "<li><a href='#'>", $paginate_links );
	$paginate_links = str_replace( '</span>', '</a>', $paginate_links );
	$paginate_links = str_replace( "<li><span class='page-numbers current'>", "<li class='current'>", $paginate_links );
	$paginate_links = str_replace( '/wp/wp-admin/admin-ajax.php?paged=', '/blog/page/', $paginate_links );
	$paginate_links = str_replace( "<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links );
	$paginate_links = preg_replace( '/\s*page-numbers/', '', $paginate_links );

	//Display the pagination if more than one page is found.
	if ( $paginate_links ):
		$pagination_html = $paginate_links;
	endif;
	ob_start();
	next_posts_link('<span icon="icon-arrow-left"></span>',$wp_query->max_num_pages);
	$pagination_html .= ob_get_contents();
	ob_end_clean();
	//
	echo json_encode( array(
		'query' => $_POST['query'],
		'paged' => $_POST['paged'],
		'page' => $_POST['current_page'],
		'posts' => json_encode( $wp_query->query_vars ),
		'max_page' => $wp_query->max_num_pages,
		'found_posts' => $wp_query->found_posts,
		'content' => $posts_html,
		//'pagination' => $pagination_html,
		'pagination' => '<a href="#" id="prev">back</a> - <a href="#" id="next">next</a>',
	) );

	// echo 'JUHU';

	// echo $posts_html;

	die();
    // print_r($_POST);
}
