<?php

add_filter('sage/blocks/accordions/data', function (array $block) {
	$tag = get_field('acc_heading_tag');

	/** Accordion Items */
    $acc = get_field('acc_items');
    $block['accBorder'] = get_field('acc_has_border');
    $bColor = get_field('acc_border_color');
    $c = 0;

    if(is_array($acc)) {
        foreach( $acc as $accItem ) {
            $title = $accItem['acc_item_title'];
            $content = $accItem['acc_item_text'];

            $block['acc_items'][] = \App\template('partials.parts.items.acc-item', [
                'title' => $title,
                'content' => $content,
                'tag' => $tag,
                'id' => 'acc-inp-' . $c,
                'bColor' => $bColor,
                ]
            );
        }
    }

    return $block;
});
