@if($adv_data['show_section'])
  {{-- @php var_dump($adv_data); @endphp --}}
  @if($adv_data['cardDesign'] == 'textblock')
  <section class="wp-block-cgb-t3-section-block b-pb-none alignfull is-style-bg-transparent">
    <div class="wp-block-cgb-t3-container-block none" style="background-color:none">
      <div class="grid-container">
        <div class="grid-x">
          <div class="cell small-12 medium-9 large-0 medium-offset-1 b-has-lsmb">
            @if(!empty($adv_data['intro_title']))
              <h2>{!! $adv_data['intro_title'] !!}</h2>
            @else
              <h2><?php _e('Vorteile', 'leeb'); ?></h2>
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="wp-block-cgb-t3-container-block none b-cont-columns">
      <div class="grid-container">
        <div class="grid-x">
          <div class="cell small-12 medium-10 large-0 medium-offset-1">
            {!! $adv_data['intro_text'] !!}
          </div>
        </div>
      </div>
    </div>
  </section>
  @else
  <section class="b-pb-small is-style-bg-transparent b-pt-none">
      <div class="wp-block-cgb-t3-container-block b-has-offset-b bg--primary">
        <div class="has-padding none grid-container-wrapper">
          <div class="grid-container">
            <div class="grid-x">
              <div class="cell small-12 medium-9 medium-offset-1">
                  <h2><?php _e('Vorteile', 'leeb'); ?></h2>
              </div>
            </div>
          </div>
        </div>
      </div>

    <div class="wp-block-cgb-block-t3-grid-block">
      <div class="grid-container">
        <div class="grid-x">
          {{-- @if(!$adv_data['intro_offset'])
            <div class="cell small-12 b-has-pt">
          @else --}}
            <div class="cell small-12">
          {{-- @endif --}}
            <div class="l-wrapper">
              @if($adv_data['cardDesign'] != 'icon')
                <ul class="c-list b-cards b-cards--{!!$adv_data['cardDesign']!!} b-cards--cont grid-x grid-margin-x grid-margin-y">
                  @if(isset($adv_data['card_items']) && count($adv_data['card_items']) > 0)
                    @foreach ($adv_data['card_items'] as $value)
                      {!! $value !!}
                    @endforeach
                  @endif
                </ul>
              @else
                <ul class="c-list b-iconlist b-iconlist--adv grid-x">
                  @if(isset($adv_data['card_items']) && count($adv_data['card_items']) > 0)
                    @foreach ($adv_data['card_items'] as $item)
                      <li class="b-iconlist__item {{$adv_data['cellClasses']}}">
                        {!! $item !!}
                      </li>
                    @endforeach
                  @endif
                </ul>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endif
@endif
