<!doctype html>
<html {!! get_language_attributes() !!} class="no-js">
  @include('partials.head')
  <body @php body_class() @endphp id="top">
    {{-- <div class="off-canvas-wrapper">
      <div class="off-canvas position-right" id="offCanvas" data-off-canvas>
        @include('partials.mobile-nav')
      </div>
      <div class="off-canvas-content" data-off-canvas-content> --}}
        @php do_action('get_header') @endphp
        @include('partials.header-mmm')
        <main class="main">
          @include('partials.featuredimage')
          <section class="u-bg--white b--pt-small">
            <div class="grid-container" role="document">
              <div class="grid-x">
                <div class="cell small-12 medium-6 large-7 medium-offset-1">
                  @yield('content')
                </div>
                @if (App\display_sidebar())
                  <aside class="sidebar cell small-12 medium-6 large-3">
                    @include('partials.sidebar')
                  </aside>
                @endif
              </div>
            </div>
          </section>
          @include('partials.parts.components.floating-nav')
        </main>
        @php do_action('get_footer') @endphp
        @include('partials.footer')
        @php wp_footer() @endphp
      {{-- </div>
    </div> --}}
  </body>
</html>
