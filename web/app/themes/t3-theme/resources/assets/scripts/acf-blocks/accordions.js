// (function ($) {
//   var initializeBlock = function( $block ) {

//   }
//   $(window).on('load', function() {
//     var acc = document.querySelectorAll('.b-acc__title');
//     var i;

//     for (i = 0; i < acc.length; i++) {
//       acc[i].addEventListener('click', function () {
//         this.classList.toggle('active');
//         var panel = this.nextElementSibling;
//         if (panel.style.maxHeight) {
//           panel.style.maxHeight = null;
//         } else {
//           panel.style.maxHeight = (panel.scrollHeight + 20) + 'px';
//         }
//       });
//     }
//   });
// })(jQuery);

(function ($) {
  var initializeBlock = function( $block ) {
    var acc = document.querySelectorAll('.b-acc__title');
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function () {
        this.classList.toggle('active');
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = (panel.scrollHeight + 20) + 'px';
        }
      });
    }
  }
  // Initialize each block on page load (front end).
  $(document).ready(function(){
    initializeBlock( $(this) );
  });

  // Initialize dynamic block preview (editor).
  if( window.acf ) {
    console.log('ADMIN');
    window.acf.addAction( 'render_block_preview/type=accordions', initializeBlock );
  }
})(jQuery);