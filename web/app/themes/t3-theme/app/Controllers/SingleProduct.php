<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleProduct extends Controller
{
  public function productTeaser() {
    return get_field('product_teaser');
  }

  public function productContent() {
    return get_field('product_text');
  }

  public function productSidebar() {
    return get_field('product_sidebar');
  }

  public function getParentCat() {
    if( is_singular('product') ) {
      $terms = wp_get_post_terms( get_the_ID(), 'product_category' );
      foreach($terms as $term) {
        if( $term->parent == '0' ) {
          return $term->slug;
        }
      }
    }
  }

  public function productOptions() {
    $options = '';
    $links = get_field('product_links');
    $image = get_field('product_rendering');

    if($image):
      !empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);
      $options .= '<img src="' . $image['sizes']['fp-small'] . '" alt="' . $alt . '" width="' . $image['sizes']['fp-small-width'] . '" height="' . $image['sizes']['fp-small-height'] . '" class="product-rendering" />';
    endif;
    // if($links['link_call']) {
    //     $options .= '<a href="#" class="button hollow">' . __('Anrufen', 't3-theme') . '</a>';
    // }


    // $linkArray = array(
    //     'de' => array(
    //         'catalog' => '/kontakt/prospekt-anfordern/',
    //         'partner' => '/kontakt/vertriebspartner/',
    //         'offert' => '/kontakt/preisangebot/',
    //     ),
    //     'at' => array(
    //         'catalog' => '/kontakt/prospekt-anfordern/',
    //         'partner' => '/kontakt/vertriebspartner/',
    //         'offert' => '/kontakt/preisangebot/',
    //     ),
    //     'ch' => array(
    //         'catalog' => '/kontakt/prospekt-anfordern/',
    //         'partner' => '/kontakt/vertriebspartner/',
    //         'offert' => '/kontakt/preisangebot/',
    //     ),
    //     'it' => array(
    //         'catalog' => '/kontakt/prospekt-anfordern/',
    //         'partner' => '/kontakt/vertriebspartner/',
    //         'offert' => '/kontakt/preisangebot/',
    //     ),
    //     'sl' => array(
    //         'catalog' => '/kontakt/prospekt-anfordern/',
    //         'partner' => '/kontakt/vertriebspartner/',
    //         'offert' => '/kontakt/preisangebot/',
    //     ),
    // );
    //
    // defined('ICL_LANGUAGE_CODE') ? $lang = ICL_LANGUAGE_CODE : $lang = 'de';

    $linkCatalog = get_field('op_link_catalog', 'options');
    $linkPartner = get_field('op_link_partner', 'options');
    $linkPrice = get_field('op_link_price', 'options');


    // if($links['link_prospekt']) {
    //     //$options .= '<a href="#" class="button">' . __('Prospekt anfordern', 't3-theme') . '</a>';
    //     $options .= '<p class="links"><a class="link-block icon-catalog" href="' . $linkArray[$lang]['catalog'] . '"><span class="title">' . __('Katalog anfordern', 't3-theme') . '</span></a></p>';
    // }
    // if($links['link_offert']) {
    //     //$options .= '<a href="#" class="button hollow">' . __('Preisanfrage', 't3-theme') . '</a>';
    //     $options .= '<p class="links"><a class="link-block icon-calculator" href="' . $linkArray[$lang]['offert'] . '?model=' . get_the_title() . '"><span class="title">' . __('Preisanfrage', 't3-theme') . '</span></a></p>';
    // }
    // if($links['link_partner']) {
    //     //$options .= '<a href="#" class="button hollow">' . __('Vertriebspartner finden', 't3-theme') . '</a>';
    //     $options .= '<p class="links"><a class="link-block icon-pin" href="' . $linkArray[$lang]['partner'] . '"><span class="title">' . __('Vertriebspartner finden', 't3-theme') . '</span></a></p>';
    // }

    // if(isset($_GET['work'])):
    //
    // endif;

    // if($links['link_prospekt'] && $linkCatalog) {
    if($linkCatalog) {
      //$options .= '<a href="#" class="button">' . __('Prospekt anfordern', 't3-theme') . '</a>';
      $options .= '<p class="links"><a class="link-block icon-catalog" href="' . esc_url( $linkCatalog ) . '"><span class="title">' . __('Katalog anfordern', 'leeb') . '</span></a></p>';
    }
    //if($links['link_offert'] && $linkPrice) {
    if($linkPrice) {
      $permalinkUrl = get_the_permalink();
      //$options .= '<a href="#" class="button hollow">' . __('Preisanfrage', 't3-theme') . '</a>';
      $options .= '<p class="links"><a class="link-block icon-calculator" href="' . esc_url( $linkPrice ) . '?model=' . get_the_title() . '&permalink=' . $permalinkUrl . '"><span class="title">' . __('Preisanfrage', 'leeb') . '</span></a></p>';
    }
    //if($links['link_partner'] && $linkPartner) {
    if($linkPartner) {
      //$options .= '<a href="#" class="button hollow">' . __('Vertriebspartner finden', 't3-theme') . '</a>';
      $options .= '<p class="links"><a class="link-block icon-pin" href="' . esc_url( $linkPartner ) . '"><span class="title">' . __('Vertriebspartner finden', 'leeb') . '</span></a></p>';
    }

    return $options;
  }

  public function productCarousel() {
    $homeurl = App::homeurl();
    $images = get_field('header_gallery');

    if( $images ):
      $counter = 1;
      foreach( $images as $image ):
        !empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);

        $productCarousel['carouselItems'][] = \App\template('partials.parts.items.carousel-item', [
          'type' => 'single',
          'counter' => 0,
          'alt' => $alt,
          'caption' => $image['caption'],
          'link' => '',
          'postType' => 'detail',
          'srcSet' => $homeurl . $image['sizes']['18to1-small'] . ' 640w, ' .
          $homeurl . $image['sizes']['18to1-medium'] . ' 1024w, ' .
          $homeurl . $image['sizes']['18to1-large'],
          //$homeurl . $image['sizes']['18to1-large'] . ' 1200w',
          'sizes' => '(max-width: 640px) 640px, (max-width: 1024px) 1024px, (max-width: 1200px) 1200px',
          'imageSmall' => $homeurl . $image['sizes']['18to1-small'],
          'width' => $image['sizes']['18to1-small-width'],
          'height' => $image['sizes']['18to1-small-height'],
          'showTitle' => false,
        ]
      );
    endforeach;
    return $productCarousel;
  endif;
}

public function gallery() {
  $homeurl = App::homeurl();
  // $data['galleryID'] = $block['id'];
  $gallery['galleryDesign'] = get_field('gallery_design');
  $gallery['galleryItems'] = array();
  $block['galleryShowTitle'] = get_field('show_title');
  $block['galleryShowCaption'] = get_field('show_caption');
  $isLightgallery = get_field('is_lightgallery');
  $showTitle = get_field('show_title');
  $showCaption = get_field('show_caption');

  $cols = get_field('gallery_columns');

  if(get_field('gallery_format')) {
    $format = get_field('gallery_format');
  }
  else {
    $format = 'card';
  }
  $smallSize = $format . '-small';
  $mediumSize = $format . '-medium';
  $largeSize = $format . '-large';

  $colSizes = '';
  if($cols == 0 || $cols == 3) {
    $colSizes .= 'small-6 medium-4';
  }
  elseif($cols == 1) {
    $colSizes = 'small-12';
  }
  elseif($cols == 2) {
    $colSizes .= 'small-6';
  }
  elseif($cols == 4) {
    $colSizes .= 'small-6 medium-4 large-3';
  }
  else {
    $colSizes .= 'small-6 medium-3 large-2';
  }

  $images = get_field('gallery');
  if( $images ):
    foreach( $images as $image ):
      !empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);

      $cols >= 3 ? $srcSet = $homeurl . $image['sizes'][$smallSize] . ' 640w, ' . $homeurl . $image['sizes'][$smallSize] : $srcSet = $homeurl . $image['sizes'][$smallSize] . ' 640w, ' . $homeurl . $image['sizes'][$smallSize] . ' 1024w, ';
      $gallery['galleryItems'][] = \App\template('partials.parts.items.gallery-item', [
        'colSizes' => $colSizes,
        'isLightgallery' => $isLightgallery,
        'showTitle' => $showTitle,
        'showCaption' => $showCaption,
        'alt' => $alt,
        'title' => $image['title'],
        'caption' => $image['caption'],
        'imgDescr' => $image['description'],
        'imageSmall' => $homeurl . $image['sizes'][$smallSize],
        'imageMedium' => $homeurl . $image['sizes'][$mediumSize],
        'imageLarge' => $homeurl . $image['sizes'][$largeSize],
        'srcSet' => $srcSet,
        'sizes' => '(max-width: 640px) 640px, (min-width: 640px) 1024px',
        'width' => '640',
        'height' => '342',
      ]
    );
  endforeach;
endif;

return $gallery;
}

public function relatedProducts() {
  $homeurl = App::homeurl();
  //$relProducts['cardDesign'] = get_field('rel_cards_design');
  // $overlay = get_field('cards_overlay');
  // $type = get_field('rel_cards_type');
  $relProducts['title'] = get_field('op_product_models_title', 'options');
  $sizes = get_field('rel_cards_layout_props');
  $relProducts['cellSizes'] = 'small-' . $sizes['cards_layout_small'] . ' medium-' . $sizes['cards_layout_medium'] . ' large-' . $sizes['cards_layout_large'];


  /** Items */
  // if($type == 'items') {
  //     $cards = get_field('rel_card_items');
  //
  //     if(is_array($cards)) {
  //         foreach( $cards as $card ) {
  //             $image = $card['card_item_img'];
  //             $link = $card['card_item_link'];
  //             $title = $card['card_item_title'];
  //
  //             if( !empty($title) ):
  //                 !empty($card['card_item_content']) ? $content = $card['card_item_content'] :  $content = '';
  //                 !empty($image['sizes']['18to1-small']) ? $imageSmall = $image['sizes']['18to1-small'] : $imageSmall = '';
  //                 !empty($image['sizes']['18to1-medium']) ? $imageMedium = $image['sizes']['18to1-medium'] : $imageMedium = '';
  //                 !empty($image['sizes']['18to1-large']) ? $imageLarge = $image['sizes']['18to1-large'] : $imageLarge = '';
  //                 !empty($image['sizes']['18to1-xlarge']) ? $imageXlarge = $image['sizes']['18to1-xlarge'] : $imageXlarge = '';
  //                 !empty($link['url']) ? $linkUrl = $link['url'] : $linkUrl = '';
  //                 !empty($link['title']) ? $linkTitle = $link['title'] : $linkTitle = '';
  //                 !empty($link['target']) ? $linkTarget = $link['target'] : $linkTarget = '';
  //
  //                 $relProducts['card_items'][] = \App\template('partials.parts.items.card-item', [
  //                     'title' => $title,
  //                     'content' => $content,
  //                     'imageSmall' => $imageSmall,
  //                     'imageMedium' => $imageMedium,
  //                     'imageLarge' => $imageLarge,
  //                     'imageXlarge' => $imageXlarge,
  //                     'linkUrl' => $linkUrl,
  //                     'linkTitle' => $linkTitle,
  //                     'linkTarget' => $linkTarget,
  //                     ]
  //                 );
  //             endif;
  //         }
  //     }
  // }
  /** If CPTs */
  // elseif($type == 'cpts') {
  $relProducts['cards_props'] = array(
    //'type' => $type,
    'cat_type' => get_field('rel_cards_cpt_type'),
  );

  /** Set categories */
  if(get_field('rel_cards_cpt_type') == 'cat') {
    $cat_type = get_field('rel_cards_cpt_cat_types');
    $relProducts['cards_props']['cat_type'] = $cat_type;

    $cat = get_field('rel_cards_cpt_categories');
    if(get_field('rel_cards_cpt_cat_types') == 'product'):
      $cat = get_field('rel_cards_cpt_categories_product');
    endif;

    $relProducts['cards_props']['categories'] = '';

    if(!empty($cat)) {
      foreach( $cat as $cat_id ):
        $relProducts['cards_props']['categories'] .= $cat_id .',';
      endforeach;

      $cat_array = array($relProducts['cards_props']['categories']);

      $query_array = array(
        'post_type' => array(rel_get_field('cards_cpt_cat_types')), //array('product'), //
        'posts_per_page'=> -1,
        'catergory__in' => $cat_array,
        'orderby' => 'title',
        'order' => 'ASC',
      );

      $query_array2 = array(
        'post_type' => array(rel_get_field('cards_cpt_cat_types')), //array('product'), //
        'posts_per_page'=> -1,
        'catergory__in' => $cat_array,
        'orderby' => 'title',
        'order' => 'ASC',
      );

      $relProducts['filterArray'] = Array();
      $relProducts['filterCats'] = 0;
      $loop1 = new \WP_Query( $query_array );
      // $loop2 = new \WP_Query( $query_array2 );
      // if($cat):
      // 	$filter_array = array();
      // 	foreach($cat as $c):
      // 		while ( $loop2->have_posts() ) : $loop2->the_post();
      // 		++$relProducts['filterCats'];
      // 			foreach(get_the_category() as $category):
      //
      // 				$relProducts['filterCats'] = $category->cat_ID;
      // 				if($category->cat_ID == $c->term_id):
      // 					$relProducts['filterArray'][$c->term_id] = $c->name;
      // 				endif;
      // 			endforeach;
      // 		endwhile;
      // 	endforeach;
      //
      // 	foreach($relProducts['filterArray'] as $key => $val):
      // 		$children = get_term_children($key,'category');
      // 		if( empty( $children ) ):
      // 			$relProducts['filterCats'][] = $key . '-' . $val;
      // 		endif;
      // 	endforeach;
      // endif;

      while ( $loop1->have_posts() ) : $loop1->the_post();
      $thumbnail_id = get_post_thumbnail_id( $loop1->id );
      $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
      if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }

      $imageData = wp_get_attachment_image_src( $thumbnail_id, 'card-small' );

      $relProducts['teaser'][] = get_post_meta('post_teaser', $loop1->id);
      $relProducts['card_items'][] = \App\template('partials.parts.items.card-item-cpts', [
        'filterIDs' => '',
        'cellSizes' => $relProducts['cellSizes'],
        'title' => get_the_title(),
        'content' => get_field('post_teaser', get_the_id()),
        'url' => get_the_permalink(),
        'imageSmall' => $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'card-small' ),
        'alt' => $alt,
        'width' => $imageData[1],
        'height' => $imageData[2],
        'design' => 'std',
        'hasOverlay' => '',
        'hasLink' => '1',
      ]);
    endwhile;


    wp_reset_query();
  }
}
/** Set selection */
else {
  $items = get_field('rel_cards_products_selection');

  if($items) {
    foreach($items as $card):
      $thumbnail_id = get_post_thumbnail_id( $card->ID );
      $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
      if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }
      
      $imageData = wp_get_attachment_image_src( $thumbnail_id, 'card-small' );

      $relProducts['card_items'][] = \App\template('partials.parts.items.card-item-cpts', [
        'filterIDs' => '',
        'cellSizes' => $relProducts['cellSizes'],
        'title' => get_the_title( $card->ID ),
        'content' => get_field('product_teaser', $card->ID ),
        'url' => get_the_permalink( $card->ID ),
        'imageSmall' => $homeurl . get_the_post_thumbnail_url( $card->ID, 'card-small' ),
        'alt' => $alt,
        'width' => $imageData[1],
        'height' => $imageData[2],
        'design' => 'std',
        'hasOverlay' => '',
        'hasLink' => '1',
      ]);
    endforeach;
  }
}

// }
return $relProducts;
}

public function advData() {
  $homeurl = App::homeurl();

  $block['show_section'] = get_field('show_adv_cards');
  $block['intro_title'] = get_field('intro_title');
  $block['intro_text'] = get_field('intro_text');

  $introProps = get_field('intro_props');
  $block['intro_offset'] = $introProps['intro_offset'];
  $block['intro_bg'] = $introProps['intro_bg'];

  $block['cardDesign'] = get_field('cards_design');
  $sizes = get_field('cards_layout_props');
  $block['cellSizes'] = '';
  if(is_array( $sizes )) {
    $block['cellSizes'] = 'small-' . $sizes['cards_layout_small'] . ' medium-' . $sizes['cards_layout_medium'] . ' large-' . $sizes['cards_layout_large'];
  }

  /** Items */
  $cardsSel = get_field('card_items_sel');
  // if( !$cardsSel ) {
  //     $cards = get_field('card_items');
  //
  //     if(is_array($cards)) {
  //         foreach( $cards as $card ) {
  //             $image = $card['card_item_img'];
  //             $link = $card['card_item_link'];
  //             $title = $card['card_item_title'];
  //
  //             if($image):
  //                 $alt = $image['alt'];
  //             endif;
  //
  //             if( !empty($title) ):
  //                 !empty($card['card_item_content']) ? $content = $card['card_item_content'] :  $content = '';
  //                 !empty($image['sizes']['fp-small']) ? $imageSmall = $image['sizes']['card-small'] : $imageSmall = '';
  //                 !empty($image['sizes']['fp-medium']) ? $imageMedium = $image['sizes']['card-small'] : $imageMedium = '';
  //                 !empty($image['sizes']['fp-large']) ? $imageLarge = $image['sizes']['card-small'] : $imageLarge = '';
  //                 !empty($image['sizes']['fp-xlarge']) ? $imageXlarge = $image['sizes']['card-small'] : $imageXlarge = '';
  //                 !empty($link['url']) ? $linkUrl = $link['url'] : $linkUrl = '';
  //                 !empty($link['title']) ? $linkTitle = $link['title'] : $linkTitle = '';
  //                 !empty($link['target']) ? $linkTarget = $link['target'] : $linkTarget = '';
  //
  //                 $block['card_items'][] = \App\template('partials.parts.items.card-item-product', [
  //                     'title' => $title,
  //                     'content' => $content,
  //                     'cellSizes' => $cellSizes,
  //                     'imageSmall' => $imageSmall,
  //                     'imageMedium' => $imageMedium,
  //                     'imageLarge' => $imageLarge,
  //                     'imageXlarge' => $imageXlarge,
  //                     'linkUrl' => $linkUrl,
  //                     'linkTitle' => $linkTitle,
  //                     'linkTarget' => $linkTarget,
  //                     'alt' => $alt,
  //                     'design' => $block['cardDesign'],
  //                     'anchorClass' => '',
  //                     'smoothScrollAttr' => $smoothScrollAttr,
  //                     ]
  //                 );
  //             endif;
  //         }
  //     }
  // }
  // else {
  /** Selection */
  //$cardsSel = get_field('card_items_sel');

  if($cardsSel) {
    foreach($cardsSel as $card):
      $image = get_the_post_thumbnail_url( $card->ID, 'card-small' );

      $thumbnail_id = get_post_thumbnail_id( $card->ID, 'card-small' );
      $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
      if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }

      $imageData = wp_get_attachment_image_src( $thumbnail_id, 'card-small' );

      $link = get_field('adv_link', $card->ID);
      !empty($link['url']) ? $linkUrl = $link['url'] : $linkUrl = '';
      !empty($link['title']) ? $linkTitle = $link['title'] : $linkTitle = '';
      !empty($link['target']) ? $linkTarget = $link['target'] : $linkTarget = '';
      $smoothScrollAttr = '';

      if($block['cardDesign'] != 'icon') {
        $block['card_items'][] = \App\template('partials.parts.items.card-item', [
          // 'anchorClass2' => '',
          'cellSizes' => $block['cellSizes'],
          'title' => get_the_title($card->ID),
          'content' => get_field('adv_txt', $card->ID),
          'imageSmall' => $homeurl . $image,
          'imageMedium' => $image,
          'imageLarge' => $image,
          'imageXlarge' => $image,
          'width' => $imageData[1],
          'height' => $imageData[2],
          'linkUrl' => $linkUrl,
          'linkTitle' => $linkTitle,
          'linkTarget' => $linkTarget,
          'alt' => $alt,
          'design' => $block['cardDesign'],
          'smoothScrollAttr' => '',
          'anchorClass' => ''
        ]
      );
    }
    else {
      $block['cellClasses'] = 'small-12 medium-6 large-4';
      $block['card_items'][] = \App\template('partials.parts.items.iconlist-item', [
        'iconName' => get_field('adv_icon', $card->ID),
        'title' => get_the_title($card->ID),
        'content' => get_field('adv_txt', $card->ID),
        'imageSmall' => $homeurl . $image,
        'width' => $imageData[1],
        'height' => $imageData[2],
        'alt' => $alt,
        'link' => $link,
      ]);
    }
  endforeach;
  // wp_reset_postdata();
}
// }

return $block;
}

// public function advIconData() {
//     // $block['show_section'] = get_field('show_adv_icons');
//     // $block['intro_title'] = get_field('intro_title');
//     // $block['intro_text'] = get_field('intro_text');
//     // $cells = get_field('list_column_props');
//     // $block['cellClasses'] = ' small-' . $cells['small'] . ' medium-'.$cells['medium'] . ' large-'.$cells['large'];
//     //
//     // if( have_rows('list_items') ):
//     //     while( have_rows('list_items') ): the_row();
//     //
//     //         $image = get_sub_field('list_item_img');
//     //         $image ? $imageSmall = $image['sizes']['fp-small'] : $imageSmall = '';
//     //
//     //         $link = get_sub_field('list_item_link');
//     //         $link ? $link : $link = '';
//     //
//     //         $block['iconItems'][] = \App\template('partials.parts.items.iconlist-item', [
//     //             'iconName' => get_sub_field('list_itemIcon'),
//     //             'title' => get_sub_field('list_item_title'),
//     //             'content' => get_sub_field('list_item_content'),
//     //             'imageSmall' => $imageSmall,
//     //             'link' => $link,
//     //         ]);
//     //     endwhile;
//     // endif;
//     //
//     // return $block;
// }

//   /** Get contact form */
public function contactData() {
  $contact = array();

  // if(!empty(get_field('contact_title'))) {
  //     $contact['title'] = get_field('contact_title');
  // }
  // if(!empty(get_field('contact_text'))) {
  //     $contact['text'] = get_field('contact_text');
  // }
  // if(!empty(get_field('contact_sidebar'))) {
  //     $contact['sidebar'] = get_field('contact_sidebar');
  // }

  $contact['title'] = get_field('op_prod_contact_title', 'options');
  $contact['text'] = get_field('op_prod_contact_teaser', 'options');

  return $contact;
}
//

/** Get topics */
public function topics() {
  $data['count'] = 0;

  $data['items'] = get_field('topics_single');
  $data['showStdIntro'] = get_field('topics_show_std_text');
  $data['showStdTopics'] = get_field('topics_show_std_topics');
  $data['showStdTopics'] != '1' ? $items = get_field('topics_single') : $items = get_field('op_product_topic_sel', 'options');
  $data['titleDefault'] = get_field('op_product_topic_title', 'options');
  $data['contentDefault'] = get_field('op_product_topic_text', 'options');
  $data['title'] = get_field('topics_title');
  $data['content'] = get_field('topics_teaser');
  $data['design'] = get_field('topics_design');
  $sizeLarge = get_field('topics_size_large');

  if($items) {
    foreach($items as $item) {
      $imageSmall = get_the_post_thumbnail_url( $item->ID, 'card-small' );
      $imageMedium = get_the_post_thumbnail_url( $item->ID, 'card-medium' );
      $thumbnail_id = get_post_thumbnail_id( $item->ID );
      $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
      if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }

      $data['card_items'][] = \App\template('partials.parts.items.card-item-topics', [
        'cellSizes' => $sizeLarge,
        'title' => get_the_title( $item->ID ),
        'content' => get_field( 'topic_txt', $item->ID ),
        'link' => get_field( 'topic_link', $item->ID ),
        'imageSmall' => $imageSmall,
        'imageMedium' => $imageMedium,
        'alt' => $alt,
      ]);
      // $topics = get_field('topic_items', $item->ID);
      // foreach($topics as $card) {
      //   $image = $card['img'];
      //
      //   if($data['design'] == 'cards') {
      //     $data['card_items'][] = \App\template('partials.parts.items.card-item-topics', [
      //       'cellSizes' => $sizeLarge,
      //       'title' => $card['title'],
      //       'content' => $card['content'],
      //       'link' => $card['link'],
      //       'imageMedium' => $image['sizes']['card-medium'],
      //       'imageSmall' => $image['sizes']['card-small'],
      //       'alt' => $image['alt'],
      //     ]);
      //   }
      //   else {
      //     $data['card_items'][] = \App\template('partials.parts.items.masonry-card-item', [
      //       'title' => $card['title'],
      //       'content' => $card['content'],
      //       'link' => $card['link'],
      //       'imageMedium' => $image['sizes']['fp-medium'],
      //       'imageSmall' => $image['sizes']['fp-small'],
      //       'alt' => $image['alt'],
      //     ]);
      //   }
      // }
    }
  }

  return $data;
}

// TODO - Remove unused header parts
public function pageHeader() {
  if(get_field('header_type') != 'wo_header') {
    // Get header type
    $header['type'] = get_field('header_type');
    $offsetClass = 'medium-offset-1';

    // Get theme color palette
    if( have_rows('theme_colors','options') ) {
      while( have_rows('theme_colors','options') ): the_row();
      $header['colors'] = (object) [
        'color_primary' =>    get_sub_field('color_primary'),
        'color_accent' =>     get_sub_field('color_accent'),
        'color_dark_gray' =>  get_sub_field('color_dark_gray'),
        'color_light_gray' => get_sub_field('color_light_gray'),
        'color_body_font' =>  get_sub_field('color_body_font'),
      ];
    endwhile;
  }
  // Get header background props
  if( have_rows('header_bg_props') ) {
    while( have_rows('header_bg_props') ): the_row();
    $header['background'] = (object) [
      'pos_x' => get_sub_field('header_bg_pos_x'),
      'pos_y' => get_sub_field('header_bg_pos_y'),
      'size' => get_sub_field('header_bg_size'),
      'parallax' =>get_sub_field('header_bg_parallax'),
    ];
  endwhile;
}
// Get overlay props
if( have_rows('header_overlay_props') ) {
  while( have_rows('header_overlay_props') ): the_row();
  $header['background'] = (object) [
    'overlay_type' => get_sub_field('header_overlay_type'),
    'overlay_color' => get_sub_field('header_overlay_color'),
    'overlay_opacity' => get_sub_field('header_overlay_opacity'),
  ];
  if(get_sub_field('header_overlay_type') != 'none') {

  }
endwhile;
}

// Get content props
if( have_rows('header_content_size_props') ) {
  while( have_rows('header_content_size_props') ): the_row();
  $content_classes = '';
  $array = array();
  $array['small'] = get_sub_field('header_content_size_small');
  $array['medium'] = get_sub_field('header_content_size_medium');
  $array['large'] = get_sub_field('header_content_size_large');
  $content_classes = setColumnClasses($array);

  $header['classes'] = $content_classes;
endwhile;
}

// Get section props
if( have_rows('header_section_props') ) {
  while( have_rows('header_section_props') ): the_row();
  $header['section'] = (object) [
    'height_class' => get_sub_field('header_section_height'),
    'vh_small' => get_sub_field('header_section_vh_small'),
    'vh_medium' => get_sub_field('header_section_vh_medium'),
    'vh_large' => get_sub_field('header_section_vh_large'),
    'show' => get_sub_field('show_section'),
  ];
endwhile;
}

// Get header image
$header['img'] = get_field('header_img');
if(!get_field('header_img')) {
  $header['img_interchange'] = 'data-interchange="[' . get_the_post_thumbnail_url(get_the_ID(), 'fp-small') .', small], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-medium') . ', medium], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-large').', largel], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-xlarge') . ', xlarge]"';
}
else {
  $header['img_interchange'] = 'data-interchange="[' . $header['header_img']['sizes']['fp-small'] .', small], [' . $header['header_img']['sizes']['fp-medium'] . ', medium], [' . $header['header_img']['sizes']['fp-large'] .', largel], [' . $header['header_img']['sizes']['fp-xlarge'] . ', xlarge]"';
}

// Get header image content
if(get_field('header_content_type') == 'fields') {
  if(!empty(get_field('header_title'))):
    $header['title'] = get_the_title();
  else:
    $header['title'] = get_field('header_title');
  endif;
}
else {
  $header['title'] = '';
}

$header['teaser'] = get_field('header_teaser');
$header['editor'] = get_field('header_editor');

// Get header carousel
if(get_field('header_type') == 'slider') {
  $header['classes'] = '';

  if(get_field('header_carousel_props')) {
    $sliderProps = get_field('header_carousel_props');

    if($sliderProps['has_nav'] !== true) {
      $header['classes'] .= " no-nav";
    }
    if($sliderProps['has_dots'] !== true) {
      $header['classes'] .= " no-dots";
    }
    if($sliderProps['is_overlay'] === true) {
      $header['classes'] .= " dots-overlay";
    }
    if(!empty($sliderProps['text_offset'])) {
      $offsetClass = ' medium-offset-' . $sliderProps['text_offset'];
    }
    elseif($sliderProps['text_offset'] == '0') {
      $offsetClass = '';
    }
    $header['classes'] .= ' dots-align-' . $sliderProps['dots_align'];
  }

  if(get_field('header_carousel_type') == 'items') {
    $slides = get_field('header_carousel');
  }
  else {
    $slides = get_field('header_gallery');
  }

  foreach( $slides as $slide ) {
    $title = '';
    $text = '';
    $currLink = '';
    $style = '';
    $classBG = '';

    if(get_field('header_carousel_type') == 'items'):
      $image = $slide['img'];
      !empty($slide['title']) ? $title = '<h2>' . $slide['title'] . '</h2>' : $title = '';
      !empty($slide['text']) ? $text = '<p>' . $slide['text'] . '</p>' : $text = '';

      $link = $slide['link'];
      if($link):
        !empty($link['target']) ? $target = ' target="' . $link['target'] . '"': $target = '';
        $currLink = '<a href="' . $link['url'] . '" ' . $target . '>' .$link['title'] . '</a>';
      endif;

      // Background props
      $props = $slide['bg_props'];

      if($props['overlay_type'] != 'none'):
        $classBG = $props['overlay_type'];
      endif;

      $bgPosX = $props['bg_pos_x'];
      $bgPosY = $props['bg_pos_y'];
      if($props['bg_pos_x'] != 'center' && $props['bg_pos_y'] != 'center'):
        $style = 'style="' .$props['bg_pos_x'] . ' ' . $props['bg_pos_y'] . '"';
      endif;
    else:
      $image = $slide;
    endif;

    if(get_field('header_max_width') === true):
      $interchange = 'data-interchange="[' . $image['sizes']['fp-small'] .', small], [' . $image['sizes']['fp-medium'] .', medium], [' . $image['sizes']['fp-large'] .', large],"';
    else:
      $interchange = 'data-interchange="[' . $image['sizes']['fp-small'] .', small], [' . $image['sizes']['fp-medium'] .', medium], [' . $image['sizes']['fp-large'] .', large], [' . $image['sizes']['fp-xlarge'] .', xlarge]"';
    endif;

    $header['carouselItems'][] = \App\template('partials.parts.items.fhcarousel-item', [
      'offsetClass' => $offsetClass,
      'sliderType' => get_field('header_carousel_type'),
      'title' => $title,
      'text' => $text,
      'link' => $currLink,
      'classBG' => $classBG,
      'style' => $style,
      'interchange' => $interchange,
    ]
  );
}
}

// Get header container width
$header['grid-container-class'] = '';
if(get_field('header_max_width') === true) {
  $header['grid-container-class'] = 'grid-container';
}

return $header;
}
}
}
