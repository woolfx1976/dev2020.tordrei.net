{{--
  Name: topics
  Title: Themen
  Description: cards, topics
  Category: t3layout
  Icon: schedule
  Keywords: topics layout
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

@if($block['design'] == 'masonry')
  <ul class="c-list masonry">
    <div class="masonry__sizer"></div>
    <div class="masonry__gutter-sizer"></div>
@else
<ul class="c-list b-cards b-cards--offset grid-x grid-margin-x">
@endif
  {{-- @if(isset($block['card_items']) && count($block['card_items']) > 0)
    @foreach ($block['card_items'] as $value)
      {!! $value !!}
    @endforeach
  @endif --}}

  @if(isset($block['card_items_sel']) && count($block['card_items_sel']) > 0)
    @foreach ($block['card_items_sel'] as $value)
      {!! $value !!}
    @endforeach
  @endif
</ul>
