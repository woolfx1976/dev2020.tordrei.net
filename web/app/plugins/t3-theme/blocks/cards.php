<?php

add_filter('sage/blocks/cards/data', function (array $block) {
	$homeurl = App::homeurl();
	$block['cardDesign'] = get_field('cards_design');
	$type = get_field('cards_type');
	$sizes = get_field('cards_layout_props');
	$block['cellSizes'] = 'small-' . $sizes['cards_layout_small'] . ' medium-' . $sizes['cards_layout_medium'] . ' large-' . $sizes['cards_layout_large'];
	!empty(get_field('cards_cpt_filter')) ? $block['filterClass'] = ' b-cards--filter' : $block['filterClass'] = '';
	$block['cats'] = array();

	$overlay = get_field('cards_overlay');
	$hasOverlay = false;
	$hasLink = false;
	if(isset($overlay)) {
		$hasOverlay = $overlay['has_overlay'];
		$hasLink = $overlay['has_link'];
	}

	// if(get_field('cards_cpt_cat_types') == 'product') {
	// 	$block['cats'] = get_field('cards_cpt_categories_product');
	// }

	/** Items */
	if($type == 'items') {
		$cards = get_field('card_items');

		if(is_array($cards)) {
			foreach( $cards as $card ) {
				$image = $card['card_item_img'];
				$link = $card['card_item_link'];
				$title = $card['card_item_title'];
				$anchor = $card['card_item_has_anchor'];
			 	$imageSmall = '';
				$imageMedium = '';

				!empty($anchor) ? $anchorClass = ' icon-down-link' : $anchorClass = '';
				!empty($anchor) ? $smoothScrollAttr = ' data-smooth-scroll data-ofset="120"' : $smoothScrollAttr = '';

				if( !empty($title) ):
					!empty($card['card_item_content']) ? $content = $card['card_item_content'] :  $content = '';
					if(get_field('card_img_size') == 'card') {
						$imageSmall = $image['sizes']['card-small'];
						$imageMedium = $image['sizes']['card-medium'];
					}
					else {
						$imageSmall = $image['sizes']['18to1-small'];
						$imageMedium = $image['sizes']['18to1-medium'];
					}

					// !empty($image['sizes']['18to1-large']) ? $imageLarge = $image['sizes']['18to1-large'] : $imageLarge = '';
					// !empty($image['sizes']['18to1-xlarge']) ? $imageXlarge = $image['sizes']['18to1-xlarge'] : $imageXlarge = '';
					!empty($link['url']) ? $linkUrl = $link['url'] : $linkUrl = '';
					!empty($link['title']) ? $linkTitle = $link['title'] : $linkTitle = '';
					!empty($link['target']) ? $linkTarget = $link['target'] : $linkTarget = '';

          !empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);
          $imgTitle = App::setImgTitleAttr( get_post_thumbnail_id( $image['id'] ) );

					$block['card_items'][] = \App\template('partials.parts.items.card-item', [
						'title' => $title,
						'content' => $content,
						'cellSizes' => $block['cellSizes'],
						'imageSmall' => $homeurl . $imageSmall,
						'imageMedium' => $homeurl . $imageMedium,
            'alt' => $alt,
            'imgTitle' => $imgTitle,
						'linkUrl' => $linkUrl,
						'linkTitle' => $linkTitle,
						'linkTarget' => $linkTarget,
						'design' => $block['cardDesign'],
						'hasOverlay' => $hasOverlay,
						'anchorClass' => $anchorClass,
						'smoothScrollAttr' => $smoothScrollAttr,
						'width' => '',
						'height' => '',
						]
					);
				endif;
			}
		}
	}
	/** If CPTs */
	elseif($type == 'cpts') {
		$block['cards_props'] = array(
			'type' => $type,
			'cat_type' => get_field('cards_cpt_type'),
		);

		$showAs = get_field('cards_cpt_type');
		$postType = get_field('cards_post_type');

		if(!is_admin()):
			$posts_per_page = -1;
		else:
			$posts_per_page = 3;
		endif;

		/** Get post by categories */
		if($showAs == 'cat') {
			$catType = get_field('cards_cpt_cat_types');
			$postType != '0' ? $postType = $postType : $postType = $catType;

			$block['cards_props']['cat_type'] = $catType;
			$block['filter_nav'] = '';
			$filterIDs = '';
			//$cat = ''; //get_field('cards_cpt_categories');
			//$cat = get_field('cards_cpt_categories');


			$block['cats'] = get_field('cards_cpt_categories');

			/** Set filter for products */
			if($catType == 'product'):
				// if(get_field('cards_cpt_cat_types') == 'product') {
					$block['cats'] = get_field('cards_cpt_categories_product');
				// }
				// $cat = get_field('cards_cpt_categories_product');

				//var_dump($cat); // selected categories
				//var_dump($block['cats']); // product categories

				if($block['cats']) {
				    $args = array(
					  	'hide_empty'=> 0,
				      'taxonomy' => 'product_category',
				      'orderby' => 'name',
				      'order'   => 'ASC',
				      'parent' => $block['cats'][0], // Fix offset
							'suppress_filters' => false,
				    );

					//print_r($args);

					/** Create filter nav */
				    $cats = get_categories($args);
					//var_dump($cats);

					if(count($cats) > 0) {
						$block['filter_nav'] .= '<ul class="c-list c-list--filter"><li><a href="#" class="filter-item active" data-filter="all">' . __('Alle anzeigen', 't3-theme') . '</a></li>';
						foreach($cats as $cat) {
						  if( $cat->parent != '0'):
							  $block['filter_nav'] .= '<li><a href="#" class="filter-item" data-filter="f' . $cat->term_id . '">' . $cat->name . '</a></li>';
						  endif;
						}
						$block['filter_nav'] .= '</ul>';
						//print_r($block['filter_nav']);
					}
			  	}
			endif;

			$block['cards_props']['categories'] = '';

			// if(!empty($cat)) {
			// 	foreach( $cat as $cat_id ):
			// 		$block['cards_props']['categories'] .= $cat_id .',';
			// 	endforeach;

			//echo $postType;

			if(!empty($block['cats'])) {
				foreach( $block['cats'] as $cat_id ):
					$block['cards_props']['categories'] .= $cat_id .',';
				endforeach;

				$cat_array = array($block['cards_props']['categories']);

				/** Build card items query */
				// $query_array = array(
				// 	'post_type' => array(get_field('cards_cpt_cat_types')), //array('product'), //
				// 	'posts_per_page'=> $posts_per_page,
				// 	'orderby' => 'title',
				// 	'order' => 'ASC',
				// );

				/** Build tax query by post type */
				if($catType == 'product'):
					$query_array = array(
						//post_type' => array(get_field('cards_cpt_cat_types')), //array('product'), //


						'post_type' => array($postType), //array('product'), //
						'posts_per_page'=> $posts_per_page,
						//'orderby' => array('product_order', 'title'),
						//'order' => 'ASC',

						'meta_query' => array(
							'relation' => 'OR',
							array(
								'key' => 'product_order',
								'compare' => '>',
								'type' => 'NUMERIC',
								'value' => '0',
							),
							array(
								'key' => 'product_order',
								'compare' => '=',
								'value' => '999',
							),
						),

						'tax_query' => array(
							array(
								'taxonomy' => 'product_category',
								'field'    => 'term_id',
								'terms'    => $cat_array,
							),
						),

						'orderby' => 'product_order title',
						'order' => 'ASC',
						'suppress_filters' => false,
					);
				else:
					$query_array = array(
						'post_type' => array($postType), //array('product'), //
						'posts_per_page'=> $posts_per_page,
						'orderby' => 'title',
						'order' => 'ASC',
						'catergory__in' => $cat_array,
						'suppress_filters' => false,
					);
				endif;

				//print_r($query_array);

				// $query_array2 = array(
				// 	'post_type' => array(get_field('cards_cpt_cat_types')), //array('product'), //
				// 	'posts_per_page'=> $posts_per_page,
				// 	'catergory__in' => $cat_array,
				// 	'orderby' => 'title',
				// 	'order' => 'ASC',
				// );

				// $block['filterArray'] = Array();
				// $block['filterCats'] = 0;
				$loop = new \WP_Query( $query_array );
				// $loop2 = new \WP_Query( $query_array2 );
				// if($cat):
				// 	$filter_array = array();
				// 	foreach($cat as $c):
				// 		while ( $loop2->have_posts() ) : $loop2->the_post();
				// 		++$block['filterCats'];
				// 			foreach(get_the_category() as $category):
				//
				// 				$block['filterCats'] = $category->cat_ID;
				// 				if($category->cat_ID == $c->term_id):
				// 					$block['filterArray'][$c->term_id] = $c->name;
				// 				endif;
				// 			endforeach;
				// 		endwhile;
				// 	endforeach;
				//
				// 	foreach($block['filterArray'] as $key => $val):
				// 		$children = get_term_children($key,'category');
				// 		if( empty( $children ) ):
				// 			$block['filterCats'][] = $key . '-' . $val;
				// 		endif;
				// 	endforeach;
				// endif;

				while ( $loop->have_posts() ) : $loop->the_post();
					// print_r($block['cats']);
					//echo '->' . get_the_ID();

					$term_list = wp_get_post_terms( get_the_ID(), 'product_category', array( 'fields' => 'all' ) );
					//print_r( $term_list );
					if(count($term_list) > 0) {
						$filterIDs = ' all';
						foreach($term_list as $category) {
							foreach($block['cats'] as $parent) {
								//echo '/' . $category->parent . ' - ' . $parent . '/';
								if ($category->parent == $parent) {
									//echo '###' . $category->term_id . ' ###';
									$filterIDs .= ' f' . $category->term_id;
								}
							}
						}
					}

					//echo '->' . $filterIDs;
					$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
					$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
					if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }

					// Get description of thumbnail
					$imgDescr = get_post($thumbnail_id)->post_content;
					!empty($imgDescr) ? $imgDescr = $imgDescr : $imgDescr = '';

					$content = '';
					switch($postType) {
						case 'product':
							$content = get_field('product_teaser', get_the_id());
							break;
						case 'references':
							$content = get_field('ref_txt', get_the_id());
							break;
					}

					$hasAnchor = get_field('card_item_has_anchor', get_the_id());
					!empty($hasAnchor) ? $anchorClass = ' icon-down-link' : $anchorClass = '';
					!empty($hasAnchor) ? $smoothScrollAttr = ' data-smooth-scroll data-ofset="120"' : $smoothScrollAttr = '';

					$block['teaser'][] = get_post_meta('post_teaser', $loop->id);
					$block['card_items'][] = \App\template('partials.parts.items.card-item-cpts', [
						'filterIDs' => $filterIDs,
						'cellSizes' => $block['cellSizes'],
						'title' => get_the_title(),
						'content' => $content,
						'url' => get_the_permalink(),
						'imageSmall' => $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'card-small' ),
						'imageDescr' => $imgDescr,
            'alt' => $alt,
            'imgTitle' => App::setImgTitleAttr( get_post_thumbnail_id( get_the_ID() ) ),
						'design' => $block['cardDesign'],
						'hasOverlay' => $hasOverlay,
						'hasLink' => $hasLink,
						'anchorClass' => $anchorClass,
						'smoothScrollAttr' => $smoothScrollAttr,
						'width' => '640',
						'height' => '432',
					]);
				endwhile;

				wp_reset_query();
			}
		}
		/** Get posts by cpt */
		elseif($showAs == 'cpt') {
			// Set number of posts to show ---
			if(!empty(get_field('cards_cpt_numer') && get_field('cards_cpt_numer') != '-1')) {
				$posts_per_page = get_field('cards_cpt_numer');
			}

			$query_array = array(
				'post_type' => array($postType),
				'posts_per_page'=> $posts_per_page,
				'orderby' => 'title',
				'order' => 'ASC',
				'suppress_filters' => false,
			);

			$loop = new \WP_Query( $query_array );

			while ( $loop->have_posts() ) : $loop->the_post();
				$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
				$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
				if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }


				// Get description of thumbnail
				$imgDescr = get_post($thumbnail_id)->post_content;
				// !empty($imgDescr) ? $imgDescr = __('Individualanfertigung', 'leeb') : $imgDescr = '';
				!empty($imgDescr) ? $imgDescr = $imgDescr : $imgDescr = '';

				$content = '';
				switch($postType) {
					case 'product':
						$content = get_field('product_teaser', get_the_id());
						break;
					case 'references':
						$content = get_field('ref_txt', get_the_id());
						break;
				}

				$hasAnchor = get_field('card_item_has_anchor', get_the_id());
				!empty($hasAnchor) ? $anchorClass = ' icon-down-link' : $anchorClass = '';
				!empty($hasAnchor) ? $smoothScrollAttr = ' data-smooth-scroll data-ofset="120"' : $smoothScrollAttr = '';

				$block['teaser'][] = get_post_meta('post_teaser', $loop->id);
				$block['card_items'][] = \App\template('partials.parts.items.card-item-cpts', [
					'filterIDs' => '',
					'cellSizes' => $block['cellSizes'],
					'title' => get_the_title(),
					'content' => $content,
					'url' => get_the_permalink(),
					'imageSmall' => $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'card-small' ),
					'imageDescr' => $imgDescr,
          'alt' => $alt,
          'imgTitle' => App::setImgTitleAttr( get_post_thumbnail_id( get_the_ID() ) ),
					'design' => $block['cardDesign'],
					'hasOverlay' => $hasOverlay,
					'hasLink' => $hasLink,
					'anchorClass' => $anchorClass,
					'smoothScrollAttr' => $smoothScrollAttr,
					'width' => '640',
					'height' => '432',
				]);
			endwhile;

			wp_reset_query();
		}
		/** Get posts by selection */
		else {
			$selected_posts = get_field('cards_cpt_selection');
			if( $selected_posts ):

				//print_r($selected_posts);

				foreach( $selected_posts as $post ):
					setup_postdata($post);

					$postType = $post->post_type;

					$thumbnail_id = get_post_thumbnail_id($post->ID);
					$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
					if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }

					// Get description of thumbnail
					$imgDescr = get_post($thumbnail_id)->post_content;
					!empty($imgDescr) ? $imgDescr = $imgDescr : $imgDescr = '';

					$hasAnchor = get_field('card_item_has_anchor', $post->ID);
					!empty($hasAnchor) ? $anchorClass = ' icon-down-link' : $anchorClass = '';
					!empty($hasAnchor) ? $smoothScrollAttr = ' data-smooth-scroll data-ofset="120"' : $smoothScrollAttr = '';

					$content = '';
					switch($postType) {
						case 'product':
							$content = get_field('product_teaser', $post->ID);
							break;
						case 'references':
							$content = get_field('ref_txt', $post->ID);
							break;
					}

					$block['teaser'][] = get_post_meta('post_teaser', $post->ID);
					$block['card_items'][] = \App\template('partials.parts.items.card-item-cpts', [
						'filterIDs' => '',
						'cellSizes' => $block['cellSizes'],
						'title' => get_the_title($post->ID),
						'content' => $content,
						'url' => get_the_permalink($post->ID),
						'imageSmall' => $homeurl . get_the_post_thumbnail_url($post->ID, 'card-small' ),
						'imageDescr' => $imgDescr,
            'alt' => $alt,
            'imgTitle' => App::setImgTitleAttr( get_post_thumbnail_id( $post->ID ) ),
						'design' => $block['cardDesign'],
						'hasOverlay' => $hasOverlay,
						'hasLink' => $hasLink,
						'anchorClass' => $anchorClass,
						'width' => '640',
						'height' => '432',
					]);
				endforeach;
		    wp_reset_postdata();
			endif;
		}
	}

return $block;
});
