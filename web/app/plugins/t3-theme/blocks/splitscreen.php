<?php

add_filter('sage/blocks/splitscreen/data', function (array $block) {
	$homeurl = App::homeurl();
	$bgProps = get_field('sps_bgs');
	$cellOffset = '';
	$classesImg;
	$classesCont;
	$content = get_field('sps_content');
	$design = '';
	get_field('sps_img') ? $image = get_field('sps_img') : $image = false;
	$margin = '';
	$minHeight = '';
	$props = get_field('sps_props');

	/* Get design and margin */
	if( $props['design'] != '0' ) {
		if($props['design'] == 'offset') {
			$design = ' pdd';
			$cellOffset = ' offset';
		}
		else {
			$design = ' ' . $props['design'];
		}
	}
	if( $props['margin'] != '0' ) {
		$margin = ' ' . $props['margin'];
	}

	/** Get cell classes */
	$imgSizes = App::getCellClasses( get_field('sps_img_size') );
	$contSizes = App::getCellClasses( get_field('sps_content_size') );
	$props['width'] != 'fw' ? $offset = App::getCellClasses( get_field('sps_offset'), 'offset' ) : 	$offset = '';
	$props['align'] == '1' ? $classesImg = $imgSizes.$offset . ' small-order-1 medium-order-1' : $classesImg = $imgSizes . ' small-order-1 medium-order-2';
	$props['align'] == '2' ? $classesCont = $contSizes.$offset . ' small-order-2 medium-order-1' : $classesCont = $contSizes . ' small-order-2 medium-order-2';

	/** Get style settings */
	$props['img_type'] == 'bg' ? $bgAlignClass = App::getBgImgAlignImagify( get_field('sps_bg_align') ) : $bgAlignClass = 'test';

	/** Get image align */
	$imgProps = get_field('sps_img_align');
	isset($props['img_type']) && isset($props['align_y']) && $props['img_type'] == 'img' ? $imgAlign = 'align-' . 	$imgProps['align_y'] : $imgAlign = '';

	/** Get min-height of bg-image */
	if( $props['height'] != '0' ) {
		$minHeight = ' ' . $props['height'];
	}

	if($image !== false) {
		$imageLarge = $homeurl . $image['sizes']['card-large'];
		$srcSet =	$homeurl . $image['sizes']['card-small'] . ' 640w, ' . $homeurl . $image['sizes']['card-medium'] . ' 1024w"';
		$width = $image['sizes']['card-small-width'];
		$height = $image['sizes']['card-small-height'];
		// $interchange = ' data-bgset="' . $homeurl . $image['sizes']['fp-small'].' [(max-width: 640px)] | ' . $homeurl . $image['sizes']['fp-medium'] . ' [(max-width: 1024px)] | ' . $homeurl . $image['sizes']['fp-large'] . '"';
		//$alt = $image['alt'];
		!empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);
	}
	else {
		$imageLarge = $srcSet = $interchange = $alt = $width = $height = false;
	}

	$block['sps-data'] = (object) [
		'alt' => $alt,
		'bgImgMinHeight' => $minHeight,
		'classesCont' => $classesCont . App::getBgColor( $bgProps['content_cell_bg'] ) . $cellOffset,
		'classesImg' => $classesImg,
		'classesInnerCell' => App::getBgColor( $bgProps['content_bg'] ) . $design,
		'classSectionBG' => App::getBgColor( $bgProps['grid_bg'] ),
		'content' => $content,
		'gridBG' => App::getBgColor( $bgProps['grid_bg'] ),
		'gridType' => $props['width'],
		'imageLarge' => $imageLarge,
		'imgType' => $props['img_type'],
		'imgAlign' => $imgAlign,
		'srcSet' => $srcSet,
		// 'interchange' => $interchange,
		'margin' => $margin,
		'bgAlignClass' => $bgAlignClass,
		'width' => $width,
		'height' => $height,
	];

	return $block;
});
