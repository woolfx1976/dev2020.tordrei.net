@php
!empty($product_teaser) ? $sectionClass = 'b-pt-none' : $sectionClass = 'b-pt-small'
@endphp

@include('partials.parts.single.columns')
@include('partials.parts.single.lightgallery')
@include('partials.parts.single.advantages')
{{-- @include('partials.parts.single.advantages-icons') --}}
@include('partials.parts.single.rel-products')
{{-- @include('partials.sections.section-topics') --}}
@include('partials.sections.contact')
