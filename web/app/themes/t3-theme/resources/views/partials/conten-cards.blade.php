<?php $loop1 = new \WP_Query( $args );
while ( $loop1->have_posts() ) : $loop1->the_post();
    //setup_postdata($GLOBALS['post'] = $post);

    $block['teaser'][] = \get_field('post_teaser', $loop1->id);
    //$post_props = ;
    $block['card_items'][] = \App\template('partials.parts.card-item-cpts', [
      'cellSizes' => $block['cellSizes'],
      'title' => get_the_title(),
      'content' => get_field('post_teaser',$loop1->ID),
      'url' => get_the_permalink(),
      'imageSmall' => get_the_post_thumbnail_url(get_the_ID(), 'fp-medium' )
    ]);
    //$block['cards_props']['items'][]['title'] = get_the_title();
endwhile;
wp_reset_query();
