<?php
/**
 * References custom post type
 */
add_action('init', function () {
    $labels = array(
        'name'                => _x( 'Referenzen', 'Post Type General Name', 'foundationpress' ),
		'singular_name'       => _x( 'Referenz', 'Post Type Singular Name', 'foundationpress' ),
		'menu_name'           => __( 'Referenzen', 'foundationpress' ),
		'parent_item_colon'   => __( 'Eltern-Referenz', 'foundationpress' ),
		'all_items'           => __( 'Alle Referenzen', 'foundationpress' ),
		'view_item'           => __( 'Referenz zeigen', 'foundationpress' ),
		'add_new_item'        => __( 'Neue Referenz erstellen', 'foundationpress' ),
		'add_new'             => __( 'Neue Referenz', 'foundationpress' ),
		'edit_item'           => __( 'Referenz bearbeiten', 'foundationpress' ),
		'update_item'         => __( 'Referenz aktualisieren', 'foundationpress' ),
		'search_items'        => __( 'Referenz suchen', 'foundationpress' ),
		'not_found'           => __( 'Nicht gefunden', 'foundationpress' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'foundationpress' ),
    );
    register_extended_post_type('references', [
        'labels' => $labels,
        'menu_icon' => 'dashicons-format-status',
        'show_in_rest' => true,
        'supports' => array('title','thumbnail','catgegories'),
        'has_archive' => false,
        'rewrite' => true,
        'taxonomies'  => array( 'product_category'),
    ], [
        'singular' => 'Referenz',
        'plural' => 'Referenzen',
        'slug' => 'referenzen',
    ]);
    /**
	*   To Activate Custom Post Type Single page
	*   @see http://en.bainternet.info/2011/custom-post-type-getting-404-on-permalinks
	*/
	$set = get_option('post_type_rules_flased_authors');
	if ($set !== true){
		flush_rewrite_rules(false);
		update_option('post_type_rules_flased_authors',true);
	}
});

/**
 * Job taxonomies
 */
// add_action('init', function () {
//     register_extended_taxonomy('job_category', 'jobs', [
//         'show_in_rest' => true,
//     ], [
//         'singular' => 'Job Category',
//         'plural' => 'Job Categories'
//     ]);
// });
