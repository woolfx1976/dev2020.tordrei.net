/**
 * BLOCK: t3-section-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.



import './editor.scss';
import './style.scss';
import classnames from 'classnames';
const { __ } = wp.i18n; // Import __() from wp.i18n
const {
	registerBlockType
} = wp.blocks; // Import registerBlockType() from wp.blocks

//const { InspectorControls, PanelBody, ToggleControl } = wp.editor;
const {
	AlignmentToolbar,
    BlockControls,
	RichText,
	InnerBlocks,
	Fragment,
	RichTextToolbarButton
} = wp.blockEditor;
const {
	InspectorControls,
	PanelColorSettings,
	MediaUpload,
} = wp.blockEditor;
const {
	PanelBody,
	ToggleControl,
	SelectControl,
	RangeControl,
	Button,
	ResponsiveWrapper,
} = wp.components;
const {
	registerFormatType,
	toggleFormat
} = wp.richText;

//import { InnerBlocks } from '@wordpress/block-editor';
wp.blocks.registerBlockStyle( 'cgb/block-t3-section-block', {
	name: 'bg-primary',
	label: 'Hintergrund Primärfarbe',
} );
wp.blocks.registerBlockStyle( 'cgb/block-t3-section-block', {
	name: 'bg-accent',
	label: 'Hintergrund Akzentfarbe',
} );
wp.blocks.registerBlockStyle( 'cgb/block-t3-section-block', {
	name: 'bg-white',
	label: 'Hintergrund Weiß',
} );
wp.blocks.registerBlockStyle( 'cgb/block-t3-section-block', {
	name: 'bg-transparent',
	label: 'Hintergrund transparent',
} );

// wp.blocks.registerBlockVariation( 'cgb/block-t3-section-block', {
//     name: 'custom',
//     title: 'Custom Columns',
//     innerBlocks: [
//         [ 'core/column', {}, [ [ 'core/heading' ], [ 'core/paragraph' ] ] ],
//         [ 'core/column', {}, [ [ 'core/paragraph' ] ] ],
//     ],
// } );



const T3ButtonLargeText = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='Large'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/large' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/large', {
        title: 'Large',
        tagName: 'span',
        className: 'large',
        edit: T3ButtonLargeText,
    }
);

const T3ButtonLightText = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='Light'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/light' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/light', {
        title: 'Light',
        tagName: 'span',
        className: 'light',
        edit: T3ButtonLightText,
    }
);

const T3ButtonH2asH1 = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='H2 as H1'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/h2ash1' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

const T3ButtonPasH1 = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='p as H1 - 300'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/pash1' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/h2ash1', {
        title: 'Light',
        tagName: 'span',
        className: 'h2ash1',
        edit: T3ButtonH2asH1,
    }
);

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
 const sectionIcon = wp.element.createElement('svg',
	{
		width: 20,
		height: 20,
		viewbox: "0 0 20 20"
	},
	wp.element.createElement( 'g',
		{
			fill: "#333",
			fillRule: "evenodd",
			transform: "translate(0 -1)"
		},
		wp.element.createElement( 'rect',
			{
				width: 14,
				height: 7,
				x: 3,
				y: 8,
				fill: "#333",
				rx: 2
			}
		),
		wp.element.createElement( 'path',
			{
				fillRule: "nonzero",
				d: "M17.5,0.5 L17.5006962,1.0415961 C18.9192103,1.27992193 20,2.51374499 20,4 L20,18 C20,19.486255 18.9192103,20.7200781 17.5006962,20.9584039 L17.5,21.5 L2.5,21.5 L2.50029469,20.9585702 C1.08129303,20.7206526 0,19.4866011 0,18 L0,4 C0,2.51339894 1.08129303,1.27934741 2.50029469,1.0414298 L2.5,0.5 L17.5,0.5 Z M15,17.5 L5,17.5 C4.17157288,17.5 3.5,18.1715729 3.5,19 L3.5,19 L3.5,20 L16.5,20 L16.5,19 C16.5,18.2690349 15.9771491,17.6601881 15.2850349,17.5270472 L15.14446,17.5068666 L15,17.5 Z M17.5007838,2.06321066 L17.5,4 C17.5,5.38071187 16.3807119,6.5 15,6.5 L15,6.5 L5,6.5 C3.61928813,6.5 2.5,5.38071187 2.5,4 L2.5,4 L2.50028362,2.06293577 C1.66851041,2.27688857 1.04608158,3.01245343 1.00244765,3.90017983 L1,4 L1,18 C1,18.9319941 1.63748815,19.7150949 2.50020656,19.9370444 L2.5,19 C2.5,17.6192881 3.61928813,16.5 5,16.5 L5,16.5 L15,16.5 C16.3807119,16.5 17.5,17.6192881 17.5,19 L17.5,19 L17.5007208,19.9368056 C18.3319906,19.7225192 18.953936,18.9871892 18.9975523,18.0998202 L19,18 L19,4 C19,3.06836258 18.3629997,2.28550455 17.5007838,2.06321066 Z M16.5,2 L3.5,2 L3.5,4 C3.5,4.73096511 4.02285085,5.33981186 4.71496507,5.47295275 L4.85553999,5.49313342 L5,5.5 L15,5.5 C15.8284271,5.5 16.5,4.82842712 16.5,4 L16.5,4 L16.5,2 Z"
			}
		)
	)
);

registerBlockType( 'cgb/block-t3-section-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Section Block' ), // Block title.
	icon: sectionIcon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 't3layout', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'T3 section-block — Section Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	supports: {
		align: [ 'full' ], // Support Wide and Full alignment controls
	},
	attributes: {
		// content: {
		// 	type: 'array',
		// 	source: 'children',
		// 	selector: 'p',
		// },
		alignment: {
            type: 'string',
            default: 'none',
        },
		enableFullWidth: {
			type: 'boolean',
			default: false
		},
		showSection: {
			type: 'boolean',
			default: true
		},
		enableSpacing: {
			type: 'boolean',
			default: false
		},
		customBackgroundColor: {
			type: 'string',
		},
		paddingTop: {
			type: 'string',
			default: '',
		},
		paddingBottom: {
			type: 'string',
			default: '',
		},
		bgImgAlignX: {
			type: 'string',
			default: '',
		},
		bgImgAlignY: {
			type: 'string',
			default: '',
		},
		bgImage: {
			type: 'object',
			default: null,
		},
		bgOptions: {
			type: 'object',
			default: {
				repeat: false,
				stretch: true,
				fixed: false,
				opacity: 0.5,
			}
		},
		sectionOptions: {
			type: 'object',
			default: {
				vhAll: 0,
				vhSmall: 0,
				vhMedium: 0,
				vhlarge: 0,
			}
		},
		sectionHeightAuto: {
			type: 'boolean',
			default: true
		},
		sectionVhAll: {
			type: 'boolean',
			default: true
		},
	},

	// example: {
    //     attributes: {
    //         content: 'Hello World',
	// 		alignment: 'right',
    //     },
    // },


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */

	edit: ( props ) => {
		const {
            attributes: {
                content,
                alignment,
				tagName,
				enableFullWidth,
				enableSpacing,
				customBackgroundColor,
				paddingTop,
				paddingBottom,
				showSection,
				bgImage,
				bgImgAlignX,
				bgImgAligny,
				bgOptions,
				sectionHeightAuto,
				sectionVhAll,
				sectionOptions
            },
            className,
			setBackgroundColor,
        } = props;

		const onSelectBgImage = ( media ) => {
			console.log(media);
			props.setAttributes( {
				bgImage: {
					id: media.id,
					image: media.sizes.large || media.sizes.full,
					imageSmall: media.sizes['fp-small'],
					imageMedium: media.sizes['fp-medium'],
					imageLarge: media.sizes['fp-large'],
					imageXlarge: media.sizes['fp-xlarge']
				}
			} )

		}
		// const onSelectBgImage = (media) => {
		// 	wp.api.media.id( media.id )
		// 	.then((response) => {
		// 		const image = transformImage(response);
		// 		console.log('Media', image);
		// 		props.setAttributes({
		// 			id: media.id,
		// 			image: image,
		// 		});
		// 	});
		// };

		const onRemoveBgImage = () => {
			props.setAttributes( {
				bgImage: null
			} )
		}

		const onChangeContent = ( newContent ) => {
            props.setAttributes( { content: newContent } );
        };

        const onChangeAlignment = ( newAlignment ) => {
            props.setAttributes( { alignment: newAlignment === undefined ? 'none' : newAlignment } );
        };
		// Creates a <p class='wp-block-cgb-block-t3-section-block'></p>.

		let sectionEditContent;
		if (enableFullWidth === true) {
			console.log('enabled: ' + enableFullWidth);
			sectionEditContent = <InnerBlocks/>;
		} else {
			console.log('enabled: ' + enableFullWidth);
			sectionEditContent = <div className="grid-container"><div><InnerBlocks/></div></div>
		}
		console.log('Spacing: ' + enableSpacing);

		// if(enableSpacing) {
		// 	if(paddingTop != '') {
		// 		newPaddingTop = ' ' + paddingTop;
		// 	}
		// 	if(paddingBottom != '') {
		// 		newPaddingBottom = ' ' + paddingBottom;
		// 	}
		// }
		let addClassNames = '';
		if(props.attributes.enableSpacing) {
			addClassNames = ' ' + props.attributes.paddingTop + ' ' + props.attributes.paddingBottom;
		}

		return (
			<section
				className={ classnames(
					className,
					addClassNames
				 ) }
				style={ {
					backgroundColor: customBackgroundColor,
					minHeight: !props.attributes.sectionHeightAuto ? props.attributes.sectionOptions.vhAll + 'vH' : 'inherit',
				} }
				>
                <BlockControls>
                    <AlignmentToolbar
                        value={ alignment }
                        onChange={ onChangeAlignment }
                    />
                </BlockControls>
				<InspectorControls>
					<PanelBody
						title={ __( 'Sektion' ) }
						initialOpen={ false }
					>
						<ToggleControl
							label={ __( 'ohne Grid-Container' ) }
							checked={ !! props.attributes.enableFullWidth }
							onChange={ ( newEnableFullWidth ) => {
								props.setAttributes( {
									enableFullWidth: newEnableFullWidth,
								} );
							} }
						/>
						<ToggleControl
							label={ __( 'Sektion anzeigen' ) }
							checked={ !! props.attributes.showSection }
							onChange={ ( newShowSection ) => {
								props.setAttributes( {
									showSection: newShowSection,
								} );
							} }
						/>
						<ToggleControl
							label={ __( 'Höhe auto' ) }
							checked={ !! props.attributes.sectionHeightAuto }
							onChange={ ( newSectionHeightAuto ) => {
								props.setAttributes( {
									sectionHeightAuto: newSectionHeightAuto,
								} );
							} }
						/>
						{ ! props.attributes.sectionHeightAuto &&<ToggleControl
							label={ __( 'vH für alle gleich' ) }
							checked={ !! props.attributes.sectionVhAll }
							onChange={ ( newSectionVhAll ) => {
								props.setAttributes( {
									sectionVhAll: newSectionVhAll,
								} );
							} }
						/>
						}
						{ ! props.attributes.sectionHeightAuto && !! props.attributes.sectionVhAll &&<RangeControl
							label={ __( 'vH all' ) }
							value={ props.attributes.sectionOptions.vhAll }
							onChange={ ( newVhAll ) => {
								props.setAttributes( {
									sectionOptions: {
										...props.attributes.sectionOptions,
										vhAll: newVhAll,
									},
								} );
							} }
							min={ 0 }
							max={ 100 }
							step={ 5 }
						/>
						}
						{ ! props.attributes.sectionVhAll &&<RangeControl
							label={ __( 'vH small' ) }
							value={ props.attributes.sectionOptions.vhSmall }
							onChange={ ( newVhSmall ) => {
								props.setAttributes( {
									sectionOptions: {
										...props.attributes.sectionOptions,
										vhSmall: newVhSmall,
									},
								} );
							} }
							min={ 0 }
							max={ 100 }
							step={ 5 }
						/>
						}
						{ ! props.attributes.sectionVhAll &&<RangeControl
							label={ __( 'vH small' ) }
							value={ props.attributes.sectionOptions.vhMedium }
							onChange={ ( newVhMedium ) => {
								props.setAttributes( {
									sectionOptions: {
										...props.attributes.sectionOptions,
										vhMedium: newVhMedium,
									},
								} );
							} }
							min={ 0 }
							max={ 100 }
							step={ 5 }
						/>
						}
						{ ! props.attributes.sectionVhAll &&<RangeControl
							label={ __( 'vH small' ) }
							value={ props.attributes.sectionOptions.vhLarge }
							onChange={ ( newVhLarge ) => {
								props.setAttributes( {
									sectionOptions: {
										...props.attributes.sectionOptions,
										vhLarge: newVhLarge,
									},
								} );
							} }
							min={ 0 }
							max={ 100 }
							step={ 5 }
						/>
						}
					</PanelBody>
					<PanelBody
						title={ __( 'Innenabstand' ) }
						initialOpen={ false }
					>
						<ToggleControl
							label={ __( 'Innenabstand aktivieren' ) }
							checked={ !! props.attributes.enableSpacing }
							onChange={ ( newEnableSpacing ) => {
								props.setAttributes( {
									enableSpacing: newEnableSpacing,
								} );
							} }
						/>
						{ !! props.attributes.enableSpacing && <SelectControl
							label="Abstand oben"
							value={ props.attributes.paddingTop }
							options={ [
								{ label: 'Standard', value: '' },
								{ label: 'klein', value: 'b-pt-small' },
								{ label: 'groß', value: 'b-pt-large' },
								{ label: 'ohne', value: 'b-pt-none' },
							] }
							onChange={ ( paddingTop ) => { props.setAttributes( { paddingTop } ) } }
						/>
						}
						{ !! props.attributes.enableSpacing && <SelectControl
							label="Abstand unten"
							value={ props.attributes.paddingBottom }
							options={ [
								{ label: 'Standard', value: '' },
								{ label: 'klein', value: 'b-pb-small' },
								{ label: 'groß', value: 'b-pb-large' },
								{ label: 'ohne', value: 'b-pb-none' },
							] }
							onChange={ ( paddingBottom ) => { props.setAttributes( { paddingBottom } ) } }
						/>
						}
					</PanelBody>
					<PanelColorSettings
						initialOpen={ false }
						title={ __( 'Farben' ) }
						colorSettings={[
							{
								label: __( 'Hintergrundfarbe' ),
								value: customBackgroundColor,
								onChange: ( newBgColor, ...whatelse ) => {
									//setBackgroundColor( newBgColor );
									props.setAttributes(
										{
											customBackgroundColor: newBgColor
										}
									)
								}
							},
						]}
					/>
					<PanelBody
						title={ __( 'Hintergrundbild' ) }
						initialOpen={ false }
					>
						{ ! props.attributes.bgImage &&
							<div>
								<MediaUpload
									title={ __('Set background image') }
									onSelect={ onSelectBgImage }
									allowedTypes={["image"]}
									modalClass="editor-post-featured-image__media-modal"
									render={ ( { open } ) => (
										<Button className="editor-post-featured-image__toggle" onClick={ open }>
											{ __( 'Hintergrundbild hinzufügen','t3-blocks' ) }
										</Button>
									) }
								/>
							</div>
						}
						{ !! props.attributes.bgImage && <MediaUpload

							title={ __( 'Hintergrundbild hinzufügen' ) }
							onSelect={ onSelectBgImage }
							allowedTypes={["image"]}
							value={ props.attributes.bgImage.id }
							modalClass="editor-post-featured-image__media-modal"
							render={ ( { open } ) => (
								<div className="editor-bg-image">
									<Button className="editor-post-featured-image__preview" onClick={ open }>
										<ResponsiveWrapper
											naturalWidth={ props.attributes.bgImage.image.width }
											naturalHeight={ props.attributes.bgImage.image.height }
										>
											<img src={ props.attributes.bgImage.image.url } alt={ __( 'BG Image' ) } />
										</ResponsiveWrapper>
									</Button>
									<Button onClick={ open } isDefault isLarge>
										{ __( 'Replace image' ) }
									</Button>
									<Button onClick={ onRemoveBgImage } isLink isDestructive>
										{ __('Hintergrundbild entfernen','t3-blocks') }
									</Button>
								</div>
							) }
						/>
						}
						{ !! props.attributes.bgImage && <div className="section-bg-settings">
							<RangeControl
								label={ __( 'Opacity' ) }
								value={ props.attributes.bgOptions.opacity * 100 }
								onChange={ ( nextOpacity ) => {
									props.setAttributes( {
										bgOptions: {
											...props.attributes.bgOptions,
											opacity: nextOpacity / 100,
										},
									} );
								} }
								min={ 0 }
								max={ 100 }
								step={ 5 }
							/>
							<ToggleControl
								label={ __( 'Fixed Background' ) }
								checked={ !! props.attributes.bgOptions.fixed }
								onChange={ ( nextFixed ) => {
									props.setAttributes( {
										bgOptions: {
											...props.attributes.bgOptions,
											fixed: nextFixed,
										},
									} );
								} }
							/>
							{ ! props.attributes.bgOptions.fixed && <ToggleControl
									label={ __( 'Stretch Background' ) }
									checked={ !! props.attributes.bgOptions.stretch }
									onChange={ ( nextStretch ) => {
										props.setAttributes( {
											bgOptions: {
												...props.attributes.bgOptions,
												stretch: nextStretch,
											},
										} );
									} }
								/>
							}
							{ ( ! props.attributes.bgOptions.fixed && ! bgOptions.stretch )  && <ToggleControl
									label={ __( 'Repeat Background' ) }
									checked={ !! props.attributes.bgOptions.repeat }
									onChange={ ( nextRepeat ) => {
										props.setAttributes( {
											bgOptions: {
												...props.attributes.bgOptions,
												repeat: nextRepeat,
											},
										} );
									} }
								/>
							}
							{ ( !! props.attributes.bgOptions.fixed || !! bgOptions.stretch )  && <SelectControl
								label="Ausrichtung horizontal"
								value={ props.attributes.bgImgAlignX }
								options={ [
									{ label: 'Standard', value: '50%' },
									{ label: 'links', value: 'left' },
									{ label: 'zentriert', value: '50%' },
									{ label: 'rechts', value: 'right' },
								] }
								onChange={ ( bgImgAlignX ) => { props.setAttributes( { bgImgAlignX } ) } }
								/>
							}
							{ ( !! props.attributes.bgOptions.fixed || !! bgOptions.stretch )  && <SelectControl
								label="Ausrichtung vertikal"
								value={ props.attributes.bgImgAlignY }
								options={ [
									{ label: 'Standard', value: '50%' },
									{ label: 'oben', value: 'top' },
									{ label: 'mittig', value: '50%' },
									{ label: 'unten', value: 'bottom' },
								] }
								onChange={ ( bgImgAlignY ) => { props.setAttributes( { bgImgAlignY } ) } }
								/>
							}
						</div>}
					</PanelBody>
				</InspectorControls>
				{ !! props.attributes.bgImage && <div
					className={ classnames(
						'l-sec-bg', {
							'l-sec-bg--repeated': props.attributes.bgOptions.repeat,
							'l-sec-bg--stretched': props.attributes.bgOptions.stretch || props.attributes.bgOptions.fixed,
							'l-sec-bg--fixed': props.attributes.bgOptions.fixed,
						} ) }
					style={ {
						opacity: props.attributes.bgOptions.opacity,
						backgroundImage: props.attributes.bgImage ? 'url(' + props.attributes.bgImage.image.url + ')' : undefined,
						backgroundPosition: props.attributes.bgImgAlignX + ' ' + props.attributes.bgImgAlignY,
					} }
				/>
				}
				{sectionEditContent}
			</section>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		// const {
        //     attributes: {
        //         content,
        //         alignment,
		// 		tagName,
		// 		enableFullWidth,
		// 		customBackgroundColor,
		// 		paddingTop,
		// 		paddingBottom,
        //     },
        //     className,
        // } = props;

		let addClassNames = '';
		if(!!props.attributes.enableSpacing) {
			addClassNames = props.attributes.paddingTop + ' ' + props.attributes.paddingBottom;
		}

		let sectionContent;
		console.log('save padding:Bottom: ' + props.attributes.paddingBottom);
		if (props.attributes.enableFullWidth) {
			sectionContent = <InnerBlocks.Content />;
		} else {
			sectionContent = <div className="grid-container"><div><InnerBlocks.Content /></div></div>
		}
		let showHideSection;
		props.attributes.showSection ? showHideSection = 'inherit' : showHideSection = 'none';

		return (
			<section
			className={ classnames(
				props.className,
				addClassNames
			 ) }
				style={ {
					display: showHideSection,
					backgroundColor: props.attributes.customBackgroundColor,
					minHeight: !props.attributes.sectionHeightAuto ? props.attributes.sectionOptions.vhAll + 'vH' : 'inherit',
				} }
				>
				{ !! props.attributes.bgImage && <div
					className={ classnames(
						'l-sec-bg', {
							'l-sec-bg--repeated': props.attributes.bgOptions.repeat,
							'l-sec-bg--stretched': props.attributes.bgOptions.stretch || props.attributes.bgOptions.fixed,
							'l-sec-bg--fixed': props.attributes.bgOptions.fixed,
						} ) }
					data-interchange={ "[" + props.attributes.bgImage.imageLarge.url +", small],[" + props.attributes.bgImage.imageLarge.url +", medium],[" + props.attributes.bgImage.imageLarge.url +", large],[" + props.attributes.bgImage.imageXlarge.url +", xlarge]"}
					style={ {
						opacity: props.attributes.bgOptions.opacity,
						backgroundPosition: props.attributes.bgImgAlignX + ' ' + props.attributes.bgImgAlignY,
					} }
				/> }
				{sectionContent}
			</section>
		);
	},
} );
