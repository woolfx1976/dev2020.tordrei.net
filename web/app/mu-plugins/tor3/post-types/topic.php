<?php
/**
 * Topic custom post type
 */
add_action('init', function () {
    $labels = array(
        'name'                => _x( 'Themenblock', 'Post Type General Name', 't3-theme' ),
		'singular_name'       => _x( 'Themenblock', 'Post Type Singular Name', 't3-theme' ),
		'menu_name'           => __( 'Themenblöcke', 't3-theme' ),
		'parent_item_colon'   => __( 'Eltern-Themen', 't3-theme' ),
		'all_items'           => __( 'Alle Themen', 't3-theme' ),
		'view_item'           => __( 'Themenblock zeigen', 't3-theme' ),
		'add_new_item'        => __( 'Neuen Themenblock erstellen', 't3-theme' ),
		'add_new'             => __( 'Neuer Themenblock', 't3-theme' ),
		'edit_item'           => __( 'Themenblock bearbeiten', 't3-theme' ),
		'update_item'         => __( 'Themenblock aktualisieren', 't3-theme' ),
		'search_items'        => __( 'Themenblock suchen', 't3-theme' ),
		'not_found'           => __( 'Nicht gefunden', 't3-theme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 't3-theme' ),
	);
    register_extended_post_type('topic', [
        'labels' => $labels,
        'menu_icon' => 'dashicons-screenoptions',
        'show_in_rest' => false,
        'supports' => array('title','thumbnail','catgegories'),
        'has_archive' => false,
        'rewrite' => true
    ], [
        'singular' => 'Themenblock',
        'plural' => 'Themenblöcke',
        'slug' => 'topics',
    ]);
    /**
	*   To Activate Custom Post Type Single page
	*   @see http://en.bainternet.info/2011/custom-post-type-getting-404-on-permalinks
	*/
	$set = get_option('post_type_rules_flased_authors');
	if ($set !== true){
		flush_rewrite_rules(false);
		update_option('post_type_rules_flased_authors',true);
	}
});
