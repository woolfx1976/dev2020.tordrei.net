<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateOnepager2 extends Controller
{
    public function getThemeColorPalette() {
        $data = array();
        if( have_rows('theme_colors','options') ):
            while( have_rows('theme_colors','options') ): the_row();
                $data['color_primary'] = get_sub_field('color_primary');
                $data['color_accent'] = get_sub_field('color_accent');
                $data['color_dark_gray'] = get_sub_field('color_dark_gray');
                $data['color_light_gray'] = get_sub_field('color_light_gray');
                $data['color_body_font'] = get_sub_field('color_body_font');
            endwhile;
            return $data;
        endif;
    }

    public static function getHeaderProps() {
        $data = [];
        if( have_rows('header_img_props') ):
            while( have_rows('header_img_props') ): the_row();
                $this_content = (object) [
                    'header_img' => get_sub_field('header_img'),
                    'header_opacity' => get_sub_field('header_opacity'),
                    'header_bg' => get_sub_field('header_bg'),
                    'header_parallax' => get_sub_field('header_parallax')
                ];
                array_push($data, $this_content);
            endwhile;
            return $data;
        endif;
    }
}
