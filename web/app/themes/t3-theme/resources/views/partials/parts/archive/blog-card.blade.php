@php
  $thumbnail_id = get_post_thumbnail_id( get_the_ID() );
  $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
  $imageSmall = App::homeurl() . get_the_post_thumbnail_url(get_the_ID(), 'card-small' );
  $url = get_permalink();
  $listProps = get_field('blog_props');
  $showDate = false;
@endphp

<li class="b-cards__item cell small-12 medium-6 large-4">
  <div class="b-cards__header">
      <a href="{!! $url !!}" class="hasHover">
        {{-- @if(!is_admin())
          <img data-src="@homeurl{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" />
        @else --}}
          <img src="{!! $imageSmall !!}"  alt="{!! $alt !!}" width="640" height="432" class="lazyload b-cards__img" />
        {{-- @endif --}}
      </a>
  </div>
  <div class="b-cards__body">
      <a href="{!! $url !!}" class="b-cards__title no-link-style">
        <h3>{!! get_the_title() !!}</h3>
      </a>
      {{-- @php
        if(isset($listProps )) {
          $showDate = $listProps['show_date'];
        }
      @endphp
    @if(!empty($showDate)) --}}
      <p class="date">{!! get_the_date('d.m.Y') !!}</p>
    {{-- @endif --}}
    {{ the_excerpt() }}
    <a href="{!! $url !!}" class="more">@php _e('weiterlesen', 't3-theme'); @endphp</a>
  </div>
</li>
