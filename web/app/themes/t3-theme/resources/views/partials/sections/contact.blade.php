@php
  if(isset($block)):
    $contact_data = $block['contact'];
  endif;
  $formIds = App::formIds();
@endphp
<section class="b-pt-none" id="contact">
  <div class="wp-block-cgb-block-t3-grid-block b-has-offset-h">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          <h2>
            <?php
              if(is_singular('jobs')) {
                _e('Jetzt bewerben', 'leeb');
              }
              else {
                if(!empty($contact_data['title'])): print($contact_data['title']); else: _e('Kontaktieren Sie uns', 'leeb'); endif;
              }
              ?>
          </h2>
        </div>
      </div>
    </div>
  </div>
  <div class="wp-block-cgb-block-t3-grid-block">
    <div class="grid-container">
      <div class="b-cont-columns b-has-pt-large grid-x grid-margin-x">
        <div class="cell small-12 medium-6 large-7 medium-offset-1">
          @if( !is_singular('product') )
            @if(isset($contact_data['text']))
              {!! $contact_data['text'] !!}
            @endif
            @if(is_singular('jobs'))
              @php
                if(!empty($job_form_id)):
                  echo do_shortcode( '[ws_form id="'  . $job_form_id . '"]' );  // staging and production
                else:
                  echo do_shortcode( '[ws_form id="'  . $formIds['job'] . '"]' );  // staging and production
                endif;
              @endphp
            @endif
          @else
            {!! $contact_data['text'] !!}
            {{-- <p>
              Immer ein Leeb-Partner in Ihrer Nähe mit 80 Partnern in Österreich, Deutschland, Italien, Frankreich, der Schweiz und Slowenien.
            </p> --}}
            @php
              //echo do_shortcode( '[ws_form id="1"]' ); //development
              echo do_shortcode( '[ws_form id="' . $formIds['contact'] . '"]' );  // staging and production
            @endphp
          @endif
        </div>
        <div class="cell small-12 medium-4 large-3 is-sidebar">
          @if(is_singular('product'))
            @php
              echo do_shortcode( '[sidebarlinks catalog="1" partner="1" offert="1"]' );
            @endphp
          @endif
          @if(isset($contact_data['sidebar']))
            {!! $contact_data['sidebar'] !!}
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
