{{--
  Name: gallery
  Title: Galerie
  Description: gallery, CPTs
  Category: t3layout
  Icon: format-gallery
  Keywords: gallery, sidebar-container
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

{{-- <div class="l-wrapper"> --}}
@if($block['hasLG'] == '1')
  @php
    wp_enqueue_style( 'lightgallery', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css', array(), '1.6.12', true);
    wp_enqueue_script( 'lightgallery', \App\asset_path('scripts/block-lightgallery.js'), array(), '1.6.12', true);
  @endphp
@endif
  <ul id="lightgallery-{{ $block['id'] }}" data-{{ $block['id'] }} class="c-list b-cards b-cards--gallery grid-x grid-margin-x {{$block['classes']}}">
      @foreach ($block['galleryItems'] as $value)
        {!! $value !!}
      @endforeach
  </ul>
{{-- </div> --}}
