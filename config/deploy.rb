#set :application, 't3_theme_app'
set :application, 't3-theme-bedrock'
set :repo_url, 'https://woolfx1976@bitbucket.org/woolfx1976/dev2020.tordrei.net.git'
#set :default_env, { path: "/kunden/476343_8020:$PATH" }

#SSHKit.config.command_map[:composer] = "/usr/php71/bin/php /usr/bin/composer"
#SSHKit.config.command_map[:composer] = 'php7cli /kunden/476343_8020/bin/composer.phar'
#SSHKit.config.command_map[:composer] = 'php7cli -d memory_limit=1G /kunden/476343_8020/bin/composer.phar'

#SSHKit.config.command_map[:bash] = "/kunden/476343_8020/bashrc"
#SSHKit.config.command_map[:composer] = "/kunden/476343_8020/composer.phar"

# Branch options
# Prompts for the branch name (defaults to current branch)
#ask :branch, -> { `git rev-parse --abbrev-ref HEAD`.chomp }

# Hardcodes branch to always be master
# This could be overridden in a stage config file
set :branch, :master

#set :deploy_to, -> { "/var/www/vhosts/tordrei-theme.com/#{fetch(:application)}" }
set :deploy_to, -> { "/kunden/476343_8020/webseiten/#{fetch(:application)}" }

# Use :debug for more verbose output when troubleshooting
set :log_level, :info

# Apache users with .htaccess files:
# it needs to be added to linked_files so it persists across deploys:
set :linked_files, fetch(:linked_files, []).push('.env', 'web/.htaccess')
set :linked_files, fetch(:linked_files, []).push('.env')

set :linked_dirs, fetch(:linked_dirs, []).push('web/app/uploads')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/cache')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/updraft')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/plugins/updraftplus')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/plugins/wp-rocket')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/wp-rocket-config')
set :linked_files, fetch(:linked_files, []).push('web/app/advanced-cache.php')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/plugins/ws-form-pro')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/plugins/sitepress-multilingual-cms')
# set :linked_dirs, fetch(:linked_dirs, []).push('web/app/plugins/wpml-string-translation')


namespace :deploy do
    desc 'Restart application'
    task :restart do
        on roles(:app), in: :sequence, wait: 5 do
            # Your restart mechanism here, for example:
            # execute :service, :nginx, :reload
        end
    end
    desc 'Run composer installation'
    task :run_config do
        on roles( :app ) do
            info 'Starting run composer'
            #execute 'sed -i -e \'s/http\:\/\/wpackagist/https\:\/\/wpackagist/\' composer.json'
            #execute 'composer install'
            #execute "cd wp rewrite structure '/%postname%/'/current/'; composer install"

            #execute "cd '/var/www/vhosts/tordrei-theme.com/#{fetch(:application)}/current/web/app/themes/t3-theme/'; composer install"
            execute "cd '/kunden/476343_8020/webseiten/#{fetch(:application)}/current/web/app/themes/t3-theme/'; composer install"
            set :linked_files, fetch(:linked_files, []).push('web/app/plugins/wp-rocket/licence-data.php')
            #execute "cd '/kunden/476343_8020/webseiten/#{fetch(:application)}/current/web/app/plugins/wp-rocket/'; composer install -–no-dev"

            #execute "cd '/var/www/vhosts/tordrei-theme.com/#{fetch(:application)}/current/web/app/themes/t3-theme/'; composer update"
            #execute "cd '/var/www/vhosts/tordrei-theme.com/#{fetch(:application)}'; sudo -u t3-admin -i -- wp rewrite structure '/%postname%/' --path='current/web/wp/'"
            #cap production wpcli:run["rewrite structure '/%postname%/'"]
        end
    end

    after :finishing, :run_config
end

# The above restart task is not run by default
# Uncomment the following line to run it on deploys if needed
# after 'deploy:publishing', 'deploy:restart'

namespace :deploy do
    desc 'Update WordPress template root paths to point to the new release'
    task :update_option_paths do
        on roles(:app) do
            within fetch(:release_path) do
                if test :wp, :core, 'is-installed'
                    [:stylesheet_root, :template_root].each do |option|
                        # Only change the value if it's an absolute path
                        # i.e. The relative path "/themes" must remain unchanged
                        # Also, the option might not be set, in which case we leave it like that
                        value = capture :wp, :option, :get, option, raise_on_non_zero_exit: false
                        if value != '' && value != '/themes'
                            execute :wp, :option, :set, option, fetch(:release_path).join('web/wp/wp-content/themes')
                        end
                    end
                end
            end
        end
    end
end



# The above update_option_paths task is not run by default
# Note that you need to have WP-CLI installed on your server
# Uncomment the following line to run it on deploys if needed
# after 'deploy:publishing', 'deploy:update_option_paths'
