<?php
/**
* Plugin Name: T3 TinyMCE Formats
* Plugin URI: http://wpbeginner.com
* Version: 1.0
* Author: Wolfgang Berger
* Author URI: https://tordrei.com
* Description: Add T3-Formats to the the Visual Editor
* License: GPL2
*/

class t3_TinyMCE_Class {

    /**
    * Constructor. Called when the plugin is initialised.
    */
    function __construct() {
        if ( is_admin() ) {
            add_action( 'init', array(  $this, 'setup_tinymce_plugin' ) );
        }
    }

    /**
    * Check if the current user can edit Posts or Pages, and is using the Visual Editor
    * If so, add some filters so we can register our plugin
    */
    function setup_tinymce_plugin() {

        // Check if the logged in WordPress User can edit Posts or Pages
        // If not, don't register our TinyMCE plugin

        if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
            return;
        }

        // Check if the logged in WordPress User has the Visual Editor enabled
        // If not, don't register our TinyMCE plugin
        if ( get_user_option( 'rich_editing' ) !== 'true' ) {
            return;
        }

        add_filter( 'tiny_mce_before_init', array( &$this, 'my_mce_before_init_insert_formats') );

    }

    /*
    * Callback function to filter the MCE settings
    */

    function my_mce_before_init_insert_formats( $init_array ) {

        // Define the style_formats array

        $style_formats = array(
            array(
                'title' => 'Large',
                'inline' => 'span',
                'classes' => 'large'
            ),
			array(
                'title' => 'Light',
                'inline' => 'span',
                'classes' => 'large'
            ),
            array(
                'title' => 'Teaser',
                'selector' => 'p',
                'classes' => 'teaser'
            ),
			array(
				'title' => 'Produkt-Teaser',
				'selector' => 'p',
				'classes' => 'product-teaser'
			),
			array(
                'title' => 'H2 as H1',
                'selector' => 'h2',
                'classes' => 'h2ash1'
            ),
			array(
                'title' => 'P as H1',
                'selector' => 'p',
                'classes' => 'pash1'
            ),
            array(
                'title' => 'small',
                'selector' => 'p',
                'classes' => 'small'
            ),
            array(
                'title' => 'Primärfarbe',
                'selector' => '*',
                'classes' => 'primary'
            ),
            array(
                'title' => 'Sekundärfarbe',
                'selector' => '*',
                'classes' => 'secondary'
            ),
            array(
                'title' => 'weiß',
                'selector' => '*',
                'classes' => 'white'
            ),
            array(
                'title' => 'Tabelle Standard',
                'selector' => 'table',
                'classes' => 'tbl--std'
            ),
        );
        // Insert the array, JSON ENCODED, into 'style_formats'
        $init_array['style_formats'] = json_encode( $style_formats );

        return $init_array;
    }
}

$t3_tinymce_class = new t3_TinyMCE_Class;

?>
