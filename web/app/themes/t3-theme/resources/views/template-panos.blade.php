{{--
  Template Name: Plugin (Panos)
  Template Post Type: post, page, product
--}}

<?php
if ( is_plugin_active( 't3-leeb-panos/plugin.php' ) ) {
  $panos = new leebPanos;
  ob_start();
  echo  leebPanos::getPanosHeader();
  $head = ob_get_contents();
  ob_end_clean();
  echo $head;
  echo $panos::getPanos();
}
