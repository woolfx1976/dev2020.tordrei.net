{{-- Slick Slider --}}
<div>
    @if(!empty($link) && !is_admin())
    <a href="{!!$link!!}" class="hasHover">
      <img src="{!! $imageSmall !!}" srcset="{!! $srcSet !!}" sizes="{!! $sizes !!}" class="b-carousel__img lazyload" alt="{!! $alt !!}" />
    </a>
    @else
      @if(!is_admin())
          <img src="{!! $imageSmall !!}" srcset="{!! $srcSet !!}" class="b-carousel__img" alt="{!! $alt !!}" width="{!! $width !!}" height="{!! $height !!}" />
      @else
        <img src="{!! $imageLarge !!}" class="b-carousel__img lazyload" />
      @endif
    @endif
      <div class="item-overlay benefits text-center">
       @if(!empty($title)) <h3 class="h3ash2">{!!$title!!}</h3> @endif
       @if(!empty($text)) <p class="teaser">{!!$text!!}</p> @endif
       @if(!empty($link)) <a href="{!!$text!!}" class="button hollow white">mehr erfahren</a> @endif
      </div>
</div>
