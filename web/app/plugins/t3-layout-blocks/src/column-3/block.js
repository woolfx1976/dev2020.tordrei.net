/**
 * BLOCK: t3/column
 *
 * Single column block, intended to be used as a sub component in layout blocks.
 * E.g. a 2 column layout would have 2 of these as InnerBlocks.
 */

import './editor.scss';

const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.editor;

registerBlockType( 't3/column-3', {

	title: 'Spalte 3/12',
	icon: 'layout',
	category: 't3layout',

	keywords: [
		'column-3',
	],

	edit: ( props ) => {
		return (
			<div className={ props.className }>
				<InnerBlocks
					templateLock={ false }
					allowedBlocks={ '*' }
					disallowedBlocks={ // NOTE this isn't actually supported yet ...
						[
							't3/layout-2-column',
							't3/layout-4-column',
						]
					} />
			</div>
		);
	},

	save: ( props ) => {
		return (
			<div className={ 'cell small-12 medium-4 large-3' }>
				<InnerBlocks.Content />
			</div>
		);
	},
} );
