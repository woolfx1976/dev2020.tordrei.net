//import { tns } from 'tiny-slider/src/tiny-slider';
// import Swiper from 'swiper';

(function($){



  /**
  * initializeBlock
  *
  * Adds custom JavaScript to the block HTML.
  *
  * @date    15/4/19
  * @since   1.0.0
  *
  * @param   object $block The block jQuery element.
  * @param   object attributes The block attributes (only available when editing).
  * @return  void
  */

  var initializeBlock = function( $block ) {
    var $currBlock;
    if( $block.attr('class') == 'acf-block-preview' ) {
      $currBlock = $block.find(' > .slider');
    }
    else {
      $currBlock = $block;
    }

    if($currBlock.hasClass('single') === false && $currBlock.hasClass('bg-img') === false ) {
      console.log('slides banner');
      $currBlock.find('.slides').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        centerMode: true,
        centerPadding: '60px',
        variableWidth: true,
        adaptiveHeight: true,
        focusOnSelect: false,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
          {
            breakpoint: 640,
            settings: {
              centerMode: true,
              slidesToShow: 1,
            },
          },
        ],
      });
    }
    else if($currBlock.hasClass('bg-img') === true) {
      console.log('slides bg-img');
      $currBlock.find('.slides').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        dots: true,
      });
    }
    else {
      console.log('slides single');
      $currBlock.find('.slides').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        focusOnSelect: false,
        autoplay: true,
        autoplaySpeed: 5000,
      });
    }
  }

  // Initialize each block on page load (front end).
  $(document).ready(function(){
    $('.slider').each(function(){
      initializeBlock( $(this) );
    });
  });

  // Initialize dynamic block preview (editor).
  if( window.acf ) {
    console.log('ADMIN');
    window.acf.addAction( 'render_block_preview/type=carousel', initializeBlock );
  }

})(jQuery);
