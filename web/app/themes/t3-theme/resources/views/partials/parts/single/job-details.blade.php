<section class="{!!$sectionClass!!} is-style-bg-transparent">
  <div class="grid-container">
    <div class="b-cont-columns grid-x grid-margin-x gird-margin-y">
      <div class="cell small-12 medium-6 large-7 medium-offset-1">
        @if(!empty($job_descr))
          {!! $job_descr !!}
        @endif

        @if(!empty($job_tasks))
          @if(!empty($job_headings['tasks']))
            <h2 class="h2-as-h3">{!! $job_headings['tasks'] !!}</h2>
          @endif
          {!! $job_tasks !!}
        @endif

        @if(!empty($job_skills))
          @if(!empty($job_headings['skills']))
            <h2 class="h2-as-h3">{!! $job_headings['skills'] !!}</h2>
          @endif
          {!! $job_skills !!}
        @endif

        @if(!empty($job_offer))
          @if(!empty($job_headings['offer']))
            <h2 class="h2-as-h3">{!! $job_headings['offer'] !!}</h2>
          @endif
          {!! $job_offer !!}
        @endif

        {{-- @if(!empty($job_contact))
          {!! $job_contact !!}
        @endif --}}
      </div>
      <div class="cell small-12 medium-4 large-3 is-sidebar">
        {{-- {!! $product_options !!} --}}
        {{-- {!! $product_sidebar !!} --}}
        @foreach ($job_sidebar as $value)
          @if(!empty($value['value']))
            @if(empty($value['type']))
              <p class="links"><span class="link-block icon-{!! $value['icon'] !!}"><span class="title">{!! $value['title'] !!}</span><span class="descr">{!! $value['value'] !!}</span></span></p>
            @else
              <p class="links"><a class="link-block icon-{!! $value['icon'] !!}" href="#"><span class="title">{!! $value['title'] !!}</span><span class="descr">{!! $value['value'] !!}</span></a></p>
            @endif
          @endif
        @endforeach
        <div class="l-pt-1">
          <a href="#contact" class="button hollow" data-smooth-scroll>Gleich bewerben</a>
        </div>
      </div>
    </div>
  </div>
  {{-- @if(!isset($_GET['job-type']))
  <div class="wp-block-cgb-block-t3-grid-block">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          @if (!empty($job_contact))
          {!! $job_contact !!}
          @endif
        </div>
      </div>
    </div>
  </div>
  @endif --}}
</section>
