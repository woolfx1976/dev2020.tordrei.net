<?php

add_filter('sage/blocks/carousel/data', function (array $block) {
	$homeurl = App::homeurl();
	$id = 'slider-' . $block['id'];
	if( !empty($block['anchor']) ) {
		$id = $block['anchor'];
	}

	$className = 'slider';
	if( !empty($block['className']) ) {
		$className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
		$className .= ' align' . $block['align'];
	}

	$carouselType = get_field('carousel_type');

	$showTitle = false;
	$showDetail = false;

	// if(isset($carouselProps)) {
	if(get_field('carousel_props')) {
		$carouselProps = get_field('carousel_props');
		$showTitle = $carouselProps['show_title'];
		$showDetail = $carouselProps['show_detail'];
	}

	$block['carouselDesign'] = get_field('carousel_design');
	$block['carouselItems'] = array();

	/** Gallery */
	if( $carouselType == 'gallery' ) {
		$images = get_field('carousel_gallery');
		if( $images ):
			$counter = 1;
			if($block['carouselDesign']!= 'bg-img') {
				foreach( $images as $image ):
					//$showTitle ? $title = $image['alt'] : $title = '';
					!empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);

					$block['carouselItems'][] = \App\template('partials.parts.items.carousel-item', [
							'type' => $block['carouselDesign'],
							'counter' => $counter++,
							'showTitle' => $showTitle,
							'link' => '',
							'postType' => '',
							'alt' => $alt,
							'caption' => $image['caption'],
							'imageSmall' => $homeurl . $image['sizes']['18to1-small'],
							'imageLarge' => $homeurl . $image['sizes']['18to1-large'],
							//'interchange' => 'data-interchange="[' . $homeurl . $image['sizes']['18to1-small'] .', small], [' . $homeurl . $image['sizes']['18to1-medium'] .', medium], [' . $homeurl . $image['sizes']['18to1-large'] .', large]"',
							// 'interchange' => 'data-interchange="[' . $homeurl . $image['sizes']['18to1-small'] .', small], [' . $homeurl . $image['sizes']['18to1-medium'] .', medium]"',
							'srcSet' => $homeurl . $image['sizes']['18to1-small'] . ' 640w, '
								. $homeurl . $image['sizes']['18to1-medium'] . ' 1024w, ',
							'sizes' => '(max-width: 640px) 640px, (min-width: 641px) 1024px',
							'src' => $homeurl .$image['sizes']['18to1-small'],
							'width' => '640',
							'height' => '342',
						]
					);
				endforeach;
			}
			else {
				foreach( $images as $image ):
					if($showTitle):
						$title = $image['alt'];
					else:
						$title = '';
					endif;

					!empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);

					$block['carouselItems'][] = \App\template('partials.parts.items.carousel-item-bg-img', [
							'type' => $block['carouselDesign'],
							'title' => $title,
							'descr' => '',
							'link' => '',
							'postType' => '',
							'alt' => $alt,
							'caption' => $image['caption'],
							'imageLarge' => $image['sizes']['18to1-large'],
							'interchange' => 'data-bgset="' . $homeurl . $image['sizes']['fp-small'].' [(max-width: 640px)] | ' . $homeurl . $image['sizes']['fp-medium'] . ' [(max-width: 1024px)] | ' . $homeurl . $image['sizes']['fp-large'] . ' [(min-width: 1024px)]" class="lazyload"',
						]
					);
				endforeach;
			}

		endif;
	}
	else if( $carouselType == 'cpts' ) {
		if( get_field('carousel_cpt_type') == 'sel') {
			$carousel = get_field('carousel_cpt_selection');
			if($carousel) {
				$counter = 1;
				foreach($carousel as $item):
					$imageSmall = $homeurl . get_the_post_thumbnail_url( $item->ID, '18to1-small' );
					$imageMedium = $homeurl . get_the_post_thumbnail_url( $item->ID, '18to1-medium' );
					$imageLarge = $homeurl . get_the_post_thumbnail_url( $item->ID, '18to1-medium' );
					$postType = get_post_type( $item->ID );

					$showTitle === true ? $title = get_the_title( $item->ID ) : $title = '';
					( $showDetail === true && $postType != 'references' ) ? $link = get_post_permalink( $item->ID ) : $link = '';
					( $postType == 'references' ) ? $caption = get_field( 'ref_txt', $item->ID ) : $caption = '';

					$image_id = get_post_thumbnail_id();
					$alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true);
					if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }

					//echo 'showDetail: ' . $showDetail . ' - postType: ' . $postType . ' - Link: ' . $link;

					$block['carouselItems'][] = \App\template('partials.parts.items.carousel-item', [
							'type' => $block['carouselDesign'],
							'counter' => $counter++,
							'link' => $link,
							'alt' => $alt,
							'title' => $title,
							'caption' => $caption,
							'postType' => $postType,
							'imageSmall' => $imageSmall,
							'imageLarge' => $imageLarge,
							// 'interchange' => 'data-interchange="[' . $homeurl . $imageSmall .', small], [' . $homeurl . $imageMedium .', medium], [' . $homeurl . $imageLarge .', large]"',
							'srcSet' => $homeurl . $imageSmall . ' 640w, '
								. $homeurl . $imageMedium . ' 1024w, '
								. $homeurl . $imageLarge . ' 1200w, ',
							'sizes' => '(max-width: 640px) 640px, (max-width: 1024px) 1024px, (max-width: 1200px) 1200px',
							'src' => $imageSmall,
							'width' => '640',
							'height' => '342',
						]
					);

				endforeach;
			}
		}
	}

	return $block;
});
