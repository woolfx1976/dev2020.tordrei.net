{{--
  Name: featured-image-backup
  Title: Seitenkopf-Backup
  Description: Hero-Image, Video
  Category: t3layout
  Icon: align-center
  Keywords: page header
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: false
--}}

<?php

$carousel = array();
$classes = '';
$attr = '';
$showBanner = false;
$offsetClass = ' medium-offset-1';
$fhHeight = '';
$homeurl = App::homeurl();
$GLOBALS['adminBgUrl'] = '';

/** Header-Design */
$headerContentDesign = false;
if(get_field('header_content_design') == 'div') {
  $headerContentDesign = true;
}

if($block['data']['header_type'] == 'text') {
  $classes .= ' c-fh--text';
  $block['data']['header_breadcrumb'] = get_field('header_breadcrumb');
}
else {
  $section_props = get_field('header_section_props');
  $fhHeight = $section_props['header_section_height'];
  $classes .= ' ' . $fhHeight;
}

if($block['data']['header_type'] == 'slider') {
  $showBanner = get_field('header_banner');
  $classes .= " c-fh--hero slider-fh";
  $sliderProps = get_field('header_carousel_props');
  if(isset($sliderProps)) {
    if($sliderProps['has_nav'] !== true) {
      $classes .= " no-nav";
    }
    if($sliderProps['has_dots'] !== true) {
      $classes .= " no-dots";
    }
    if($sliderProps['is_overlay'] === true) {
      $classes .= " dots-overlay";
    }
    $classes .= " dots-align-" . $sliderProps['dots_align'];

    if(!empty($sliderProps['text_offset'])) {
      $offsetClass = ' medium-offset-' . $sliderProps['text_offset'];
    }
    elseif($sliderProps['text_offset'] == '0') {
      $offsetClass = '';
    }
  }

  /** Carousel */
  if(get_field('header_carousel_type') == 'items') {
    if(get_field('header_carousel')) {
      $counter = 0;
      foreach(get_field('header_carousel') as $item) {

        $props = $item['props'];
        $classBG = ' ' . $props['bg'];
        $style= 'style="background-position: ' . $props['bg_x'] . ' ' . $props['bg_y'] . '"';
        $image = $item['img'];
        $GLOBALS['adminBgUrl'] == $homeurl . $image['sizes']['fp-large'];

        // Preload first image ---
        if( $counter == 0) {
          $GLOBALS['fh_img'] = $homeurl . $image['sizes']['fp-large'];
          $GLOBALS['fh_imgUrls'] = $image['sizes']['fp-medium'].' 640w, ' . $homeurl . $image['sizes']['fp-large'] . ' 1024w, ' . $homeurl . $image['sizes']['fp-mlarge'] . ' 1500w,';

          if(!is_admin()):
            function hook_preload() {
              ?>
              <link rel="preload" as="image" href="<?php echo $GLOBALS['fh_img']; ?>" imagesrcset="<?php echo $GLOBALS['fh_imgUrls'] ?>" imagesizes="80vw" />
              <?php
            }
            add_action('wp_head', 'hook_preload', 5, 1);
          endif;
        }

        $counter++;

        $carousel[] = \App\template('partials.parts.items.fhcarousel-item', [
          'sliderType' => 'item',
          'offsetClass' => $offsetClass,
          'counter' => $counter,
          'title' => $item['title'],
          'text' => $item['text'],
          'link' => $item['link'],
          'classBG' => $classBG . ' lazyload',
          'style' => $style,
          // 'interchange' => ' data-bgset="' . $homeurl . $image['sizes']['fp-small'].' [(max-width: 640px)] | ' . $homeurl . $image['sizes']['fp-medium'] . ' [(max-width: 1024px)] | ' . $homeurl . $image['sizes']['fp-mlarge'] . ' [(max-width: 1200px)]' . ' | ' . $homeurl . $image['sizes']['fp-large'] . ' [(max-width: 1500px)]' . ' | ' . $homeurl . $image['sizes']['fp-xlarge'] . '"',
          'interchange' => ' data-bgset="' . $homeurl . $image['sizes']['fp-medium'].' [(max-width: 640px)] | ' . $homeurl . $image['sizes']['fp-large'] . ' [(max-width: 1024px)] | ' . $homeurl . $image['sizes']['fp-mlarge'] . ' [(max-width: 1500px)]' . ' | ' . $homeurl . $image['sizes']['fp-xlarge'] . '"',
          // 'srcSet' => $homeurl . $image['sizes']['fp-medium'].' 640w, ' . $homeurl . $image['sizes']['fp-large'] . ' 1024w, ' . $homeurl . $image['sizes']['fp-mlarge'] . ' 1500w, ' . $homeurl . $image['sizes']['fp-xlarge'],
          // 'imgLarge' =>  $homeurl . $image['sizes']['fp-large'],
          'showBanner' => $showBanner,
        ]);
      }
    }
  }
  else {
    if(get_field('header_gallery')) {
      foreach(get_field('header_gallery') as $image) {
        $carousel[] = \App\template('partials.parts.items.fhcarousel-item', [
          'sliderType' => 'gallery',
          'offsetClass' => $offsetClass,
          'title' => '',
          'text' => '',
          'link' => '',
          'classBG' => ' lazyload',
          'style' => '',
          //'interchange' => ' data-bgset="' . $homeurl . $image['sizes']['fp-small'].' [(max-width: 640px)] | ' . $homeurl . $image['sizes']['fp-medium'] . ' [(max-width: 1024px)] | ' . $homeurl . $image['sizes']['fp-large'] . ' [(max-width: 1200px)]' . ' | ' . $homeurl . $image['sizes']['fp-mlarge'] . ' [(max-width: 1500px)]' . ' | ' . $homeurl . $image['sizes']['fp-xlarge'] . '"',
          'interchange' => ' data-bgset="' . $homeurl . $image['sizes']['fp-medium'].' [(max-width: 640px)] | ' . $homeurl . $image['sizes']['fp-large'] . ' [(max-width: 1024px)] | ' . $homeurl . $image['sizes']['fp-mlarge'] . ' [(max-width: 1500px)]' . ' | ' . $homeurl . $image['sizes']['fp-xlarge'] . '"',
          // 'srcSet' => $homeurl . $image['sizes']['fp-medium'].' 640w, ' . $homeurl . $image['sizes']['fp-large'] . ' 1024w, ' . $homeurl . $image['sizes']['fp-mlarge'] . ' 1500w, ' . $homeurl . $image['sizes']['fp-xlarge'],
          // 'imgLarge' =>  $homeurl . $image['sizes']['fp-large'],
          'showBanner' => false,
        ]);
      }
    }
  }
}
else {
  /** Featured Image */
  if($block['data']['header_type'] == 'fh') {
    $classes .= ' c-fh--hero';
    $overlayProps = get_field('header_overlay_props');
    if($overlayProps) {
      if($overlayProps['header_overlay_type'] != 'none' && $overlayProps['header_overlay_type'] != 'solid') {
        $classes .= ' '. $overlayProps['header_overlay_type'];
      }
    }

    // FH-Image ---
    $bgImage = get_field('header_img');
    if(!$bgImage) {
      $classes .= ' lazyload';
      $GLOBALS['adminBgUrl'] = $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-medium');
      // $headerImg = ' data-bgset="' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fp-small') .' [(max-width: 640px)] | ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-medium') . ' [(max-width: 1024px)] | ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-large') . ' [(max-width: 1200px)]' . ' | ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-xlarge') . '" data-sizes="auto"';
      $headerImg = ' data-bgset="' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-small') .' [(max-width: 640px)] | ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-medium') . ' [(max-width: 768px)] | ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-large') . ' [(max-width: 1200px)] | ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-mlarge') . ' [(max-width: 1500px)] | ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-xlarge') . '" data-sizes="auto"';
      $block['data']['srcset'] = $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-small') .' 640w, ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-medium') . ' 768w, ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-large') . ' 1200w, ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-mlarge') . ' 1500w, ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-xlarge');
      $block['data']['imgsmall'] = $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-small');
    }
    else {
      $classes .= ' lazyload';
      $GLOBALS['adminBgUrl'] = $homeurl . $bgImage['sizes']['fh-medium'];
      // if($headerContentDesign === true) {
        //$headerImg = ' data-bgset="' . $homeurl . $bgImage['sizes']['fp-small'].' [(max-width: 640px)] | ' . $homeurl . $bgImage['sizes']['fh-medium'] . ' [(max-width: 1024px)] | ' . $homeurl . $bgImage['sizes']['fh-large'] . ' [(max-width: 1200px)]' . ' | ' . $homeurl . $bgImage['sizes']['fh-xlarge'] . '" data-sizes="auto"';
      // }
      // else {
        // $headerImg = ' data-bgset="' . $homeurl . $bgImage['sizes']['fp-small'].' [(max-width: 640px)] | ' . $homeurl . $bgImage['sizes']['fh-medium'] . ' [(max-width: 1024px)] | ' . $homeurl . $bgImage['sizes']['fh-large'] . ' [(max-width: 1200px)]' . ' | ' . $homeurl . $bgImage['sizes']['fh-xlarge'] . '" data-sizes="auto"';
        $headerImg = ' data-bgset="' . $homeurl . $bgImage['sizes']['fh-small'] .' [(max-width: 640px)] | ' . $homeurl . $bgImage['sizes']['fh-medium'] . ' [(max-width: 768px)] | ' . $homeurl . $bgImage['sizes']['fh-large']. ' [(max-width: 1200px)] | ' . $homeurl . $bgImage['sizes']['fh-mlarge'] . ' [(max-width: 1500px)] | ' . $homeurl . $bgImage['sizes']['fh-xlarge'] . '" data-sizes="auto"';
        $block['data']['srcset']  = $homeurl . $bgImage['sizes']['fh-small'] .' 640w, ' . $homeurl . $bgImage['sizes']['fh-medium'] . ' 768w, ' . $homeurl . $bgImage['sizes']['fh-large'] . ' 1200w, ' . $homeurl . $bgImage['sizes']['fh-mlarge'] . ' 1500w, ' . $homeurl . $bgImage['sizes']['fh-xlarge'];
        $block['data']['imgsmall'] = $homeurl . $bgImage['sizes']['fh-small'];
      // }
    }

    if(!is_admin()) {
      $attr = $headerImg;

      function hook_preload_hero() {
        ?>
        <link rel="preload" as="image" href="<?php echo $GLOBALS['adminBgUrl']; ?>" />
        <?php
      }
      add_action('wp_head', 'hook_preload_hero', 5, 1);
    }
    else {
      $attr = ' style="background-image: url(' . $GLOBALS['adminBgUrl'] . ');"';
    }
  }

  /** Get header content */
  $block['data']['header_content'] = '';
  $block['data']['header_teaser'] = '';
  if(get_field('header_content_type') == 'fields') {

      if(!empty(get_field('header_title'))) {
        $block['data']['header_content'] = '<h1>' . get_field('header_title') . '</h1>';
      }
      else {
        $block['data']['header_content'] = '<h1>' . get_the_title() . '</h1>';
      }

      if($headerContentDesign) {
        if(!empty(get_field('header_teaser'))) {
          $block['data']['header_teaser'] .= '<p>' . get_field('header_teaser') . '</p>';
        }
      }
      else {
        $block['data']['header_content'] .= '<p>' . get_field('header_teaser') . '</p>';
      }

    // if(!empty(get_field('header_topic'))):
    //   $block['data']['header_content'] .= '<p class="topic">' . get_field('header_topic') . '</p>';
    // endif;
  }
  else {
    $block['data']['header_content'] = get_field('header_editor');
  }
}

if($headerContentDesign) {
  $classes .= ' c-fh--div';
}
if($block['data']['header_max_width']) {
  $classes .= ' grid-container';
}
if(!empty($block['banner']['img'])) {
  $bannerImg = $block['banner']['img'];

  if(!empty($block['banner']['link'])) {
    $bannerLink = $block['banner']['link'];
  }
}
?>
{{-- <header data-{{ $block['id'] }} class="c-fh {{$classes}} none"{!!$attr!!}> --}}

<header data-{{ $block['id'] }} class="c-fh {{$classes}} none">
  @if($block['data']['header_type'] == 'slider')
    @foreach ($carousel as $value)
      {!! $value !!}
    @endforeach
  @elseif($block['data']['header_type'] == 'fh')
    @include('partials.parts.featured-image.hero-image')
  @else
    @include('partials.parts.featured-image.text')
  @endif
  @if($showBanner && !empty($block['banner']['img']))
    <div class="c-banner">
      <div class="grid-container">
          <div class="cell small-12 medium-10 medium-offset-1">
            @if(isset($bannerLink['url']))
              <a href="{{$bannerLink['url']}}" class="none">
                <img src="{{$bannerImg['url']}}" alt="{{$bannerImg['alt']}}" width="270" height="270" />
              </a>
            @else
              <img src="{{$bannerImg['url']}}" alt="{{$bannerImg['alt']}}" width="270" height="270" />
            @endif
          </div>
      </div>
    </div>
  @endif
</header>
@if(isset($block['data']['header_content_design']) && $block['data']['header_content_design'] == 'div')
  <section class="c-fh b-pt-medium b-pb-none is-style-bg-transparent">
    @include('partials.parts.featured-image.text')
  </section>
@endif
