(function($){

  /**
  * initializeBlock
  *
  * Adds custom JavaScript to the block HTML.
  *
  * @date    15/4/19
  * @since   1.0.0
  *
  * @param   object $block The block jQuery element.
  * @param   object attributes The block attributes (only available when editing).
  * @return  void
  */

  $(window).on('load', function() {

    console.log('BLOCK: blog');

    $('#cat').on('change', function(){


      var filter = $('#blog-filter');

      var newJson = {'testing' : '1'};

      // t3_blog_params.posts.listType = 'TESTING' + $('input[name="listType"]').val();
      // console.log('Testing Blog Filter Form' + t3_blog_params.posts + newJson);
      console.log('CAT: ' + $(this).val());
      $.ajax({
        url : t3_blog_params.ajaxurl, // AJAX handler
        data : {
          'action': 'filterb', // the parameter for admin-ajax.php
          'query': t3_blog_params.posts, // loop parameters passed by wp_localize_script()
          'listType': $('input[name="listType"]').val(),
          'postsPerPage': $('input[name="postsPerPage"]').val(),
          'postType': $('input[name="postType"]').val(),
          'pagination': $('input[name="pagination"]').val(),
          'showDate': $('select[name="showDate"]').val(),
          'cat': $(this).val(),
          'page' : t3_blog_params.current_page, // current page
        },
        type : 'POST',
        dataType : 'json',
        beforeSend : function ( xhr ) {
          // $('#loadmore').text( t3_loadmore_params.loadingText ); // some type of preloader
        },
        success : function( posts ){
          console.log('CURRENT PAGE: ' + t3_blog_params.current_page);
          // console.log( posts );
          if( posts ) {
            console.log( posts );

            // $('#loadmore').text( t3_loadmore_params.moreText );
            // $('#ref_content').append( posts ); // insert new posts
            //t3_blog_params.current_page++;

            t3_blog_params.current_page = posts.page;
            t3_blog_params.max_page = posts.max_page;

            $('#blog-list, #pagination-wrapper').empty();
            $('#blog-list').append( posts.content ); // insert new posts
            $('#pagination-wrapper').html(posts.pagination);

            // if (t3_blog_params.current_page == t3_blog_params.max_page )

            // $('#loadmore').hide(); // if last page, HIDE the button

          } //else {
          //   // $('#loadmore').hide(); // if no data, HIDE the button as well
          // }
        },


        // url:filter.attr('action'),
        // data:filter.serialize(), // form data
        // type:filter.attr('method'), // POST
        // dataType : 'json',
        // beforeSend:function(xhr){
        //   // filter.find('button').text('Processing...'); // changing the button label
        // },
        // success:function(data){
        //   console.log(data);
        //   // if( data ) {
        //     $('#blog-list, #pagination-wrapper').empty();
        //     $('#blog-list').append( data.posts ); // insert new posts
        //     $('#pagination-wrapper').html(data.pagination);
        //   // }
        // },

      });

      return false;
    });

    $(document).on('click', '#next' , function() {
      var cat = $('#cat').val();

      if (t3_blog_params.current_page >= t3_blog_params.max_page ) {
        t3_blog_params.current_page = t3_blog_params.max_page;
        $(this).hide();
        console.log('gleich/größer / Cat ->' + cat + ' - ' + t3_blog_params.current_page);
      }
      else if (t3_blog_params.current_page < t3_blog_params.max_page) {
        t3_blog_params.current_page++;
        console.log('kleiner / Cat ->' + ' - ' + t3_blog_params.current_page);
      }

      $.ajax({
        url : t3_blog_params.ajaxurl, // AJAX handler
        data : {
          'action': 'filterb', // the parameter for admin-ajax.php
          'query': t3_blog_params.posts, // loop parameters passed by wp_localize_script()
          'listType': $('input[name="listType"]').val(),
          'postsPerPage': $('input[name="postsPerPage"]').val(),
          'postType': $('input[name="postType"]').val(),
          'pagination': $('input[name="pagination"]').val(),
          'showDate': $('select[name="showDate"]').val(),
          'cat': cat,
          'page' : t3_blog_params.current_page, // current page
          'next' : 1,
        },
        type : 'POST',
        dataType : 'json',
        beforeSend : function ( xhr ) {
          // $('#loadmore').text( t3_loadmore_params.loadingText ); // some type of preloader
        },
        success : function( posts ){
          // console.log( posts );
          if( posts ) {
            // console.log('CURRENT PAGE: ' + posts.page);
            // console.log('MAX PAGE: ' + posts.max_page);

            var cat = $('#cat').val();

            console.log( posts );

            // $('#loadmore').text( t3_loadmore_params.moreText );
            // $('#ref_content').append( posts ); // insert new posts


            $('#blog-list, #pagination-wrapper').empty();
            $('#blog-list').append( posts.content ); // insert new posts
            $('#pagination-wrapper').html(posts.pagination);

            // if (t3_blog_params.current_page == t3_blog_params.max_page )

            // $('#loadmore').hide(); // if last page, HIDE the button

          } //else {
          //   // $('#loadmore').hide(); // if no data, HIDE the button as well
          // }
        },


        // url : t3_loadmore_params.ajaxurl, // AJAX handler
        // data : {
        //   'action': 'loadmorebutton', // the parameter for admin-ajax.php
        //   'blocktype': $('input[name="blocktype"]').val(),
        //   'query': t3_loadmore_params.posts, // loop parameters passed by wp_localize_script()
        //   'page' : t3_loadmore_params.current_page, // current page
        // },
        // type : 'POST',
        // beforeSend : function ( xhr ) {
        //   $('#loadmore').text( t3_loadmore_params.loadingText ); // some type of preloader
        // },
        // success : function( posts ){
        //   if( posts ) {
        //
        //     $('#loadmore').text( t3_loadmore_params.moreText );
        //     $('#ref_content').append( posts ); // insert new posts
        //     t3_loadmore_params.current_page++;
        //
        //     if (t3_loadmore_params.current_page == t3_loadmore_params.max_page )
        //     $('#loadmore').hide(); // if last page, HIDE the button
        //
        //   } else {
        //     $('#loadmore').hide(); // if no data, HIDE the button as well
        //   }
        // },
        // complete : function() {
        //   initBeerSlider();
        // },

      });
      return false;
    });
  });

})(jQuery);
