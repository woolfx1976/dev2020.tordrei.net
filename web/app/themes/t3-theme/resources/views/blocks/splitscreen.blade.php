{{--
  Name: splitscreen
  Title: Split-Screen
  Description: Split-Screen
  Category: t3layout
  Icon: align-left
  Keywords: splitscreen, layout, image, text
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
  $imgSrc = $block['sps-data']->imageLarge;
  $imgSrcSet = $block['sps-data']->srcSet;
  $width = $block['sps-data']->width;
  $height = $block['sps-data']->height;
  $alt = $block['sps-data']->alt;
  //!empty($block['sps-data']->style) ? $style = ' style=' . $block['sps-data']->style .'"' : $style = '';
@endphp
<div class="b-sps{!! $block['sps-data']->classSectionBG !!}{!! $block['sps-data']->margin !!}">
  @if($block['sps-data']->gridType == 'grid')
  <div class="grid-container">
  @endif
    <div class="grid-x">
      @if($block['sps-data']->imgType == 'img')
        <div class="cell b-sps__img {!! $block['sps-data']->classesImg !!} {!! $block['sps-data']->imgAlign !!}">
          @if(!is_admin())
            <img src="{!! $imgSrc !!}" srcset="{!! $imgSrcSet !!}" alt="{!! $alt !!}" @if($width) width="{!! $width !!}"@endif @if($height) height="{!! $height !!}"@endif class="lazyload" />
          @else
            <img src="{!! $imgSrc !!}" />
          @endif
        </div>
      @else
        <div class="cell b-sps__bgimg lazyload {!! $block['sps-data']->bgAlignClass !!}{!! $block['sps-data']->classesImg !!}{!! $block['sps-data']->bgImgMinHeight !!}">
          @if(!is_admin())
            <img src="{!! $imgSrc !!}" srcset="{!! $imgSrcSet !!}" alt="{{$alt}}" @if($width) width="{!! $width !!}"@endif @if($height) height="{!! $height !!}"@endif class="lazyload" />
          @else
            <img src="{!! $imgSrc !!}" />
          @endif
        </div>
      @endif
      <div class="cell b-sps__cont {{$block['sps-data']->classesCont}}">
        <div class="b-sps__inner-cont {{$block['sps-data']->classesInnerCell}}">
          {!! $block['sps-data']->content !!}
        </div>
      </div>
    </div>
  @if($block['sps-data']->gridType == 'grid')
  </div>
  @endif
</div>
