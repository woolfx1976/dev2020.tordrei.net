{{--
  Title: Accordion
  Description: accordion
  Category: t3layout
  Icon: grid-view
  Keywords: accordion
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}
@php
$block['accBorder'] == '1' ? $hasBorder = 'has-border' : $hasBorder = '';
@endphp

<div data-{{ $block['id'] }} class="b-acc {!! $hasBorder !!}">
  @if(isset($block['acc_items']) && count($block['acc_items']) > 0)
    @foreach ($block['acc_items'] as $value)
      {!! $value !!}
    @endforeach
  @endif
</div>
