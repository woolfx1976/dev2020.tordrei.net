{{--
  Title: Container-Spalten
  Description: columns, CPTs
  Category: t3layout
  Icon: columns
  Keywords: columns, sidebar-container
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

<div class="b-cont-columns grid-x grid-margin-x grid-margin-y {{$block['col_css']}}">
  @foreach($block['column'] as $val)
    @if($val['show'])
      <div class="cell {{$val['classes']}}">
        {!! $val['content'] !!}
      </div>
    @endif
  @endforeach
</div>
