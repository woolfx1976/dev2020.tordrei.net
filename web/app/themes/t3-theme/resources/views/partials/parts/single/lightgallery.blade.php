<section class="b-pt-none b-pb-small">
  <div class="wp-block-cgb-block-t3-grid-block b-has-offset-h">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          <h2><?php _e('Ansichten', 'leeb'); ?></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="wp-block-cgb-block-t3-grid-block">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 b-has-pt">
          <ul id="lightgallery-block" class="c-list b-cards b-cards--gallery grid-x grid-margin-x gallery alignwide">
            @foreach ($gallery['galleryItems'] as $value)
              {!! $value !!}
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
