<?php

add_filter('sage/blocks/iconlist/data', function (array $block) {
	$homeurl = App::homeurl();
	$cells = get_field('list_column_props');
	$block['cellClasses'] = ' small-' . $cells['small'] . ' medium-'.$cells['medium'] . ' large-'.$cells['large'];
	$itemsProps = get_field('list_items_props');
	if(isset($itemsProps)) {
		$itemsAlign = $itemsProps['list_items_align'];
	}
	isset($itemsAlign) ? $block['itemAlign'] = $itemsAlign : $block['itemAlign'] = 'justify';

	if( have_rows('list_items') ):
		while( have_rows('list_items') ): the_row();

			$image = get_sub_field('list_item_img');
			if($image) {
				$imageSmall = $image['sizes']['fp-small'];
				$width = $image['sizes']['fp-small-width'];
				$height = $image['sizes']['fp-small-height'];
				!empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);
				//$alt = $image['alt'];
			}
			else {
				$imageSmall = $width = $height = $alt = '';
			}

			$link = get_sub_field('list_item_link');
			$link ? $link : $link = '';

			$block['iconItems'][] = \App\template('partials.parts.items.iconlist-item', [
				'iconName' => get_sub_field('list_itemIcon'),
				'title' => get_sub_field('list_item_title'),
				'content' => get_sub_field('list_item_content'),
				'imageSmall' => $homeurl . $imageSmall,
				'link' => $link,
				'alt' => $alt,
				'width' => $width,
				'height' => $height,
			]);
		endwhile;
	endif;

	return $block;
});
