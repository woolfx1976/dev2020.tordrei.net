{{--
  Title: Jobs
  Description: jobs, list, CPTs
  Category: t3layout
  Icon: grid-view
  Keywords: list layout
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

@if(!empty($block['filter_nav']))
  {!! $block['filter_nav'] !!}
@endif
<ul data-{{ $block['id'] }} class="c-list b-list b-list--{{ $block['listDesign'] }}{{ $block['filterClass'] }}">
  @if(isset($block['list_items']) && count($block['list_items']) > 0)
    @foreach ($block['list_items'] as $value)
      {!! $value !!}
    @endforeach
  @endif
</ul>
