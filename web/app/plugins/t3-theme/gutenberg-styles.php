<?php
/**
 * Registers support for Gutenberg features.
 */
function theme_slug_gutenberg_support() {
	// Get theme color palette from backend.
	$theme_colors = array();

	$theme_colors['color_primary'] = '#EE7203';
	$theme_colors['color_accent'] = '#333333';
	$theme_colors['color_dark_gray'] = '#5D5D5D';
	$theme_colors['color_light_gray'] = '#F5F5F5';


	if( have_rows('theme_colors','options') ):
		while( have_rows('theme_colors','options') ): the_row();
		// TODO check array index if colors are not definied
			if(!empty(get_sub_field('color_primary'))): $theme_colors['color_primary'] = get_sub_field('color_primary'); endif;
			if(!empty(get_sub_field('color_accent'))): $theme_colors['color_accent'] = get_sub_field('color_accent'); endif;
			if(!empty(get_sub_field('color_dark_gray'))): $theme_colors['color_dark_gray'] = get_sub_field('color_dark_gray'); endif;
			if(!empty(get_sub_field('color_light_gray'))): $theme_colors['color_light_gray'] = get_sub_field('color_light_gray'); endif;
		endwhile;
	endif;

	// Add theme support for custom color palette.
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => esc_html__( 'Primary', 'theme-slug' ),
			'slug'  => 'primary',
			'color' => $theme_colors['color_primary'],
		),
		array(
			'name'  => esc_html__( 'Accent', 'theme-slug' ),
			'slug'  => 'accent',
			'color' => $theme_colors['color_accent'],
		),
		array(
			'name'  => esc_html__( 'Light Gray', 'theme-slug' ),
			'slug'  => 'light-gray',
			'color' => $theme_colors['color_light_gray'],
		),
		array(
			'name'  => esc_html__( 'Dark Gray', 'theme-slug' ),
			'slug'  => 'dark-gray',
			'color' => $theme_colors['color_dark_gray'],
		),
		array(
			'name'  => esc_html__( 'White', 'theme-slug' ),
			'slug'  => 'white',
			'color' => '#fff',
		),
	) );

	// Disable theme support for custom colors.
	add_theme_support( 'disable-custom-colors' );
}
add_action( 'after_setup_theme', 'theme_slug_gutenberg_support' );
