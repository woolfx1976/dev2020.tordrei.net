<li class="b-cards__item cell small-12 medium-6 large-4">
  <div class="b-cards__header test">
    @if(!empty($linkUrl))
      <a href="{!! $linkUrl !!}" class="hasHover">
        {{-- @if(!is_admin())
          <img data-src="@homeurl{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" />
        @else --}}
          <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" width="640" height="432" class="lazyload b-cards__img"@if(isset($imgTitle)){!! $imgTitle !!}@endif />
        {{-- @endif --}}
      </a>
    @else
      {{-- @if(!is_admin())
        <img data-src="@homeurl{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" />
      @else --}}
        <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" width="640" height="432" class="lazyload b-cards__img"@if(isset($imgTitle)){!! $imgTitle !!}@endif />
      {{-- @endif --}}
    @endif
  </div>
  <div class="b-cards__body">
    @if(!empty($linkUrl))
      <a href="{!! $linkUrl !!}" class="b-cards__title no-link-style">
        <h3>{!! $title !!}</h3>
      </a>
    @else
      <h3 class="b-cards__title">{!! $title !!}</h3>
    @endif
    @if(!empty($showDate))
      <p class="date">{!! $date !!}</p>
    @endif
    <p>{!! $content !!}</p>
    @if(!empty($linkUrl))
    <a href="{!! $linkUrl !!}" class="more">{!! $linkTitle !!}</a>
    @endif
  </div>
</li>
