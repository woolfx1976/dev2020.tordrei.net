@if(!empty($link))
<a href="{!! $link['url'] !!}" class="b-iconlist__link" target="{!! $link['target'] !!}">
@endif
  @if(!empty($iconName))
    <div class="b-iconlist__icon icon-{!! $iconName !!}"></div>
  @else
    <div class="b-iconlist__icon img">
      <img src="{!! $imageSmall !!}" width="{!! $width !!}" height="{!! $height !!}" class="b-iconlist__img" alt="{!! $alt !!}" />
    </div>
  @endif
  @if(!empty($title) || !empty($content))
  <div class="b-iconlist__body">
    @if(!empty($title))
      <h3>{!! $title !!}</h3>
    @endif
    @if(!empty($content))
      <p>{!! $content !!}</p>
    @endif
  </div>
  @endif
@if(!empty($link))
</a>
@endif
