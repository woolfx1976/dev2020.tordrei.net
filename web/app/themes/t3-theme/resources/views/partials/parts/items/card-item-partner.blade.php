<li class="b-cards__item cell {!! $anchorClass !!} {!! $cellSizes !!}">
  <div class="b-cards__header">
    {{-- @if(!is_admin())
      <img data-src="@homeurl{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" />
    @else --}}
      <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" width="640" height="432" class="lazyload b-cards__img" />
    {{-- @endif --}}
  </div>
  <div class="b-cards__body">
    <h3 class="b-cards__title">{!! $title !!}</h3>
    {!! $content !!}
  </div>
</li>
