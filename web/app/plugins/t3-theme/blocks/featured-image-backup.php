<?php
/**
 * Set classnames
 * @param string|string[] $array Array of given classnames
 * @return string list of classnames
 */
// function setColumnClasses($array) {
//     $classes = '';
//     foreach($array as $key => $val):
//         if(!empty($val)):
//             $classes .= ' ' .$key . '-' . $val;
//         endif;
//     endforeach;
//     return $classes;
// }
// add_action('setClasses', 'setClasses', 10, 1);

add_filter('sage/blocks/featuredimage/data', function (array $block) {
    // Get theme color palette
    // if( have_rows('theme_colors','options') ):
    //     while( have_rows('theme_colors','options') ): the_row();
    //         $block['colors'] = (object) [
    //             'color_primary' =>    get_sub_field('color_primary'),
    //             'color_accent' =>     get_sub_field('color_accent'),
    //             'color_dark_gray' =>  get_sub_field('color_dark_gray'),
    //             'color_light_gray' => get_sub_field('color_light_gray'),
    //             'color_body_font' =>  get_sub_field('color_body_font'),
    //         ];
    //     endwhile;
    // endif;

	// Get type of header
	$block['fh_type'] = get_field('fh_type');
	$block['max_width'] = get_field('header_max_width');

	// Check content
	$block['show_content'] = false;
	if($block['fh_type'] != 'slider'):
		$block['show_content'] = get_field('header_show_content');
	endif;

	// Get header background props
	$block['styles'] = '';
	if( have_rows('header_bg_props') ):
	    while( have_rows('header_bg_props') ): the_row();
	        $block['background'] = (object) [
	            'bg_pos_x' => get_sub_field('header_bg_pos_x'),
	            'bg_pos_y' => get_sub_field('header_bg_pos_y'),
	            'bg_size' => get_sub_field('header_bg_size'),
	            'bg_parallax' =>get_sub_field('header_bg_parallax'),
	        ];

			// if( (get_sub_field('header_bg_pos_x') !=  'center' || get_sub_field('header_bg_pos_x') != 'center') || get_sub_field('header_bg_size') != 'cover' ):
				$block['styles'] .= 'style="';
				$block['styles'] .= 'background-position: ' . get_sub_field('header_bg_pos_x') . ' ' . get_sub_field('header_bg_pos_y') . ';';
				if(get_sub_field('header_bg_size') != 'cover'):
					$block['styles'] .= 'background-size: ' . get_sub_field('header_bg_size') . ';';
				endif;
				$block['styles'] .= '"';

			// endif;
	    endwhile;
	endif;
	// Get overlay props
	if( have_rows('header_overlay_props') ):
	    while( have_rows('header_overlay_props') ): the_row();
	        $block['background'] = (object) [
	            'overlay_type' => get_sub_field('header_overlay_type'),
	            'overlay_color' => get_sub_field('header_overlay_color'),
	            'overlay_opacity' => get_sub_field('header_overlay_opacity'),
	        ];
	    endwhile;
	endif;
	// Get content props
	if( have_rows('header_content_size_props') ):
	    while( have_rows('header_content_size_props') ): the_row();
	        $content_classes = '';
	        $array = array();
	        $array['small'] = get_sub_field('header_content_size_small');
	        $array['medium'] = get_sub_field('header_content_size_medium');
	        $array['large'] = get_sub_field('header_content_size_large');
	        $content_classes = setColumnClasses($array);

	        $block['content'] = (object) [
	            'classes' => $content_classes,
	        ];
	    endwhile;
	endif;
	// Get section props
	if( have_rows('header_section_props') ):
	    while( have_rows('header_section_props') ): the_row();
	        $block['section'] = (object) [
	            'section_height_class' => get_sub_field('header_section_height'),
	            'section_vh_small' => get_sub_field('header_section_vh_small'),
	            'section_vh_medium' => get_sub_field('header_section_vh_medium'),
	            'section_vh_large' => get_sub_field('header_section_vh_large'),
	            'show_section' => get_sub_field('show_section'),
	        ];
	    endwhile;
	endif;
	// Get header image
	$block['header_img'] = get_field('header_img');
	$block['header_img_interchange'] = '[' . $block['header_img']['sizes']['fp-small'] .', small], [' . $block['header_img']['sizes']['fp-medium'] . ', medium], [' . $block['header_img']['sizes']['fp-large'] .', largel], [' . $block['header_img']['sizes']['fp-xlarge'] . ', xlarge]';
	if(get_field('header_content_type') == 'fields'):
		if(!empty(get_field('header_title'))):
			$block['header_title'] = get_the_title();
		else:
			$block['header_title'] = get_field('header_title');
		endif;
	else:
		$block['header_title'] = '';
	endif;
	$block['header_teaser'] = get_field('header_teaser');
	$block['header_editor'] = get_field('header_editor');

	// Get header image
	$block['img'] = get_field('header_img');
	if(!get_field('header_img')):
		$block['img_interchange'] = 'data-interchange="[' . get_the_post_thumbnail_url(get_the_ID(), 'fp-small') .', small], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-medium') . ', medium], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-large').', largel], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-xlarge') . ', xlarge]"';
	else:
		$block['img_interchange'] = 'data-interchange="[' . $block['img']['sizes']['fp-small'] .', small], [' . $block['img']['sizes']['fp-medium'] . ', medium], [' . $block['img']['sizes']['fp-large'] .', largel], [' . $block['img']['sizes']['fp-xlarge'] . ', xlarge]"';
	endif;

	// Get header image content
	$block['header_content'] = '';
	if(get_field('header_content_type') == 'fields'):
		if(!empty(get_field('header_title'))):
			$block['header_content'] = get_the_title();
		else:
			$block['header_content'] = get_field('header_title');
		endif;
			$block['header_content'] .= get_field('header_teaser');
	else:
		$block['header_content'] = get_field('header_editor');
	endif;

	// Get header carousel
	if(get_field('header_type') == 'slider'):
		// if(get_field('header_carousel_type') == 'items'):
		// 	$slides = 'header_carousel';
		// else:
		// 	$slides = 'header_gallery';
		// endif;

		$title = '';
		$text = '';
		$currLink = '';
		$style = '';
		$classBG = '';
		$imgLarge = '';


		if(get_field('header_carousel_type') == 'items'):
			if( have_rows('header_carousel') ):
	        	while( have_rows('header_carousel') ): the_row();

				$image = get_sub_field('img');
				!empty(get_sub_field('title')) ? $title = '<h2>' . get_sub_field('title') . '</h2>' : $title = '';
				!empty(get_sub_field('text')) ? $text = '<p>' . get_sub_field('text') . '</p>' : $text = '';

				$link = get_sub_field('link');
				if($link):
					!empty($link['target']) ? $target = ' target="' . $link['target'] . '"': $target = '';
					$currLink = '<a href="' . $link['url'] . '" ' . $target . '>' .$link['title'] . '</a>';
				endif;

				//Background props
				$props = $slide['bg_props'];

				if($props['overlay_type'] != 'none'):
					$classBG = $props['overlay_type'];
				endif;

				$bgPosX = $props['bg_pos_x'];
				$bgPosY = $props['bg_pos_y'];
				if($props['bg_pos_x'] != 'center' && $props['bg_pos_y'] != 'center'):
					$style = 'style="' .$props['bg_pos_x'] . ' ' . $props['bg_pos_y'] . '"';
				endif;

				$imgLarge = $image['sizes']['fp-large'];

				if(get_field('header_max_width') === true):
					$interchange = 'data-interchange="[' . $image['sizes']['fp-small'] .', small], [' . $image['sizes']['fp-medium'] .', medium], [' . $image['sizes']['fp-large'] .', large],"';
				else:
					$interchange = 'data-interchange="[' . $image['sizes']['fp-small'] .', small], [' . $image['sizes']['fp-medium'] .', medium], [' . $image['sizes']['fp-large'] .', large], [' . $image['sizes']['fp-xlarge'] .', xlarge]"';
				endif;

				$block['carouselItems'][] = \App\template('partials.parts.items.fhcarousel-item', [
						'title' => $title,
						'text' => $text,
						'link' => $currLink,
						'classBG' => $classBG,
						'style' => $style,
						'interchange' => $interchange,
						'imgLarge' => $imgLarge,
					]
				);

				endwhile;
			endif;
		else:
			$images = get_field('header_gallery');
			if($images):
				foreach( $images as $image ):
					if(get_field('header_max_width') === true):
						$interchange = 'data-interchange="[' . $image['sizes']['fp-small'] .', small], [' . $image['sizes']['fp-medium'] .', medium], [' . $image['sizes']['fp-large'] .', large],"';
					else:
						$interchange = 'data-interchange="[' . $image['sizes']['fp-small'] .', small], [' . $image['sizes']['fp-medium'] .', medium], [' . $image['sizes']['fp-large'] .', large], [' . $image['sizes']['fp-xlarge'] .', xlarge]"';
					endif;

					$block['carouselItems'][] = \App\template('partials.parts.items.fhcarousel-item', [
							'title' => $title,
							'text' => $text,
							'link' => $currLink,
							'classBG' => $classBG,
							'style' => $style,
							'interchange' => $interchange,
							'imgLarge' => $imgLarge,
						]
					);
				endforeach;
			endif;
		endif;

		//Get header container width
		$block['grid_container_class'] = '';
		if(get_field('header_max_width')):
			$block['grid_container_class'] = true;
		endif;
	endif;

    return $block;
	});
