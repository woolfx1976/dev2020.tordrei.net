<?php
/**
 * Plugin Name: TORDREI MU Plugin
 * Description: Adds t3-theme mu funcionality.
 * Author: Wolfgang Berger
 */

/**
* Load modules
*/
// require_once(dirname(__FILE__) . '/tor3/duplicator.php');
require_once(dirname(__FILE__) . '/tor3/post-types/advantages.php');
require_once(dirname(__FILE__) . '/tor3/post-types/jobs.php');
require_once(dirname(__FILE__) . '/tor3/post-types/partners.php');
require_once(dirname(__FILE__) . '/tor3/post-types/posts.php');
require_once(dirname(__FILE__) . '/tor3/post-types/product.php');
require_once(dirname(__FILE__) . '/tor3/post-types/references.php');
require_once(dirname(__FILE__) . '/tor3/post-types/team.php');
require_once(dirname(__FILE__) . '/tor3/post-types/topic.php');
require_once(dirname(__FILE__) . '/tor3/post-types/topics.php');

?>
