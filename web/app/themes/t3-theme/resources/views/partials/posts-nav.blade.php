@if(is_singular('post') || is_category())
<section class="b-pt-none bg--white">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="cell small-12">
        @if(is_singular('post'))
          {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
          <a href="javascript:history.back()" class="more">
            <span class="icon-arrow-left-line"></span> {{ _e('zurück zur Übersicht', 't3-theme')}}
          </a>
        @elseif(is_category())
          {!! get_the_posts_navigation(array('prev_text' => __('<span class="icon-arrow-left-line"></span> Ältere Beiträge', 't3-theme'), 'next_text'=>__('Neuere Beiträge <span class="icon-arrow-right-line"></span>', 't3-theme'),)) !!}
        @endif
      </div>
    </div>
  </div>
</section>
@endif
