import lightgallery from 'lightgallery';
import 'lg-thumbnail';
import 'lg-fullscreen';


(function($){

  /**
  * initializeBlock
  *
  * Adds custom JavaScript to the block HTML.
  *
  * @date    15/4/19
  * @since   1.0.0
  *
  * @param   object $block The block jQuery element.
  * @param   object attributes The block attributes (only available when editing).
  * @return  void
  */
  console.log('Lightgallery');

  var initializeBlock = function( $block ) {
    $block.find('.b-cards--gallery').lightGallery();
  }


  // Initialize each block on page load (front end).
    $(document).ready(function() {
      console.log('lg function');
      $('[id*=lightgallery-]').lightGallery({
        selector: '.b-cards__item a',
        thumbnail: true,
        fullscreen: true,
      });
    });

  // Initialize dynamic block preview (editor).
  if( window.acf ) {
    window.acf.addAction( 'render_block_preview/type=gallery', initializeBlock );
  }

})(jQuery);
