<?php
/**
 * Job custom post type
 */
add_action('init', function () {
    $labels = array(
        'name'                => _x( 'Jobs', 'Post Type General Name', 'foundationpress' ),
		'singular_name'       => _x( 'Job', 'Post Type Singular Name', 'foundationpress' ),
		'menu_name'           => __( 'Jobs', 'foundationpress' ),
		'parent_item_colon'   => __( 'Eltern-Jobs', 'foundationpress' ),
		'all_items'           => __( 'Alle Jobs', 'foundationpress' ),
		'view_item'           => __( 'Jobs zeigen', 'foundationpress' ),
		'add_new_item'        => __( 'Neuen Job erstellen', 'foundationpress' ),
		'add_new'             => __( 'Neuer Job', 'foundationpress' ),
		'edit_item'           => __( 'Job bearbeiten', 'foundationpress' ),
		'update_item'         => __( 'Job aktualisieren', 'foundationpress' ),
		'search_items'        => __( 'Job suchen', 'foundationpress' ),
		'not_found'           => __( 'Nicht gefunden', 'foundationpress' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'foundationpress' ),
    );
    register_extended_post_type('jobs', [
        'labels' => $labels,
        'menu_icon' => 'dashicons-id-alt',
        'show_in_rest' => false,
        'supports' => array('title','editor','thumbnail'),
        'has_archive' => false,
        'rewrite' => true,
    ], [
        'singular' => 'Job',
        'plural' => 'Jobs',
        'slug' => 'jobs',
    ]);
    /**
	*   To Activate Custom Post Type Single page
	*   @see http://en.bainternet.info/2011/custom-post-type-getting-404-on-permalinks
	*/
	$set = get_option('post_type_rules_flased_authors');
	if ($set !== true){
		flush_rewrite_rules(false);
		update_option('post_type_rules_flased_authors',true);
	}
});

/**
 * Job taxonomies
 */
add_action('init', function () {
    register_extended_taxonomy('job_category', 'jobs', [
        'show_in_rest' => true,
    ], [
        'singular' => 'Job Category',
        'plural' => 'Job Categories'
    ]);
});

// function jobs_post_link( $post_link, $id = 0 ){
//     $post = get_post( $id );
//     if ( is_object( $post ) ){
//       if(defined('ICL_LANGUAGE_CODE')): $lang = ICL_LANGUAGE_CODE; else: $lang = 'de'; endif;
//       if( $lang == 'de' || $lang == 'at' ) {
//         return str_replace( "/jobs" , "/karriere-bei-leeb/offene-stellen" , $post_link );
//       }
//       elseif($lang == 'sl') {
//         return str_replace( "/jobs" , "/kariera-pri-podjetju-leeb/prosto-delovno-mesto" , $post_link );
//       }
//     }

//     return $post_link;
// }

// add_filter( 'post_type_link', 'jobs_post_link', 1, 3 );

// function custom_add_jobs_rewrite_rules( $rules ) {
//   $new = array();
//   // if(defined('ICL_LANGUAGE_CODE')): $lang = ICL_LANGUAGE_CODE; else: $lang = 'de'; endif;
//   // if( $lang == 'de' || $lang == 'at' ) {
//       $new['karriere-bei-leeb/offene-stellen/(.+)/?$'] = 'index.php?jobs=$matches[1]';
//       $new['kariera-pri-podjetju-leeb/prosto-delovno-mesto/(.+)/?$'] = 'index.php?jobs=$matches[1]';
//       if(!isset($rules)) {
//           return $new;
//       }
//       else {
//           return array_merge( $new, $rules ); // Ensure our rules come first
//       }
//   // }
// }
// add_filter( 'rewrite_rules_array', 'custom_add_jobs_rewrite_rules' );
