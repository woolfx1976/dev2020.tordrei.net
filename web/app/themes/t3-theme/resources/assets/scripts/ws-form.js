import $ from 'jquery';

$(document).on('wsf-rendered', function(e, form, form_id, instance_id) {
  if( $('.file-upload-wrapper').length ) {
    $('.file-upload-wrapper > input[type="file"]').on( 'change', function () {
      var files = $(this)[0].files;
      $('.filenames').html('')
      for (var i = 0; i < files.length; i++) {
        if(i > 0) {
          $('.filenames').append(', ');
        }
        $('.filenames').append(files[i].name);
      }
    });
  }
  // $('.mega-fw > .mega-sub-menu').offset({ left: 0 });

  // app.setMegaTabOffset();

  /** Set TracingID-Cookie for Webuser */
  function getCookie(c_name)
  {
    if(typeof localStorage != 'undefined')
    {
      return localStorage.getItem(c_name);
    }
    else
    {
      var c_start = document.cookie.indexOf(c_name + '=');
      if (document.cookie.length > 0)
      {
        if (c_start !== -1)
        {
          return getCookieSubstring(c_start, c_name);
        }
      }
      return '';
    }
  }

  function setCookie(c_name, value, expiredays)
  {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    if(typeof localStorage != 'undefined')
    {
      //console.log("This place has local storage!");
      localStorage.setItem(c_name, value);
    }
    else
    {
      // console.log("No local storage here");
      // console.log('UTCString: ' + exdate.toUTCString());
      document.cookie = c_name + '=' + escape(value) +
      ((expiredays === null) ? '' : ';expires=' + exdate.toUTCString());
    }
  }

  function generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16;//random number between 0 and 16
      if(d > 0){//Use timestamp until depleted
        r = (d + r)%16 | 0;
        d = Math.floor(d/16);
      } else {//Use microseconds since page-load if supported
        r = (d2 + r)%16 | 0;
        d2 = Math.floor(d2/16);
      }
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

  /** For testing only - deleting cookie */
  // $('#leebCookieDelBtn').click(function(e) {
  //   e.preventDefault();
  //   if (localStorage.getItem('leebWebuserGUID')){
  //     console.log('Cookie wird gelöscht!');
  //     localStorage.removeItem('leebWebuserGUID');
  //   };
  // });

  var wsfID = false;
  var traceIDfield;
  var partnerFormId = 4; // 3 = development
  wsfID = $('form.wsf-form').data('id');
  //console.log('wsfID: ' + wsfID);

  if(wsfID) {
    // DE / AT / CH
    // testing
    // if(wsfID == 1) {
    //   //console.log('Found');
    //   traceIDfield = $('input[name="field_40"]'); // Set field_id for staging and production / 40 = development / 94 = staging and production
    // }

    // contact form simple - Dev / Production
    if(wsfID == 4) {
      traceIDfield = $('input[name="field_94"]'); // Set field_id for staging and production / 40 = development / 94 = production
    }
    // contact form simple - Staging
    // else if(wsfID == 30) {
    //   traceIDfield = $('input[name="field_605"]');
    // }

    // catalog form simple - Production / catalog form 3-steps - Staging
    else if(wsfID == 5) {
      traceIDfield = $('input[name="field_95"]'); // Set field_id for staging and production / 39 = development / 95 = production / NEW staging: 369
    }
    // catalog form 2-steps - Production
    else if(wsfID == 28) {
      traceIDfield = $('input[name="field_421"]');
    }
    // catalog form 3-steps - Production
    else if(wsfID == 26) {
      traceIDfield = $('input[name="field_390"]');
    }
    // catalog form 2-steps - Staging
    // else if(wsfID == 31) {
    //   traceIDfield = $('input[name="field_630"]');
    // }

    // Contact form 3-steps - Dev / Production
    else if(wsfID == 25) {
      traceIDfield = $('input[name="field_367"]'); // Set field_id for staging and production / XXX = development / 367 = production
    }
    // Contact form 3-steps - Staging
    // else if(wsfID == 27) {
    //   traceIDfield = $('input[name="field_538"]');
    // }
    
    //3D-Planung 2-steps - Production
    else if(wsfID == 30) {
      traceIDfield = $('input[name="field_495"]');
    }

    // offert form
    else if(wsfID == 7) {
      traceIDfield = $('input[name="field_96"]'); // Set field_id for staging and production /
    }
    // IT
    // contact form
    else if(wsfID == 8) {
      traceIDfield = $('input[name="field_117"]');
    }
    // catalog form
    else if(wsfID == 9) {
      traceIDfield = $('input[name="field_134"]');
    }

    // offert form
    else if(wsfID == 10) {
      traceIDfield = $('input[name="field_152"]');
    }
    // SI
    // contact form
    else if(wsfID == 11) {
      traceIDfield = $('input[name="field_170"]');
    }
    // catalog form
    else if(wsfID == 12) {
      traceIDfield = $('input[name="field_187"]');
    }

    // offert form
    else if(wsfID == 13) {
      traceIDfield = $('input[name="field_205"]');
    }

    // --- DEV ---------------------
    // contact form
    // if(wsfID == 1) {
    //   traceIDfield = $('input[name="field_49"]'); // Set field_id for staging and production / 40 = development / 94 = staging and production
    // }
    // // catalog form
    // else if(wsfID == 2) {
    //   traceIDfield = $('input[name="field_95"]'); // Set field_id for staging and production / 39 = development / 95 = staging and production
    // }
    //
    // // contact us form
    // else if(wsfID == 3) {
    //   traceIDfield = $('input[name="field_96"]'); // Set field_id for staging and production /
    // }
    // // bewerbung form
    // else if(wsfID == 4) {
    //   traceIDfield = $('input[name="field_117"]');
    // }
  }

  if (localStorage.getItem('leebWebuserGUID')){
    if(getCookie('leebWebuserGUID') == 'b5969ffd-cdbc-41be-b720-f062d8755ace') {
      setCookie('leebWebuserGUID', generateUUID(), 90);
    }
    // else {
      //console.log('leebWebuserGUID is set:' + getCookie('leebWebuserGUID'));
      //if(wsfID >= 4 && wsfID <= 13 || wsfID == 25 || wsfID == 26 || wsfID == 27 || wsfID == 30 || wsfID == 31 || wsfID == 32) {
      if(wsfID >= 4 && wsfID <= 13 || wsfID == 25 || wsfID == 26 || wsfID == 30 || wsfID == 28) {
        if(wsfID != 6) {
          traceIDfield.val(getCookie('leebWebuserGUID'));
        }
      // if(wsfID >= 1 && wsfID <= 3 ) {
        // console.log('Feld wird befüllt: #wsf-'+wsfID+'-field-40');
      }
    // }
  }

  else {
    console.log('cookie not set');
    setCookie('leebWebuserGUID', generateUUID(), 90);
    //setCookie('leebWebuserGUID', 'b5969ffd-cdbc-41be-b720-f062d8755ace', 30);
    //console.log('cookie has set: ' + getCookie('leebWebuserGUID'));
    // if(wsfID >= 4 && wsfID <= 13 || wsfID == 25 || wsfID == 26 || wsfID == 27 || wsfID == 30 || wsfID == 31 || wsfID == 32) {
    if(wsfID >= 4 && wsfID <= 13 || wsfID == 25 || wsfID == 26 || wsfID == 30 || wsfID == 28) {
      if(wsfID != 6) {
        // if(wsfID >= 1 && wsfID <= 3 ) {
        traceIDfield.val(getCookie('leebWebuserGUID'));
      }
    }
  }
  /** end cookie */
});