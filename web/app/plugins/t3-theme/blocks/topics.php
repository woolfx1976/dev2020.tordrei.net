<?php

add_filter('sage/blocks/topics/data', function (array $block) {
	$homeurl = App::homeurl();
	$block['count'] = 0;
	// $items = get_field('topics');
	// $block['items'] = get_field('topics');
	$selected_topics = get_field('topics_sel');
	$block['title'] = get_field('topics_title');
	$block['content'] = get_field('topics_teaser');
	$block['design'] = get_field('topics_design');
	$sizeLarge = get_field('topics_size_large');

	// foreach((array) $items as $item):
	// 	$topics = get_field('topic_items', $item->ID);
	// 	foreach($topics as $card):
	// 		$image = $card['img'];
	//
	// 		if($block['design'] == 'cards') {
	// 			$block['card_items'][] = \App\template('partials.parts.items.card-item-topics', [
	// 				'cellSizes' => $sizeLarge,
	// 				'title' => $card['title'],
	// 				'content' => $card['content'],
	// 				'link' => $card['link'],
	// 				'imageMedium' => $image['sizes']['card-medium'],
	// 				'imageSmall' => $image['sizes']['card-small'],
	// 				'alt' => $image['alt'],
	// 			]);
	// 		}
	// 		else {
	// 			$block['card_items'][] = \App\template('partials.parts.items.masonry-card-item', [
	// 				'title' => $card['title'],
	// 				'content' => $card['content'],
	// 				'link' => $card['link'],
	// 				'imageMedium' => $image['sizes']['fp-medium'],
	// 				'imageSmall' => $image['sizes']['fp-small'],
	// 				'alt' => $image['alt'],
	// 			]);
	// 		}
	//
	// 	endforeach;
	// endforeach;


	if( $selected_topics ):
		foreach( $selected_topics as $post ):
			setup_postdata($post);
			$thumbnail_id = get_post_thumbnail_id($post->ID);
			$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
			if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }

			if($block['design'] == 'cards') {
				$block['card_items_sel'][] = \App\template('partials.parts.items.card-item-topics', [
					'cellSizes' => $sizeLarge,
					'title' => get_the_title($post->ID),
					'content' => get_field('topic_txt', $post->ID),
					'link' => get_field('topic_link', $post->ID),
					'imageMedium' => $homeurl . get_the_post_thumbnail_url($post->ID, 'card-medium' ),
					'imageSmall' => $homeurl . get_the_post_thumbnail_url($post->ID, 'card-small' ),
					'alt' => $alt,
					'width' => '640',
					'height' => '432',
				]);
			}
			else {
				$block['card_items_sel'][] = \App\template('partials.parts.items.masonry-card-item', [
					'title' => $card['title'],
					'content' => $card['content'],
					'link' => $card['link'],
					'imageMedium' => $homeurl . get_the_post_thumbnail_url($post->ID, 'card-medium' ),
					'imageSmall' => $homeurl . get_the_post_thumbnail_url($post->ID, 'card-small' ),
					'alt' => $alt,
					'width' => '640',
					'height' => '432',
				]);
			}

		endforeach;
	endif;

	return $block;
});
