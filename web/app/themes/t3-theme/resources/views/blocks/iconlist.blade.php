{{--
  Name: iconlist
  Title: Iconliste
  Description: Icon-List, CPTs
  Category: t3layout
  Icon: ellipsis
  Keywords: icons, columns
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

<div class="l-wrapper">
  <ul data-{{ $block['id'] }} class="c-list b-iconlist grid-x align-{{ $block['itemAlign'] }} {{ $block['classes'] }}">
    @php
      $cellSizes = $block['cellClasses'];
    @endphp
    @if(count((array)$block['iconItems']))
      @foreach ($block['iconItems'] as $item)
        <li class="b-iconlist__item {{$block['cellClasses']}}">
          {!! $item !!}
        </li>
      @endforeach
    @endif
  </ul>
</div>
