<?php
// Creating the widget
class t3_mega_img_txt_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

			// Base ID of your widget
			't3_mega_img_txt_widget',

			// Widget name will appear in UI
			__('MMM Image/Text-Link', 't3_widget_domain'),

			// Widget description
			array( 'description' => __( 'Adds an Image/Text-Link-Block for the Max-Mega-Menu (Required)', 't3_widget_domain' ), )
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
		//$title = apply_filters( 'widget_title', $instance['title'] );

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		//if ( ! empty( $title ) )
		//echo $args['before_title'] . $title . $args['after_title'];

		//echo 'ID: ' . 'widget_' . $this->id;

		// This is where you run the code and display the output
		$img = '';
		$content = '';
		$linkElm = '';
		$link = get_field('mmm_link', 'widget_' . $this->id);
		$wgtType = get_field('mmm_link_type', 'widget_' . $this->id);
		$wgtType == '3' ? $highlight = true : $highlight = false;

		// Link-Wrapper
		$classes = array(
			'wrapper' => '',
			'img' => '',
			'cont' => '',
		);

		switch( $wgtType ) {
			case '2':
				$classes = array(
					'wrapper' => 'mega-wgt-highlight',
					'img' => '',
					'cont' => '',
				);
				break;
			case '1':
				$classes = array(
					'wrapper' => 'mega-wgt-img-large',
					'img' => '',
					'cont' => ' class="mega-wgt-cont"',
				);
				break;
			case '0':
				$classes = array(
					'wrapper' => 'mega-wgt-wrapper',
					'img' => ' class="mega-wgt-img"',
					'cont' => ' class="mega-wgt-cont"',
				);
				break;
		}

		// Link / Highlight-Image ---
		// 0 = none; 1 = post thumbnail; 2 = Image large; 3 = custom image

		$imgType = get_field('mmm_img_type', 'widget_' . $this->id);
		$image = get_field('mmm_img', 'widget_' . $this->id);

		if( $imgType != 0 ) {


			if( $link ) {
				if(isset($_GET['work'])):
					echo '<p>imgType: ' . $imgType . '</p>';
					echo 'ID ->' . url_to_postid($link['url']) . '<-<br/>';
					echo 'IMG ->' . get_the_post_thumbnail_url( url_to_postid($link['url']), 'card-xsmall' ) . '<-';
				endif;
				$url = '';
				if( $imgType == 1 ) {
						//$page = get_page_by_path($link['url']);
					//$url = get_the_post_thumbnail_url( url_to_postid($link['url']), 'card-xsmall' );

					// if(!empty($url)) {
						// $img = '<div' . $classes['img'] . '>' . $link['url'] . ' - ' . $pageID . '<img src="' . $url . '" alt="' . $link['title'] . '" /></div>';
						$img = '<div' . $classes['img'] . ' data-id="' . url_to_postid($link['url']) . '" data-link-url="' . $link['url'] . '" data-img-url="' . $url . '" data-link-title="' . $link['title'] . '"><img src="' . get_the_post_thumbnail_url( url_to_postid($link['url'] ), 'card-xsmall' ) . '" alt="' . $link['title'] . '" /></div>';
					// }
				}
				if( $imgType == 2 ) {
					if($image) {
						$img = '<div' . $classes['img'] . '><img src="' . $image['sizes']['card-xsmall'] . '" alt="' . $image['alt'] . '" /></div>';
					}
				}
			}
		}

		// Link-Title

		if(!empty(get_field('mmm_title', 'widget_' . $this->id))) {
			if($highlight) {
				$content .= '<span>' . get_field('mmm_title', 'widget_' . $this->id) . '</span>';
			}
			else {
				$content .= '<p class="title">' . get_field('mmm_title', 'widget_' . $this->id) . '</p>';
			}
		}

		// Highlight-Link

		if($highlight && $link ) {
			if(!empty($link['title'])) {

				$linkElm .= '<p><a href="' . $link['url'] . '" class="more">' . $link['title'] . '</a></p>';
			}
			else {
				$linkElm  .= '<p><a href="' . $link['url'] . '" class="more">weiterlesen</a></p>';
			}
		}

		// Link-Content

		if(!empty(get_field('mmm_txt', 'widget_' . $this->id))) {
			$content .= '<p>' . get_field('mmm_txt', 'widget_' . $this->id) . $linkElm . '</p>';
		}
		else {
			$content .= $linkElm;
		}

		if(!empty($content)) {
			$content = '<div' . $classes['cont'] . '>' . $content . '</div>';
		}

		// Get widget code

		if($link && !$highlight) {
			global $wp;
			$currUrl = home_url( $wp->request );
			if(substr($currUrl, -1) != '/'): $currUrl .= '/'; endif;
			if(isset($_GET['work'])):
				echo $currUrl . ' - ' . $link['url'];
			endif;
			if($currUrl == $link['url']):
				$classes['wrapper'] .= ' is-active';
			endif;
			echo '<a href="' . $link['url'] . '" class="' . $classes['wrapper'] . '">' . $img . $content . '</a>';
		}
		else {
			echo '<div class="' . $classes['wrapper'] . '">' . $img . $content . '</div>';
		}

		//echo __( 'Hello, World!', 't3_widget_domain' );
		echo $args['after_widget'];
	}

	// Widget Backend

	public function form( $instance ) {
		// if ( isset( $instance[ 'title' ] ) ) {
		// 	$title = $instance[ 'title' ];
		// }
		// else {
		// 	$title = __( 'New title', 't3_widget_domain' );
		// }
		// Widget admin form
		?>
		<!-- <p>
			<label for="<?php //echo $this->get_field_id( 'title' ); ?>"><?php //_e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php //echo $this->get_field_id( 'title' ); ?>" name="<?php //echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p> -->
		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		//$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
}


// Register and load the widget
function t3_load_widget() {
	register_widget( 't3_mega_img_txt_widget' );
}
add_action( 'widgets_init', 't3_load_widget' );
