<?php

	$homeurl = App::homeurl();
  $curr_cat = '';

  if ( is_singular('product') ) {
      $terms = get_the_terms($post->ID, 'product_category');
      if(isset($_GET['work'])):
          var_dump($terms);
      endif;
      foreach ($terms as $term) {
        if($term->parent != '0') {
          $term_link = get_term_link($term, 'product_category');
          if (is_wp_error($term_link))
              continue;
              $curr_cat = $term->term_id;
              if(isset($_GET['work'])):
                echo '<a href="' . $term_link . '">' . $term->name . '</a>, ';
              endif;
        }
      }
  }

  $args = array(
    'post_type'=> 'product',
    'numberposts' => -1,
    // 'taxonomy' => 'product_category',
    // 'cat' => $curr_cat,
    'orderby' => 'post_title',
    'order'=> 'ASC',
    'suppress_filters' => 0,
    'tax_query' => array(
              array(
                  'taxonomy' => 'product_category',
                  'field' => 'term_id',
                  'terms' => $curr_cat,
            )),
  );
  $posts = get_posts($args);
  // if(isset($_GET['work'])):
  //   echo '<p><b>$curr_cat:</b> ' . $curr_cat . '</p>';
  //   //var_dump($posts);
  //   echo '<ul>';
  //   foreach ($posts as $thepost) {
  //     echo  '<li>'. $thepost->ID . ' - ' . $thepost->post_title . '</li>';
  //   }
  //   echo '</ul>';
  //   echo '<p><b>Count Posts:</b> ' . count($posts) . '</p>';
  // endif;
  // Get IDs of posts retrieved by get_posts function
  $ids = array();
  foreach ($posts as $thepost) {
    $ids[] = $thepost->ID;
  }

  $index = array_search($post->ID, $ids);
  if(($index-1) > -1):
    if($ids[$index-1]):
      $prev_post = $ids[$index-1];
    endif;
  endif;
  if(isset($ids[$index+1])):
    $next_post = $ids[$index+1];
  endif;
?>
<?php if ( !empty($prev_post) ) { ?>
  <a href="<?php echo get_permalink($prev_post) ?>" class="c-fe-postnav left">
      <span class="icon-arrow-left" style="position: absolute; left: 8px; top: calc(50% - 8px);"></span>
      <div class="c-fe-postnav__wrapper">
          <div class="c-fe-postnav__content">
            <span class="title"><?php echo get_the_title($prev_post); ?></span>
            <span>
              <img src="<?php echo $homeurl . get_the_post_thumbnail_url($prev_post, 'square-small'); ?>" width="640" height="640" class="img" alt="" />
            </span>
          </div>
      </div>
    </a>
  </div>
<?php } ?>
<?php if ( !empty($next_post) ) { ?>
  <a href="<?php echo get_permalink($next_post) ?>" class="c-fe-postnav right">
      <span class="icon-arrow-right" style="position: absolute; left: 8px; top: calc(50% - 8px);"></span>
      <div class="c-fe-postnav__wrapper">
          <div class="c-fe-postnav__content">
            <span class="title"><?php echo get_the_title($next_post); ?></span>
            <span>
              <img src="<?php echo $homeurl . get_the_post_thumbnail_url($next_post, 'square-small'); ?>" width="640" height="640" class="img" alt="" />
            </span>
          </div>
      </div>
    </a>
  </div>
<?php } ?>
