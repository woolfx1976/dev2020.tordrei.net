<?php

namespace App;

/**
* Add <body> classes
*/
add_filter('body_class', function (array $classes) {
  /** Add page slug if it doesn't exist */
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
    if( is_singular('product') || is_singular('jobs') ) {
      $classes[] = 'has-text-header';
    }
  }

  if (is_page()) {
    $post = get_post(get_the_ID());
    $blocks = parse_blocks($post->post_content);
    foreach($blocks as $block) {
      if($block['blockName'] == 'acf/featured-image') {
        if($block['attrs']['data']['header_type'] == 'text') {
          $classes[] = 'has-text-header';
        }
      }
    }
  }

  if (is_archive()) {
    $classes[] = 'has-text-header';
  }

  /** Add class if sidebar is active */
  if (display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  /** Clean up class names for custom templates */
  $classes = array_map(function ($class) {
    return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
  }, $classes);

  return array_filter($classes);
});

/**
* Add "… Continued" to the excerpt
*/
add_filter('excerpt_more', function () {
  // return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
  return ' &hellip;';
});

/**
* Template Hierarchy should search for .blade.php files
*/
collect([
  'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
  'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
  ])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
  });

  /**
  * Render page using Blade
  */
  add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
      ob_start();
      do_action($tag);
      $output = ob_get_clean();
      remove_all_actions($tag);
      add_action($tag, function () use ($output) {
        echo $output;
      });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
      return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
      echo template($template, $data);
      return get_stylesheet_directory().'/index.php';
    }
    return $template;
  }, PHP_INT_MAX);

  /**
  * Render comments.blade.php
  */
  add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
      [get_stylesheet_directory(), get_template_directory()],
      '',
      $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
      return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
      echo template($theme_template, $data);
      return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
  }, 100);

  /**
  * Parsing image sizes to Gutenberg blocks
  */
  add_filter( 'image_size_names_choose', function ($sizes) {
    return array_merge( $sizes, array(
      'fp-small' => __('fpSmall'),
      'fp-medium' => __('fpMedium'),
      'fp-large' => __('fpLarge'),
      'fp-xlarge' => __('fpXlarge'),
      // 'card-xsmall' => __('cardSmall'),
      'card-small' => __('cardSmall'),
      'card-medium' => __('cardMedium'),
      'card-large' => __('cardLarge'),
      '18to1-small' => __('18to1Small'),
      '18to1-medium' => __('18to1Medium'),
      '18to1-large' => __('18to1Large'),
      //'card-xlarge' => __('cardXlarge'),
    ) );
  } );

  /**
  * Language Selector
  */
  // add_filter( 'wp_nav_menu_items', function ($items, $args) {
  //     if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
  //         if( $args->theme_location == 'primary_navigation' ) {
  //             $languages = icl_get_languages('skip_missing=0&orderby=code&order=desc');
  //             $selector = '';
  //             if(!empty($languages)){
  //                 //print_r($languages);
  //                 foreach($languages as $l){
  //                     if(!$l['active']) {
  //                         $selector .= '<li class="c-lang__item">'
  //                         . '<a href="'.$l['url'].'" class="flag-'.$l['language_code'].'">'
  //                         . $l['translated_name']
  //                         . '</a>'
  //                         . '</li>';
  //                     }
  //                 }
  //             }
  //
  //             $items .= '<li class="menu-item c-lang">'
  //             . '<button class="c-lang__btn flag-' . strtolower(ICL_LANGUAGE_CODE) . '" type="button" data-toggle="language-selector"></button>'
  //             . '<ul class="c-lang__list c-list dropdown-pane large-dropdown" id="language-selector" data-dropdown data-hover="true" data-hover-pane="true">'
  //             . $selector
  //             . '</ul>'
  //             . '</li>';
  //         }
  //         return $items;
  //     }
  // }, 10, 2 );




  /**
  * Remove everything from the include, so we can render ourselfves afterwards.
  */

  add_filter('tribe_get_template_part_path', 'App\tribe_render_detault_blade', PHP_INT_MAX, 2);
  function tribe_render_detault_blade($file, $template) {
    $theme_template = locate_template(["views/tribe/events/{$template}"]);

    if ($theme_template) {

      $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
      }, []);

      echo template($theme_template, $data);

      return get_stylesheet_directory().'/index.php';
    }
    return $file;
  }

  add_filter('tribe_get_current_template', function( $template ){
    $theme_template = locate_template(["views/tribe/events/{$template}"]);

    if ($theme_template)
    return $theme_template;

    return $template;
  }, PHP_INT_MAX, 2);

  add_filter('tribe_events_template', 'App\\tribe_add_blade_templates', PHP_INT_MAX, 2);
  function tribe_add_blade_templates($file, $template) {
    $theme_template = locate_template(["views/tribe/events/{$template}"]);

    if ($theme_template)
    return $theme_template;

    return $file;
  }

  add_action( 'tribe_events_before_view', function($file) {
    ob_start();
  } );

  add_action( 'tribe_events_after_view', function($file) {
    $html = ob_get_clean();

    if(strpos($file, '.blade.php') !== false) {

      $data = collect(get_body_class())->reduce(function ($data, $class) use ($file) {
        return apply_filters("sage/template/{$class}/data", $data, $file);
      }, []);

      echo template($file, $data);

    } else echo $html;
  } );

  add_filter( 'get_the_archive_title', function($title) {
    return str_replace('Category: ', '', $title);
  });

  /* Remove Yoast SEO Canonical From All Pages
  * Credit: Yoast Team
  * Last Tested: Jun 16 2017 using Yoast SEO 4.9 on WordPress 4.8
  */

  add_filter( 'wpseo_canonical', '__return_false' );

  /**
  * Fix WPML-Language-Switcher-URLs for single product and archive (posts)
  */
  add_filter( 'icl_ls_languages', function( $languages ) {

    // Archive - Fix URL for main language (add domain to URL)
    if(is_archive()) {
      $home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
      $prefix = '/category/';
      $domainArray = array(
        // 'de' => 'https://staging.t3-theme.com',
        // 'at' => 'https://staging-at.t3-theme.com',
        // 'ch' => 'https://staging-ch.t3-theme.com',
        // 'it' => 'https://staging-it.t3-theme.com',
        // 'sl' => 'https://staging-si.t3-theme.com',
        'de' => 'https://www.leeb-balkone.com',
        'at' => 'https://www.leeb.at',
        'ch' => 'https://www.leeb-balkone.ch',
        'it' => 'https://www.leeb.it',
        'sl' => 'https://www.leeb.si',
      );
      foreach( $languages as $lang => $element ) {
        $currUrl = $languages[$lang]['url'];
        if(strncmp($currUrl, $prefix, strlen($prefix)) === 0) {
          $languages[$lang]['url'] = $domainArray[$languages[$lang]['code']] . $currUrl;
        }
      }
    }
    elseif(is_singular('product')) {
      // global $icl_adjust_id_url_filter_off;
      // $icl_adjust_id_url_filter_off = false;
      //$languages = icl_get_languages('skip_missing=1');
      // $post = get_post();
      // $type = $post->post_type;
      // print_r($languages);
      // foreach ($languages as $l) {
      //   // in $count is already the count from the current language
      //   if (!$l['active']) {
      //     $otherID = icl_object_id(get_the_ID(), $type, false, $l['language_code']);
      //     if ($otherID) {
      //       echo '<p>' . $l['language_code'] .' - ' . $otherID . '</p>';
      //       // cannot use call_user_func due to php regressions
      //       // if ($type == 'page') {
      //       //   $otherpost = get_page($otherID);
      //       // } else {
      //       //   $otherpost = get_post($otherID);
      //       // }
      //       // if ($otherpost) {
      //       //   // increment comment count using translation post comment count.
      //       //   $count = $count + $otherpost->comment_count;
      //       // }
      //     }
      //   }
      // }

      global $sitepress;
      global $post;
      $current_lang = $sitepress->get_current_language();
      $default_lang = $sitepress->get_default_language();

      $sitepress->switch_lang($default_lang);
      $categories = get_terms(array(
        'taxonomy' => 'product_category',
        'orderby'    => 'slug',
        'order'      => 'ASC',
        'suppress_filters' => true,
      ));

      $categories = get_the_terms(get_the_ID(), 'product_category');
      $productIsCat = get_field('product_is_cat', get_the_ID());

      // var_dump($categories);
      $sitepress->switch_lang($current_lang);



      $langArray = array($default_lang, 'at', 'ch', 'it', 'sl');
      $langCats = array(
        $default_lang => array(
          'langID' => $default_lang,
          'postID' => '',
          'parent' => 'produkte',
          'category' => '',
          'title' => '',
          'missing' => false,
        ),
        'at' => array(
          'langID' => 'at',
          'postID' => '',
          'parent' => 'produkte',
          'category' => '',
          'title' => '',
          'missing' => false,
        ),
        'ch' => array(
          'langID' => 'ch',
          'postID' => '',
          'parent' => 'produkte',
          'category' => '',
          'title' => '',
          'missing' => false,
        ),
        'it' => array(
          'langID' => 'it',
          'postID' => '',
          'parent' => 'prodotti',
          'category' => '',
          'title' => '',
          'missing' => false,
        ),
        'sl' => array(
          'langID' => 'sl',
          'postID' => '',
          'parent' => 'produkti',
          'category' => '',
          'title' => '',
          'missing' => false,
        )
      );

      $productArr['de'] = icl_object_id( get_the_ID(), 'product', false, 'de');
      $productArr['at'] = icl_object_id( get_the_ID(), 'product', false, 'at');
      $productArr['ch'] = icl_object_id( get_the_ID(), 'product', false, 'ch');
      $productArr['it'] = icl_object_id( get_the_ID(), 'product', false, 'it');
      $productArr['sl'] = icl_object_id( get_the_ID(), 'product', false, 'sl');

      $home_url = array();

      $c = 0;

      foreach( $categories as $category ) {
        if(isset($category->parent) && $category->parent == 0) {
          $id = icl_object_id( $category->term_id, 'product_category', true, ICL_LANGUAGE_CODE );
          $c == 0 ? $currentCatId = $id : $currentCatId = '';
          $c++;


          //if(isset($_GET['work'])): echo '<hr/><p><b>' . $category->parent . ' / ' . $category->slug . ' / ' . $currentCatId . '</b>'; endif;

          if($category->parent == 0 && $id == $currentCatId):
            //if(isset($_GET['work'])): echo '<br/><b>' . $category->term_id . ' / ' . $category->slug . ' / ' . $currentCatId . '</b></p>'; endif;

            foreach($languages as $key => $val ) {
              $sitepress->switch_lang($val['code']); // Switch to new language
              $home_url[$val['code']] = apply_filters( 'wpml_home_url', get_option( 'home' ) );
              $currCategory[$val['code']] = get_term( icl_object_id( $currentCatId, 'product_category', false, $val['code']) );
              $catID = icl_object_id( $category->term_id, 'product_category', false, $val['code']);
              //if(isset($_GET['work'])): echo '<b>' . $c . ' - ' . $val['code'] . ' - ' . $catID . '</b><br/>'; endif;
              //$postID = apply_filters( 'wpml_object_id', get_the_ID(), 'product', false, $lang );
              $postID = $productArr[$val['code']];
              // $productId =  apply_filters( 'wpml_object_id', get_the_ID(), 'product', TRUE,  'it' );
              //if(isset($_GET['work'])): echo '<p>postID: ' . $postID . '</p>'; endif;
              if($postID && isset($currCategory[$val['code']]->term_id) && isset($currCategory[$val['code']]->slug)) {

                $id = icl_object_id(get_the_ID(), 'product', false, $val['code']);
                $langCats[$val['code']]['postID'] = $postID;
                $langCats[$val['code']]['id'] = $currCategory[$val['code']]->term_id;
                $productIsCat != '1' ? $langCats[$val['code']]['slug'] = $currCategory[$val['code']]->slug : $langCats[$val['code']]['slug'] = '';
                //$langCats[$val['code']]['slug'] = $currCategory[$val['code']]->slug;
                $post = get_post($postID);
                $langCats[$val['code']]['title'] = $post->post_name;


              }
              else {
                $langCats[$val['code']] = array('missing' => true);
              }
            }

            $sitepress->switch_lang($current_lang);

          endif;
        }

      }
      //if(isset($_GET['wpmltest'])):
      //var_dump($langCats);
      //var_dump($home_url);
      //endif;

      global $wpml_url_converter;
      $abs_home = $wpml_url_converter->get_abs_home();

      // $url = get_the_permalink();
      // echo '<li>' . $wpml_permalink = apply_filters( 'wpml_permalink', $url , 'de' ) . '</li>';
      // echo '<li>' . $wpml_permalink = apply_filters( 'wpml_permalink', $url , 'at' ) . '</li>';
      // echo '<li>' . $wpml_permalink = apply_filters( 'wpml_permalink', $url , 'ch' ) . '</li>';
      // echo '<li>' . $wpml_permalink = apply_filters( 'wpml_permalink', $url , 'it' ) . '</li>';
      // echo '<li>' . $wpml_permalink = apply_filters( 'wpml_permalink', $url , 'sl' ) . '</li>';

      foreach( $languages as $lang => $element ) {
        if(!$languages[$lang]['missing'] && isset($langCats[$lang]['parent'])):
          $path[$lang] = '';
          $path[$lang] .= '/' . $langCats[$lang]['parent'];
          if(!empty($langCats[$lang]['slug'])) { $path[$lang] .= '/' . $langCats[$lang]['slug']; }
          // $path .= '/' . $langCats[$lang]['slug'];
          $path[$lang] .= '/' . $langCats[$lang]['title'] . '/';

          //echo '[' . $lang . ' - ' . $path[$lang] . ']';

          // $correct_url = get_site_url();
          //
          // echo $correct_url;

          // if($lang == 'de') {
          //   $languages[$lang]['url'] = rtrim($abs_home, '/wp/');
          //   $languages[$lang]['url'] .= $path[$lang];
          // }
          // else {
          //$languages[$lang]['url'] = rtrim($correct_url, '/wp/');
          $languages[$lang]['url'] = $home_url[$lang] . $path[$lang];
          // }
        else:
          $languages[$lang]['url'] = $home_url[$lang];
        endif;
      }


    }
    return $languages;
  });
