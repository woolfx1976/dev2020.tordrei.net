/**
* BLOCK: t3-formats
*
* Registering a basic block with Gutenberg.
* Simple block, renders and saves the same content without any interactivity.
*/

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { registerBlockStyle } = wp.blocks;

/**
* Register: aa Gutenberg Block.
*
* Registers a new block provided a unique name and an object defining its
* behavior. Once registered, the block is made editor as an option to any
* editor interface where blocks are implemented.
*
* @link https://wordpress.org/gutenberg/handbook/block-api/
* @param  {string}   name     Block name.
* @param  {Object}   settings Block settings.
* @return {?WPBlock}          The block, if it has been successfully
*                             registered; otherwise `undefined`.
*/
const { createElement, Fragment } = window.wp.element
const { registerFormatType, toggleFormat } = window.wp.richText
const { RichTextToolbarButton, RichTextShortcut } = window.wp.editor;

/** Teaser */
const T3ButtonTeaser = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='Teaser'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/teaser' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/teaser', {
        title: 'Teaser',
        tagName: 'span',
        className: 'teaser',
        edit: T3ButtonTeaser,
    }
);

/** Uppercase */
const T3ButtonTxtUpper = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='Text in Grossbuchstaben'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/upper' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/upper', {
        title: 'Uppercase',
        tagName: 'span',
        className: 'uppercase',
        edit: T3ButtonTxtUpper,
    }
);

/** Product-Teaser */
const T3ButtonProductTeaser = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='Produkt-Teaser'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/product-teaser' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/product-teaser', {
        title: 'Produkt-Teaser',
        tagName: 'span',
        className: 'product-teaser',
        edit: T3ButtonProductTeaser,
    }
);

/** Large Text (same as Teaser) */
const T3ButtonLargeText = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='Large'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/large' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/large', {
        title: 'Large',
        tagName: 'span',
        className: 'large',
        edit: T3ButtonLargeText,
    }
);

/** Light */
const T3ButtonLightText = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='Light'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/light' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/light', {
        title: 'Light',
        tagName: 'span',
        className: 'light',
        edit: T3ButtonLightText,
    }
);

/** H2 text */
const T3ButtonH2Text = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='H2 Text'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/h2text' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/h2text', {
        title: 'H2 Text',
        selector: 'span',
        className: 'h2-text',
        edit: T3ButtonH2Text,
    }
);

/** H2 as Teaser-Large */
const T3ButtonH2asTeaser = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='H2 as Teaser-Large'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/h2asteaser' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/h2asteaser', {
        title: 'H2 as Teaser-Large',
        tagName: 'span',
        className: 'h2asteaser',
        edit: T3ButtonH2asTeaser,
    }
);

/** H2 as H1 */
const T3ButtonH2asH1 = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='H2 as H1'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/h2ash1' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/h2ash1', {
        title: 'Light',
        tagName: 'span',
        className: 'h2ash1',
        edit: T3ButtonH2asH1,
    }
);

/** H2 as H3 */
const T3ButtonH2asH3 = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='H2 as H3'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/h2ash3' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/h2ash3', {
        title: 'H2 as H3',
        tagName: 'span',
        className: 'h2ash3',
        edit: T3ButtonH2asH3,
    }
);

/** H3 as H4 */
const T3ButtonH3asH4 = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='H3 as H4'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/h3ash4' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/h3ash4', {
        title: 'H3 as H4',
        tagName: 'span',
        className: 'h3ash4',
        edit: T3ButtonH3asH4,
    }
);

/** P as H1 */
const T3ButtonPasH1 = ( props ) => {
    return <RichTextToolbarButton
        icon='admin-customizer'
        title='p as H1 - 300'
        onClick={ () => {
            props.onChange( toggleFormat(
                props.value,
                { type: 't3-formats/pash1' }
            ) );
        } }
        isActive={ props.isActive }
    />;
};

registerFormatType(
    't3-formats/pash1', {
        title: 'P as H1 - 300',
        tagName: 'span',
        className: 'pash1',
        edit: T3ButtonPasH1,
    }
);

registerBlockStyle(
	'core/list', {
	 name: 'arrows-small',
	 label: 'kleine Pfeile',
	}
);

registerBlockStyle(
	'core/list', {
	 name: 'arrows-large',
	 label: 'große Pfeile',
	}
);

registerBlockStyle(
	'core/list', {
	 name: 'bullet-arrows',
	 label: 'Kreis + Pfeile',
	}
);

registerBlockStyle(
	'core/list', {
	 name: 'bullet-checkmarks',
	 label: 'Kreis + Check',
	}
);

registerBlockStyle(
	'core/list', {
	 name: 'alt-rows',
	 label: 'alternierende Zeilen',
	}
);
