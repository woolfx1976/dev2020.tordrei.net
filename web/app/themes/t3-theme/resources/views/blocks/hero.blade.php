{{--
  Name: hero
  Title: Hero-Image
  Description: Hero-Image
  Category: t3layout
  Icon: grid-view
  Keywords: hero, image, text
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

@if(!empty($block['hero-data']->inlineStyle))
<style>
  #hero-{{ $block['id'] }}::before {
    {!! $block['hero-data']->inlineStyle !!}
  }
</style>
@endif
@php
  if(!is_admin()):
    $attributes = $block['hero-data']->style . ' ' . $block['hero-data']->interchange;
  else:
    $attributes = ' style="background-image: url(' . $block['hero-data']->imageLarge. '); background-size: cover; background-position: center;"';
  endif;
@endphp
<div id="hero-{{$block['id']}}" class="b-hero {{$block['hero-data']->design}}" {!!$attributes!!}>
  @if(!empty($block['hero-data']->content))
    <div class="grid-container {{$block['hero-data']->textAlignY}}">
      <div class="grid-x align-{{$block['hero-data']->textAlignX}}">
        <div class="cell{{$block['hero-data']->classes}}">
            {!! $block['hero-data']->content !!}
        </div>
      </div>
    </div>
  @endif
</div>
