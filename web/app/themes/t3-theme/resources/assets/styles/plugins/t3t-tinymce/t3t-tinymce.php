<?php
/**
* Plugin Name: T3T TinyMCE
* Plugin URI: http://wpbeginner.com
* Version: 1.0
* Author: WPBeginner
* Author URI: http://www.wpbeginner.com
* Description: A simple TinyMCE Plugin to add a custom functions to the the Visual Editor
* License: GPL2
*/

class t3t_TinyMCE_Class {

    /**
    * Constructor. Called when the plugin is initialised.
    */
    function __construct() {
        if ( is_admin() ) {
            add_action( 'init', array(  $this, 'setup_tinymce_plugin' ) );
        }
    }

    /**
    * Check if the current user can edit Posts or Pages, and is using the Visual Editor
    * If so, add some filters so we can register our plugin
    */
    function setup_tinymce_plugin() {

        // Check if the logged in WordPress User can edit Posts or Pages
        // If not, don't register our TinyMCE plugin

        if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
            return;
        }

        // Check if the logged in WordPress User has the Visual Editor enabled
        // If not, don't register our TinyMCE plugin
        if ( get_user_option( 'rich_editing' ) !== 'true' ) {
            return;
        }

        // Setup some filters
        add_filter( 'mce_external_plugins', array( &$this, 'add_tinymce_plugin' ) );
        add_filter( 'mce_buttons', array( &$this, 'add_tinymce_toolbar_button' ) );

        // Setup Visual Editor
        add_filter('mce_buttons_2', array( &$this, 'wpb_mce_buttons_2') );
        // Attach callback to 'tiny_mce_before_init'
        add_filter( 'tiny_mce_before_init', array( &$this, 'my_mce_before_init_insert_formats') );

    }

    /**
    * Adds a TinyMCE plugin compatible JS file to the TinyMCE / Visual Editor instance
    *
    * @param array $plugin_array Array of registered TinyMCE Plugins
    * @return array Modified array of registered TinyMCE Plugins
    */
    function add_tinymce_plugin( $plugin_array ) {

        // $plugin_array['t3t_tinymce_class'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';
        // $plugin_array['t3t_tinymce_class2'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';
        // $plugin_array['t3t_tinymce_class3'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';
        // $plugin_array['t3t_tinymce_class4'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';
        // $plugin_array['t3t_tinymce_class5'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';
        // $plugin_array['t3t_tinymce_class6'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';
        // $plugin_array['t3t_tinymce_class7'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';
        // $plugin_array['t3t_tinymce_class8'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';

        // $plugin_array['t3t_tinymce_class-center'] = plugin_dir_url( __FILE__ ) . 't3t-tinymce-class.js';
        return $plugin_array;

    }

    /**
    * Adds a button to the TinyMCE / Visual Editor which the user can click
    * to insert a link with a custom CSS class.
    *
    * @param array $buttons Array of registered TinyMCE Buttons
    * @return array Modified array of registered TinyMCE Buttons
    */
    function add_tinymce_toolbar_button( $buttons ) {

        //array_push( $buttons, '|', 't3t_tinymce_class','t3t_tinymce_class2','t3t_tinymce_class3','t3t_tinymce_class4','t3t_tinymce_class5','t3t_tinymce_class6','t3t_tinymce_class7','t3t_tinymce_class8' );
        return $buttons;
    }

    // Add styles to the Visual Editor ---
    function wpb_mce_buttons_2($buttons) {
        array_unshift($buttons, 'styleselect');
        return $buttons;
    }

    /*
    * Callback function to filter the MCE settings
    */

    function my_mce_before_init_insert_formats( $init_array ) {

        // Define the style_formats array

        $style_formats = array(
            /*
            * Each array child is a format with it's own settings
            * Notice that each array has title, block, classes, and wrapper arguments
            * Title is the label which will be visible in Formats menu
            * Block defines whether it is a span, div, selector, or inline style
            * Classes allows you to define CSS classes
            * Wrapper whether or not to add a new block-level element around any selected elements
            */
            // array(
            //     'title' => 'Content Block',
            //     'block' => 'span',
            //     'classes' => 'content-block',
            //     'wrapper' => true,
            //
            // ),
            // array(
            //     'title' => 'Blue Button',
            //     'block' => 'span',
            //     'classes' => 'blue-button',
            //     'wrapper' => true,
            // ),
            // array(
            //     'title' => 'Teaser',
            //     'selector' => 'p',
            //     'classes' => 'teaser'
            // ),
            // array(
            //     'title' => 'Teaser large',
            //     'selector' => 'p',
            //     'classes' => 'large'
            // ),
            // array(
            //     'title' => 'green',
            //     'selector' => '*',
            //     'classes' => 'primary'
            // ),
            // array(
            //     'title' => 'light-green',
            //     'selector' => '*',
            //     'classes' => 'secondary'
            // ),
            // array(
            //     'title' => 'List large',
            //     'selector' => 'ul',
            //     'classes' => 't3-list--large'
            // ),
            array(
                'title' => 'H1 xlarge',
                'inline' => 'span',
                'classes' => 'large'
            ),
            array(
                'title' => 'Teaser',
                'selector' => 'p',
                'classes' => 'teaser'
            ),
            array(
                'title' => 'P large',
                'selector' => 'p',
                'classes' => 'large'
            ),
            array(
                'title' => 'Zitat',
                'selector' => 'p',
                'classes' => 'blockquote'
            ),
            array(
                'title' => 'P as H2',
                'selector' => 'p',
                'classes' => 'p-as-h2'
            ),
            array(
                'title' => 'light',
                'inline' => 'span',
                'classes' => 'light'
            ),
            array(
                'title' => 'small',
                'selector' => 'p',
                'classes' => 'small'
            ),
            array(
                'title' => 'Primärfarbe',
                'selector' => '*',
                'classes' => 'primary'
            ),
            array(
                'title' => 'Sekundärfarbe',
                'selector' => '*',
                'classes' => 'secondary'
            ),
            array(
                'title' => 'weiß',
                'selector' => '*',
                'classes' => 'white'
            ),
            array(
                'title' => 'Tabelle Standard',
                'selector' => 'table',
                'classes' => 't3-tbl--std'
            ),
            array(
                'title' => 'Spezial-Font',
                'selector' => '*',
                'classes' => 'special'
            ),
            array(
                'title' => 'Sidebar small',
                'selector' => 'p',
                'classes' => 'sidebar-small'
            ),
            // array(
            //     'title' => 'List large',
            //     'selector' => 'ul',
            //     'classes' => 't3-list--large'
            // ),
        );
        // Insert the array, JSON ENCODED, into 'style_formats'
        $init_array['style_formats'] = json_encode( $style_formats );

        return $init_array;

    }


}

$t3t_tinymce_class = new t3t_TinyMCE_Class;

?>
