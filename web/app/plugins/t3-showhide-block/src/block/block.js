/**
 * BLOCK: t3-showhide-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import classnames from 'classnames';
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

const {
  BlockControls,
	InnerBlocks,
	InspectorControls,
} = wp.blockEditor;

const {
	PanelBody,
	SelectControl,
  TextControl,
} = wp.components;


/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 't3/showhide-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Show-Hide-Block' ), // Block title.
	icon: 'insert-after', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 't3layout', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Show-Hide-Block' ),
		__( 'Bereiche ein- und ausblenden' ),
		__( 'T3-Layouts' ),
	],

  supports: {
		align: [ 'full' ], // Support Wide and Full alignment controls
	},

  attributes: {
    linkText: {
      type: 'string',
      default: __('Mehr anzeigen'),
    },
    linkTextHide: {
      type: 'string',
      default: __('Weniger anzeigen'),
    },
    iconLabel: {
      type: 'string',
      default: '',
    },
    design: {
      type: 'string',
      default: '',
    },
    type: {
      type: 'string',
      default: 'text',
    },
  },
    // alignment: {
    //         type: 'string',
    //         default: 'none',
    //     },
    // enableWidth: {
    //   type: 'boolean',
    //   default: false
    // },
    // widthOptions: {
    //   type: 'object',
    //   default: widthOptionsDefault,
    // },
    // offsetOptions: {
    //   type: 'object',
    //   default: {
    //     small: 0,
    //     medium: 0,
    //     large: 0,
    //   }
    // },
    // enableOffset: {
    //   type: 'boolean',
    //   default: false
    // },
    // enableBG: {
    //   type: 'boolean',
    //   default: false
    // },
    // design: {
    //         type: 'string',
    //         default: 'none',
    //     },
    // designPaddingTop: {
    //         type: 'string',
    //         default: 'none',
    //     },
    // customBackgroundColor: {
    //         type: 'string',
    //         default: 'none',
    //     },
    // enablePaddingTop: {
    //   type: 'boolean',
    //   default: false
    // },
    // enableMarginBottom: {
    //   type: 'boolean',
    //   default: false
    // },
    // enableContentCell: {
    //   type: 'boolean',
    //   default: true,
    // }

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
    const {
        attributes: {
          alignment,
          linkText,
          iconLabel,
          design,
          type,
        },
        className,
      } = props;

      let labelClass = '';
      if(props.attributes.iconLabel != '') {
        labelClass = 'has-label';
      }

		// Creates a <p class='wp-block-cgb-block-t3-showhide-block'></p>.
		return (
			<div className={ classnames(
        props.className,
        props.attributes.type,
        props.attributes.design,
      ) }>
        <InspectorControls>
          { props.attributes.type == 'text' &&
  				<PanelBody
  					title={ __( 'Link' ) }
  					initialOpen={ false }
  				>
          <TextControl
            label={ __( 'Link-Text (geschlossen)' ) }
            help={ __( 'Geben Sie einen optionalen Linktext ein.' ) }
            value={ props.attributes.linkText || '' }
            onChange={ ( nextValue ) => {
                props.setAttributes( {
                    linkText: nextValue,
                } );
            } } />
            <TextControl
              label={ __( 'Link-Text (offen)' ) }
              help={ __( 'Geben Sie einen optionalen Linktext ein.' ) }
              value={ props.attributes.linkTextHide || '' }
              onChange={ ( nextValue ) => {
                  props.setAttributes( {
                      linkTextHide: nextValue,
                  } );
              } } />
          </PanelBody>
          }
          <PanelBody
  					title={ __( 'Design' ) }
  					initialOpen={ false }
  				>
            <SelectControl
              label="Link-Typ"
              value={ props.attributes.type }
              options={ [
                { label: 'Text', value: 'text' },
                { label: 'Icon', value: 'icon' },
              ] }
              onChange={ ( type ) => { props.setAttributes( { type } ) } }
            />
  					<SelectControl
  						label="Container-Design"
  						value={ props.attributes.design }
  						options={ [
  							{ label: 'Standard', value: '' },
  							{ label: 'Inhaltsbox Primärfarbe', value: 'bg-primary' },
  							{ label: 'Inhaltsbox Sekundärfarbe', value: 'bg-secondary' },
                { label: 'Inhaltsbox Grau', value: 'bg-gray' },
  						] }
  						onChange={ ( design ) => { props.setAttributes( { design } ) } }
  					/>
            { props.attributes.type == 'icon' &&
              <TextControl
                label={ __( 'Icon-Label-Text' ) }
                help={ __( 'optional' ) }
                value={ props.attributes.iconLabel || '' }
                onChange={ ( nextValue ) => {
                    props.setAttributes( {
                        iconLabel: nextValue,
                    } );
                } } />
            }
  				</PanelBody>
        </InspectorControls>
				<div className={ classnames(
          'b-showhide__ctrls',
          labelClass,
        ) }>
        { props.attributes.iconLabel != '' &&
          <span className="b-showhide__label">
            { props.attributes.iconLabel }
          </span>
        }
        { props.attributes.type == 'text'
        ?
          <a href="#" className="b-showhide__link" data-open={ props.attributes.linkText } data-close={ props.attributes.linkTextHide }>{ props.attributes.linkText }</a>
          :
          <a href="#" className="b-showhide__link icon"><span class="icon-plus"></span></a>
        }
        </div>
        <div className={ classnames(
          'b-showhide__body',
        ) }>
          <InnerBlocks/>
        </div>
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
    /** If container has offset design and needs padding top like section */
		let design = '';
		if(props.attributes.design != '') {
			design = props.attributes.design;
		}

    let type = 'text';
    let centerClass = '';
		if(props.attributes.type != 'text') {
			type = props.attributes.type;
      centerClass = 'text-center';
		}

    let labelClass = '';
    if(props.attributes.iconLabel != '') {
      labelClass = 'has-label';
    }

		return (
      <div className={ classnames(
        props.className,
        design,
        props.attributes.type,
        'h-mobp',
      ) }>
				<div className={ classnames(
          'b-showhide__ctrls',
          centerClass,
          props.attributes.design,
          labelClass,
        ) }>
          { props.attributes.iconLabel != '' &&
            <span className="b-showhide__label">
              { props.attributes.iconLabel }
            </span>
          }
          { props.attributes.type == 'text'
  				?
            <a href="#" className="b-showhide__link" data-open={ props.attributes.linkText } data-close={ props.attributes.linkTextHide }>{ props.attributes.linkText }</a>
            :
            <a href="#" className="b-showhide__link icon"><span class="icon-plus"></span></a>
          }
        </div>
        <div className={ classnames(
          'b-showhide__body',
        ) }>
          <InnerBlocks.Content />
        </div>
			</div>
		);
	},
} );
