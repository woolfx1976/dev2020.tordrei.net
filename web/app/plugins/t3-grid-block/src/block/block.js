/**
 * BLOCK: t3-grid-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import classnames from 'classnames';
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	AlignmentToolbar,
    BlockControls,
	RichText,
	InnerBlocks,
	InspectorControls,
	PanelColorSettings,
} = wp.blockEditor;
const {
	PanelBody,
	ToggleControl,
	SelectControl,
	RangeControl,
	ResponsiveWrapper,
	ToolbarButton,
} = wp.components;
const {
	registerFormatType,
	toggleFormat
} = wp.richText;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
 const widthOptionsDefault = {
	 small: 12,
	 medium: 0,
	 large: 0,
 };

registerBlockType( 'cgb/block-t3-grid-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Grid-Container' ), // Block title.
	icon: 'align-none', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 't3layout', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Grid-Container' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	supports: {
		align: [ 'full' ], // Support Wide and Full alignment controls
	},
	attributes: {
		alignment: {
            type: 'string',
            default: 'none',
        },
		enableWidth: {
			type: 'boolean',
			default: false
		},
		widthOptions: {
			type: 'object',
			default: widthOptionsDefault,
		},
		offsetOptions: {
			type: 'object',
			default: {
				small: 0,
				medium: 0,
				large: 0,
			}
		},
		enableOffset: {
			type: 'boolean',
			default: false
		},
		enableBG: {
			type: 'boolean',
			default: false
		},
		design: {
            type: 'string',
            default: 'none',
        },
		designPaddingTop: {
            type: 'string',
            default: 'none',
        },
		customBackgroundColor: {
            type: 'string',
            default: 'none',
        },
		enablePaddingTop: {
			type: 'boolean',
			default: false
		},
		enableMarginBottom: {
			type: 'boolean',
			default: false
		},
		enableContentCell: {
			type: 'boolean',
			default: true,
		}
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const {
            attributes: {
                alignment,
				enableWidth,
				enableOffset,
				enableBG,
				design,
				widthOptions,
				offsetOptions,
				customBackgroundColor,
				enablePaddingTop,
				enableMarginBottom,
				designPaddingTop,
				enableContentCell
            },
            className,
        } = props;

		const onChangeAlignment = ( newAlignment ) => {
            props.setAttributes( { alignment: newAlignment === undefined ? 'none' : newAlignment } );
        };

		/** Container width */
		let widthSmall = '';
		if(props.attributes.enableWidth === true && props.attributes.widthOptions.small > 0) {
			widthSmall = 'small-' + props.attributes.widthOptions.small;
		}
		else {
			widthSmall = widthOptionsDefault.small;
		}
		let widthMedium = '';
		if(props.attributes.enableWidth === true && props.attributes.widthOptions.medium > 0) {
			widthMedium = 'medium-' + props.attributes.widthOptions.medium;
		}
		else {
			widthSmall = widthOptionsDefault.medium;
		}
		let widthLarge = '';
		if(props.attributes.enableWidth === true && props.attributes.widthOptions.large > 0) {
			widthLarge = 'large-' + props.attributes.widthOptions.large;
		}
		else {
			widthLarge = widthOptionsDefault.medium;
		}
		console.log('MediumWidth:' + widthMedium);

		/** Container offset */
		let offsetSmall = '';
		if(props.attributes.enableOffset === true && props.attributes.offsetOptions.small > 0) {
			offsetSmall = 'small-offset-' + props.attributes.offsetOptions.small;
		}
		let offsetMedium = '';
		if(props.attributes.enableOffset === true && props.attributes.offsetOptions.medium > 0) {
			offsetMedium = 'medium-offset-' + props.attributes.offsetOptions.medium;
		}
		let offsetLarge = '';
		if(props.attributes.enableOffset === true && props.attributes.offsetOptions.large > 0) {
			offsetLarge = 'large-offset-' + props.attributes.offsetOptions.large;
		}

		/** Padding and margin for container */
		let contPaddingTop = '';
		if(props.attributes.enablePaddingTop === true) {
			contPaddingTop = 'b-has-pt';
		}
		let lastChildMarginBottom = '';
		if(props.attributes.enableMarginBottom === true) {
			lastChildMarginBottom = 'b-has-lsmb';
		}

		/** If container has offset design and needs padding top like section */
		let hasContainerPadding = '';
		if(props.attributes.designPaddingTop != '') {
			hasContainerPadding = 'has-padding ' + props.attributes.designPaddingTop;
		}

		return (
			<div
				className={ classnames(
					className,
				) }
				style={ { backgroundColor: props.attributes.customBackgroundColor} }
				>
			<InspectorControls>
				<PanelBody
					title={ __( 'Container' ) }
					initialOpen={ false }
				>
					<ToggleControl
						label={ __( 'Content-Spalte' ) }
						checked={ props.attributes.enableContentCell }
						onChange={ ( newEnableContentCell ) => {
							props.setAttributes( {
								enableContentCell: newEnableContentCell,
							} );
						} }
					/>
					{ !! props.attributes.enableContentCell &&<ToggleControl
						label={ __( 'Breite individuell' ) }
						checked={ !! props.attributes.enableWidth }
						onChange={ ( newEnableWidth ) => {
							props.setAttributes( {
								enableWidth: newEnableWidth,
							} );
						} }
					/>
					}
					{ !! props.attributes.enableWidth && !! props.attributes.enableContentCell &&<RangeControl
						label={ __( 'small' ) }
						value={ props.attributes.widthOptions.small }
						onChange={ ( newWidthSmall ) => {
							props.setAttributes( {
								widthOptions: {
									...props.attributes.widthOptions,
									small: newWidthSmall,
								},
							} );
						} }
						min={ 0 }
						max={ 12 }
						step={ 1 }
					/>
					}
					{ !! props.attributes.enableWidth && !! props.attributes.enableContentCell &&<RangeControl
						label={ __( 'medium' ) }
						value={ props.attributes.widthOptions.medium }
						onChange={ ( newWidthMedium ) => {
							props.setAttributes( {
								widthOptions: {
									...props.attributes.widthOptions,
									medium: newWidthMedium,
								},
							} );
						} }
						min={ 0 }
						max={ 12 }
						step={ 1 }
					/>
					}
					{ !! props.attributes.enableWidth && !! props.attributes.enableContentCell &&<RangeControl
						label={ __( 'large' ) }
						value={ props.attributes.widthOptions.large }
						onChange={ ( newWidthLarge ) => {
							props.setAttributes( {
								widthOptions: {
									...props.attributes.widthOptions,
									large: newWidthLarge,
								},
							} );
						} }
						min={ 0 }
						max={ 12 }
						step={ 1 }
					/>
					}
					{ !! props.attributes.enableContentCell &&<ToggleControl
						label={ __( 'Offset' ) }
						checked={ !! props.attributes.enableOffset }
						onChange={ ( newEnableOffset ) => {
							props.setAttributes( {
								enableOffset: newEnableOffset,
							} );
						} }
					/>
					}
					{ !! props.attributes.enableOffset && !! props.attributes.enableContentCell &&<RangeControl
						label={ __( 'small' ) }
						value={ props.attributes.offsetOptions.small }
						onChange={ ( newOffsetSmall ) => {
							props.setAttributes( {
								offsetOptions: {
									...props.attributes.offsetOptions,
									small: newOffsetSmall,
								},
							} );
						} }
						min={ 0 }
						max={ 6 }
						step={ 1 }
					/>
					}
					{ !! props.attributes.enableOffset && !! props.attributes.enableContentCell &&<RangeControl
						label={ __( 'medium' ) }
						value={ props.attributes.offsetOptions.medium }
						onChange={ ( newOffsetMedium ) => {
							props.setAttributes( {
								offsetOptions: {
									...props.attributes.offsetOptions,
									medium: newOffsetMedium,
								},
							} );
						} }
						min={ 0 }
						max={ 6 }
						step={ 1 }
					/>
					}
					{ !! props.attributes.enableOffset && !! props.attributes.enableContentCell &&<RangeControl
						label={ __( 'large' ) }
						value={ props.attributes.offsetOptions.large }
						onChange={ ( newOffsetLarge ) => {
							props.setAttributes( {
								offsetOptions: {
									...props.attributes.offsetOptions,
									large: newOffsetLarge,
								},
							} );
						} }
						min={ 0 }
						max={ 6 }
						step={ 1 }
					/>
					}
					<ToggleControl
						label={ __( 'Hintergrund' ) }
						checked={ !! props.attributes.enableBG }
						onChange={ ( newEnableBG ) => {
							props.setAttributes( {
								enableBG: newEnableBG,
							} );
						} }
					/>
				</PanelBody>
				{ !! props.attributes.enableBG &&<PanelColorSettings
					initialOpen={ false }
					title={ __( 'Hintergrundfarbe' ) }
					colorSettings={[
						{
							label: __( 'Hintergrundfarbe' ),
							value: customBackgroundColor,
							onChange: ( newBgColor, ...whatelse ) => {
								//setBackgroundColor( newBgColor );
								props.setAttributes(
									{
										customBackgroundColor: newBgColor
									}
								)
							}
						},
					]}
				/>
				}
				<PanelBody
					title={ __( 'Abstände' ) }
					initialOpen={ false }
				>
					<ToggleControl
						label={ __( 'Abstand innen oben' ) }
						checked={ !! props.attributes.enablePaddingTop }
						onChange={ ( newEnablePaddingTop ) => {
							props.setAttributes( {
								enablePaddingTop: newEnablePaddingTop,
							} );
						} }
					/>
					<ToggleControl
						label={ __( 'Abstand unten (letztes Element)' ) }
						checked={ !! props.attributes.enableMarginBottom }
						onChange={ ( newEnableMarginBottom ) => {
							props.setAttributes( {
								enableMarginBottom: newEnableMarginBottom,
							} );
						} }
					/>
				</PanelBody>
				<PanelBody
					title={ __( 'Design' ) }
					initialOpen={ false }
				>
					<SelectControl
						label="Container-Design"
						value={ props.attributes.design }
						options={ [
							{ label: 'Standard', value: '' },
							{ label: 'Offset (unten)', value: 'b-has-offset-b' },
							{ label: 'Versatz (Headline)', value: 'b-has-offset-h' },
						] }
						onChange={ ( design ) => { props.setAttributes( { design } ) } }
					/>
					{ props.attributes.design == 'b-has-offset-b' && <SelectControl
						label="Abstand oben"
						value={ props.attributes.designPaddingTop }
						options={ [
							{ label: 'Standard', value: '' },
							{ label: 'ohne', value: 'b-has-pt-none' },
							{ label: 'klein', value: 'b-has-pt-small' },
							{ label: 'groß', value: 'b-has-pt-large' },
						] }
						onChange={ ( designPaddingTop ) => { props.setAttributes( { designPaddingTop } ) } }
					/>
					}
				</PanelBody>


			</InspectorControls>
			{ props.attributes.enableBG
				? <div
					className={ classnames (
						hasContainerPadding,
						'grid-container-wrapper',
					) }
					>
						<div className="grid-container">
							<div className="grid-x">
							   <div className={ classnames(
								   'cell',
								   widthSmall,
								   widthMedium,
								   widthLarge,
								   offsetSmall,
								   offsetMedium,
								   offsetLarge,
								   contPaddingTop,
								   lastChildMarginBottom,
							   ) }
	   							>
								   <InnerBlocks/>
							   </div>
						   </div>
						</div>
					</div>
				:
				<div className="grid-container">
					<div className="grid-x">
					   <div className={ classnames(
						   'cell',
						   widthSmall,
						   widthMedium,
						   widthLarge,
						   offsetSmall,
						   offsetMedium,
						   offsetLarge,
						   contPaddingTop,
						   lastChildMarginBottom,
					   ) }>
						   <InnerBlocks/>
					   </div>
				   	</div>
				</div>
			}
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		/** Container width */
		let widthSmall = '';
		if(props.attributes.enableWidth === true && props.attributes.widthOptions.small > 0) {
			widthSmall = 'small-' + props.attributes.widthOptions.small;
		}
		else {
			widthSmall = 'small-12';
		}
		let widthMedium = '';
		if(props.attributes.enableWidth === true && props.attributes.widthOptions.medium > 0) {
			widthMedium = 'medium-' + props.attributes.widthOptions.medium;
		}
		let widthLarge = '';
		if(props.attributes.enableWidth === true && props.attributes.widthOptions.large > 0) {
			widthLarge = 'large-' + props.attributes.widthOptions.large;
		}

		/** Container offset */
		let offsetSmall = '';
		if(props.attributes.enableOffset === true && props.attributes.offsetOptions.small > 0) {
			offsetSmall = 'small-offset-' + props.attributes.offsetOptions.small;
		}
		let offsetMedium = '';
		if(props.attributes.enableOffset === true && props.attributes.offsetOptions.medium > 0) {
			offsetMedium = 'medium-offset-' + props.attributes.offsetOptions.medium;
		}
		let offsetLarge = '';
		if(props.attributes.enableOffset === true && props.attributes.offsetOptions.large > 0) {
			offsetLarge = 'large-offset-' + props.attributes.offsetOptions.large;
		}

		/** Padding and margin for container */
		let contPaddingTop = '';
		if(props.attributes.enablePaddingTop === true) {
			contPaddingTop = 'b-has-pt';
		}
		let lastChildMarginBottom = '';
		if(props.attributes.enableMarginBottom === true) {
			lastChildMarginBottom = 'b-has-lsmb';
		}

		/** If container has offset design and needs padding top like section */
		let hasContainerPadding = '';
		if(props.attributes.designPaddingTop != '') {
			hasContainerPadding = 'has-padding ' + props.attributes.designPaddingTop;
		}

		/** Padding top and margin bottom for non content cell block */
		let outerClasses = '';
		if(props.attributes.enableContentCell === false) {
			outerClasses = contPaddingTop + ' ' + lastChildMarginBottom;
		}

		return (
			<div
				className={ classnames(
					props.className,
					props.attributes.design,
					outerClasses,
				) }
				style={ { backgroundColor: props.attributes.customBackgroundColor} }
				>
			{ !! props.attributes.enableBG
				? <div
					className={ classnames (
						hasContainerPadding,
						'grid-container-wrapper',
					) }
					>
					<div className="grid-container">
						{ !! props.attributes.enableContentCell
							? <div className="grid-x">
								<div className={ classnames(
								   'cell',
								   'small-' + props.attributes.widthOptions.small,
								   'medium-' + props.attributes.widthOptions.medium,
								   'large-' + props.attributes.widthOptions.large,
								   offsetSmall,
								   offsetMedium,
								   offsetLarge,
								   contPaddingTop,
								   lastChildMarginBottom,
							   ) }>
								   <InnerBlocks.Content/>
								</div>
							</div>
							: <InnerBlocks.Content/>
						}
					</div>
				</div>
				: <div className="grid-container">
					{ !! props.attributes.enableContentCell
						? <div className="grid-x">
							<div className={ classnames(
							   'cell',
							   'small-' + props.attributes.widthOptions.small,
							   'medium-' + props.attributes.widthOptions.medium,
							   'large-' + props.attributes.widthOptions.large,
							   offsetSmall,
							   offsetMedium,
							   offsetLarge,
							   contPaddingTop,
							   lastChildMarginBottom,
						   ) }>
							   <InnerBlocks.Content/>
							</div>
						</div>
						: <InnerBlocks.Content/>
					}
				</div>
			}
			</div>
		);
	},
} );
