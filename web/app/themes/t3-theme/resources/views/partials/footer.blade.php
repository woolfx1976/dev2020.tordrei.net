<footer>
  <div class="grid-container">
    <div class="grid-x grid-margin-x h-mobp">
      <div class="cell small-12 medium-6 large-3">
        @php dynamic_sidebar('sidebar-footer-1') @endphp
      </div>
      <div class="cell small-12 medium-6 large-3">
        @php dynamic_sidebar('sidebar-footer-2') @endphp
      </div>
      <div class="cell small-12 medium-6 large-3">
        @php dynamic_sidebar('sidebar-footer-3') @endphp
      </div>
      <div class="cell small-12 medium-6 large-3">
        @php dynamic_sidebar('sidebar-footer-4') @endphp
        @php if(isset($_GET['testtracingid'])): @endphp
        <a href="#" id="leebCookieDelBtn">leebWebuserCookie löschen</a>
        @php endif; @endphp
      </div>
    </div>
  </div>
</footer>
<a href="#top" class="back-to-top icon-arrow-up smooth-scroll" data-smooth-scroll></a>
