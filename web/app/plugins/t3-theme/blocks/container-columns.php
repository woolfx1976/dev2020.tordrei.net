<?php

add_filter('sage/blocks/container-columns/data', function (array $block) {
	$layout = get_field('col_layout');
	$block['col_css'] = get_field('col_css');
	$block['column'] = array();
	$contCol1 = '';
	$contCol2 = '';
	$contCol3 = '';

	if(get_field('col1_content')) {
		$contCol1 = get_field('col1_content');
	}
	if(get_field('col2_content')) {
		$contCol2 = get_field('col2_content');
	}
	if(get_field('col3_content')) {
		$contCol3 = get_field('col3_content');
	}

	if($layout == 'right' || $layout == 'left'):
		$offset = get_field('col1_offset');
		$block['column'][0]['show'] = true;
		$block['column'][1]['show'] = true;
		$block['column'][0]['content'] = $contCol1;
		$block['column'][1]['content'] = $contCol2;
		$offset == 1 ? $block['column'][0]['classes'] = 'small-12 medium-6 large-7 medium-offset-1' : $block['column'][0]['classes'] = 'small-12 medium-6 large-8';
		$offset == 1 ? $block['column'][1]['classes'] = 'small-12 medium-4 large-3 is-sidebar' : $block['column'][1]['classes'] = 'small-12 medium-6 large-4 is-sidebar';
	elseif($layout == 'left'):
		$offset == 1 ? $block['column'][0]['classes'] = 'small-12 medium-4 large-3 medium-offset-1 is-sidebar' : $block['column'][0]['classes'] = 'small-12 medium-6 large-4 is-sidebar';
		$offset == 1 ? $block['column'][1]['classes'] = 'small-12 medium-6 large-7' : $block['column'][1]['classes'] = 'small-12 medium-6 large-8';
	elseif($layout == '2cols'):
		$block['column'][0]['show'] = true;
		$block['column'][1]['show'] = true;
		$block['column'][0]['content'] = $contCol1;
		$block['column'][1]['content'] = $contCol2;
		$block['column'][0]['classes'] = 'small-12 medium-6';
		$block['column'][1]['classes'] = 'small-12 medium-6';
	elseif($layout == '3cols'):
		$block['column'][0]['show'] = true;
		$block['column'][1]['show'] = true;
		$block['column'][2]['show'] = true;
		$block['column'][0]['content'] = $contCol1;
		$block['column'][1]['content'] = $contCol2;
		$block['column'][2]['content'] = $contCol3;
		$block['column'][0]['classes'] = 'small-12 medium-6 large-4';
		$block['column'][1]['classes'] = 'small-12 medium-6 large-4';
		$block['column'][2]['classes'] = 'small-12 medium-6 large-4';
	endif;

    return $block;
});
