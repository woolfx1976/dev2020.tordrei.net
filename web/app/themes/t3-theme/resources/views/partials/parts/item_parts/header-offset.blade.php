{{-- @if(!is_admin()) --}}
  <img src="@homeurl{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" width="{!! $width !!}" height="{!! $height !!}" />
{{-- @else
  <img src="{!! $imageSmall !!}" class="lazyload b-cards__img" />
@endif --}}
@if($hasOverlay)
<div class="item-overlay">
  <p>{!! $content !!}</p>
</div>
@endif
