/**
 * BLOCK: t3/layout-2-column
 *
 * Two column layout block.
 */

import './editor.scss';

const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n;
const { InnerBlocks } = wp.editor;

const {
	InspectorControls,
} = wp.blockEditor;
const {
	PanelBody,
	TextControl,
} = wp.components;

registerBlockType( 't3/layout-2-column', {

	title: 'Layout 2-spaltig',
	icon: 'layout',
	category: 't3layout',

	keywords: [
		'2', 'two', '2 column', 'column', 'layout', 't3',
	],

	supports: {
		align: [ 'full' ],
	},

  attributes: {
    elementId: {
            type: 'string',
            default: '',
        },
  },

	edit: ( props ) => {
		return (
			<div className={ props.className }>
        <InspectorControls>
          <PanelBody
            title={ __( 'Element-ID' ) }
            initialOpen={ false }
          >
            <TextControl
                            label={ __( 'Element-ID (optional)' ) }
                            help={ __( '' ) }
                            value={ props.attributes.elementId || '' }
                            onChange={ ( nextValue ) => {
                                props.setAttributes( {
                                    elementId: nextValue,
                                } );
                            } } />
          </PanelBody>
        </InspectorControls>
				<InnerBlocks
					allowedBlocks={ [ 't3/column-6' ] }
					templateLock="insert"
					template={
						[
							[ 't3/column-6' ],
							[ 't3/column-6' ],
						]
					}
				/>
			</div>
		);
	},

	save: ( props ) => {
    let boolId = false;
		if(props.attributes.elementId != '') { boolId = true }

		return (
			<div
        {...(boolId ? {id:props.attributes.elementId} : {})}
        className={ 'grid-x grid-margin-x grid-margin-y' }>
				<InnerBlocks.Content />
			</div>
		);
	},
} );
