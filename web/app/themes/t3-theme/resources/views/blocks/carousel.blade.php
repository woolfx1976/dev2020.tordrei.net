{{--
  Name: carousel
  Title: Carousel
  Description: columns, CPTs
  Category: t3layout
  Icon: slides
  Keywords: columns, sidebar-container
  Mode: preview
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

<div class="b-carousel {{$block['className']}} {{$block['carouselDesign']}} slider" data-{{$block['id']}}>
  <div class="slides">
    @foreach ($block['carouselItems'] as $value)
      {!! $value !!}
    @endforeach
  </div>

</div>
