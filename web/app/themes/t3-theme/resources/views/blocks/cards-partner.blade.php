{{--
  Title: Vertriebspartner-Cards
  Description: cards, CPTs
  Category: t3layout
  Icon: grid-view
  Keywords: card layout
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

<ul data-{{ $block['id'] }} class="c-list b-cards b-cards--{{ $block['cardDesign'] }} grid-x grid-margin-x grid-margin-y {{$block['classes']}}">
  @if(isset($block['card_items']) && count($block['card_items']) > 0)
    @foreach ($block['card_items'] as $value)
      {!! $value !!}
    @endforeach
  @endif
</ul>
