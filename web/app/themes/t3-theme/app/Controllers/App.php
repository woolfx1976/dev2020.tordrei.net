<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function homeurl()
    {
        //return 'https://mk0t3themefok4uwj9r3.kinstacdn.com';
        if(!is_admin()) {
          return get_home_url();
        }
        //return ''; // testing optimize on staging
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            // return sprintf(__('Search Results for %s', 'sage'), get_search_query());
            return sprintf(__('<span class="light">Suchergebnisse für:</span><br/>%s', 't3-theme'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function breadcrumbNav() {
        $breadcrumb = '<p class="breadcrumb">';
        $delimiter = ' / ';
        $home = __('Startseite', 'leeb');
        $before = '<span class="current-page">';
        $after = '</span>';

        $productArr = array(
          // Developement
          //'de' => 395,
          // Staging / Production
          'de' => 420,
          'at' => 17273,
          'ch' => 17274,
          'it' => 17272,
          'sl' => 17275,
        );

        $parentArr = array(
          // Development
          // 'de' => array(
          //   'ba' => array(
          //     'ids' => array(109112,544),
          //     'id' => '109504',
          //   ),
          // ),
          // Staging / Production
          'de' => array(
            'balko' => array(
              // 'ids' => array(411,67947,3962,5645,24904), // last id = Staging
              'ids' => array(411,3962,5645,24904), // last id = Staging
              'id' => '19947',
            ),
            'zaeun' => array(
              'ids' => array(4438,5334,4791),
              'id' => '19964',
            ),
            'ueber' => array(
              'ids' => array(4854,4876,4926,4944,4956),
              'id' => '19973',
            ),
            'sonst' => array(
              'ids' => array(5633,9999999999),
              'id' => '19983',
            ),
          ),
          'at' => array(
            'balko' => array(
              // 'ids' => array(56567,67984,56570,23273),
              'ids' => array(56567,56570,23273),
              'id' => '19999',
            ),
            'zaeun' => array(
              'ids' => array(23271,23275,23405),
              'id' => '20012',
            ),
            'ueber' => array(
              'ids' => array(23415,23413,23409,23407,23411),
              'id' => '20008',
            ),
            'sonst' => array(
              'ids' => array(18358),
              'id' => '20004',
            ),
          ),
          'ch' => array(
            'balko' => array(
              // 'ids' => array(56568,67985,56571,23274),
              'ids' => array(56568,56571,23274),
              'id' => '20000',
            ),
            'zaeun' => array(
              'ids' => array(23272,23276,23406),
              'id' => '20013',
            ),
            'ueber' => array(
              'ids' => array(23427,23455,23417,23462,23463),
              'id' => '20009',
            ),
            'sonst' => array(
              'ids' => array(18359),
              'id' => '20005',
            ),
          ),
          'it' => array(
            'balko' => array(
              'ids' => array(15538,18555,18337),
              'id' => '19998',
            ),
            'zaeun' => array(
              'ids' => array(18320,19047,17865),
              'id' => '20011',
            ),
            'ueber' => array(
              'ids' => array(17885,17871,20116,17775,21268),
              'id' => '20007',
            ),
            'sonst' => array(
              'ids' => array(18357),
              'id' => '20003',
            ),
          ),
          'sl' => array(
            'balko' => array(
              'ids' => array(15540,18558,18340),
              'id' => '20001',
            ),
            'zaeun' => array(
              'ids' => array(18323,19050,17868),
              'id' => '20014',
            ),
            'ueber' => array(
              'ids' => array(17888,17874,20196,17898,21269),
              'id' => '20010',
            ),
            'sonst' => array(
              'ids' => array(18360),
              'id' => '20006',
            ),
          ),
        );

        if ( !is_home() && !is_front_page() || is_paged() ) {

            global $post;
            $homeLink = get_bloginfo('url');
            $breadcrumb .= '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

            if ( is_category()) {
                global $wp_query;
                $cat_obj = $wp_query->get_queried_object();
                $thisCat = $cat_obj->term_id;
                $thisCat = get_category($thisCat);
                $parentCat = get_category($thisCat->parent);
                $breadcrumb .= '<a href="/blog/">' . __('Blog', 't3-theme') . '</a> ' . $delimiter . ' '; // Get blog page for breadcrumb
                if ($thisCat->parent != 0) $breadcrumb .= (get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
                $breadcrumb .= $before . single_cat_title('', false) . $after;

            } elseif ( is_day() ) {
                $breadcrumb .= '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
                $breadcrumb .= '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
                $breadcrumb .= $before . get_the_time('d') . $after;

            } elseif ( is_month() ) {
                $breadcrumb .= '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
                $breadcrumb .= $before . get_the_time('F') . $after;

            } elseif ( is_year() ) {
                $breadcrumb .= $before . get_the_time('Y') . $after;

            } elseif ( is_single() && !is_attachment() ) {
                if ( get_post_type() != 'post' && !is_singular( 'product' ) && !is_singular( 'jobs' ) ) {
                    $post_type = get_post_type_object(get_post_type());
                    $slug = $post_type->rewrite;
                    $breadcrumb .= '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
                    $breadcrumb .= $before . get_the_title() . $after;
                }
                elseif( is_singular( 'jobs' ) ) {
                    $permalink = explode('/', get_the_permalink());
                    $path = '/';
                    foreach($permalink as $k => $v) {
                        if($k > 2 && $k < 5) {
                            $path .= $v . '/';
                            $page = get_page_by_path( $path );
                            if(!$page) {
                                $breadcrumb .= $before . get_the_title() . $after;
                            }
                            else {
                                $breadcrumb .= '<a href="' . $path . '">' . $page->post_title . '</a> ' . $delimiter . ' ';
                            }
                        }
                        if($k == 5 && !empty($v)) { // = job layer
                            $breadcrumb .= $before . get_the_title() . $after;
                        }
                    }
                }
                elseif ( is_singular( 'product' ) ) {
                    $permalink = explode('/', get_the_permalink());
                    $count = count($permalink);
                    $path = '/';
                    if(isset($_GET['breadcrumb'])):
                      var_dump($permalink);
                    endif;

                    foreach($permalink as $k => $v) {
                      $prodCatName = '';
                      $prodCatPath = '';
                      $productCatPath = '';
                      $currPath = '';

                        if($k == 1) {
                            $path .=  $v;
                        }
                        if($k > 2 && $k < 5) {
                            $currPath .= $v . '/';
                            if(isset($_GET['breadcrumb'])):
                              echo 'count: ' . $count . ' - key: ' . $k .'->' . $currPath . '<- ' . url_to_postid( $currPath ) . '</br/>';
                              //var_dump($parentArr[ICL_LANGUAGE_CODE]);
                            endif;
                            if( $k == 4 && $count == 7 || $k == 3 && $count == 6 ):
                              if( ICL_LANGUAGE_CODE ):
                                foreach($parentArr[ICL_LANGUAGE_CODE] as $key => $val) {
                                  $count == 7 ? $pid = url_to_postid( $currPath ) : $pid = get_the_ID();
                                  if(isset($_GET['breadcrumb'])):
                                  echo 'currentPath: ' . $currPath . ' - '  . url_to_postid( $currPath ) . '<br/>';
                                  echo 'pid: ' . $pid . '</br/>';
                                  print_r($val);
                                  endif;
                                  if(in_array($pid, $val['ids'])) {
                                    $productCatPath = get_the_permalink($val['id']);
                                    $prodCatName = get_the_title($val['id']);
                                    $prodCatPath = '/' . $productCatPath . '/';
                                  }
                                }
                              endif;
                            endif;

                            $path .= $v . '/';
                            $page = get_page_by_path( $path );

                            if(!$page) {
                              $breadcrumb .= $before . get_the_title() . $after;
                            }
                            else {
                              if(!empty($prodCatName)) {
                                if($count == 6) {
                                  $breadcrumb .= '<a href="' . $path . '">' . $page->post_title . '</a> ' . $delimiter . ' <a href="' . $productCatPath . '">' . $prodCatName . '</a> ' . $delimiter . ' ';
                                }
                                else {
                                  $breadcrumb .= '<a href="' . $productCatPath . '">' . $prodCatName . '</a> ' . $delimiter . ' <a href="' . $path . '">' . $page->post_title . '</a> ' . $delimiter . ' ';
                                }
                              }
                              else {
                                $breadcrumb .= '<a href="' . $path . '">' . $page->post_title . '</a> ' . $delimiter . ' ';
                              }
                            }

                        }
                        if($k == 5 && !empty($v)) { // = product layer
                            $breadcrumb .= $before . get_the_title() . $after;
                        }
                    }
                }
                elseif ( is_singular( 'post' ) ) {
                    $permalink = explode('/', get_the_permalink());
                    if(isset($_GET['work'])):
                      print_r($permalink);
                    endif;
                    $path = '/';

                    foreach($permalink as $k => $v) {
                        if($k == 1) {
                            $path .=  $v;
                        }
                        if($k > 2 && $k < 5) {
                            $path .= $v . '/';
                            $page = get_page_by_path( $path );
                            if(!$page) {
                                $breadcrumb .= $before . get_the_title() . $after;
                            }
                            else {
                                $breadcrumb .= '<a href="' . $path . '">' . $page->post_title . '</a> ' . $delimiter . ' ';
                            }

                        }
                        // if($k == 5 && !empty($v)) { // = product layer
                        //     $breadcrumb .= get_the_title();
                        // }
                    }
                }
                else {
                    $cat = get_the_category(); $cat = $cat[0];
                    $breadcrumb .= get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                    $breadcrumb .= $before . get_the_title() . $after;
                }

            } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() && !is_search() ) {
                $post_type = get_post_type_object(get_post_type());
                $breadcrumb .= $before . $post_type->labels->singular_name . $after;


            } elseif ( is_attachment() ) {
                $parent = get_post($post->post_parent);
                $cat = get_the_category($parent->ID); $cat = $cat[0];
                $breadcrumb .= get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                $breadcrumb .= '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
                $breadcrumb .= $before . get_the_title() . $after;

            } elseif ( is_page() && !$post->post_parent ) {
                $breadcrumb .= $before . get_the_title() . $after;

            } elseif ( is_page() && $post->post_parent ) {
              //echo 'Test';
                $parent_id = $post->post_parent;
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_post($parent_id);
                    $parentCatElement = '';
                    if( ICL_LANGUAGE_CODE ) {
                      if($productArr[ICL_LANGUAGE_CODE] == $parent_id) {
                        if(isset($_GET['breadcrumb'])):
                          var_dump($parentArr[ICL_LANGUAGE_CODE]);
                        endif;
                        foreach($parentArr[ICL_LANGUAGE_CODE] as $key => $val) {
                          $pid = get_the_ID();
                          if(isset($_GET['breadcrumb'])):
                            echo $pid;
                          endif;
                          if(in_array($pid, $val['ids'])) {
                            $productCatPath = get_the_permalink($val['id']);
                            $prodCatName = get_the_title($val['id']);
                            $prodCatPath = get_permalink($val['id']); //'/' . $productCatPath . $val['url'] . '/';
                            $breadcrumbs[] = '<a href="' . $prodCatPath . '">' . $prodCatName . '</a>';
                          }
                        }
                      }
                    }

                    $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                foreach ($breadcrumbs as $crumb) $breadcrumb .= $crumb . ' ' . $delimiter . ' ';
                $breadcrumb .= $before . get_the_title() . $after;

            } elseif ( is_search() ) {
                $breadcrumb .= $before . 'Ergebnisse für Ihre Suche nach "' . get_search_query() . '"' . $after;

            } elseif ( is_tag() ) {
                $breadcrumb .= $before . 'Beiträge mit dem Schlagwort "' . single_tag_title('', false) . '"' . $after;

            } elseif ( is_404() ) {
                $breadcrumb .= $before . 'Fehler 404' . $after;
            }

            if ( get_query_var('paged') ) {
                if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) $breadcrumb .= ' (';
                $breadcrumb .= ': ' . __('Seite') . ' ' . get_query_var('paged');
                if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) $breadcrumb .= ')';
            }

            $breadcrumb .= '</nav>';
        }
        $breadcrumb .= '</p>';

        return $breadcrumb;
    }

    public static function getSocialLinks() {
        $html = '<ul class="c-list c-list--h c-list--social">';
        if(!empty(get_field('theme_social_facebook', 'options'))) {
            $html .= '<li><a class="icon-facebook" href="' . get_field('theme_social_facebook', 'options') . '" rel="noopener" target="_blank"></a></li>';
        }
        if(!empty(get_field('theme_social_instagram', 'options'))) {
            $html .= '<li><a class="icon-instagram" href="' . get_field('theme_social_instagram', 'options') . '" rel="noopener" target="_blank"></a></li>';
        }
        if(!empty(get_field('theme_social_youtube', 'options'))) {
            $html .= '<li><a class="icon-youtube" href="' . get_field('theme_social_youtube', 'options') . '" rel="noopener" target="_blank"></a></li>';
        }
        if(!empty(get_field('theme_social_linkedin', 'options'))) {
            $html .= '<li><a class="icon-linkedin" href="' . get_field('theme_social_linkedin', 'options') . '" rel="noopener" target="_blank"></a></li>';
        }
        if(!empty(get_field('theme_social_twitter', 'options'))) {
            $html .= '<li><a class="icon-twitter" href="' . get_field('theme_social_twitter', 'options') . '" rel="noopener" target="_blank"></a></li>';
        }
        //return $html .= '</ul>';
        $html .= '</ul>';
        if(!empty(get_field('theme_footer_logo1', 'options'))) {
          $img = get_field('theme_footer_logo1', 'options');
          $link = get_field('theme_footer_logo1_link', 'options');
          if($link && $img) {
            $html .= '<p class="footer-logos"><a href="' . $link['url'] . '" target="_blank" rel="nofollow"><img src="' . $img['url'] . '" alt="' . $img['alt'] . '" class="footer-logo-1" /></a></p>';
          }
        }
        return $html;
    }

    public static function getCellClasses( $array, $type = '' ) {
        $classes = '';

        if(isset($array) && is_array($array)) {
            if(!empty($type)) {
                $type = '-' . $type;
            }

            foreach( $array as $k => $v ) {
                if($v != '0') {
                    $classes .= ' ' . $k . $type . '-' . $v;
                }
            }
        }

        return $classes;
    }

    public static function getBgImgAlign( $align ) {
        $css = '';
        if($align) {
            if( $align['align_x'] != 'center' || $align['align_y'] != 'center' ) {
                $css = 'background-position: ' . $align['align_x'] . ' ' . $align['align_y'] . ';';
            }
        }
        return $css;
    }

    public static function getBgImgAlignImagify( $align ) {
        $class = '';
        if($align) {
            $class .= 'bgx-' . $align['align_x'];
            $class .= ' bgy-' . $align['align_y'];
        }
        return $class;
    }

    public static function getBgColor( $bgSelector ) {
        $bgColor ='';
        if($bgSelector != 'trans') {
        	$bgColor = ' bg--' . $bgSelector;
        }
        return $bgColor;
    }
    // }

    // public function featuredImageSmall() {
    //     if(has_post_thumbnail()) {
    //         return get_the_post_thumbnail_url(get_the_ID(), 'fp-small');
    //     }
    // }
    //
    // public function featuredImageMedium() {
    //     if(has_post_thumbnail()) {
    //         return get_the_post_thumbnail_url(get_the_ID(), 'fp-medium');
    //     }
    // }
    //
    // public function featuredImageLarge() {
    //     if(has_post_thumbnail()) {
    //         return get_the_post_thumbnail_url(get_the_ID(), 'fp-large');
    //     }
    // }
    //
    // public function featuredImageXlarge() {
    //     if(has_post_thumbnail()) {
    //         return get_the_post_thumbnail_url(get_the_ID(), 'fp-xlarge');
    //     }
    // }

    // public function featuredImageInterchange() {
    //     if(has_post_thumbnail()) {
    //         return 'data-interchange="['. get_the_post_thumbnail_url(get_the_ID(), 'fp-small') .', small], ['. get_the_post_thumbnail_url(get_the_ID(), 'fp-medium') .', medium], ['. get_the_post_thumbnail_url(get_the_ID(), 'fp-large') .', large], ['. get_the_post_thumbnail_url(get_the_ID(), 'fp-xlarge') .', xlarge]"';
    //     }
    // }

    public function postProps() {
        if(get_field('post_props')) {
            return get_field('post_props');
        }
    }

    public function desktopLogo() {
        $logo = get_field('theme_logo', 'options');
        if($logo) {
            return $logo['url'];
        }
    }


    public static function floatingSideNavBtns() {
      defined('ICL_LANGUAGE_CODE') ? $lang = ICL_LANGUAGE_CODE : $lang = 'de';
      $fTel = 'op_hotline_' . $lang;
      $html = '
        <div class="c-fe-side right">
          <a href="tel:' . get_field($fTel, 'options') . '" class="icon-phone"></a>
          <a href="' . esc_url(get_field('op_link_formular', 'options')) . '" class="icon-mail"></a>
        </div>
      ';

      return $html;
    }

    public function pageTopBar() {
        // Get topbar props
        if( have_rows('theme_header_props','options') ):
            while( have_rows('theme_header_props','options') ): the_row();
            $topbar['topbar'] = (object) [
                'layout' =>    get_sub_field('layout'),
                'size_logo' => get_sub_field('size_logo'),
                'size_nav' =>  get_sub_field('size_nav'),
                'alignment' => get_sub_field('alignment'),
                'alignment_text' => get_sub_field('alignment_text'),
            ];
        endwhile;

        return $topbar;
    endif;
    }

    // public function getSiteBanner() {
    //     $data['test'] = 'Test';
    //     // $banner['img'] = get_field('banner_img', 'options');
    //     // $banner['link']= get_field('banner_link', 'options');
    //     return $data;
    // }

    /** Get form ids */
    public static function formIds() {
      $formIds = array(
        'contact' => get_field('op_form_contact_id', 'options'),
        'offert' => get_field('op_form_offert_id', 'options'),
        'catalog' => get_field('op_form_catalog_id', 'options'),
        'job' => get_field('op_form_job_id', 'options'),
      );

      return $formIds;
    }

    /** Get image filename */
    public static function getImgFilename($id) {
      $filename = str_replace('-scaled', '', basename( get_attached_file( $id ) ) );
      $filename = str_replace('-', ' ', $filename );
      return preg_replace("/\.[^.]+$/", "", $filename);
    }

    // Set Image title attribute
    public static function setImgTitleAttr($imgID) {
      if($imgID):
        $html = ' title="' . get_post($imgID)->post_title . '"';
        return $html;
      endif;
    }
}
