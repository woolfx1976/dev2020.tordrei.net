<?php

add_filter('sage/blocks/hero/data', function (array $block) {
	$content = get_field('hero_content');
	$props = get_field('hero_props');
	$img_props = get_field('hero_img_props');
	$image = get_field('hero_img');
	$cellType = get_field('hero_cell_type');
	$cellProps = get_field('hero_grid');
	$offsetProps = get_field('hero_offset');
	$style = '';
	$inlineStyle = '';
	$homeurl = App::homeurl();

	if( $img_props['img_align_x'] != 'center' && $img_props['img_align_y'] != 'center' ) {
		$style .= 'background-position: ' . $img_props['img_align_x'] . ' ' . $img_props['img_align_y'] . ';';
	}
	if( !empty($img_props['bg_color']) ) {
		$inlineStyle .= 'background-color: ' . $img_props['bg_color'] . ';';
	}
	if( $img_props['opacity'] != '0' ) {
		$inlineStyle .= 'opacity: ' . $img_props['opacity'] . ';';
	}

	/** Get cell type */
	if( $cellType ) {
		$sizes = App::getCellClasses( $cellProps );
		$offset = App::getCellClasses( $offsetProps );
		$classes = $sizes.$offset;
	}
	else {
		$classes = ' small-12 medium-6';
	}

	$block['hero-data'] = (object) [
		'imageLarge' => $image['sizes']['fp-large'],
		'interchange' => 'data-bgset="' . $homeurl . $image['sizes']['fp-small'].' [(max-width: 640px)] | ' . $homeurl . $image['sizes']['fp-medium'] . ' [(max-width: 1024px)] | ' . $homeurl . $image['sizes']['fp-large'] . ' [(min-width: 1024px)] | ' . $homeurl . $image['sizes']['fp-xlarge'] . ' [(min-width: 1200px)]" class="lazyload"',
		//'interchange' => 'data-interchange="[' . $image['sizes']['fp-small'] .', small], [' . $image['sizes']['fp-medium'] .', medium], [' . $image['sizes']['fp-large'] .', large], [' . $image['sizes']['fp-xlarge'] .', xlarge]"',
		'content' => $content,
		'design' => $props['design'],
		'style' => 'style="' . $style . '"',
		'textAlignX' => $props['text_align_x'],
		'textAlignY' => $props['text_align_y'],
		'inlineStyle' => $inlineStyle,
		'classes' => $classes,
	];

	return $block;
});
