<?php
// /**
// * Partners custom post type
// */
// add_action('init', function () {
//     $labels = array(
//         'name'                => _x( 'Vertriebspartner', 'Post Type General Name', 't3-theme' ),
//         'singular_name'       => _x( 'Vertriebspartner', 'Post Type Singular Name', 't3-theme' ),
//         'menu_name'           => __( 'Vertriebspartner', 't3-theme' ),
//         'parent_item_colon'   => __( 'Eltern-Vertriebspartner', 't3-theme' ),
//         'all_items'           => __( 'Alle Vertriebspartner', 't3-theme' ),
//         'view_item'           => __( 'Vertriebspartner zeigen', 't3-theme' ),
//         'add_new_item'        => __( 'Neuen Vertriebspartner erstellen', 't3-theme' ),
//         'add_new'             => __( 'Neuer Vertriebspartner', 't3-theme' ),
//         'edit_item'           => __( 'Vertriebspartner bearbeiten', 't3-theme' ),
//         'update_item'         => __( 'Vertriebspartner aktualisieren', 't3-theme' ),
//         'search_items'        => __( 'Vertriebspartner suchen', 't3-theme' ),
//         'not_found'           => __( 'Nicht gefunden', 't3-theme' ),
//         'not_found_in_trash'  => __( 'Not found in Trash', 't3-theme' ),
//     );
//     register_extended_post_type('partner', [
//         'labels' => $labels,
//         'menu_icon' => 'dashicons-star-filled',
//         'show_in_rest' => false,
//         'supports' => array('title','thumbnail','catgegories'),
//         'has_archive' => false,
//         'rewrite' => true
//     ], [
//         'singular' => 'Vertriebspartner',
//         'plural' => 'Vertriebspartner',
//         'slug' => 'partner',
//     ]);
//     /**
//     *   To Activate Custom Post Type Single page
//     *   @see http://en.bainternet.info/2011/custom-post-type-getting-404-on-permalinks
//     */
//     $set = get_option('post_type_rules_flased_authors');
//     if ($set !== true){
//         flush_rewrite_rules(false);
//         update_option('post_type_rules_flased_authors',true);
//     }
//
//
// });
