<?php
/**
* Place ACF JSON in field-groups directory
*/
add_filter('acf/settings/save_json', function ($path) {
    return plugin_dir_path( __FILE__ ) . '/field-groups';
});
add_filter('acf/settings/load_json', function ($paths) {
    unset($paths[0]);
    $paths[] = plugin_dir_path( __FILE__ ) . '/field-groups';
    return $paths;
});
