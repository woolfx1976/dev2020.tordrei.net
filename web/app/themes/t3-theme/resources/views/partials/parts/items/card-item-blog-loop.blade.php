@php
  $thumbnail_id = get_post_thumbnail_id( get_the_ID() );
  $imgUrl = get_the_post_thumbnail_url(get_the_ID(), 'card-small' );
  $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
  $imgTitle = App::setImgTitleAttr($thumbnail_id);
  $excerpt = get_the_excerpt();
  $listProps = get_field('blog_props');
  $url = get_the_permalink();
  isset($listProps['show_date']) ? $showDate = $listProps['show_date'] :  $showDate = false;
@endphp

<li class="b-cards__item cell small-12 medium-6 large-4">
  <div class="b-cards__header">
      <a href="{!! $url !!}" class="hasHover">
        <img src="{!! $imgUrl !!}" alt="{!! $alt !!}" width="640" height="432" class="lazyload b-cards__img"@if(isset($imgTitle)){!! $imgTitle !!}@endif />
      </a>
  </div>
  <div class="b-cards__body">

      <a href="{!! $url !!}" class="b-cards__title no-link-style">
        <h3>{!! the_title() !!}</h3>
      </a>

    @if($showDate)
      <p class="date">{!! get_the_date('d.m.Y') !!}</p>
    @endif
    <p>{!! $excerpt !!}</p>
    <a href="{!! $url !!}" class="more">@php _e('weiterlesen', 't3-theme'); @endphp</a>
  </div>
</li>
