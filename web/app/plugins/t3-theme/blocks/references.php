<?php

add_filter('sage/blocks/references/data', function (array $block) {
	$block['referencesID'] = $block['id'];
	$block['type'] = get_field('ref_type');
	return $block;
});

add_action('wp_ajax_loadmorebutton', 'loadmore_ajax_handler');
add_action('wp_ajax_nopriv_loadmorebutton', 'loadmore_ajax_handler');

function loadmore_ajax_handler(){
	// $homeurl = 'https://mk0t3themefok4uwj9r3.kinstacdn.com';
	$homeurl = ''; // testing optimize on staging
	$homeurl = App::homeurl();
	$type = $_POST['blocktype'];
	$imgSizes = 'width="640" height="432"';

	//echo 'Type: ' . $_POST['blocktype'];
	if ( isset( $_GET[ 'wpml_lang' ] ) ) {
		do_action( 'wpml_switch_language',  $_GET[ 'wpml_lang' ] );
	}

	$params = json_decode( stripslashes( $_POST['query'] ), true );
	$params['paged'] = $_POST['page'] + 1;
	$params['post_status'] = 'publish';
	$params['suppress_filters'] = false;

	if($_POST['blocktype'] == '1') {
		$params['meta_key'] = 'ref_before';
		$params['meta_query'] = array(
			array(
				'key' => 'ref_before',
				'value' => '',
				'compare' => '!=',
			)
		);
	}
	else {
		$params['meta_query'] = array(
			'relation'		=> 'OR',
			array(
				'key'		=> 'ref_txt',
				'value'		=> '',
				'compare'	=> '!='
			),
			array(
				'key'		=> 'ref_txt',
				'value'		=> ' ',
				'compare'	=> '!='
			)
		);
	}

	query_posts( $params );
	global $wp_query;

	if( have_posts() ) :
		while( have_posts() ): the_post();
		$imageUrl = get_the_post_thumbnail_url(get_the_ID(), 'card-small' );
		$thumbnailId = get_post_thumbnail_id( get_the_ID() );
		$alt = get_post_meta($thumbnailId, '_wp_attachment_image_alt', true);
		if(empty($alt)) { $alt = App::getImgFilename($thumbnailId); }

		echo '<li class="b-cards__item cell small-12 medium-6 large-6">
		<div class="b-cards__header">';

		if($type != '1') {
			echo '
			<div class="hasHover dark2">';
			// if(!is_admin()):
			// 	echo '<img data-src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="lazyload b-cards__img" />';
			// else:
				echo '<img src="' . $homeurl . $imageUrl . '" class="lazyload b-cards__img" ' . $imgSizes .' />';
			// endif;
			echo '
			<div class="item-overlay">
			<p>' . get_field('ref_txt') . '</p>
			</div>
			</div>';
		}

		else {
			$imageUrl2 = get_field('ref_before');
			echo '
			<div id="slider" class="beer-slider" data-beer-label="' . __('vorher', 't3-theme') . '" data-start="75">';
			// if(!is_admin()):
			// 	echo '<img data-src="' . $homeurl . $imageUrl2['sizes']['card-small'] . '" alt="' . $alt . '" class="b-cards__img" />';
			// else:
				echo '<img src="' . $homeurl . $imageUrl2['sizes']['card-small'] . '" alt="' . $alt . '" class="b-cards__img" ' . $imgSizes .' />';
			// endif;
			echo '
			<div class="beer-reveal" data-beer-label="' . __('nachher', 't3-theme') . '">';
			// if(!is_admin()):
			// 	echo '<img data-src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="b-cards__img" />';
			// else:
				echo '<img src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="b-cards__img" ' . $imgSizes .' />';
			// endif;
			echo '
			</div>
			</div>';
		}

		echo '
		</div>
		<div class="b-cards__body">
		<h3 class="b-cards__title">' . get_the_title() . '</h3>
		</div>
		</li>';
	endwhile;
endif;
die; // here we exit the script and even no wp_reset_query() required!
}

add_action('wp_ajax_reffilter', 'ref_filter_function');
add_action('wp_ajax_nopriv_reffilter', 'ref_filter_function');

function ref_filter_function(){
	// $homeurl = 'https://mk0t3themefok4uwj9r3.kinstacdn.com';
	$homeurl = ''; // testing optimize on staging
	$homeurl = App::homeurl();
	//$homeurl =  get_home_url();
	$type = $_POST['blocktype'];
	$imgSizes = 'width="640" height="432"';

	if ( isset( $_GET[ 'wpml_lang' ] ) ) {
		do_action( 'wpml_switch_language',  $_GET[ 'wpml_lang' ] ); // switch the content language
	}

	$params = array(
		'post_type' => array('references'),
		'posts_per_page' => 10,
		'orderby' => 'date',
		'order'	=> 'DESC',
		'suppress_filters' => false,
	);

	if($_POST['blocktype'] == '1') {
		$params['meta_key'] = 'ref_before';
		$params['meta_query'] = array(
			array(
				'key' => 'ref_before',
				'value' => '',
				'compare' => '!=',
			)
		);
	}
	else {
		$params['meta_query'] = array(
			'relation'		=> 'OR',
			array(
				'key'		=> 'ref_txt',
				'value'		=> '',
				'compare'	=> '!='
			),
			array(
				'key'		=> 'ref_txt',
				'value'		=> ' ',
				'compare'	=> '!='
			)
		);
	}

	if($_POST['cat'] != '0') {
		$params['tax_query'] = array(
			array(
				'taxonomy' => 'product_category',
				'field'    => 'term_id',
				'terms'    => $_POST['cat'],
			),
		);
	}

	query_posts( $params );

	global $wp_query;

	if( have_posts() ) :

		ob_start();

		while( have_posts() ): the_post();
			$imageUrl = get_the_post_thumbnail_url(get_the_ID(), 'card-small' );
			$thumbnailId = get_post_thumbnail_id( get_the_ID() );
			$alt = get_post_meta($thumbnailId, '_wp_attachment_image_alt', true);
			if(empty($alt)) { $alt = App::getImgFilename($thumbnailId); }

			echo '<li class="b-cards__item cell small-12 medium-6 large-6">
			<div class="b-cards__header">';
			// echo 'Type: ' . $type;
			if($type != '1') {
				echo '
				<div class="hasHover dark2">';
				// if(!is_admin()):
				// 	echo '<img data-src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="lazyload b-cards__img" />';
				// else:
					echo '<img src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="lazyload b-cards__img" ' . $imgSizes .' />';
				// endif;
				//echo '<h3 class="b-cards__title">' . get_the_title() . '</h3>
				echo '
					<div class="item-overlay">
						<p>' . get_field('ref_txt') . '</p>
					</div>
				</div>';
			}

			else {
				$imageUrl2 = get_field('ref_before');
				echo '
				<div id="slider" class="beer-slider" data-beer-label="' . __('vorher', 't3-theme') . '" data-start="75">';
					// if(!is_admin()):
					// 	echo '<img data-src="' . $homeurl . $imageUrl2['sizes']['card-small'] . '" alt="' . $alt . '" class="b-cards__img" />';
					// else:
						echo '<img src="' . $homeurl . $imageUrl2['sizes']['card-small'] . '" alt="' . $alt . '" class="b-cards__img" ' . $imgSizes .' />';
					// endif;
					echo '
					<div class="beer-reveal" data-beer-label="' . __('nachher', 't3-theme') . '">';
					// if(!is_admin()):
					// 	echo '<img data-src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="b-cards__img" />';
					// else:
						echo '<img src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="b-cards__img" ' . $imgSizes .' />';
					// endif;
					echo '
					</div>
				</div>';
			}

			echo '
				</div>
				<div class="b-cards__body">
					<h3 class="b-cards__title">' . get_the_title() . '</h3>
				</div>
			</li>';
		endwhile;

		$posts_html = ob_get_contents();
		ob_end_clean();
	else:
		$posts_html = '<div class="cell small-12 medium-10 medium-offset-1"><p>' . __('Es wurden keine Einträge mit diesen Kriterien gefunden.', 't3-theme') . '</p></div>';
	endif;

	echo json_encode( array(
		'posts' => json_encode( $wp_query->query_vars ),
		'max_page' => $wp_query->max_num_pages,
		'found_posts' => $wp_query->found_posts,
		'content' => $posts_html
	) );

	die();
}

/**
* first load
**/
add_action('wp_ajax_firstload', 'firstload_ajax_handler');
add_action('wp_ajax_nopriv_firstload', 'firstload_ajax_handler');

function firstload_ajax_handler(){
	// $homeurl = 'https://mk0t3themefok4uwj9r3.kinstacdn.com';
	$homeurl = ''; // testing optimize on staging
	//$homeurl =  get_home_url();
	$homeurl = App::homeurl();
	$type = $_POST['blocktype'];
	$imgSizes = 'width="640" height="432"';

	if ( isset( $_GET[ 'wpml_lang' ] ) ) {
		do_action( 'wpml_switch_language',  $_GET[ 'wpml_lang' ] ); // switch the content language
	}

	$params = array(
		'post_type' => array('references'),
		'posts_per_page' => 10,
		'orderby' => 'date',
		'order'	=> 'DESC',
		'suppress_filters' => false,
	);

	if($_POST['blocktype'] == '1') {
		$params['meta_key'] = 'ref_before';
		$params['meta_query'] = array(
			array(
				'key' => 'ref_before',
				'value' => '',
				'compare' => '!=',
			)
		);
	}
	else {
		$params['meta_query'] = array(
			'relation'		=> 'OR',
			array(
				'key'		=> 'ref_txt',
				'value'		=> '',
				'compare'	=> '!='
			),
			array(
				'key'		=> 'ref_txt',
				'value'		=> ' ',
				'compare'	=> '!='
			)
		);
	}

	query_posts( $params );
	global $wp_query;

	if( have_posts() ) :
		ob_start();

		while( have_posts() ): the_post();

			$imageUrl = get_the_post_thumbnail_url(get_the_ID(), 'card-small' );
			$thumbnailId = get_post_thumbnail_id( get_the_ID() );
			$alt = get_post_meta($thumbnailId, '_wp_attachment_image_alt', true);
			if(empty($alt)) { $alt = App::getImgFilename($thumbnailId); }

			echo '<li class="b-cards__item cell small-12 medium-6 large-6">
				<div class="b-cards__header">';

				if($type != '1') {
					echo '
					<div class="hasHover dark2">';
					// if(!is_admin()):
					// 	echo '<img data-src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="lazyload b-cards__img" />';
					// else:
						echo '<img src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="lazyload b-cards__img" ' . $imgSizes .' />';
					// endif;
					echo '
						<div class="item-overlay">
							<p>' . get_field('ref_txt') . '</p>
						</div>
					</div>';

				}

				else {
					$imageUrl2 = get_field('ref_before');
					echo '
					<div id="slider" class="beer-slider" data-beer-label="' . __('vorher', 't3-theme') . '" data-start="75">';
						// if(!is_admin()):
						// 	echo '<img data-src="' . $homeurl . $imageUrl2['sizes']['card-small'] . '" alt="' . $alt . '" class="b-cards__img" />';
						// else:
							echo '<img src="' . $homeurl . $imageUrl2['sizes']['card-small'] . '" alt="' . $alt . '" class="b-cards__img" ' . $imgSizes .' />';
						// endif;
						echo '
						<div class="beer-reveal" data-beer-label="' . __('nachher', 't3-theme') . '">';
						// if(!is_admin()):
						// 	echo '<img data-src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="b-cards__img" />';
						// else:
							echo '<img src="' . $homeurl . $imageUrl . '" alt="' . $alt . '" class="b-cards__img" ' . $imgSizes .' />';
						// endif;
					echo '
					</div>
					</div>';
				}

				echo '
				</div>
				<div class="b-cards__body">
					<h3 class="b-cards__title">' . get_the_title() . '</h3>
				</div>
			</li>';
		endwhile;

		$posts_html = ob_get_contents();
		ob_end_clean();
	else:
		$posts_html = '<p>' . __('Es wurden keine Einträge mit diesen Kriterien gefunden.', 't3-theme') . '</p>';
	endif;

	echo json_encode( array(
		'posts' => json_encode( $wp_query->query_vars ),
		'max_page' => $wp_query->max_num_pages,
		'found_posts' => $wp_query->found_posts,
		'content' => $posts_html
	) );

	die();
}
