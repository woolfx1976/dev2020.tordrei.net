{{-- @php
  if(isset($postType)) {
    $cpt = $postType;
  }
  else {
    $cpt = 'std';
  }
@endphp --}}
<li class="b-cards__item cell {{ $cellSizes }}{{ $filterIDs }}">
  {{-- Standard Card --}}
  @if($design == 'std')
    @if($hasLink == '1')
      <a href="{!! $url !!}" class="hasHover">
    @endif
    <div class="b-cards__header">
      @if($hasOverlay == '1')
        <div class="hasHover dark">
        @include('partials.parts.item_parts.header-offset')
        </div>
      @else
        <div>
          {{-- @if(!is_admin()) --}}
            <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" width="{!! $width !!}" height="{!! $height !!}"@if(isset($imgTitle)){!! $imgTitle !!}@endif />
          {{-- @else
            <img src="{!! $imageSmall !!}" class="lazyload b-cards__img" />
          @endif --}}
          @if(!empty($imageDescr))
            <div class="b-cards__badge">{!! $imageDescr !!}</div>
          @endif
        </div>
      @endif
    </div>
    <div class="b-cards__body">
      <h3 class="b-cards__title">{!! $title !!}</h3>
      @if(!empty($content) && !$hasOverlay)
        <p>{!! $content !!}</p>
      @endif
    </div>
    @if($hasLink == '1')
      </a>
    @endif
  @else
    {{-- Offset / Overlay Card --}}
    <div class="b-cards__header">
      @if($hasLink == '1')
        @if($hasOverlay != '1')
          <a href="{!! $url !!}" class="hasHover">
            @include('partials.parts.item_parts.header-offset')
          </a>
        @else
        <a href="{!! $url !!}" class="hasHover dark2">
            @include('partials.parts.item_parts.header-offset')
          </a>
        @endif
      @else
        @if($hasOverlay != '1')
          <div class="hasHover">
            @include('partials.parts.item_parts.header-offset')
          </div>
        @else
        <div class="hasHover dark2">
            @include('partials.parts.item_parts.header-offset')
          </div>
        @endif
      @endif
    </div>
    <div class="b-cards__body">
      @if(!empty($title))
        <h3 class="b-cards__title">{!! $title !!}</h3>
      @endif
      @if(!empty($content) && !$hasOverlay)
        <p>{!! $content !!}</p>
      @endif
    </div>
  @endif
</li>
