<?php

namespace App;

use App\Controllers\App;

/**
 * Return if Shortcodes already exists.
 */
if (class_exists('Shortcodes')) {
    return;
}

/**
 * Shortcodes
 */
class Shortcodes
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $shortcodes = [
            'sidebarlinks',
            'sidebarHotlineLinks',
            'catlist',
            'searchform',
        ];

        return collect($shortcodes)
            ->map(function ($shortcode) {
                return add_shortcode($shortcode, [$this, strtr($shortcode, ['-' => '_'])]);
            });
    }

    /**
     * Sidebar Links (Leeb)
     * Get Leeb-Sidebar-Links.
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     */
    public function sidebarlinks($atts, $content = null)
    {
        $html = '';

        // $linkArray = array(
        //     'de' => array(
        //         'catalog' => '/kontakt/prospekt-anfordern/',
        //         'partner' => '/kontakt/vertriebspartner/',
        //     ),
        //     'at' => array(
        //         'catalog' => '/kontakt/prospekt-anfordern/',
        //         'partner' => '/kontakt/vertriebspartner/',
        //     ),
        //     'ch' => array(
        //         'catalog' => '/kontakt/prospekt-anfordern/',
        //         'partner' => '/kontakt/vertriebspartner/',
        //     ),
        //     'sl' => array(
        //         'catalog' => '/kontakt/prospekt-anfordern/',
        //         'partner' => '/kontakt/vertriebspartner/',
        //     ),
        //     'it' => array(
        //         'catalog' => '/kontakt/prospekt-anfordern/',
        //         'partner' => '/kontakt/vertriebspartner/',
        //     ),
        // );
        //
        // defined('ICL_LANGUAGE_CODE') ? $lang = ICL_LANGUAGE_CODE : $lang = 'de';

        $linkCatalog = get_field('op_link_catalog', 'options');
        $linkPartner = get_field('op_link_partner', 'options');

        // if(array_key_exists('catalog', $atts)) {
        //     $html .= '<p class="links"><a class="link-block icon-catalog" href="' . $linkArray[$lang]['catalog'] . '"><span class="title">' . __('Gratis Katalog', 't3-theme') . '</span</a></p>';
        // }
        // if(array_key_exists('partner', $atts)) {
        //     $html .= '<p class="links"><a class="link-block icon-pin" href="' . $linkArray[$lang]['partner'] . '"><span class="title">' . __('Vertriebspartner finden', 't3-theme') . '</span></a></p>';
        // }

        if(array_key_exists('catalog', $atts) && $linkCatalog) {
            $html .= '<p class="links"><a class="link-block icon-catalog" href="' . esc_url( $linkCatalog ) . '"><span class="title">' . __('Gratis Katalog', 'leeb') . '</span</a></p>';
        }
        if(array_key_exists('partner', $atts) && $linkPartner) {
            $html .= '<p class="links"><a class="link-block icon-pin" href="' . esc_url( $linkPartner ) . '"><span class="title">' . __('Vertriebspartner finden', 'leeb') . '</span></a></p>';
        }

        // if(array_key_exists('catalog', $atts)) {
        //     $html .= '<p class="links"><a class="link-block icon-calculator" href="#"><span class="title">' . __('Katalog anfordern', 't3-theme') . '</span><span class="descr">' . __('Gratis Katalog wird Ihnen per Post zugeschickt', 't3-theme') . '</span></a></p>';
        // }
        // if(array_key_exists('partner', $atts)) {
        //     $html .= '<p class="links"><a class="link-block icon-pin" href="#"><span class="title">' . __('Vertriebspartner finden', 't3-theme') . '</span><span class="descr">' . __('Leeb Vertriebspartner immer in Ihrer Nähe', 't3-theme') . '</span></a></p>';
        // }
        // if(array_key_exists('offert', $atts)) {
        //     $html .= '<p class="links"><a class="link-block icon-catalog" href="#"><span class="title">' . __('Preisanfrage', 't3-theme') . '</span><span class="descr">' . __('Jetzt Preisangebot nach Maß anfordern', 't3-theme') . '</span></a></p>';
        // }


        return $html;
    }

    /**
     * Sidebar Links (Leeb)
     * Get Leeb-Sidebar-Links.
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     */
    public function sidebarHotlineLinks($atts, $content = null)
    {
        $html = '';

        $langArr = array('de', 'at', 'ch', 'it', 'si' ,'int');
        $c = 0;
        foreach($langArr as $lang) {
          $c == 0 ? $class = ' h-mt' : $class = ''; $c++;
          $hotlineTitle = get_field('op_hotline_' . $lang . '_title', 'options');
          $hotlineText = get_field('op_hotline_' . $lang, 'options');
          $hotlineNumber = get_field('op_hotline_' . $lang . '_nr', 'options');

          if(array_key_exists($lang, $atts) && $hotlineTitle) {
              $html .= '<p class="links' . $class . '"><a class="link-block bgimg ' . $lang . '" href="tel:' . $hotlineNumber . '"><span class="title">' . $hotlineTitle . '</span><span class="descr">' . $hotlineText . '</span></a></p>';
          }
        }


        // if(array_key_exists('catalog', $atts)) {
        //     $html .= '<p class="links"><a class="link-block icon-calculator" href="#"><span class="title">' . __('Katalog anfordern', 't3-theme') . '</span><span class="descr">' . __('Gratis Katalog wird Ihnen per Post zugeschickt', 't3-theme') . '</span></a></p>';
        // }
        // if(array_key_exists('partner', $atts)) {
        //     $html .= '<p class="links"><a class="link-block icon-pin" href="#"><span class="title">' . __('Vertriebspartner finden', 't3-theme') . '</span><span class="descr">' . __('Leeb Vertriebspartner immer in Ihrer Nähe', 't3-theme') . '</span></a></p>';
        // }
        // if(array_key_exists('offert', $atts)) {
        //     $html .= '<p class="links"><a class="link-block icon-catalog" href="#"><span class="title">' . __('Preisanfrage', 't3-theme') . '</span><span class="descr">' . __('Jetzt Preisangebot nach Maß anfordern', 't3-theme') . '</span></a></p>';
        // }

        $html .= '<p class="links h-mt"><span class="link-block text-left">' . get_field('op_address', 'options') .'</span></p>';


        return $html;
    }

    /**
     * Category List
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     */

    public function catlist($atts, $content = null)
    {
        $html = '';
        $homeurl = App::homeurl();
        $cat = get_category( $atts['id'] );
        if($cat && !empty($atts['id'])) {
            $html .= '<h3 class="b-blog-catheader"><a href="' . $homeurl .'/category/' . $cat->slug . '/">' . $cat->name . '</h3>';

            $args = array(
                'type'                     => 'post',
                'child_of'                 => $cat->term_id,
                'orderby'                  => 'name',
                'order'                    => 'ASC',
                'hide_empty'               => FALSE,
                'hierarchical'             => 1,
                'taxonomy'                 => 'category',
            );

            $child_categories = get_categories($args );

            $category_list = array();
            $category_list[] = $cat->term_id;

            if ( !empty ( $child_categories ) ) {
                $html .= '<ul class="c-list b-blog-catlist">';
                foreach ( $child_categories as $child_category ) {
                    $category_list[] = $child_category->term_id;
                    $html .= '<li><a href="' . $homeurl .'/category/' . $cat->slug .'/' . $child_category->slug . '/">'. $child_category->cat_name . '</a></li>';
                }
                $html .= '</ul>';
            }
        }

        return $html;
    }

    /**
     * Custom search form
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     */

    public function searchform($atts, $content = null)
    {
        $attributes = shortcode_atts(
          array(
             'post-type' => '',
             'showlabel' => '1',
           ),
          $atts
      );

        $html = '';
        $postType = $attributes['post-type'];
        $showLabel = $attributes['showlabel'];

        if($postType && !empty($attributes['post-type'])) {
          $value = __('Suchbegriff eingeben', 't3-theme');
          $post_type = $postType;
          $form_value = (isset($value)) ? $value : attribute_escape(apply_filters('the_search_query', get_search_query()));
          $langField = '';
          ob_start();
            do_action( 'wpml_add_language_form_field' );
            $langField = ob_get_contents();
          ob_end_clean();

          $home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );

          $html = '<form method="get" id="searchform" action="' . $home_url . '/" class="h-mobp">
            <div class="c-searchform" data-test="'.$attributes['showlabel'].'">';
                if($showLabel == '1'):
                  $html .= '<span class="show-for-medium">' . __('Blog durchsuchen nach:', 't3-theme') . '</span>';
                endif;
                $html .='
                <input type="hidden" name="post_type" value="'.$postType.'" />
                <input type="text" value="" name="s" id="s" placeholder="' . $form_value . '" />
                <input type="submit" id="searchsubmit" class="button" value="'.esc_attr(__('Suchen', 't3-theme')).'" />
                ' . $langField . '
            </div>
          </form>';
        }

        return $html;
    }
}

new Shortcodes();
