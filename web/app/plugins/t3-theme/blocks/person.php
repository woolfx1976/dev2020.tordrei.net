<?php

add_filter('sage/blocks/person/data', function (array $block) {
	$homeurl = App::homeurl();
	//$block['test'] = "ABC-Test";
	// $block['count'] = 0;
	// $block['test'] = get_field('person_select');
	$person = get_field('person_select');
	$personProps = get_field('person_props');
	//
	// // $block['items'] = get_field('topics');
	// // $block['title'] = get_field('topics_title');
	// // $block['content'] = get_field('topics_teaser');
	// // $block['design'] = get_field('topics_design');
	// // $sizeLarge = get_field('topics_size_large');
	//
	// $block['getData']['person_detail'] = '1111';
	//
	$block['button'] = get_field('person_button');
	$block['layout'] = get_field('person_layout');
	$block['txt'] = get_field('person_txt');
	$block['onlyText'] = get_field('person_only_txt');
	$block['props'] = $personProps;

	if( $person ):
	    foreach( $person as $post ):
	//
	// // foreach($person as $item):
		$block['graduation'] = get_field('person_grad', $post->ID);
		$block['firstname'] = get_field('person_firstname', $post->ID);
		$block['lastname'] = get_field('person_lastname', $post->ID);
		$block['function'] = get_field('person_function', $post->ID);
		$block['blockquote'] = get_field('person_blockquote', $post->ID);
		$block['email'] = get_field('person_email', $post->ID);
		$block['emailDomainname'] = get_field('person_email_domain', $post->ID);
		$block['emailTLD'] = get_field('person_email_tld', $post->ID);
		$block['tel'] = get_field('person_tel', $post->ID);
		$block['permalink'] = get_post_permalink($post->ID);

		$thumbnail_id = get_post_thumbnail_id( $post->ID );
		$block['alt'] = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
		if(empty($block['alt'])) { $block['alt'] = App::getImgFilename($thumbnail_id); }

		$block['imageSmall'] = $homeurl . get_the_post_thumbnail_url($post->ID, 'fp-square' );
	//
	//
	//
	// 	// $block['person_items'][] = \App\template('partials.parts.items.person-item', [
	// 	// 			'item' => $item,
	// 	// 			// 'cellSizes' => $sizeLarge,
	// 	// 			// 'title' => $card['title'],
	// 	// 			// 'content' => $card['content'],
	// 	// 			// 'link' => $card['link'],
	// 	// 			// 'imageMedium' => $image['sizes']['card-medium'],
	// 	// 			// 'imageSmall' => $image['sizes']['card-small'],
	// 	// 			// 'alt' => $image['alt'],
	// 	// 		]);
	// 	//$topics = get_field('topic_items', $item->ID);
	// 	// foreach($topics as $card):
	// 	// 	$image = $card['img'];
	// 	//
	// 	// 	if($block['design'] == 'cards') {
	// 	// 		$block['card_items'][] = \App\template('partials.parts.items.card-item-topics', [
	// 	// 			'cellSizes' => $sizeLarge,
	// 	// 			'title' => $card['title'],
	// 	// 			'content' => $card['content'],
	// 	// 			'link' => $card['link'],
	// 	// 			'imageMedium' => $image['sizes']['card-medium'],
	// 	// 			'imageSmall' => $image['sizes']['card-small'],
	// 	// 			'alt' => $image['alt'],
	// 	// 		]);
	// 	// 	}
	// 	// 	else {
	// 	// 		$block['card_items'][] = \App\template('partials.parts.items.masonry-card-item', [
	// 	// 			'title' => $card['title'],
	// 	// 			'content' => $card['content'],
	// 	// 			'link' => $card['link'],
	// 	// 			'imageMedium' => $image['sizes']['fp-medium'],
	// 	// 			'imageSmall' => $image['sizes']['fp-small'],
	// 	// 			'alt' => $image['alt'],
	// 	// 		]);
	// 	// 	}
	// 	//
	// 	// endforeach;
	endforeach;
	endif;

	return $block;
});
