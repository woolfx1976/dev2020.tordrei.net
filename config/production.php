<?php
/**
 * Configuration overrides for WP_ENV === 'production'
 */

use Roots\WPConfig\Config;

/**
 * You should try to keep staging as close to production as possible. However,
 * should you need to, you can always override production configuration values
 * with `Config::define`.
 *
 * Example: `Config::define('WP_DEBUG', true);`
 * Example: `Config::define('DISALLOW_FILE_MODS', false);`
 */
 Config::define('WP_DEBUG', false);
 Config::define('DISALLOW_FILE_MODS', false);
 Config::define( 'WP_MEMORY_LIMIT' , '512M' );
 Config::define( 'KINSTA_CDN_USERDIRS', 'app');
 Config::define( 'ACF_EXPERIMENTAL_PRELOAD_BLOCKS', true);
