<!doctype html>
<html {!! get_language_attributes() !!} class="no-js">
  @include('partials.head')
  <body @php body_class() @endphp id="top" data-test="testing-rollback">
    {{-- <div class="off-canvas-wrapper">
      <div class="off-canvas position-right" id="offCanvas" data-off-canvas>
        @include('partials.mobile-nav')
      </div>
      <div class="off-canvas-content" data-off-canvas-content> --}}
        @php do_action('get_header') @endphp
        @include('partials.header-mmm')
        @if(!is_singular('tribe_events'))
        <main class="main">
          @if(is_singular('product'))
            @include('partials.featuredimage')
          @endif
          @yield('content')
          @include('partials.parts.components.floating-nav')
        </main>
        @else
          @yield('content')
        @endif
        @php do_action('get_footer') @endphp
        @include('partials.footer')
        @php wp_footer() @endphp
      {{-- </div>
    </div> --}}
  </body>
</html>
