<header class="c-fh c-fh--text none">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell small-12 medium-10 medium-offset-1">
        <div class="c-fh__wrapper">
          <h1>{!! App::title() !!}</h1>
          {!! App::breadcrumbNav() !!}
        </div>
      </div>
    </div>
  </div>
</header>
<section class="is-style-bg-transparent b-pt-none b-pb-none none">
  <div class="grid-container">
    <div class="b-carousel b-carousel--product single slider">
      <div class="slides">
        @foreach ($product_carousel['carouselItems'] as $value)
          {!! $value !!}
        @endforeach
      </div>
    </div>
  </div>
</section>
