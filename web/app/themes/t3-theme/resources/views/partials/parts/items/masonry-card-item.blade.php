<li class="masonry__item">
    <div class="b-cards__header">
      <div>
        @if(!empty($link['url']))
          <a href="{!! $link['url'] !!}">
            <img src="{!! $imageSmall !!}" width="{!! $width !!}" width="{!! $height !!}" alt="{!! $alt !!}" class="b-cards__img lazyload" />
          </a>
        @else
          <img src="{!! $imageSmall !!}" width="{!! $width !!}" width="{!! $height !!}" alt="{!! $alt !!}" class="b-cards__img" />
        @endif
      </div>
    </div>
    <div class="b-cards__body">
      <h3 class="b-cards__title">{!! $title !!}</h3>
      <p>{!! $content !!}</p>
      @if(!empty($link['url']))
      <a href="{!! $link['url'] !!}" target="{!! $link['target'] !!}">{!! $link['title'] !!}</a>
      @endif
    </div>
</li>
