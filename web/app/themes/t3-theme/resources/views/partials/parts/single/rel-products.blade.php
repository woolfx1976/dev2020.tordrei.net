<section class="b-pb-medium is-style-bg-transparent">
  <div class="wp-block-cgb-block-t3-grid-block">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          <h2><?php empty($related_products['title']) ? _e('Weitere Modelle', 'leeb') : print($related_products['title']); ?></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="wp-block-cgb-block-t3-grid-block b-has-pt">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 b-has-pt">
          <div class="l-wrapper">
            <ul class="c-list b-cards b-cards--std grid-x grid-margin-x grid-margin-y">
              @if(isset($related_products['card_items']) && count($related_products['card_items']) > 0)
                @foreach ($related_products['card_items'] as $value)
                  {!! $value !!}
                @endforeach
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
