(function($){

  /**
  * initializeBlock
  *
  * Adds custom JavaScript to the block HTML.
  *
  * @date    15/4/19
  * @since   1.0.0
  *
  * @param   object $block The block jQuery element.
  * @param   object attributes The block attributes (only available when editing).
  * @return  void
  */
  console.log('Topics');

  var initializeBlock = function() {
    if ( $( '.masonry' ).length) {
      console.log('ISOTOPE');
      $('.masonry').isotope({
        itemSelector: '.masonry__item',
        percentPosition: true,
        masonry: {
          columnWidth: '.masonry__sizer',
          gutter: '.masonry__gutter-sizer',
        },
      });
    }
  }


  // Initialize each block on page load (front end).
    // $(document).ready(function() {
    //   initializeBlock();
    // });

    $(window).on('load', function() {
      initializeBlock();
    });

  // Initialize dynamic block preview (editor).
  if( window.acf ) {
    window.acf.addAction( 'render_block_preview/type=topics', initializeBlock  );
  }

})(jQuery);
