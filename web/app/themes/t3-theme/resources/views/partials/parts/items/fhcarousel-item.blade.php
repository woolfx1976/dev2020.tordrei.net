<div class="slides{!! $classBG !!}" data-show-banner="{!! $showBanner !!}">
    <!-- Image Neu -->
    <img src="{!! $imgSmall !!}" srcset="{!! $srcSet !!}"@if(isset($alt)) alt="{!! $alt !!}"@endif width="640" height="432" />
    <div class="grid-container">
      <div class="grid-x">
        @php
          if($showBanner):
            $smallSize = '9';
          else:
            $smallSize = '12';
          endif;
        @endphp
        <div class="cell small-{!! $smallSize !!} medium-7 large-6{!! $offsetClass !!}">
          <div class="c-fh__wrapper">
            @if(!empty($text))
              @if($counter == 1)
                <h1>{!! $title !!}</h1>
              @else
                <h2 class="h2ash1">{!! $title !!}</h2>
              @endif
            @endif
            @if(!empty($text))
              <p>{!! $text !!}</p>
            @endif
            @if(!empty($link))
              <a href="{!! $link['url'] !!}">{!! $link['title'] !!}</a>
            @endif
          </div>
        </div>
      </div>
  </div>
</div>
