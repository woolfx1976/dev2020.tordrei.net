<section id="first-section" class="wp-block-cgb-t3-section-block alignfull b-pt-none is-style-bg-transparent">
  {{-- <div class="wp-block-cgb-t3-container-block none" style="background-color:none">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-9 large-0 medium-offset-1 b-has-lsmb">
        </div>
      </div>
    </div>
  </div> --}}

  <div class="wp-block-cgb-t3-container-block none" style="background-color:none">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-0 large-0 b-has-pt">
          @if(have_posts())
            <ul id="blog-list" class="c-list b-cards b-cards--offset grid-x grid-margin-x grid-margin-y">
              @while(have_posts()) @php the_post() @endphp
                @include('partials.parts.archive.blog-card')
              @endwhile
            </ul>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
