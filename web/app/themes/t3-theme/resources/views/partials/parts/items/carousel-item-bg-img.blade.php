@php
  if(!is_admin()):
    $attributes = $interchange;
  else:
    $attributes = ' style="background-image: url(' . $imageLarge. '); background-size: cover; background-position: center;"';
  endif;
@endphp
<div {!! $attributes !!}>
  {{-- @if(!empty($alt))
    <div class="b-carousel__title">
      {!! $alt !!}
    </div>
  @endif --}}
</div>
