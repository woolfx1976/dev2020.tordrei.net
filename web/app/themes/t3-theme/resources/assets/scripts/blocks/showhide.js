(function($){

  /**
  * initializeBlock
  *
  * Adds custom JavaScript to the block HTML.
  *
  * @date    15/4/19
  * @since   1.0.0
  *
  * @param   object $block The block jQuery element.
  * @param   object attributes The block attributes (only available when editing).
  * @return  void
  */
  console.log('Topics');

  var initializeBlock = function() {
    if ( $( this ).length) {
      console.log('show-hide-block');


      $('.b-showhide__link').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('is-active');
        var container = $(this).parent().next();

        // if(!$(this).hasClass('icon')) {
          var open = $(this).data('open');
          var close = $(this).data('close');
          var currText = $(this).text();
          console.log('currText: >' + currText + '< >open: ' + open + '< >close: ' + close);
          currText == open ? $(this).text(close) : $(this).text(open);
        // }


        // $(this).text(function(text){
        //   text === open ? text = close : text = open;
        //   v
        //   return text;
        // });

        if(!container.hasClass('opened')) {
          container.slideDown(300,function(){
            $(this).addClass('opened')
          });
        } else {
          container.slideUp(250,function(){
            $(this).removeClass('opened')
          });
        }

        // e.stopPropagation();
        // var currElm = $(this).parent().next();
        // e.preventDefault();
        // console.log('Click button');
        // currElm.slideToggle(300);

        // $(this).text(function(i, text){
        //
        //   text === $(this).data('open') ? text = $(this).data('close') : text = $(this).data('open');
        //   console.log('text: ' + text);
        //   return text;
        // });
      })
    }
  }


  // Initialize each block on page load (front end).
    // $(document).ready(function() {
    //   initializeBlock();
    // });

    $(window).on('load', function() {
      // $('.wp-block-t3-showhide-block').each(function(){
        initializeBlock( $(this) );
      // });
    });

  // Initialize dynamic block preview (editor).
  // if( window.acf ) {
  //   window.acf.addAction( 'render_block_preview/type=topics', initializeBlock  );
  // }

})(jQuery);
