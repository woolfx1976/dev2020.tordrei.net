<li class="b-cards__item cell {!! $anchorClass !!} {!! $cellSizes !!}">
  <div class="b-cards__header">
    @if(!empty($linkUrl))
      <a href="{!! $linkUrl !!}" class="hasHover" {!! $smoothScrollAttr !!}>
        {{-- @if(!is_admin()) --}}
          <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" width="{!! $width !!}" height="{!! $height !!}" class="lazyload b-cards__img"@if(isset($imgTitle)){!! $imgTitle !!}@endif />
        {{-- @else
          <img src="{!! $imageSmall !!}" class="lazyload b-cards__img" />
        @endif --}}
      </a>
    @else
      {{-- @if(!is_admin()) --}}
        <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" width="{!! $width !!}" height="{!! $height !!}" class="lazyload b-cards__img"@if(isset($imgTitle)){!! $imgTitle !!}@endif />
      {{-- @else
        <img src="{!! $imageSmall !!}" class="lazyload b-cards__img" />
      @endif --}}
    @endif
  </div>
  <div class="b-cards__body">
    @if(!empty($linkUrl))
      <a href="{!! $linkUrl !!}" class="b-cards__title" {!! $smoothScrollAttr !!}>
        <h3>{!! $title !!}</h3>
      </a>
    @else
      <h3 class="b-cards__title">{!! $title !!}</h3>
    @endif
    <p>{!! $content !!}</p>
    @if(!empty($linkUrl))
    <a href="{!! $linkUrl !!}" target="{!! $linkTarget !!}" class="more">{!! $linkTitle !!}</a>
    @endif
  </div>
</li>
