@php
  if($page_top_bar['topbar']->layout != 'std'):
    $page_top_bar['topbar']->size_nav = 12;
  endif;
@endphp
<header class="banner nav-type--fixed">
      <div class="title-bar" data-responsive-toggle="responsive-menu" data-hide-for="large" >
        <div class="title-bar-title">
          <a class="brand" href="/">
            <img src="{{$desktop_logo}}" class="logo" alt="{{ get_bloginfo('name', 'display') }}" width="167" height="47" />
            {{-- {{ get_bloginfo('name', 'display') }} --}}
          </a>
        </div>
        <button class="hamburger hamburger--3dy" type="button" data-toggle="offCanvas">
          <span class="hamburger-box"><span class="hamburger-inner"></span></span>
        </button>
      </div>

      <div id="topbar" class="grid-container">
        <nav class="top-bar grid-x" id="responsive-menu">
          @if($page_top_bar['topbar']->layout == 'std')
            <div class="top-bar-left cell large-{{$page_top_bar['topbar']->size_logo}}">
              <a class="brand" href="/">
                <img src="{{$desktop_logo}}" class="logo" alt="{{ get_bloginfo('name', 'display') }}" width="167" height="47" />
              </a>
            </div>
          @else
            <a class="brand abs" href="/">
              <img src="{{$desktop_logo}}" class="logo" alt="{{ get_bloginfo('name', 'display') }}" />
            </a>
          @endif
          <div class="top-bar-right cell large-{{$page_top_bar['topbar']->size_nav}}">
            @if($page_top_bar['topbar']->layout != 'std')
              <div class="c-nav-wrapper">
            @endif
            @if (has_nav_menu('meta_navigation'))
              {!! wp_nav_menu(['theme_location' => 'meta_navigation', 'items_wrap' => '<ul class="meta-menu text-' . $page_top_bar['topbar']->alignment_text . '">%3$s</ul>']) !!}
            @endif
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'items_wrap' => '<ul class="menu dropdown" data-dropdown-menu>%3$s</ul>']) !!}
            @endif
            @if($page_top_bar['topbar']->layout == 'std')
            </div>
            @endif
            {{-- <ul class="menu">
              <li><input type="search" placeholder="Search"></li>
              <li><button type="button" class="button">Search</button></li>
            </ul> --}}
          </div>
          @if($page_top_bar['topbar']->layout != 'std')
            </div>
          @endif
        </nav>
      </div>
</header>
