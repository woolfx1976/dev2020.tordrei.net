<?php

add_filter('sage/blocks/gallery/data', function (array $block) {
	$homeurl = App::homeurl();
	$block['galleryID'] = $block['id'];
	$block['galleryDesign'] = get_field('gallery_design');
	$showTitle = get_field('show_title');
	$showCaption = get_field('show_caption');
	$isLightgallery = get_field('is_lightgallery');
	$block['hasLG'] = $isLightgallery;

	if(get_field('gallery_format')) {
		$format = get_field('gallery_format');
	}
	else {
		$format = 'card';
	}
	$smallSize = $format . '-small';
	$mediumSize = $format . '-medium';
	$largeSize = $format . '-large';

	$cols = get_field('gallery_columns');

	$colSizes = '';
	if($cols == 0 || $cols == 3) {
		$colSizes .= 'small-6 medium-4';
	}
	elseif($cols == 1) {
		$colSizes = 'small-12';
	}
	elseif($cols == 2) {
		$colSizes .= 'small-6';
	}
	elseif($cols == 4) {
		$colSizes .= 'small-6 medium-4 large-3';
	}
	else {
		$colSizes .= 'small-6 medium-3 large-2';
	}

	$images = get_field('gallery');
	if( $images ):
		foreach( $images as $image ):
			!empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);

			$block['galleryItems'][] = \App\template('partials.parts.items.gallery-item', [
        'colSizes' => $colSizes,
        'format' => $format,
				'isLightgallery' => $isLightgallery,
				'showTitle' => $showTitle,
				'showCaption' => $showCaption,
				'alt' => $alt,
				'title' => $image['title'],
				'caption' => $image['caption'],
				'imgDescr' => $image['description'],
				// 'imageXSmall' => $image['sizes']['card-xsmall'],
				'imageSmall' => $homeurl . $image['sizes'][$smallSize],
				'imageMedium' => $homeurl . $image['sizes'][$mediumSize],
				'imageLarge' => $homeurl . $image['sizes'][$largeSize],
				'srcSet' => $homeurl . $image['sizes'][$smallSize] . ' 640w, '
					. $homeurl . $image['sizes'][$mediumSize] . ' 1024w, ',
				'sizes' => '(max-width: 640px) 640px, (min-width: 640px) 1024px',
				'width' => $image['sizes'][$smallSize . '-width'],
				'height' => $image['sizes'][$smallSize . '-height'],
				//'interchange' => '[' . $image['sizes']['card-small'] .', small], [' . $image['sizes']['card-medium'] .', medium], [' . $image['sizes']['card-large'] .', large], [' . $image['sizes']['card-xlarge'] .', xlarge]',
			]
		);
	endforeach;
endif;

return $block;
});
