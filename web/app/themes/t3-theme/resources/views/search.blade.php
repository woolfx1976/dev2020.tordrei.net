@extends('layouts.search')

@section('content')
  {{-- @include('partials.page-header') --}}

  <section class="wp-block-cgb-t3-section-block is-style-bg-transparent b-pt-small b-pb-small">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-9 medium-offset-1 h-mobp">
            @if (!have_posts())
            <div class="callout alert">
              {{-- {{ __('Sorry, no results were found.', 'sage') }} --}}
              {{ __('Es tut uns leid, wir konnten zu Ihrer Suchanfrage nichts finden', 't3-theme') }}
            </div>
            @endif
            {{-- general search form --}}
            {{-- {!! get_search_form(false) !!} --}}
            @php echo do_shortcode('[searchform post-type="post" showlabel="1"]'); @endphp
        </div>
      </div>
    </div>
  </section>
  @php

  global $wp_query;
  $query = $wp_query->query;
  @endphp
  <section class="wp-block-cgb-t3-section-block is-style-bg-transparent b-pt-small">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12">
          @if (isset($query['post_type']) && $query['post_type'] == 'post')
            <ul class="c-list b-cards b-cards--offset grid-x grid-margin-x grid-margin-y">
              @while(have_posts()) @php the_post() @endphp
                @include('partials.parts.items.card-item-blog-loop')
              @endwhile
            </ul>
          @else
            @while(have_posts()) @php the_post() @endphp
              @include('partials.content-search')
            @endwhile
          @endif
          {!! get_the_posts_navigation() !!}
      </div>
    </div>
  </div>
</section>

@endsection
