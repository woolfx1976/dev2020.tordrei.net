<?php
function setColumnClasses($array) {
    $classes = '';
    foreach($array as $key => $val):
        if(!empty($val)):
            $classes .= ' ' .$key . '-' . $val;
        endif;
    endforeach;
    return $classes;
}
add_action('setClasses', 'setClasses', 10, 1);

add_filter('sage/blocks/featured-image/data', function (array $block) {
    $block['banner']['img'] = '';
    $block['banner']['img'] = get_field('banner_img', 'options');
	$block['banner']['link'] = get_field('banner_link', 'options');

// 	// // $block['header_type'] = get_field('header_type');
// 	// $block['data']['header_max_width'] = get_field('header_max_width');
// 	//
// 	// // Check content
// 	// $block['data']['show_content'] = false;
// 	// if($block['data']['header_type'] != 'slider'):
// 	// 	$block['data']['show_content'] = get_field('header_show_content');
// 	// endif;
// 	//
// 	// // Get header background props
// 	// $block['data']['styles'] = '';
// 	// if( have_rows('header_bg_props') ):
// 	//     while( have_rows('header_bg_props') ): the_row();
// 	//         $block['data']['background'] = (object) [
// 	//             'bg_pos_x' => get_sub_field('header_bg_pos_x'),
// 	//             'bg_pos_y' => get_sub_field('header_bg_pos_y'),
// 	//             'bg_size' => get_sub_field('header_bg_size'),
// 	//             'bg_parallax' => get_sub_field('header_bg_parallax'),
// 	//         ];
// 	//
// 	// 			$block['data']['styles'] .= 'style="';
// 	// 			$block['data']['styles'] .= 'background-position: ' . get_sub_field('header_bg_pos_x') . ' ' . get_sub_field('header_bg_pos_y') . ';';
// 	// 			if(get_sub_field('header_bg_size') != 'cover'):
// 	// 				$block['data']['styles'] .= 'background-size: ' . get_sub_field('header_bg_size') . ';';
// 	// 			endif;
// 	// 			$block['data']['styles'] .= '"';
// 	//
// 	//     endwhile;
// 	// endif;
// 	// // Get overlay props
// 	// if( have_rows('header_overlay_props') ):
// 	//     while( have_rows('header_overlay_props') ): the_row();
// 	//         $block['data']['background'] = (object) [
// 	//             'overlay_type' => get_sub_field('header_overlay_type'),
// 	//             'overlay_color' => get_sub_field('header_overlay_color'),
// 	//             'overlay_opacity' => get_sub_field('header_overlay_opacity'),
// 	//         ];
// 	//     endwhile;
// 	// endif;
// 	// // Get content props
// 	// if( have_rows('header_content_size_props') ):
// 	//     while( have_rows('header_content_size_props') ): the_row();
// 	//         $content_classes = '';
// 	//         $array = array();
// 	//         $array['small'] = get_sub_field('header_content_size_small');
// 	//         $array['medium'] = get_sub_field('header_content_size_medium');
// 	//         $array['large'] = get_sub_field('header_content_size_large');
// 	//         $content_classes = setColumnClasses($array);
// 	//
// 	//         $block['content'] = (object) [
// 	//             'classes' => $content_classes,
// 	//         ];
// 	//     endwhile;
// 	// endif;
// 	// // Get section props
// 	// if( have_rows('header_section_props') ):
// 	//     while( have_rows('header_section_props') ): the_row();
// 	//         $block['data']['section'] = (object) [
// 	//             'section_height_class' => get_sub_field('header_section_height'),
// 	//             'section_vh_small' => get_sub_field('header_section_vh_small'),
// 	//             'section_vh_medium' => get_sub_field('header_section_vh_medium'),
// 	//             'section_vh_large' => get_sub_field('header_section_vh_large'),
// 	//             'show_section' => get_sub_field('show_section'),
// 	//         ];
// 	//     endwhile;
// 	// endif;
// 	// // Get header image
// 	// $block['data']['header_img'] = get_field('header_img');
// 	// $block['data']['header_img_interchange'] = '[' . $block['data']['header_img']['sizes']['fp-small'] .', small], [' . $block['data']['header_img']['sizes']['fp-medium'] . ', medium], [' . $block['data']['header_img']['sizes']['fp-large'] .', largel], [' . $block['data']['header_img']['sizes']['fp-xlarge'] . ', xlarge]';
// 	// if(get_field('header_content_type') == 'fields'):
// 	// 	if(!empty(get_field('header_title'))):
// 	// 		$block['data']['header_title'] = get_the_title();
// 	// 	else:
// 	// 		$block['data']['header_title'] = get_field('header_title');
// 	// 	endif;
// 	// else:
// 	// 	$block['data']['header_title'] = '';
// 	// endif;
// 	// $block['data']['header_teaser'] = get_field('header_teaser');
// 	// $block['data']['header_editor'] = get_field('header_editor');
// 	//
// 	// // Get header image
// 	// $block['data']['img'] = get_field('header_img');
// 	// if(!get_field('header_img')):
// 	// 	$block['data']['img_interchange'] = 'data-interchange="[' . get_the_post_thumbnail_url(get_the_ID(), 'fp-small') .', small], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-medium') . ', medium], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-large').', largel], [' . get_the_post_thumbnail_url(get_the_ID(), 'fp-xlarge') . ', xlarge]"';
// 	// else:
// 	// 	$block['data']['img_interchange'] = 'data-interchange="[' . $block['data']['img']['sizes']['fp-small'] .', small], [' . $block['data']['img']['sizes']['fp-medium'] . ', medium], [' . $block['data']['img']['sizes']['fp-large'] .', largel], [' . $block['data']['img']['sizes']['fp-xlarge'] . ', xlarge]"';
// 	// endif;
// 	//
// 	// // Get header image content
// 	// $block['data']['header_content'] = '';
// 	// if(get_field('header_content_type') == 'fields'):
// 	// 	if(!empty(get_field('header_title'))):
// 	// 		$block['data']['header_content'] = get_the_title();
// 	// 	else:
// 	// 		$block['data']['header_content'] = get_field('header_title');
// 	// 	endif;
// 	// 		$block['data']['header_content'] .= get_field('header_teaser');
// 	// else:
// 	// 	$block['data']['header_content'] = get_field('header_editor');
// 	// endif;
// 	//
// 	// // Get header carousel
//
//
// 	// --->
//
// 	//
// 	// 	//Get header container width
// 	// 	$block['data']['grid_container_class'] = '';
// 	// 	if(get_field('header_max_width')):
// 	// 		$block['data']['grid_container_class'] = true;
// 	// 	endif;
// 	// endif;
//
    return $block;
});
