<!doctype html>

<html {!! get_language_attributes() !!} class="no-js">
  @include('partials.head')
  <body @php body_class() @endphp id="top">
    {{-- <div class="off-canvas-wrapper">
      <div class="off-canvas position-right" id="offCanvas" data-off-canvas>
        @include('partials.mobile-nav')
      </div>
      <div class="off-canvas-content" data-off-canvas-content> --}}
        @php do_action('get_header') @endphp
        @include('partials.header-mmm')
        <main class="main">
          @include('partials.parts.single.featuredimage-txt')
          @yield('content')
          @include('partials.parts.components.floating-nav')
        </main>
        @php do_action('get_footer') @endphp
        @include('partials.footer')
        @php wp_footer() @endphp
      {{-- </div>
    </div> --}}
  </body>
</html>
