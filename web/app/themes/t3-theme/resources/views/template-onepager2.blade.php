{{--
  Template Name: Onepager-2
  Template Post Type: post, page, product
--}}

@extends('layouts.onepager2')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-onepager')
  @endwhile
@endsection
