{{-- Silick Slider / tns --}}
<div>

{{-- Swiper --}}
{{-- <div class="swiper-slide"> --}}
  {{-- @if($type == 'band large' || $type == 'single') --}}
    {{-- <img {!! $interchange !!} class="b-carousel__img" alt="{!! $alt !!}" /> --}}
    @if(!empty($link) && !is_admin())
    <a href="{!!$link!!}" class="hasHover">
      <img src="{!! $imageSmall !!}" srcset="{!! $srcSet !!}" sizes="{!! $sizes !!}" class="b-carousel__img lazyload" alt="{!! $alt !!}" />
    </a>
    @else
      {{-- <div class="hasHover"> --}}

      @if(!is_admin())
        {{-- <img data-srcset="{!! $srcSet !!}" sizes="{!! $sizes !!}" class="b-carousel__img lazyload" alt="{!! $alt !!}" /> --}}
        {{-- @if(!is_singular('product'))
          <img {!! $interchange !!} class="b-carousel__img lazyload" alt="{!! $alt !!}" />
        @else
          <img data-srcset="{!! $srcSet !!}" sizes="{!! $sizes !!}" class="b-carousel__img lazyload" alt="{!! $alt !!}" />
        @endif --}}
          <img src="{!! $imageSmall !!}" srcset="{!! $srcSet !!}" class="b-carousel__img" alt="{!! $alt !!}" width="{!! $width !!}" height="{!! $height !!}" />
      @else
        <img src="{!! $imageLarge !!}" class="b-carousel__img lazyload" />
      @endif
      {{-- </div> --}}
    @endif
    @if($postType == 'references' && !empty($caption) )
      <div class="item-overlay">
        <p>{!!$caption!!}</p>
      </div>
    @endif
  {{-- @else
    <img src="{!! $imageSmall !!}" class="b-carousel__img" alt="{!! $alt !!}" />
  @endif --}}
  @if(!empty($alt) && $showTitle == true)
    <div class="b-carousel__title">
      {!! $alt !!}
    </div>
  @endif
</div>
