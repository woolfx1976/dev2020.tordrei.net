<?php
  $type == 'jobs' ? $moreLinkText = __('Job ansehen', 't3-theme') : $moreLinkText = __('weiterlesen', 't3-theme') .' ';
?>
<li class="b-list__item grid-x grid-margin-x">
  <div class="b-list__img cell small-12 medium-4">
    @if(!empty($url))
      <a href="{!! $url !!}" class="hasHover">
        <div class="hasHover">
        {{-- @if(!is_admin()) --}}
          <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" width="640" height="342" class="lazyload" />
        {{-- @else
          <img src="{!! $imageSmall !!}" class="lazyload" />
        @endif --}}
        </div>
      </a>
    @else

      {{-- @if(!is_admin()) --}}
        <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" width="640" height="342" class="lazyload" />
      {{-- @else
        <img src="{!! $imageSmall !!}" class="lazyload" />
      @endif --}}
    @endif
  </div>
  <div class="b-list__body cell small-12 medium-8">
    @if(!empty($url))
      <a href="{!! $url !!}" class="b-list__title">
        <h3>{!! $title !!}</h3>
      </a>
    @else
      <h3 class="b-list__title">{!! $title !!}</h3>
    @endif
    @if(!empty($teaser))
      <p class="b-list__teaser">{!! $teaser !!}</p>
    @endif
    <p>{!! $content !!}</p>
    @if(!empty($url))
    <a href="{!! $url !!}" class="more">{!! $moreLinkText !!}</a>
    @endif
  </div>
</li>
