<?php
/**
 * Advantages custom post type
 */
add_action('init', function () {
    $labels = array(
        'name'                => _x( 'Vorteil', 'Post Type General Name', 't3-theme' ),
		'singular_name'       => _x( 'Vorteil', 'Post Type Singular Name', 't3-theme' ),
		'menu_name'           => __( 'Vorteile', 't3-theme' ),
		'parent_item_colon'   => __( 'Eltern-Vorteil', 't3-theme' ),
		'all_items'           => __( 'Alle Vorteile', 't3-theme' ),
		'view_item'           => __( 'Vorteil zeigen', 't3-theme' ),
		'add_new_item'        => __( 'Neuen Vorteil erstellen', 't3-theme' ),
		'add_new'             => __( 'Neuer Vorteil', 't3-theme' ),
		'edit_item'           => __( 'Vorteil bearbeiten', 't3-theme' ),
		'update_item'         => __( 'Vorteil aktualisieren', 't3-theme' ),
		'search_items'        => __( 'Vorteil suchen', 't3-theme' ),
		'not_found'           => __( 'Nicht gefunden', 't3-theme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 't3-theme' ),
	);
    register_extended_post_type('advantages', [
        'labels' => $labels,
        'menu_icon' => 'dashicons-star-filled',
        'show_in_rest' => false,
        'supports' => array('title','thumbnail','catgegories'),
        'has_archive' => false,
        'rewrite' => true
    ], [
        'singular' => 'Vorteil',
        'plural' => 'Vorteile',
        'slug' => 'vorteile',
    ]);
    /**
	*   To Activate Custom Post Type Single page
	*   @see http://en.bainternet.info/2011/custom-post-type-getting-404-on-permalinks
	*/
	$set = get_option('post_type_rules_flased_authors');
	if ($set !== true){
		flush_rewrite_rules(false);
		update_option('post_type_rules_flased_authors',true);
	}
});
