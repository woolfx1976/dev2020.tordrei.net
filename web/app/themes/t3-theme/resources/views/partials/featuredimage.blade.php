<?php
//print_r($page_header['classes']);
?>
@if(isset($page_header))
  <header class="c-fh c-fh--hero none c-fh--vh @if($page_header['type']=='slider') slider-fh @endif {!!$page_header['grid-container-class']!!}" @if($page_header['type']!='slider' && $page_header['type']!='wo_header'){!!$page_header['img_interchange']!!}@endif>
    @if($page_header['type']=='fh')
      @include('partials.parts.featured-image.hero-image')
    @elseif($page_header['type']=='slider')
      @foreach ($page_header['carouselItems'] as $value)
        {!! $value !!}
      @endforeach
    @elseif($page_header['type']=='text')
      @include('partials.parts.featured-image.text')
  @endif
  </header>
  {{-- @if($page_header['type']=='slider' || is_singular('product'))
    <section class="l-sec--title b-pt-none b-pb-none is-style-bg-transparent">
      <div class="grid-container">
        <div class="grid-x">
          <div class="cell small-12 medium-6 large-7 medium-offset-1">
            @if(is_singular('product'))
              <ul class="c-list breadcrumb">
                <li><a href="/">Home</a> / </li>
                <li><a href="/produkte/balkongelaender-alu">Produkte</a> / </li>
                <li><a href="/produkte/balkongelaender-alu">Alubalkone</a> / </li>
                <li>{!! the_title() !!}</li>
              </ul>
            @endif
            <h1>{{ the_title() }}</h1>
          </div>
        </div>
      </div>
    </section>
  @endif --}}
@else
  <header class="">
  {{-- <header class="page-header"> --}}
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell small-12 mdium-7 large-8 medium-offset-1">
        <h1>{!! App::title() !!}</h1>
      </div>
    </div>
  </div>
  </header>
@endif
