{{--
  Template Name: Onepager
  Template Post Type: post, page, product
--}}

@extends('layouts.onepager')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-onepager')
  @endwhile
@endsection
