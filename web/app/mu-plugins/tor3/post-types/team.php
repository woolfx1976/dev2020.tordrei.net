<?php
/**
 * Team custom post type
 */
add_action('init', function () {
    $labels = array(
        'name'                => _x( 'Person', 'Post Type General Name', 'foundationpress' ),
		'singular_name'       => _x( 'Person', 'Post Type Singular Name', 'foundationpress' ),
		'menu_name'           => __( 'Team', 'foundationpress' ),
		'parent_item_colon'   => __( 'Eltern-Personen', 'foundationpress' ),
		'all_items'           => __( 'Alle Personen', 'foundationpress' ),
		'view_item'           => __( 'Personen zeigen', 'foundationpress' ),
		'add_new_item'        => __( 'Neue Person erstellen', 'foundationpress' ),
		'add_new'             => __( 'Neue Person', 'foundationpress' ),
		'edit_item'           => __( 'Person bearbeiten', 'foundationpress' ),
		'update_item'         => __( 'Person aktualisieren', 'foundationpress' ),
		'search_items'        => __( 'Person suchen', 'foundationpress' ),
		'not_found'           => __( 'Nicht gefunden', 'foundationpress' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'foundationpress' ),
    );
    register_extended_post_type('team', [
        'labels' => $labels,
        'menu_icon' => 'dashicons-groups',
        'show_in_rest' => false,
        'supports' => array('title','editor','thumbnail')
    ], [
        'singular' => 'Team Member',
        'plural' => 'Team Members',
        'slug' => 'team',
    ]);
    /**
	*   To Activate Custom Post Type Single page
	*   @see http://en.bainternet.info/2011/custom-post-type-getting-404-on-permalinks
	*/
	$set = get_option('post_type_rules_flased_authors');
	if ($set !== true){
		flush_rewrite_rules(false);
		update_option('post_type_rules_flased_authors',true);
	}
});

/**
 * Team taxonomies
 */
add_action('init', function () {
    register_extended_taxonomy('team_category', 'team', [
        'show_in_rest' => true,
    ], [
        'singular' => 'Team Category',
        'plural' => 'Team Categories'
    ]);
});
