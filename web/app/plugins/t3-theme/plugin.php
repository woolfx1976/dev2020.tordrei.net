<?php
/**
 * Plugin Name: TORDREI Theme Plugin
 * Description: Adds T3-theme funcionality.
 * Author: Wolfgang Berger
 */

/**
* Load modules
*/
require_once(plugin_dir_path( __FILE__ ) . '/acf-field-groups.php');
require_once(plugin_dir_path( __FILE__ ) . '/acf-theme-options.php');
require_once(plugin_dir_path( __FILE__ ) . '/gutenberg-styles.php');
require_once(plugin_dir_path( __FILE__ ) . '/max-mega-widgets/img-txt-link.php');

/**
* Load block modules
*/
require_once(plugin_dir_path( __FILE__ ) . '/blocks/accordions.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/blog.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/featured-image.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/cards.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/carousel.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/container-columns.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/gallery.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/hero.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/iconlist.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/jobs.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/person.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/references.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/splitscreen.php');
require_once(plugin_dir_path( __FILE__ ) . '/blocks/topics.php');

?>
