
<?php

?>
<section class="{!!$sectionClass!!} is-style-bg-transparent">
  <div class="grid-container">
    <div class="b-cont-columns grid-x grid-margin-x gird-margin-y">
      <div class="cell small-12 medium-6 large-7 medium-offset-1">
        @if(!empty($product_teaser))
          <h2 class="product-teaser">{!! $product_teaser !!}</h2>
        @endif
        {!! $product_content !!}
      </div>
      <div class="cell small-12 medium-4 large-3 is-sidebar">
        {!! $product_options !!}
        {!! $product_sidebar !!}
      </div>
    </div>
  </div>
</section>
