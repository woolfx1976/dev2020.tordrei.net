<li class="b-cards__item cell {!!$colSizes!!}">
  {{-- @php
    $colSizes != 4 ? $img = $imageSmall : $img = $imageXSmall;
  @endphp --}}
  <div class="b-cards__header">
    @if($isLightgallery)
    <a href="{!!$imageLarge!!}" class="hasHover dark">
    @endif
      {{-- @if($showTitle || $showCaption)
        <figure>
      @endif --}}
        @if($colSizes != 'small-12')
          <img src="{!!$imageSmall!!}" srcset="{!! $srcSet !!}" alt="{!! $alt !!}" width="{!! $width !!}" height="{!! $height !!}" class="lazyload" />
        @else
          {{-- @if(!is_admin())
            <img data-srcset="{!! $srcSet !!}" sizes="{!! $sizes !!}" class="lazyload" alt="{!! $alt !!}" />
          @else --}}
            <img src="{!! $imageMedium !!}" alt="{!!$alt!!}" width="{!! $width !!}" height="{!! $height !!}" class="lazyload" />
          {{-- @endif --}}
        @endif
      {{-- @if($showTitle || $showCaption)
          <caption>
            @if($showTitle && !empty($title))
              <b>{!!$title!!}</b>
            @endif
            @if($showCaption && !empty($caption))
              <p>{!!$caption!!}
            @endif
          </caption>
        </figure>
      @endif --}}
      @if(!empty($imgDescr))
        <div class="b-cards__badge">{!! $imgDescr !!}</div>
      @endif
      @if($isLightgallery)
        </a>
      @endif
  </div>
  @if($showTitle || $showCaption)
      <div class="caption">
        @if($showTitle && !empty($title))
          <b>{!!$title!!}</b>
        @endif
        @if($showCaption && !empty($caption))
          <p>{!!$caption!!}
        @endif
      </div>
  @endif
</li>
