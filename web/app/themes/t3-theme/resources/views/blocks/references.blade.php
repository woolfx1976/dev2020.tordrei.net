{{--
  Name: references
  Title: Referenzen mit Filter
  Description: referneces, CPTs
  Category: t3layout
  Icon: images-alt
  Keywords: references
  Mode: view
  Align: wide
  PostTypes: page post
  SupportsMode: true
  SupportsMultiple: true
--}}

<?php

global $wp_query;

?>
<div class="wp-block-cgb-t3-container-block alignfull b-has-offset-b b-ref" style="background-color:#F5F5F5">
  <div class="wp-block-cgb-t3-container-block alignfull b-has-offset-h" style="background-color:none">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-6 large-0 medium-offset-1">
          <h2><?php _e('Referenzen sortieren', 't3-theme'); ?></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="has-padding b-pt-small none grid-container-wrapper">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          <form action="#" id="ref_filter">
            <?php
              $loop = new WP_Query( array(
                  'post_type' => 'references',
                  'posts_per_page' => -1
                )
              );

              $active_cats = array();
              while ( $loop->have_posts() ) : $loop->the_post();
                $id = get_the_ID();
                $categories = get_the_terms( $id, 'product_category' );

                //print_r($categories);

                if (is_array($categories) || is_object($categories)) {
                  foreach($categories as $category) {
                      if (!in_array($category->term_id, $active_cats )){
                          $active_cats[] = $category->term_id;
                      }
                  }
                }
              endwhile; wp_reset_query();

              function get_state($id, $active_cats) {
                foreach($active_cats as $aid) {
                  if($aid == $id) {
                    return true;
                  }
                }
              }

              $args = array(
                'post_type' => 'references',
                'taxonomy' => 'product_category',
                'orderby' => 'name',
                'order'   => 'ASC',
                'hierarchical' => 1,
                'suppress_filters' => 1,
              );

              $cats = get_categories($args);
              $currCats = array();

              foreach($cats as $cat) {
                if(get_state($cat->term_id, $active_cats)) {
                  $currCats[] = $cat;
                }
              }

             //print_r($currCats);

              function sort_terms_hierarchicaly(Array $cats, $parentId = 0)
              {
                $into = [];
                foreach ($cats as $i => $cat) {
                    if ($cat->parent == $parentId) {
                        $cat->children = sort_terms_hierarchicaly($cats, $cat->term_id);
                        $into[$cat->term_id] = $cat;
                    }
                }
                return $into;
              }

              $new_cats = sort_terms_hierarchicaly($currCats, $parentId = 0);
              //print_r($new_cats);
            ?>
            <select name="cat" id="cat" class="postform">
          	<option value="0" selected="selected"><?php _e('Alle anzeigen', 't3-theme'); ?></option>
            <?php
              foreach($new_cats as $cat) {
            ?>
              <option value="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></option>
              <?php
                if(isset($cat->children)) {
                  foreach($cat->children as $child) {
                    ?>
                    <option value="<?php echo $child->term_id; ?>">&nbsp;&nbsp;&nbsp;<?php echo $child->name; ?></option>
                    <?php
                  }
                }
              }
            ?>
            </select>
          	<?php
              //$selectAllStr = urlencode(__('Alle anzeigen', 't3-theme'));
              //wp_dropdown_categories( 'post_type=references&show_count=1&hierarchical=1&taxonomy=product_category&show_option_all=' . $selectAllStr . '&hide_empty=1&suppress_filters=1');
          	?>
          	<div id="message"></div>
          	<input type="hidden" name="action" value="reffilter">
            <input type="hidden" name="blocktype" value="{{ $block['type'] }}"  />
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="wp-block-cgb-t3-container-block b-ref none">
  <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12">
          <ul data-{{ $block['id'] }} id="ref_content" class="c-list b-cards<?php if($block['type'] != '1'): echo ' b-cards--offset'; endif; ?> grid-x grid-margin-x grid-margin-y"></ul>
          <div class="text-center">
            <a href="#" id="loadmore" class="button"><?php _e('Mehr anzeigen', 't3-theme'); ?></a>
          </div>
        </div>
      </div>
  </div>
</div>
