{{-- @extends('layouts.app')

@section('content') --}}
{{-- @include('partials.page-header') --}}

{{-- @if (!have_posts())
<div class="alert alert-warning">
{{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}
</div>
{!! get_search_form(false) !!}
@endif
@endsection --}}
@php
  $headerImg = '';
  $homeurl = App::homeurl();
  $bgImage = get_field('op_404_header_img', 'options');
  $alt = '';
  
  if($bgImage) {
    $imgSrc = $homeurl . $bgImage['sizes']['fp-small'];
    $imgSrcSet = $homeurl . $bgImage['sizes']['fh-small'] .' 640w, ' . $homeurl . $bgImage['sizes']['fh-medium'] . ' 768w, ' . $homeurl . $bgImage['sizes']['fh-large'] . ' 1200w, ' . $homeurl . $bgImage['sizes']['fh-mlarge'] . ' 1500w, ' . $homeurl . $bgImage['sizes']['fh-xlarge'];
    if(!empty($bgImage['sizes']['alt'])) {
      $alt = $bgImage['sizes']['alt'];
    }
  }
@endphp

@extends('layouts.onepager')

@section('content')
  <header class="c-fh l-mh-small c-fh--hero c-grad-l2r c-fh--div none">
    @if($bgImage)
      <img src="{!! $imgSrc !!}" srcset="{!! $imgSrcSet !!}" alt="{!! $alt !!}" width="640" height="342" />
    @endif
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-6 medium-offset-1">
          <div class="c-fh__wrapper">
            <p><b><?php echo get_field('op_404_header_title', 'options'); ?></b></p>
          </div>
        </div>
      </div>
    </div>
    </header>
      <section class="c-fh b-pt-medium b-pb-none is-style-bg-transparent">
        <div class="grid-container">
          <div class="grid-x">
            <div class="cell small-12 medium-9 medium-offset-1">
              <div class="c-fh__wrapper">
                <h1><?php echo get_field('op_404_title', 'options'); ?></h1>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="first-section" class="wp-block-cgb-t3-section-block alignfull b-pt-small is-style-bg-transparent">
        <div class="wp-block-cgb-t3-container-block none" style="background-color:none">
          <div class="grid-container">
            <div class="grid-x">
              <div class="cell small-12 medium-9 large-0 medium-offset-1 b-has-lsmb">
                <?php echo get_field('op_404_text', 'options'); ?>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection

{{-- @if (!have_posts())
  <div class="alert alert-warning">
    {{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}
  </div>
  {!! get_search_form(false) !!}
@endif --}}
