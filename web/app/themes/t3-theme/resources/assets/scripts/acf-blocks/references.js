import BeerSlider from 'beerslider/dist/BeerSlider';

(function($){

  /**
  * initializeBlock
  *
  * Adds custom JavaScript to the block HTML.
  *
  * @date    15/4/19
  * @since   1.0.0
  *
  * @param   object $block The block jQuery element.
  * @param   object attributes The block attributes (only available when editing).
  * @return  void
  */

  function initBeerSlider() {
    $.fn.BeerSlider = function( options ) {
      options = options || {};
      return this.each( function () {
        new BeerSlider( this, options );
      });
    };
    $( '.beer-slider' ).each( function( index, el ) {
      console.log('BEERSLIDER START');
      $( el ).BeerSlider( {start: $( el ).data( 'start' ) } )
    });
  }

  $(window).on('load', function() {
    $.ajax({
      url : t3_loadmore_params.ajaxurl,
      data : 'action=firstload&blocktype=' + $('input[name="blocktype"]').val(), // form data
      dataType : 'json', // this data type allows us to receive objects from the server
      type : 'POST',
      success : function( data ){

        t3_loadmore_params.current_page = 1;
        t3_loadmore_params.posts = data.posts;
        t3_loadmore_params.max_page = data.max_page;
        // insert the posts to the container
        $('#ref_content').html(data.content);
        // hide load more button, if there are not enough posts for the second page
        if ( data.max_page < 2 ) {
          $('#loadmore').hide();
        } else {
          $('#loadmore').show();
        }

        initBeerSlider();
      },
    });

    /*
    * Load More
    */
    $(document).on('click', '#loadmore' , function() {
      $.ajax({
        url : t3_loadmore_params.ajaxurl, // AJAX handler
        data : {
          'action': 'loadmorebutton', // the parameter for admin-ajax.php
          'blocktype': $('input[name="blocktype"]').val(),
          'query': t3_loadmore_params.posts, // loop parameters passed by wp_localize_script()
          'page' : t3_loadmore_params.current_page, // current page
        },
        type : 'POST',
        beforeSend : function ( xhr ) {
          $('#loadmore').text( t3_loadmore_params.loadingText ); // some type of preloader
        },
        success : function( posts ){
          if( posts ) {

            $('#loadmore').text( t3_loadmore_params.moreText );
            $('#ref_content').append( posts ); // insert new posts
            t3_loadmore_params.current_page++;

            if (t3_loadmore_params.current_page == t3_loadmore_params.max_page )
            $('#loadmore').hide(); // if last page, HIDE the button

          } else {
            $('#loadmore').hide(); // if no data, HIDE the button as well
          }
        },
        complete : function() {
          initBeerSlider();
        },

      });
      return false;
    });



    /*
    * Filter
    */
    $('#ref_filter').on('change', function(){
      $.ajax({
        url : t3_loadmore_params.ajaxurl,
        data : $('#ref_filter').serialize() + '&blocktype=' + $('input[name="blocktype"]').val(), // form data
        dataType : 'json', // this data type allows us to receive objects from the server
        type : 'POST',
        beforeSend : function(xhr){
          $('#message').text( t3_loadmore_params.loadingText );
        },
        success : function( data ){

          // when filter applied:
          // set the current page to 1
          t3_loadmore_params.current_page = 1;

          // set the new query parameters
          t3_loadmore_params.posts = data.posts;

          // set the new max page parameter
          t3_loadmore_params.max_page = data.max_page;

          // change the button label back
          //$('#ref_filter').find('button').text('Suche starten');

          // insert the posts to the container
          $('#ref_content').html(data.content);
          $('#message').text( '' );

          // hide load more button, if there are not enough posts for the second page
          if ( data.max_page < 2 ) {
            $('#loadmore').hide();
          } else {
            $('#loadmore').show();
          }

          initBeerSlider();
        },
      });

      // do not submit the form
      return false;

    });
  });
  // $(document).ready(function(){
  //   console.log('REFERENCES_LIST');
  //
  //
  //     function loadRefs() {
  //
  //     }
  //
  //     loadRefs();
  //
  //
  //   });

  // });


})(jQuery);
