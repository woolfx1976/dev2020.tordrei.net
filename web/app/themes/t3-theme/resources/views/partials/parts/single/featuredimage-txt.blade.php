<header class="c-fh c-fh--text none">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell small-12 medium-8 medium-offset-1">
        <div class="c-fh__wrapper">
          <h1>{!! App::title() !!}</h1>
          @if(!is_search())
            {!! App::breadcrumbNav() !!}
          @endif
        </div>
      </div>
    </div>
  </div>
</header>
