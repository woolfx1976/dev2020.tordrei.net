{{--
Name: featured-image
Title: Seitenkopf
Description: Hero-Image, Video
Category: t3layout
Icon: align-center
Keywords: page header
Mode: view
Align: wide
PostTypes: page post product
SupportsMode: true
SupportsMultiple: false
--}}

<?php

$carousel = array();
$classes = '';
$attr = '';
$showBanner = false;
$offsetClass = ' medium-offset-1';
$fhHeight = '';
$homeurl = App::homeurl();
$GLOBALS['firstImage'] = '';
$GLOBALS['fh_imgUrls'] = '';

/** Header-Design */

get_field('header_content_design') == 'div' ? $headerContentDesign = true : $headerContentDesign = false;

if($block['data']['header_type'] == 'text') {
  $classes .= ' c-fh--text';
  $block['data']['header_breadcrumb'] = get_field('header_breadcrumb');
}
else {
  $section_props = get_field('header_section_props');
  $fhHeight = $section_props['header_section_height'];
  $classes .= ' ' . $fhHeight;
}

if($block['data']['header_type'] == 'slider') {
  $showBanner = get_field('header_banner');
  $classes .= " c-fh--hero slider-fh";
  $sliderProps = get_field('header_carousel_props');

  if(isset($sliderProps)) {
    if($sliderProps['has_nav'] !== true) {
      $classes .= " no-nav";
    }
    if($sliderProps['has_dots'] !== true) {
      $classes .= " no-dots";
    }
    if($sliderProps['is_overlay'] === true) {
      $classes .= " dots-overlay";
    }
    $classes .= " dots-align-" . $sliderProps['dots_align'];

    if(!empty($sliderProps['text_offset'])) {
      $offsetClass = ' medium-offset-' . $sliderProps['text_offset'];
    }
    elseif($sliderProps['text_offset'] == '0') {
      $offsetClass = '';
    }
  }

  /** Carousel / Slider */

  /* Items --- */
  if(get_field('header_carousel_type') == 'items') {

    if(get_field('header_carousel')) {
      $counter = 0;
      foreach(get_field('header_carousel') as $item) {
        $props = $item['props'];
        $classBG = ' ' . $props['bg'];
        $style= 'style="background-position: ' . $props['bg_x'] . ' ' . $props['bg_y'] . '"';
        $image = $item['img'];
				!empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);


        // Preload first image ---
        if( $counter == 0) {
          $GLOBALS['firstImage'] = $homeurl . $image['sizes']['fp-large'];
          $GLOBALS['fh_imgUrls'] = $homeurl . $image['sizes']['fp-medium'].' 640w, ' . $homeurl . $image['sizes']['fp-large'] . ' 1024w, ' . $homeurl . $image['sizes']['fp-mlarge'] . ' 1500w';

          if(!is_admin()):
            add_action('wp_head', function() {
              echo '<link rel="preload" as="image" href="' . $GLOBALS['firstImage'] .'" imagesrcset="' . $GLOBALS['fh_imgUrls'] . '" imagesizes="80vw" />';
            }, 5, 1);
          endif;
        }
        $counter++;

        $carousel[] = \App\template('partials.parts.items.fhcarousel-item', [
          'sliderType' => 'item',
          'offsetClass' => $offsetClass,
          'counter' => $counter,
          'title' => $item['title'],
          'text' => $item['text'],
          'link' => $item['link'],
          'classBG' => $classBG . ' lazyload',
          'style' => $style,
          'imgSmall' => $homeurl . $image['sizes']['fp-medium'],
          'srcSet' => $homeurl . $image['sizes']['fp-medium'].' 640w, ' . $homeurl . $image['sizes']['fp-large'] . ' 1024w, ' . $homeurl . $image['sizes']['fp-mlarge'] . ' 1500w, ' . $homeurl . $image['sizes']['fp-xlarge'],
          'showBanner' => $showBanner,
          'alt' => $alt,
        ]);
      }
    }
  }
  else {
    if(get_field('header_gallery')) {
      foreach(get_field('header_gallery') as $image) {
        !empty($image['alt']) ? $alt = $image['alt'] : $alt = App::getImgFilename($image['id']);

        $carousel[] = \App\template('partials.parts.items.fhcarousel-item', [
          'sliderType' => 'gallery',
          'offsetClass' => $offsetClass,
          'title' => '',
          'text' => '',
          'link' => '',
          'classBG' => ' lazyload',
          'style' => '',
          'imgSmall' => $homeurl . $image['sizes']['fp-medium'],
          'srcSet' => $homeurl . $image['sizes']['fp-medium'].' 640w, ' . $homeurl . $image['sizes']['fp-large'] . ' 1024w, ' . $homeurl . $image['sizes']['fp-mlarge'] . ' 1500w, ' . $homeurl . $image['sizes']['fp-xlarge'],
          'showBanner' => false,
          'alt' => $alt,
        ]);
      }
    }
  }
}
else {
  /** Featured Image */
  if($block['data']['header_type'] == 'fh') {
    $classes .= ' c-fh--hero';
    $overlayProps = get_field('header_overlay_props');
    if($overlayProps) {
      if($overlayProps['header_overlay_type'] != 'none' && $overlayProps['header_overlay_type'] != 'solid') {
        $classes .= ' '. $overlayProps['header_overlay_type'];
      }
    }

    // FH-Image ---
    // TODO - Check if bg image is still necessary 
    $bgImage = get_field('header_img');
    if(!$bgImage) {
      $classes .= ' lazyload';
      //$GLOBALS['firstImage'] = $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-medium');
      $block['data']['srcset'] = $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-small') .' 640w, ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-medium') . ' 768w, ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-large') . ' 1200w, ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-mlarge') . ' 1500w, ' . $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-xlarge');
      $block['data']['imgsmall'] = $homeurl . get_the_post_thumbnail_url(get_the_ID(), 'fh-small');

      $thumbnail_id = get_post_thumbnail_id( get_the_ID() );
			$block['data']['alt'] = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
			if(empty($alt)) { $block['data']['alt'] = App::getImgFilename($thumbnail_id); }
      //$block['data']['alt'] = $bgImage['alt'];

      $GLOBALS['firstImage'] = $block['data']['imgsmall'];
      $GLOBALS['fh_imgUrls'] = $block['data']['srcset'];
    }
    else {
      $classes .= ' lazyload';
      //$GLOBALS['firstImage'] = $homeurl . $bgImage['sizes']['fh-medium'];
      //$headerImg = ' data-bgset="' . $homeurl . $bgImage['sizes']['fh-small'] .' [(max-width: 640px)] | ' . $homeurl . $bgImage['sizes']['fh-medium'] . ' [(max-width: 768px)] | ' . $homeurl . $bgImage['sizes']['fh-large']. ' [(max-width: 1200px)] | ' . $homeurl . $bgImage['sizes']['fh-mlarge'] . ' [(max-width: 1500px)] | ' . $homeurl . $bgImage['sizes']['fh-xlarge'] . '" data-sizes="auto"';
      $block['data']['srcset']  = $homeurl . $bgImage['sizes']['fh-small'] .' 640w, ' . $homeurl . $bgImage['sizes']['fh-medium'] . ' 768w, ' . $homeurl . $bgImage['sizes']['fh-large'] . ' 1200w, ' . $homeurl . $bgImage['sizes']['fh-mlarge'] . ' 1500w, ' . $homeurl . $bgImage['sizes']['fh-xlarge'];
      $block['data']['imgsmall'] = $homeurl . $bgImage['sizes']['fh-small'];
      !empty($bgImage['alt']) ? $block['data']['alt'] = $bgImage['alt'] : $block['data']['alt'] = App::getImgFilename($bgImage['id']);
      //$block['data']['alt'] = $bgImage['alt'];
      $GLOBALS['firstImage'] = $block['data']['imgsmall'];
      $GLOBALS['fh_imgUrls'] = $block['data']['srcset'];
    }

    if(!is_admin()) {
      add_action('wp_head', function() {
        echo '<link rel="preload" as="image" href="' . $GLOBALS['firstImage'] .'"  imagesrcset="' . $GLOBALS['fh_imgUrls'] . '" />';
      }, 5, 1);
    }
  }

  /** Get header content */
  $block['data']['header_content'] = '';
  $block['data']['header_teaser'] = '';
  if(get_field('header_content_type') == 'fields') {

    if(!empty(get_field('header_title'))) {
      $block['data']['header_content'] = '<h1>' . get_field('header_title') . '</h1>';
    }
    else {
      $block['data']['header_content'] = '<h1>' . get_the_title() . '</h1>';
    }

    if($headerContentDesign) {
      if(!empty(get_field('header_teaser'))) {
        $block['data']['header_teaser'] .= '<p>' . get_field('header_teaser') . '</p>';
      }
    }
    else {
      $block['data']['header_content'] .= '<p>' . get_field('header_teaser') . '</p>';
    }

    // if(!empty(get_field('header_topic'))):
    //   $block['data']['header_content'] .= '<p class="topic">' . get_field('header_topic') . '</p>';
    // endif;
  }
  else {
    $block['data']['header_content'] = get_field('header_editor');
  }
}

if($headerContentDesign) {
  $classes .= ' c-fh--div';
}
if($block['data']['header_max_width']) {
  $classes .= ' grid-container';
}
if(!empty($block['banner']['img'])) {
  $bannerImg = $block['banner']['img'];

  if(!empty($block['banner']['link'])) {
    $bannerLink = $block['banner']['link'];
  }
}
?>
{{-- <header data-{{ $block['id'] }} class="c-fh {{$classes}} none"{!!$attr!!}> --}}

<header data-{!! $block['id'] !!} class="c-fh {!! $classes !!} none">
  @if($block['data']['header_type'] == 'slider')
    @foreach ($carousel as $value)
      {!! $value !!}
    @endforeach
  @elseif($block['data']['header_type'] == 'fh')
    @include('partials.parts.featured-image.hero-image')
  @else
    @include('partials.parts.featured-image.text')
  @endif
  @if($showBanner && !empty($block['banner']['img']))
    <div class="c-banner">
      <div class="grid-container">
        <div class="cell small-12 medium-10 medium-offset-1">
          @if(isset($bannerLink['url']))
            <a href="{!! $bannerLink['url'] !!}" class="none">
              <img src="{!! $bannerImg['url'] !!}" class="c-banner__img"@if(isset($block['data']['alt'])) alt="{!! $block['data']['alt'] !!}"@endif width="270" height="270" />
            </a>
          @else
            <img src="{!! $bannerImg['url'] !!}" class="c-banner__img"@if(isset($block['data']['alt'])) alt="{!! $block['data']['alt'] !!}"@endif width="270" height="270" />
          @endif
        </div>
      </div>
    </div>
  @endif
</header>
@if(isset($block['data']['header_content_design']) && $block['data']['header_content_design'] == 'div')
  <section class="c-fh b-pt-medium b-pb-none is-style-bg-transparent">
    @include('partials.parts.featured-image.text')
  </section>
@endif
