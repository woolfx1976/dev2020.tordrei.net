<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleJobs extends Controller
{
    /** Get job data */
    public function jobTitle()
    {
        return get_field('job_title');
    }

    public function jobShortDescr()
    {
        return get_field('job_short_descr');
    }

    public function jobDescr()
    {
        return get_field('job_descr');
    }

    public function jobTasks()
    {
        return get_field('job_tasks');
    }

    public function jobSkills()
    {
        return get_field('job_skills');
    }

    public function jobOffer()
    {
        return get_field('job_offer');
    }

    public function jobPensum()
    {
        return get_field('job_pensum');
    }

    public function jobStart()
    {
        return get_field('job_start');
    }

    public function jobBranche()
    {
        return get_field('job_branche');
    }

    public function jobLocation()
    {
        return get_field('job_location');
    }

    public function jobSalary()
    {
        return get_field('job_salary');
    }

    public function jobContact()
    {
        return get_field('job_contact');
    }

    public function jobVideoId()
    {
        return get_field('jobs_video_id');
    }

    public function jobBenefitsLink()
    {
        return get_field('job_benefits_link');
    }

    public function jobShowBenefits()
    {
        return get_field('job_show_benefits');
    }

    public function jobFormId()
    {
        return get_field('job_form_id');
    }

    /** Set job headings */
    public function jobHeadings()
    {
        $headings = array(
            'descr' => __('Beschreibung', 'leeb'),
            'tasks' => __('Zuständigkeiten / Hauptaufgaben', 'leeb'),
            'skills' => __('Qualifikationen / Anforderungen', 'leeb'),
            'offer' => __('Wir bieten', 'leeb'),
        );
        return $headings;
    }

    /** Set icons */
    public function jobSidebar()
    {
        $sidebar = array(
            array(
                'icon' => 'clock',
                'title' => __('Arbeitsmodell', 'leeb'),
                'value' => get_field('job_pensum'),
                'type' => '',
            ),
            array(
                'icon' => 'calendar',
                'title' => __('Start Anstellung', 'leeb'),
                'value' => get_field('job_start'),
                'type' => '',
            ),
            array(
                'icon' => 'tool',
                'title' => __('Industrie / Gewerbe', 'leeb'),
                'value' => get_field('job_branche'),
                'type' => '',
            ),
            array(
                'icon' => 'pin',
                'title' => __('Arbeitsort', 'leeb'),
                'value' => get_field('job_location'),
                'type' => '',
            ),
            array(
                'icon' => 'money',
                'title' => __('Basislohn', 'leeb'),
                'value' => get_field('job_salary'),
                'type' => '',
            ),
            array(
                'icon' => 'document-2',
                'title' => __('PDF-Download', 'leeb'),
                'value' => '',
                'type' => 'link',
            ),
        );

        return $sidebar;
    }

    /** Get contact form */
    public function contactData()
    {
        $contact = array();

        if (!empty(get_field('contact_title'))) {
            $contact['title'] = get_field('contact_title');
        }
        if (!empty(get_field('contact_text'))) {
            $contact['text'] = get_field('contact_text');
        }
        if (!empty(get_field('contact_sidebar'))) {
            $contact['sidebar'] = get_field('contact_sidebar');
        }
        return $contact;
    }

    /** Get benefits icon list */
    public function benefits()
    {
        $homeurl = App::homeurl();
        $cells = get_field('list_column_props');
        $benefits['iconItems'] = array();
        // print_r($cells);
        if(is_array($cells)) {
          $benefits['cellClasses'] = ' small-' . $cells['small'] . ' medium-' . $cells['medium'] . ' large-' . $cells['large'];
        }
        $itemsProps = get_field('list_items_props');
        if (isset($itemsProps)) {
            $itemsAlign = $itemsProps['list_items_align'];
        }
        isset($itemsAlign) ? $benefits['itemAlign'] = $itemsAlign : $benefits['itemAlign'] = 'justify';

        if (have_rows('list_items')) :
            while (have_rows('list_items')) : the_row();

                $image = get_sub_field('list_item_img');
                if ($image) {
                    $imageSmall = $image['sizes']['fp-small'];
                    $width = $image['sizes']['fp-small-width'];
                    $height = $image['sizes']['fp-small-height'];
                    $alt = $image['alt'];
                } else {
                    $imageSmall = $width = $height = $alt = '';
                }

                $link = get_sub_field('list_item_link');
                $link ? $link : $link = '';

                $benefits['iconItems'][] = \App\template('partials.parts.items.iconlist-item', [
                    'iconName' => get_sub_field('list_item_icon_jobs'),
                    'title' => get_sub_field('list_item_title'),
                    'content' => get_sub_field('list_item_content'),
                    'imageSmall' => $homeurl . $imageSmall,
                    'link' => $link,
                    'alt' => $alt,
                    'width' => $width,
                    'height' => $height,
                ]);
            endwhile;
        endif;

        return $benefits;
    }

    /** Get benefits carousel */
    public function benefitsCarousel()
    {
        $homeurl = App::homeurl();
        $carousel = get_field('carousel_items');
        $benefits_carousel = array();

        if(is_array($carousel)) {
			foreach( $carousel as $item ) {
				$image = $item['list_items_img'];
				$link = $item['list_items_link'];
                $title = $item['list_items_title'];
                $text = $item['list_items_text'];

                if($image) {
                    $benefits_carousel['carouselItems'][] = \App\template('partials.parts.items.carousel-item-basic', [
                        // 'type' => $block['carouselDesign'],
                        // 'counter' => $counter++,
                        'link' => $link,
                        'alt' => $image['alt'],
                        'title' => $title,
                        'text' => $text,
                        'srcSet' => $homeurl . $image['sizes']['18to1-small'] . ' 640w, '
                            . $homeurl . $image['sizes']['18to1-medium'] . ' 1024w, '
                            . $homeurl . $image['sizes']['18to1-large'] . ' 1200w, ',
                        'sizes' => '(max-width: 640px) 640px, (max-width: 1024px) 1024px, (max-width: 1200px) 1200px',
                        'src' => $image['sizes']['18to1-small'],
                        'width' => '640',
                        'height' => '342',
                    ]);
                }
            }
        }

        return $benefits_carousel;
    }
}
