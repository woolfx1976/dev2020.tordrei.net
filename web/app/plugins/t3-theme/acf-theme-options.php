<?php
/**
* ACF-Option-Pages
*/
add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {
	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page(array(
			'page_title' 	=> 'Allgemeine Einstellungen',
			'menu_title'	=> 'Theme-Settings',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Header-Einstellungen',
			'menu_title'	=> 'Header',
			'parent_slug'	=> 'theme-general-settings',
		));
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Footer-Einstellungen',
			'menu_title'	=> 'Footer',
			'parent_slug'	=> 'theme-general-settings',
		));
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Leeb-Einstellungen',
			'menu_title'	=> 'Leeb',
			'parent_slug'	=> 'theme-general-settings',
		));
		// acf_add_options_sub_page(array(
		// 	'page_title' 	=> 'Theme CTA Settings',
		// 	'menu_title'	=> 'CTAs',
		// 	'parent_slug'	=> 'theme-general-settings',
		// ));
		// acf_add_options_sub_page(array(
		// 	'page_title' 	=> 'Theme CPTs Settings',
		// 	'menu_title'	=> 'CPTs',
		// 	'parent_slug'	=> 'theme-general-settings',
		// ));
	}
}
