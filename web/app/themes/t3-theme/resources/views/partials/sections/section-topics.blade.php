@if(isset($topics['card_items']) && count($topics['card_items']) > 0)
  <section class="wp-block-cgb-block-t3-section-block b-pt-none b-pb-small is-style-bg-transparent">
    @if(is_singular('product') && $topics['showStdIntro'] == '1')
      <div class="wp-block-cgb-block-t3-grid-block b-has-offset-b" style="background-color:#F5F5F5">
        <div class="has-padding none grid-container-wrapper">
          <div class="grid-container">
            <div class="grid-x">
              <div class="cell small-12 medium-9 medium-offset-1">
                <h2 class="h2ash1">{!!$topics['titleDefault']!!}</h2>
                {!!$topics['contentDefault']!!}
              </div>
            </div>
          </div>
        </div>
      </div>
    @else
      @if( !empty($topics['content']) || !empty($topics['title']) )
        <div class="wp-block-cgb-block-t3-grid-block b-has-offset-b" style="background-color:#F5F5F5">
          <div class="has-padding none grid-container-wrapper">
            <div class="grid-container">
              <div class="grid-x">
                <div class="cell small-12 medium-9 medium-offset-1">
                  <h2 class="h2ash1">{!!$topics['title']!!}</h2>
                  {!!$topics['content']!!}
                </div>
              </div>
            </div>
          </div>
        </div>
      @endif
    @endif
    <div class="wp-block-cgb-block-t3-grid-block">
      <div class="grid-container">
        <div class="grid-x">
          @if($topics['design'] == 'masonry')
            <div class="cell small-12 medium-10 medium-offset-1">
              <ul class="c-list masonry">
                <div class="masonry__sizer"></div>
                <div class="masonry__gutter-sizer"></div>
          @else
            <div class="cell small-12">
              <ul class="c-list b-cards b-cards--offset grid-x grid-margin-x">
          @endif
              @if(isset($topics['card_items']) && count($topics['card_items']) > 0)
                @foreach ($topics['card_items'] as $value)
                  {!! $value !!}
                @endforeach
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
@endif

@php
    /**
     * Update product fields ---
     **/
    if(isset($_GET['updatefields']) && $_GET['updatefields'] == '1'):
      $args = array(
        'post_type' => 'product', /* product post type */
        'posts_per_page' => -1 /* all products */
      );

      $products = new WP_Query( $args );

      // check if products exists
      if( $products->have_posts() ) {

        // loop products
        while( $products->have_posts() ): $products->the_post();

        // Topics checkboxes
        // $new_value = 1;
        //
        // if ( $new_value !== get_field( 'topics_show_std_text' ) ) {
        //     update_field( 'topics_show_std_text', $new_value ); /* update field */
        // }
        //
        // if ( $new_value !== get_field( 'topics_show_std_topics' ) ) {
        //     update_field( 'topics_show_std_topics', $new_value ); /* update field */
        // }

        // Topics Design
        $new_value = 'cards';

        if ( $new_value !== get_field( 'topics_design' ) ) {
            update_field( 'topics_design', $new_value ); /* update field */
        }

        endwhile;

        // reset query to default query
        wp_reset_postdata();
      }
    endif;
@endphp
