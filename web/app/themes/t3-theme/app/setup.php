<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

add_action('wp_ajax_wpml_get_files_to_scan', function () {
  if ($_POST['theme'] === 't3-theme/resources') {
    $_POST['theme'] = 't3-theme';
  }
}, 0);

define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);

/**
* Theme assets
*/
add_action( 'enqueue_block_editor_assets', function() {
  wp_enqueue_style( 'sage/editor.css', asset_path('styles/editor.css'), false, null );
});

add_action('wp_enqueue_scripts', function () {
  wp_dequeue_style( 'wp-block-library' );
  wp_dequeue_style( 'wp-block-library-theme' );
  wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS

  if ( ! is_user_logged_in() ) {
    wp_deregister_style( 'dashicons' );
  }

  //$theme_fonts_google = get_field('theme_fonts_google','options');
  // if (!empty($theme_fonts_google)) {
  //   $url_param = 'https://fonts.googleapis.com/css?family='.$theme_fonts_google;
  //   wp_enqueue_style( 'goolge-webfonts', $url_param, array(), '1', false );
  // }

  if(is_front_page()) {
    if(!empty(get_field('op_fb_country_verification_code', 'options'))) {
      echo '<meta name="facebook-domain-verification" content="' . get_field('op_fb_country_verification_code', 'options') . '" />';
    }
  }

  if(((is_singular() && (!is_404() && !is_search() )) || is_home()) && !is_category() ) {
    echo '<link rel="canonical" href="' . get_permalink( get_the_ID() ).'" />';
  }
  else if( is_category()) {
    $category = get_queried_object();
    $url = \App::homeurl() . esc_url( get_category_link( $category->term_id ) );
    echo '<link rel="canonical" href="' . $url .'" />';
  }

  echo '<link rel="preload" as="font" href="' . asset_path('fonts/roboto-v30-latin-300.woff2') .'" type="font/woff2" crossorigin />';
  echo '<link rel="preload" as="font" href="' . asset_path('fonts/roboto-v30-latin-regular.woff2') .'" type="font/woff2" crossorigin />';
  echo '<link rel="preload" as="font" href="' . asset_path('fonts/roboto-v30-latin-500.woff2') .'" type="font/woff2" crossorigin />';

  echo '<link rel="preload" as="style" href="' . asset_path('styles/main.css') .'"  />';
  echo '<link rel="preload" as="script" href="' . asset_path('scripts/main.js') .'"  />';
  echo '<link rel="preload" as="font" href="' . asset_path('fonts/icomoon.woff2') .'" type="font/woff2" crossorigin />';

  // $theme_fonts_adobe = get_field('theme_fonts_adobe','options');
  // if (!empty($theme_fonts_adobe)) {
  //     wp_enqueue_style( 'adobe-webfonts', $theme_fonts_adobe, array(), '1', 'all' );
  // }

  wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
  wp_enqueue_script( 'dpe', 'https://unpkg.com/default-passive-events', array(), null, true);
  wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), array('jquery','megamenu'), null, true);
  wp_enqueue_script( 'ws-form-cookies', \App\asset_path('scripts/ws-form.js'), array(), '1.0.0', false);


  // if (is_single() && comments_open() && get_option('thread_comments')) {
  //     wp_enqueue_script('comment-reply');
  // }

  if ( has_block( 'acf/accordions' ) || has_block( 'acf/accordions' ) ) {
    wp_enqueue_style( 'accordions', \App\asset_path('styles/block-accordions.css'), array(), '1.0.0');
    wp_enqueue_script( 'accordions', \App\asset_path('scripts/block-accordions.js'), array(), '1.0.0', true);
  }

  if ( has_block( 'acf/carousel' ) || has_block( 'acf/featured-image' ) || is_singular('product') || is_singular('jobs') ) {
    wp_enqueue_style( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '1.8.1');
    wp_enqueue_style( 'slick-theme', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css', array(), '1.8.1');
    wp_enqueue_script('slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', ['jquery'], '1.8.1');
  }

  if ( has_block( 'acf/carousel' ) || is_singular('product') || is_singular('jobs') || has_block( 'acf/featured-image' ) ) {
    wp_enqueue_style( 'carousel', \App\asset_path('styles/block-carousel.css'), array(), '1.0.0');
    wp_enqueue_script( 'carousel', \App\asset_path('scripts/block-carousel.js'), array(), '1.0.0', true);
  }

  if ( has_block( 'acf/featuredimage' ) || has_block( 'acf/featured-image' ) ) {
    wp_enqueue_script( 'header-carousel', \App\asset_path('scripts/block-header.js'), array(), '1.0.0', true);
  }

  if ( has_block( 'acf/hero' ) ) {
    wp_enqueue_style( 'hero', \App\asset_path('styles/block-hero.css'), array(), '1.0.0');
  }

  if ( has_block( 'acf/person' ) ) {
    wp_enqueue_style( 'person', \App\asset_path('styles/block-person.css'), array(), '1.0.0');
  }

  if ( has_block( 'acf/splitscreen' ) ) {
    wp_enqueue_style( 'splitscreen', \App\asset_path('styles/block-splitscreen.css'), array(), '1.0.0');
  }

  if ( has_block( 'acf/jobs' ) || has_block( 'acf/blog' ) ) {
    wp_enqueue_style( 'jobs', \App\asset_path('styles/block-jobs.css'), array(), '1.0.0');
  }

  // if ( has_block( 'acf/blog' ) ) {
  //     global $wp_query;
  //     wp_enqueue_script( 'blog-filter', \App\asset_path('scripts/block-blog.js'), array(), '1.0.0');
  //
  //     $my_current_lang = apply_filters( 'wpml_current_language', NULL );
  //     $ajax_blog_params = array(
  //         'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php?wpml_lang=' . $my_current_lang, // WordPress AJAX
  //         'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
  //         'current_page' => $wp_query->query_vars['paged'] ? $wp_query->query_vars['paged'] : 1,
  //         'cat' => $wp_query->query_vars['cat'],
  //         'max_page' => $wp_query->max_num_pages,
  //         // 'loadingText' => __('Wird geladen...', 't3-theme'),
  //         // 'moreText' => __('Mehr anzeigen', 't3-theme'),
  //         // 'filterText' => __('Wird gefiltert...', 't3-theme'),
  //         'wpml_lang' => $my_current_lang,
  //     );
  //
  //     wp_localize_script( 'blog-filter', 't3_blog_params', $ajax_blog_params );
  // }

  if ( is_singular('product') || has_block( 'acf/gallery') ) {
    wp_enqueue_style( 'lightgallery', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css', array(), '1.6.12');
    wp_enqueue_script( 'lightgallery', \App\asset_path('scripts/block-lightgallery.js'), array(), '1.6.12', true);
  }

  if( has_block( 'acf/references' ) ) {
    global $wp_query;

    wp_enqueue_script( 'ref_scripts', \App\asset_path('scripts/block-references.js'), array(), '1.0.0', true);
    wp_enqueue_style( 'references', \App\asset_path('styles/block-references.css'), array(), '1.0.0');

    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
    $ajax_params = array(
      'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php?wpml_lang=' . $my_current_lang, // WordPress AJAX
      'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
      'current_page' => $wp_query->query_vars['paged'] ? $wp_query->query_vars['paged'] : 1,
      'max_page' => $wp_query->max_num_pages,
      'loadingText' => __('Wird geladen...', 't3-theme'),
      'moreText' => __('Mehr anzeigen', 't3-theme'),
      'filterText' => __('Wird gefiltert...', 't3-theme'),
      'wpml_lang' => $my_current_lang,
    );

    if( has_block( 'acf/topics') || is_singular('product')) {
      //wp_enqueue_script( 'isotope', 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array(), '3.0.6', true);
      //wp_enqueue_script( 'topics', \App\asset_path('scripts/block-topics.js'), array(), '1.6.12', true);
    }

    wp_localize_script( 'ref_scripts', 't3_loadmore_params', $ajax_params );
  }

  if ( has_block( 't3/showhide-block' ) ) {
    wp_enqueue_script( 'showhide', \App\asset_path('scripts/block-showhide.js'), array(), '1.0.0', true);
    wp_enqueue_style( 'showhide', \App\asset_path('styles/block-showhide.css'), array(), '1.0.0');
  }

  if ( tribe_is_event() || tribe_is_event_category() || tribe_is_in_main_loop() || tribe_is_view() || 'tribe_events' == get_post_type() || is_singular( 'tribe_events' ) ) {
    wp_enqueue_style( 't3-tribe-events', \App\asset_path('styles/plugin-events-calendar-pro.css'), array(), '1.0.0');
  }
  else {
    wp_deregister_style( 'tribe-events-pro-views-v2-skeleton' );
    wp_deregister_style( 'tribe-events-pro-views-v2-full' );
    wp_deregister_style( 'tribe-events-views-v2-skeleton' );
    wp_deregister_style( 'tribe-events-views-v2-full' );
    wp_deregister_style( 'tribe-common-skeleton-style' );
    wp_deregister_style( 'tribe-common-full-style' );
    wp_deregister_script( 'tribe_events_google_maps_api' );
    wp_deregister_script( 'tribe-events-calendar-script' );
  }

}, 100);


add_action('admin_enqueue_scripts', function () {
  wp_register_style('sage/editor-main.css', \App\asset_path('styles/editor.css'), false, '1.0.0'); // styles intended for admin
  wp_enqueue_style( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '1.8.1',);
  wp_enqueue_style( 'slick-theme', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css', array(), '1.8.1');
  wp_enqueue_script('slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', ['jquery'], '1.8.1');
  wp_enqueue_style( 'carousel', \App\asset_path('styles/block-carousel.css'), array(), '1.0.0');
  wp_enqueue_script( 'carousel', \App\asset_path('scripts/block-carousel.js'), array(), '1.0.0');
  wp_enqueue_style( 'lightgallery', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css', array(), '1.6.12');
  wp_enqueue_script( 'lightgallery', \App\asset_path('scripts/block-lightgallery.js'), array(), '1.6.12');
  wp_enqueue_script( 'header-carousel', \App\asset_path('scripts/block-header.js'), array(), '1.0.0');
  wp_enqueue_script( 'isotope', 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array(), '3.0.6');
  wp_enqueue_script( 'topics', \App\asset_path('scripts/block-topics.js'), array(), '1.6.12');
  wp_enqueue_style( 'splitscreen', \App\asset_path('styles/block-splitscreen.css'), array(), '1.0.0');
});

/**
* Theme setup
*/
add_action('after_setup_theme', function () {

  /**
  * Enable features from Soil when plugin is activated
  * @link https://roots.io/plugins/soil/
  */
  add_theme_support('soil-clean-up');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-relative-urls');

  add_theme_support('wp-block-styles');
  add_theme_support('align-wide');
  add_theme_support('disable-custom-font-sizes');

  add_theme_support( 'responsive-embeds' );

  /**
  * Enable plugins to manage the document title
  * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
  */
  add_theme_support('title-tag');

  /**
  * Register navigation menus
  * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
  */
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'meta_navigation' => __('Meta Navigation', 'sage'),
    'mobile_nav' => __('Mobile Navigation', 'sage'),
    'footer_nav' => __('Footer Navigation', 'sage'),
  ]);

  /**
  * Enable post thumbnails
  * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
  */
  add_theme_support('post-thumbnails');

  /** Add featured image sizes
  *
  * Sizes are optimized and cropped for landscape aspect ratio
  * and optimized for HiDPI displays on 'small' and 'medium' screen sizes.
  */
  // add_image_size( 'featured-small', 640, 200, true ); // name, width, height, crop
  // add_image_size( 'featured-medium', 1280, 400, true );
  // add_image_size( 'featured-large', 1440, 400, true );
  // add_image_size( 'featured-xlarge', 1920, 400, true );

  /**
  * Add additional image sizes
  */
  add_image_size( 'fp-square', 640, 640, true );
  add_image_size( 'square-small', 240, 240, true );
  // add_image_size( 'fp-small-landscape', 640, 430, true );
  add_image_size( 'fp-small', 640 );
  add_image_size( 'fp-medium', 1024 );
  add_image_size( 'fp-large', 1300 );
  add_image_size( 'fp-mlarge', 1600 ); // Added 2021.04.19
  add_image_size( 'fp-xlarge', 1920 );
  add_image_size( 'fp-splitscreen', 680, 580, true );
  add_image_size( 'fp-splitscreen-large', 1360, 1160, true );

  /**
  * Leeb Sizes
  */
  add_image_size( 'fh-small', 640, 375, true );
  add_image_size( 'fh-medium', 1024, 375, true );
  add_image_size( 'fh-large', 1300, 433, true );
  add_image_size( 'fh-mlarge', 1600, 530, true );
  add_image_size( 'fh-xlarge', 1920, 600, true );

  add_image_size( '18to1-small', 640, 342, true );
  add_image_size( '18to1-medium', 1024, 548, true );
  add_image_size( '18to1-large', 1300, 694, true );
  // add_image_size( '18to1-xlarge', 1920, 1026, true );

  add_image_size( 'card-xsmall', 400, 270, true );
  add_image_size( 'card-small', 640, 432, true );
  add_image_size( 'card-medium', 1024, 691, true );
  add_image_size( 'card-large', 1200, 810, true );
  // add_image_size( 'card-xlarge', 1920, 1297, true );

  /**
  * Enable HTML5 markup support
  * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
  */
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  /**
  * Enable selective refresh for widgets in customizer
  * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
  */
  add_theme_support('customize-selective-refresh-widgets');

  /**
  * Use main stylesheet for visual editor
  * @see resources/assets/styles/layouts/_tinymce.scss
  */
  add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
* Register sidebars
*/
add_action('widgets_init', function () {
  $config = [
    'before_widget' => '<div class="idget %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ];
  $configSocial = [
    'before_widget' => '<div class="idget %1$s %2$s">',
    'after_widget'  => Controllers\App::getSocialLinks() . '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ];
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
      'name'          => __('Footer-1', 'sage'),
      'id'            => 'sidebar-footer-1',
      ] + $config);
      register_sidebar([
        'name'          => __('Footer-2', 'sage'),
        'id'            => 'sidebar-footer-2'
        ] + $config);
        register_sidebar([
          'name'          => __('Footer-3', 'sage'),
          'id'            => 'sidebar-footer-3'
          ] + $config);
          register_sidebar([
            'name'          => __('Footer-4', 'sage'),
            'id'            => 'sidebar-footer-4'
            ] + $configSocial);
          });

          /**
          * Updates the `$post` variable on each iteration of the loop.
          * Note: updated value is only available for subsequently loaded views, such as partials
          */
          add_action('the_post', function ($post) {
            sage('blade')->share('post', $post);
          });

          /**
          * Setup Sage options
          */
          add_action('after_setup_theme', function () {
            /**
            * Add JsonManifest to Sage container
            */
            sage()->singleton('sage.assets', function () {
              return new JsonManifest(config('assets.manifest'), config('assets.uri'));
            });

            /**
            * Add Blade to Sage container
            */
            sage()->singleton('sage.blade', function (Container $app) {
              $cachePath = config('view.compiled');
              if (!file_exists($cachePath)) {
                wp_mkdir_p($cachePath);
              }
              (new BladeProvider($app))->register();
              return new Blade($app['view']);
            });

            /**
            * Create @asset() Blade directive
            */
            sage('blade')->compiler()->directive('asset', function ($asset) {
              return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
            });

            /**
            * Create @startCont() Blade directive
            */
            sage('blade')->compiler()->directive('startCont', function () {
              $output = '<?php if($has_grid_container): ?>';
              $output .= '<?php echo "<div class=grid-container>"; ?>';
              $output .= '<?php endif; ?>';
              return $output;
            });

            /**
            * Create @endCont() Blade directive
            */
            sage('blade')->compiler()->directive('endCont', function () {
              $output = '<?php if($has_grid_container): ?>';
              $output .= '<?php echo "</div>"; ?>';
              $output .= '<?php endif; ?>';
              return $output;
            });

            /**
            * Create @homeurl Blade directive
            */
            sage('blade')->compiler()->directive('homeurl', function () {
              //$homeurl = 'https://mk0t3themefok4uwj9r3.kinstacdn.com/';
              /*$output = '<?php echo get_home_url(); ?>';*/
              $homeurl = ''; // testing optimize on staging
              return $homeurl;
            });


          });

          // Setup menu for option pages
          add_action('init', function () {
            if( function_exists('acf_add_options_sub_page') ) {
              acf_add_options_sub_page(array(
                'page_title' 	=> 'Vertriebspartner zuweisen',
                'menu_title'	=> 'Vertriebspartner zuweisen',
                'parent_slug'	=> 'edit.php?post_type=partner',
              ));

              acf_add_options_sub_page(array(
                'page_title' 	=> 'Vertriebspartner Optionen',
                'menu_title'	=> 'Vertriebspartner Optionen',
                'parent_slug'	=> 'edit.php?post_type=partner',
              ));
            }
          });

          // Add trusted shops badge / klickschutz
          add_action('wp_footer', function () {
            ?>
            <!-- Clickcease.com tracking--> <script type='text/javascript'>var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; var target = 'https://www.clickcease.com/monitor/stat.js'; script.src = target;var elem = document.head;elem.appendChild(script); </script> <noscript> <a href='https://www.clickcease.com' rel='nofollow'><img src='https://monitor.clickcease.com/stats/stats.aspx' alt='ClickCease'/></a> </noscript> <!-- Clickcease.com tracking-->
            <?php
            if(ICL_LANGUAGE_CODE && (ICL_LANGUAGE_CODE != 'it' && ICL_LANGUAGE_CODE != 'sl')) {
              if(get_field('op_ts_show_widget', 'options') == '1') { ?>
                <script type="text/javascript">
                (function () {
                  var _tsid = '<?php echo get_field('op_ts_id', 'options'); ?>';
                  _tsConfig = {
                    'responsive': {'variant':'floating', 'position':'right', 'yOffset':'40'},
                    'yOffset': '', /* offset from page bottom */
                    'variant': 'reviews', /* default, reviews, custom, custom_reviews */
                    'customElementId': '', /* required for variants custom and custom_reviews */
                    'trustcardDirection': '', /* for custom variants: topRight, topLeft, bottomRight, bottomLeft */
                    'customBadgeWidth': '', /* for custom variants: 40 - 90 (in pixels) */
                    'customBadgeHeight': '', /* for custom variants: 40 - 90 (in pixels) */
                    'disableResponsive': 'false', /* deactivate responsive behaviour */
                    'disableTrustbadge': 'false' /* deactivate trustbadge */
                  };
                  var _ts = document.createElement('script');
                  _ts.type = 'text/javascript';
                  _ts.charset = 'utf-8';
                  _ts.async = true;
                  _ts.src = '//widgets.trustedshops.com/js/' + _tsid + '.js';
                  var __ts = document.getElementsByTagName('script')[0];
                  __ts.parentNode.insertBefore(_ts, __ts);
                })();
                </script>
                
                <?php
              }
            }
          });

          // Add goole jobs schema
          add_action('wp_head', function () {

            if ( is_singular('jobs') && get_field('google-jobs') == '1') {

              global $post;
              $post_slug = $post->post_name;

              $jobTitle = get_field('job_title');
              $jobDescr = get_field('job_descr');
              if(!empty( get_field('job_tasks') )) {
                $jobDescr .= __('<p>Zuständigkeiten / Hauptaufgaben:', 'leeb') . '</br>' . get_field('job_tasks') . '</p>';
              }
              if(!empty( get_field('job_skills') )) {
                $jobDescr .=  __('<p>Qualifikationen:', 'leeb') . '</br>' . get_field('job_skills') . '</p>';
              }
              if(!empty( get_field('job_offer') )) {
                $jobDescr .=  __('<p>Wir bieten:', 'leeb') . '</br>' . get_field('job_offer') . '</p>';
              }

              $jobType = get_field('job_type');
              $jobPlace = get_field('job_place');
              $jobAddress = get_field('job_address');
              $jobZip = get_field('job_zip');
              $jobCountry = get_field('job_country');
              $jobState = get_field('job_state');
              $jobSalary = get_field('job_salary_gjobs');
              $jobValidThrough = get_field('job_valid_through');

              !empty($jobAddress) ? $address = $jobAddress : $address = '';
              //!empty($jobPlace) ? $place = ', ' . $jobPlace : $place = '';
              !empty($jobPlace) ? $place = $jobPlace : $place = '';
              !empty($jobZip) ? $zip = $jobZip : $zip = '';
              !empty($jobCountry) ? $country = $jobCountry : $country = '';
              !empty($jobState) ? $state = $jobState : $state = '';
              !empty($jobSalary) ? $salary = $jobSalary : $salary = '';
              !empty($jobValidThrough) ? $validThrough = $jobValidThrough : $validThrough = '';


              //get all html elements on a line by themselves
              $string_html_on_lines = str_replace (array("<",">"),array("\n<",">\n"), $jobDescr);
              //find lines starting with a '<' and any letters or numbers upto the first space. throw everything after the space away.
              $string_attribute_free = preg_replace("/\n(<[\w123456]+)\s.+/i","\n$1>", $string_html_on_lines);
              $jobDescr = $string_attribute_free;

              ?>

              <script type="application/ld+json">
              {
                "@context" : "https://schema.org/",
                "@type" : "JobPosting",
                "title" : "<?php echo $jobTitle; ?>",
                "description" : "<?php echo $jobDescr; ?>",
                "identifier": {
                  "@type": "PropertyValue",
                  "name": "LEEB Balkone GmbH",
                  "value": "<?php echo $post_slug; ?>"
                },
                "validThrough": "<?php echo $validThrough; ?>",
                "datePosted" : "<?php echo get_the_date('Y-m-d'); ?>",
                "employmentType" : "<?php echo $jobType; ?>",
                "hiringOrganization" : {
                  "@type" : "Organization",
                  "name" : "LEEB Balkone GmbH",
                  "sameAs" : "https://www.leeb.at",
                  "logo" : "https://www.leeb-balkone.com/app/uploads/2021/05/logo-jobs.png"
                },
                "jobLocation": {
                  "@type": "Place",
                  "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "<?php echo $address; ?>",
                    "addressLocality": "<?php echo $place; ?>",
                    "addressRegion": "<?php echo $jobState; ?>",
                    "postalCode": "<?php echo $zip; ?>",
                    "addressCountry": "<?php echo $country; ?>"
                  }
                },
                "baseSalary": {
                  "@type": "MonetaryAmount",
                  "currency": "EUR",
                  "value": {
                    "@type": "QuantitativeValue",
                    "value": "<?php echo $salary; ?>",
                    "unitText": "MONTH"
                  }
                }
              }
              </script>

              <?php
            }
          });
