import lightgallery from 'lightgallery';
import 'lg-thumbnail';
import 'lg-fullscreen';

export default {
  init() {

    $(document).ready(function() {
      // console.log('lg function');
      // // $('[id*=lightgallery-]').lightGallery();
      console.log('lg function');
      $('[id*=lightgallery-]').lightGallery({
        selector: '.b-cards__item a',
        thumbnail: true,
        fullscreen: true,
      });
    });

    // var initializeCarousel = function( $carousel ) {
    //   $carousel.find('.slider .slides').slick({
    //     dots: false,
    //     infinite: true,
    //     speed: 300,
    //     slidesToShow: 1,
    //     centerMode: true,
    //     variableWidth: true,
    //     adaptiveHeight: true,
    //     focusOnSelect: true,
    //   });
    // }

    // Initialize each block on page load (front end).
    $(document).ready(function(){
      console.log('singleProduct');
      $('.slider-fh').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        dots: true,
      });
    });

    $(window).on('load', function() {
      if ( $( '.masonry' ).length) {
        console.log('ISOTOPE');
        $('.masonry').isotope({
          itemSelector: '.masonry__item',
          percentPosition: true,
          masonry: {
            columnWidth: '.masonry__sizer',
            gutter: '.masonry__gutter-sizer',
          },
        });
      }
    });

  },
  finalize() {

  },
};
