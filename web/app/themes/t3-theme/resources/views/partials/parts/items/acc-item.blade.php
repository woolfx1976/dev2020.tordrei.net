<div class="b-acc__item {!! $bColor !!}">
    <div class="b-acc__title {!! $bColor !!}">
      @if($tag != 'b')
        @if($tag == 'h3-small')
          <h3><span class="h3ash4">{!! $title !!}</span></h3>
        @else
          <{{$tag}}>{!! $title !!}</{{$tag}}>
        @endif
      @else
        <p><b>{!! $title !!}</b></p>
      @endif
      <i class="icon icon-plus"></i>
    </div>
    <div class="b-acc__content">
        {!! $content !!}
    </div>
</div>