<li class="b-cards__item cell small-12 medium-6 {!! $cellSizes !!}">
    <div class="b-cards__header">
      <div>
        @if(!empty($link['url']))
          <a href="{!! $link['url'] !!}" class="hasHover">
            {{-- @if(!is_admin()) --}}
              <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" width="{!! $width !!}" height="{!! $height !!}" />
            {{-- @else
              <img src="{!! $imageSmall !!}" class="lazyload b-cards__img" />
            @endif --}}
          </a>
        @else
          <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" width="{!! $width !!}" height="{!! $height !!}" />
          {{-- @if(!is_admin())
            <img src="{!! $imageSmall !!}" alt="{!! $alt !!}" class="lazyload b-cards__img" />
          @else
            <img src="{!! $imageSmall !!}" class="lazyload b-cards__img" />
          @endif --}}
        @endif
      </div>
    </div>
    <div class="b-cards__body">
      @if(!empty($link['url']))
        <a href="{!! $link['url'] !!}" class="b-cards__title">
          <h3>{!! $title !!}</h3>
        </a>
      @else
        <h3 class="b-cards__title">{!! $title !!}</h3>
      @endif
      <p>{!! $content !!}</p>
      @if(!empty($link['url']))
      <a href="{!! $link['url'] !!}" target="{!! $link['target'] !!}" class="more">{!! $link['title'] !!}</a>
      @endif
    </div>
</li>
