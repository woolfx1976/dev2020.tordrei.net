<?php
/**
 * Advantages custom post type
 */
add_action('init', function () {
    $labels = array(
        'name'                => _x( 'Themen', 'Post Type General Name', 't3-theme' ),
		'singular_name'       => _x( 'Thema', 'Post Type Singular Name', 't3-theme' ),
		'menu_name'           => __( 'Themen', 't3-theme' ),
		'parent_item_colon'   => __( 'Eltern-Thema', 't3-theme' ),
		'all_items'           => __( 'Alle Themen', 't3-theme' ),
		'view_item'           => __( 'Thema zeigen', 't3-theme' ),
		'add_new_item'        => __( 'Neues Thema erstellen', 't3-theme' ),
		'add_new'             => __( 'Neues Thema', 't3-theme' ),
		'edit_item'           => __( 'Thema bearbeiten', 't3-theme' ),
		'update_item'         => __( 'Thema aktualisieren', 't3-theme' ),
		'search_items'        => __( 'Thema suchen', 't3-theme' ),
		'not_found'           => __( 'Nicht gefunden', 't3-theme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 't3-theme' ),
	);
    register_extended_post_type('topics', [
        'labels' => $labels,
        'menu_icon' => 'dashicons-pressthis',
        'show_in_rest' => false,
        'supports' => array('title','thumbnail','catgegories'),
        'has_archive' => false,
        'rewrite' => true
    ], [
        'singular' => 'Thema',
        'plural' => 'themen',
        'slug' => 'themen',
    ]);
    /**
	*   To Activate Custom Post Type Single page
	*   @see http://en.bainternet.info/2011/custom-post-type-getting-404-on-permalinks
	*/
	$set = get_option('post_type_rules_flased_authors');
	if ($set !== true){
		flush_rewrite_rules(false);
		update_option('post_type_rules_flased_authors',true);
	}
});
