@php the_content() @endphp
@if(is_singular('post'))
<section class="b-pt-none bg--white">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="cell small-12">
        {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
        <a href="javascript:history.back()" class="more">
          <span class="icon-arrow-left-line"></span> {{ _e('zurück zur Übersicht', 't3-theme')}}
        </a>
      </div>
    </div>
  </div>
</section>
@endif
