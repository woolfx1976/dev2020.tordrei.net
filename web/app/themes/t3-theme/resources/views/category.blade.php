@extends('layouts.category')

@section('content')
  @include('partials.content-category-blog')
  @include('partials.posts-nav')
@endsection
