{{-- @php
if(isset($_GET['job-type'])):
  $type = $_GET['job-type'];
@endphp --}}

@if($job_show_benefits == '1')

<section class="b-pt-none is-style-bg-@php if(empty($job_video_id)): echo 'lightgrey l-mbxl';else:echo'transparent';endif;@endphp">
  <div class="wp-block-cgb-block-t3-grid-block b-has-offset-h bg--lightgray">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          <h2>
            <?php _e('Benefits für Mitarbeiter', 'leeb'); ?>
          </h2>
        </div>
      </div>
    </div>
  </div>
  <div class="wp-block-cgb-block-t3-grid-block bg--lightgray @if (!empty($job_video_id)) b-has-offset-b @endif">
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class="cell small-12">
          <ul class="c-list b-iconlist grid-x align-{!! $benefits['itemAlign'] !!}">
            @php
              $cellSizes = $benefits['cellClasses'];
            @endphp
            @if(count((array)$benefits['iconItems']))
              @foreach ($benefits['iconItems'] as $item)
              <li class="b-iconlist__item {!! $benefits['cellClasses'] !!}">
                {!! $item !!}
              </li>
              @endforeach
            @endif
          </ul>
        </div>
        <div class="cell small-12 text-center btn-group">
          @if($job_benefits_link)
          <a href="{!! $job_benefits_link !!}" class="button hollow">Alle Leeb-Benefits</a>
          @endif
          <a href="#contact" class="button" data-smooth-scroll>Jetzt bewerben</a>
        </div>
      </div>
    </div>
  </div>

  @if (!empty($job_video_id))
  <div class="wp-block-cgb-block-t3-grid-block">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          <div class="video-container">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/{!! $job_video_id !!}"
              title="YouTube video player" frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen></iframe>
            {{-- <iframe id="ytplayer" type="text/html" width="640" height="360" src="http://www.youtube.com/embed/{!!$job_video_id!!}?autoplay=1" frameborder="0" /> --}}
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif

  {{-- <div class="wp-block-cgb-block-t3-grid-block">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          @if (!empty($job_contact))
          {!! $job_contact !!}
          @endif
        </div>
      </div>
    </div>
  </div> --}}
</section>

@endif
<section class="b-pt-none is-style-bg-transparent">
  <div class="wp-block-cgb-block-t3-grid-block">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-10 medium-offset-1">
          @if (!empty($job_contact))
          {!! $job_contact !!}
          @endif
        </div>
      </div>
    </div>
  </div>
</section>


