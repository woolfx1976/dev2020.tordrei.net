@if($adv_icon_data['show_section'])
  <section class="b-pt-none is-style-default">
    <div class="wp-block-cgb-block-t3-grid-block b-has-offset-h" style="border-top: 36px solid white;">
      <div class="grid-container">
        <div class="grid-x">
          <div class="cell small-12 medium-10 medium-offset-1">
            @if(empty($adv_icon_data['intro_title']))
              <h2><?php _e('Vorteile', 'leeb'); ?></h2>
            @else
              <h2>{{$adv_icon_data['intro_title']}}</h2>
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="wp-block-cgb-block-t3-grid-block">
      <div class="grid-container">
        <div class="grid-x">
          <div class="cell small-12 b-has-pt">
            <div class="l-wrapper">
              <ul class="c-list b-iconlist b-iconlist--adv grid-x">
                @if(isset($adv_icon_data['iconItems']) && count($adv_icon_data['iconItems']) > 0)
                  @foreach ($adv_icon_data['iconItems'] as $item)
                    <li class="b-iconlist__item {{$adv_icon_data['cellClasses']}}">
                      {!! $item !!}
                    </li>
                  @endforeach
                @endif
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endif
