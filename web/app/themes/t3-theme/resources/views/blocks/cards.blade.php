{{--
  Title: Cards
  Description: cards, CPTs
  Category: t3layout
  Icon: grid-view
  Keywords: card layout
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
  // $has_grid_container = true;
  // if(!is_admin()):
  //   $attributes = ' data-interchange="' . $block['header_img_interchange'] . '" style=""';
  // else:
  //   $attributes = ' style="background-image: url(' . $block['header_img']['sizes']['fp-large'] . ');"';
  // endif;
  // $block['classes'] = $block['background']->header_overlay_type . ' ' . $block['section']->header_section_height_class . ' ' . $block['classes'];
@endphp

{{-- <div class="l-wrapper"> --}}
@if(!empty($block['filter_nav']))
  {!! $block['filter_nav'] !!}
@endif
<ul data-{{ $block['id'] }} class="c-list b-cards b-cards--{{ $block['cardDesign'] }}{{ $block['filterClass'] }} grid-x grid-margin-x grid-margin-y {{$block['classes']}}">
  @if(isset($block['card_items']) && count($block['card_items']) > 0)
    @foreach ($block['card_items'] as $value)
      {!! $value !!}
    @endforeach
  @endif
</ul>
