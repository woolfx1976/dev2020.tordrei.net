{{--
  Title: Blog-Liste
  Description: blog, list
  Category: t3layout
  Icon: grid-view
  Keywords: list layout
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

<ul data-{{ $block['id'] }} id="blog-list" class="c-list {{ $block['listDesign'] }}">
  @if(isset($block['list_items']) && count($block['list_items']) > 0)
    @foreach ($block['list_items'] as $value)
      {!! $value !!}
    @endforeach
  @endif
</ul>

<div>
  <b><?php //echo 'Current Page: ' . $_POST['current_page'] . ' - ' . 'Max Page: ' . $_POST['max_page']; ?></b>
</div>

<div id="pagination-wrapper">
  {!! $block['pagination_html'] !!}
</div>
