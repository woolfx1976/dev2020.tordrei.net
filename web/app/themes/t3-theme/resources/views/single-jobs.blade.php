@extends('layouts.single')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-single-jobs')
  @endwhile
@endsection
