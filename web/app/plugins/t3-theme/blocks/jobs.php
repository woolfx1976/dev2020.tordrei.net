<?php

add_filter('sage/blocks/jobs/data', function (array $block) {
	$homeurl = App::homeurl();
	$block['listDesign'] = get_field('list_design');
	$select_type = get_field('select_type');
	$postType = 'jobs';
	$orderby = 'publish_date';
	$imgSize = '18to1-small';
	!empty(get_field('cpt_filter')) ? $block['filterClass'] = ' b-list--filter' : $block['filterClass'] = '';

	/** Jobs list */
	if(!is_admin()):
		$posts_per_page = -1;
	else:
		$posts_per_page = 3;
	endif;

	/** Get post by categories */
	if($select_type == 'cat') {
		$block['cats'] = get_field('categories');

		/** Set filter for jobs */
		// if($block['cats']) {
		//     $args = array(
		// 	  'hide_empty'=> 0,
		//       'taxonomy' => 'job_category',
		//       'orderby' => 'name',
		//       'order'   => 'ASC',
		//       'parent' => $block['cats'][0], // Fix offset
		//     );
		//
		// 	/** Create filter nav */
		//     $cats = get_categories($args);
		//
		// 	if(count($cats) > 0) {
		// 		$block['filter_nav'] .= '<ul class="c-list c-list--filter"><li><a href="#" class="filter-item active" data-filter="all">' . __('Alle anzeigen', 't3-theme') . '</a></li>';
		// 		foreach($cats as $cat) {
		// 		  if( $cat->parent != '0'):
		// 			  $block['filter_nav'] .= '<li><a href="#" class="filter-item" data-filter="f' . $cat->term_id . '">' . $cat->name . '</a></li>';
		// 		  endif;
		// 		}
		// 		$block['filter_nav'] .= '</ul>';
		// 	}
	  	// }

		if(!empty($block['cats'])) {
			$categories = array();

			foreach( $block['cats'] as $cat_id ):
				$categories .= $cat_id .',';
			endforeach;

			$query_array = array(
				'post_type' => array($postType), //array('product'), //
				'posts_per_page'=> $posts_per_page,
				// 'orderby' => $orderby,
				// 'order' => 'ASC',
				'meta_key'			=> 'job_order_date',
				'orderby'			=> 'meta_value',
				'order'				=> 'DESC',
				'catergory__in' => $categories,
			);

			$loop = new \WP_Query( $query_array );

			while ( $loop->have_posts() ) : $loop->the_post();
				// print_r($block['cats']);
				//echo '->' . get_the_ID();

				//$term_list = wp_get_post_terms( get_the_ID(), 'product_category', array( 'fields' => 'all' ) );
				//print_r( $term_list );
				// if(count($term_list) > 0) {
				// 	$filterIDs = ' all';
				// 	foreach($term_list as $category) {
				// 		foreach($block['cats'] as $parent) {
				// 			//echo '/' . $category->parent . ' - ' . $parent . '/';
				// 			if ($category->parent == $parent) {
				// 				//echo '###' . $category->term_id . ' ###';
				// 				$filterIDs .= ' f' . $category->term_id;
				// 			}
				// 		}
				// 	}
				// }

				$loc = get_field('job_location_short');
				!empty($loc) ? $pens = ' / ' . get_field('job_pensum') : $pens = get_field('job_pensum');
				$content = '' . __('Arbeitsort / Arbeitsmodell:', 't3-theme') . ' <b>' . $loc . $pens . '</b><br/>
				' . __('Start Anstellung:', 't3-theme') . ' <b>' . get_field('job_start') . '</b>';
				$moreLinkText = '' . __('Job ansehen', 't3-theme');

				//echo '->' . $filterIDs;
				$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
				$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
				if(empty($alt)) { $alt = App::getImgFilename($thumbnail_id); }

				// $block['teaser'][] = get_post_meta('post_teaser', $loop->id);
				$block['list_items'][] = \App\template('partials.parts.items.list-item-cpts', [
					// 'filterIDs' => $filterIDs,
					'title' => get_the_title(),
					'teaser' => get_field('job_short_descr'),
					'content' => $content,
					'url' => get_the_permalink(),
					'imageSmall' => $homeurl . get_the_post_thumbnail_url(get_the_ID(), $imgSize ),
					'alt' => $alt,
					'type' => 'jobs',
				]);
			endwhile;

			wp_reset_query();
		}
	}
	/** Get posts by cpt */
	elseif($select_type == 'cpt') {
		// Set number of posts to show ---
		if(!empty(get_field('cpt_number') && get_field('cpt_number') != '-1')) {
			$posts_per_page = get_field('cards_cpt_number');
		}
		else {
			$posts_per_page = '-1';
		}

		$query_array = array(
			'post_type' => array($postType),
			'posts_per_page'=> $posts_per_page,
			'meta_key'			=> 'job_order_date',
			'orderby'			=> 'meta_value_num',
			'order'				=> 'DESC'
		);

		$loop = new \WP_Query( $query_array );

		while ( $loop->have_posts() ) : $loop->the_post();
			$id = get_the_ID();
			$loc = get_field('job_location_short', $id);
			!empty($loc) ? $pens = ' / ' . get_field('job_pensum', $id) : $pens = get_field('job_pensum', $id);
			$content = '' . __('Arbeitsort / Arbeitsmodell:', 't3-theme') . ' <b>' . $loc . $pens . '</b><br/>
			' . __('Start Anstellung:', 't3-theme') . ' <b>' . get_field('job_start', $id) . '</b>';
			$moreLinkText = '' . __('Job ansehen', 't3-theme');

			//echo '->' . $filterIDs;
			$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
			$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);

			// $block['teaser'][] = get_post_meta('post_teaser', $loop->id);
			$block['list_items'][] = \App\template('partials.parts.items.list-item', [
				// 'filterIDs' => $filterIDs,
				'title' => get_the_title(),
				'teaser' => get_field('job_short_descr', $id),
				'content' => $content,
				'url' => get_the_permalink(),
				'imageSmall' => $homeurl . get_the_post_thumbnail_url($id, $imgSize ),
				'alt' => $alt,
				'type' => 'jobs',
				'order_date' => get_field('job_order_date', $id),
			]);
		endwhile;

		wp_reset_query();
	}

	/** Get posts by selection */
	else {
		$selected_posts = get_field('cpt_selection');
		if( $selected_posts ):
			foreach( $selected_posts as $post ):
				setup_postdata($post);
				$postType = $post->post_type;
				$thumbnail_id = get_post_thumbnail_id($post->ID);
				$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);

				$loc = get_field('job_location_short');
				!empty($loc) ? $pens = ' / ' . get_field('job_pensum') : $pens = get_field('job_pensum');
				$content = '' . __('Arbeitsort / Arbeitsmodell:', 't3-theme') . ' <b>' . $loc . $pens . '</b><br/>
				' . __('Start Anstellung:', 't3-theme') . ' <b>' . get_field('job_start') . '</b>';
				$thumbnail_id = get_post_thumbnail_id( $post->ID );
				$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);

				$block['list_items'][] = \App\template('partials.parts.items.list-item-cpts', [
					// 'filterIDs' => $filterIDs,
					'title' => get_the_title($post->ID),
					'teaser' => get_field('job_short_descr'),
					'content' => $content,
					'url' => get_the_permalink($post->ID),
					'imageSmall' => $homeurl . get_the_post_thumbnail_url($post->ID, $imgSize ),
					'alt' => $alt,
					'type' => 'jobs',
				]);
			endforeach;
	    wp_reset_postdata();
		endif;
	}

return $block;
});
