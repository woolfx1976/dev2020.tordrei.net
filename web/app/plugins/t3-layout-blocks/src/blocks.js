/** @file blocks.js - Include all of the plugin blocks. */

import './column-6/block';
import './column-4/block';
import './column-3/block';
import './layout-2-column/block';
import './layout-3-column/block';
import './layout-4-column/block';
