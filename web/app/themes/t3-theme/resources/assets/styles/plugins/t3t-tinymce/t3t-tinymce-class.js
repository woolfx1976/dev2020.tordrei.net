(function() {
//     tinymce.init({
//   selector: "textarea",  // change this value according to your HTML
//   plugins: "image",
//   menubar: "insert",
//   toolbar: "image",
//   image_caption: true
// });
    tinymce.PluginManager.add( 't3t_tinymce_class', function( editor, url ) {
        var cteaser = 'Teasertext - Mus, que nosae evellessum rersper ehentia quundeliquae de dolorepta istiam quo ipist vendit lis eriae. Itataquam sento ilit es dunto od quos et lantiae.';
        var ctext = 'Beispieltext - Mus, que nosae evellessum rersper ehentia quundeliquae de dolorepta istiam quo ipist vendit lis eriae. Itataquam sento ilit es dunto od quos et lantiae. Ut etur sum etus, seque pratem fugit es ellaborrum reictur solum et aut occuptaecto officid expel intoria siminve litiant otatem abora verib.';
        // Add Button to Visual Editor Toolbar
        editor.addButton('t3t_tinymce_class', {
            title: 'Insert 3 Columns Content',
            cmd: 't3t_tinymce_class',
            image: url + '/tinymce-icons/tinymce-3cols.svg',
        });
        // Add 3 Column Content Layout
        editor.addCommand('t3t_tinymce_class', function() {
            var string = '[row teaser="1" cols="3"][col colspan=4]'+cteaser+'[/col][col colspan=4]'+ctext+'[/col][col colspan=4]'+ctext+'[/col][/row]';
            editor.execCommand('mceInsertContent', false, string);
        });

        // Add Button to Visual Editor Toolbar
        editor.addButton('t3t_tinymce_class2', {
            title: 'Insert 2 Columns Content',
            cmd: 't3t_tinymce_class2',
            image: url + '/tinymce-icons/tinymce-2cols.svg',
        });
        // Add 2 Column Content Layout
        editor.addCommand('t3t_tinymce_class2', function() {
            var string = '[row teaser="1" cols="2"][col colspan=6]'+cteaser+'[/col][col colspan=6]'+ctext+'[/col][/row]';
            editor.execCommand('mceInsertContent', false, string);
        });

        // Add Button to Visual Editor Toolbar
        editor.addButton('t3t_tinymce_class3', {
            title: 'Insert Centered Content',
            cmd: 't3t_tinymce_class3',
            image: url + '/tinymce-icons/tinymce-center.svg',
        });
        // Add 2 Column Content Layout
        editor.addCommand('t3t_tinymce_class3', function() {
            var string = '[row center="1" align="center"][col colspan="10"]<h4>Hier steht eine Subüberschrift</h4><h2>Hier steht eine Überschrift</h2><h3 class="t3t-hl--teaser">'+ctext+'</h3>[/col][/row]';
            editor.execCommand('mceInsertContent', false, string);
        });

        // Add Button to Visual Editor Toolbar
        editor.addButton('t3t_tinymce_class8', {
            title: 'Insert Full-Width-Content',
            cmd: 't3t_tinymce_class8',
            image: url + '/tinymce-icons/tinymce-center.svg',
        });
        // Add 2 Column Content Layout
        editor.addCommand('t3t_tinymce_class8', function() {
            var string = '[row align="center"][col colspan="10"]<h2>Hier steht eine Überschrift</h2><h3>'+ctext+'</h3><p>'+ctext+'</p>[/col][/row]';
            editor.execCommand('mceInsertContent', false, string);
        });

        // Add Button to Visual Editor Toolbar
        editor.addButton('t3t_tinymce_class4', {
            title: 'Insert Spacer',
            cmd: 't3t_tinymce_class4',
            image: url + '/tinymce-icons/tinymce-spacer-medium.png',
        });
        // Add Spacer
        editor.addCommand('t3t_tinymce_class4', function() {
            var string = '<hr class="spacer" />';
            editor.execCommand('mceInsertContent', false, string);
        });

        // Add Button to Visual Editor Toolbar
        editor.addButton('t3t_tinymce_class5', {
            title: 'Insert 2 columns with images',
            cmd: 't3t_tinymce_class5',
            image: url + '/tinymce-icons/tinymce-2imgs.svg',
        });
        // Add Images to Content
        editor.addCommand('t3t_tinymce_class5', function() {
            var string = '<div class="t3t-block-content__img row align-left"><div class="columns small-12 medium-6 large-6"><figure><img class="alignnone fp-small wp-image-2130" src="http://solvion.domainfactory-kunde.at/wp-content/uploads/2017/08/ChristophKubesch_Solvion.jpg" alt="Hier Alt-Text eingeben" /><figcaption>Bildtext</figcaption></figure></div><div class="columns small-12 medium-6 large-6"><figure><img class="alignnone fp-small wp-image-2130" src="http://solvion.domainfactory-kunde.at/wp-content/uploads/2017/08/ChristophKubesch_Solvion.jpg" alt="Hier Alt-Text eingeben" /><figcaption>Bildtext</figcaption></figure></div></div>';
            editor.execCommand('mceInsertContent', false, string);
        });

        // Add Button to Visual Editor Toolbar
        editor.addButton('t3t_tinymce_class6', {
            title: 'Insert full-width image',
            cmd: 't3t_tinymce_class6',
            image: url + '/tinymce-icons/tinymce-img.svg',
        });
        // Add Full-Width-Image to Content
        editor.addCommand('t3t_tinymce_class6', function() {
            var string = '<div class="t3t-block-content__img row align-left"><div class="columns small-12"><figure><img class="alignnone size-full wp-image-2130" src="http://solvion.domainfactory-kunde.at/wp-content/uploads/2017/08/ChristophKubesch_Solvion.jpg" alt="Hier Alt-Text eingeben" /><figcaption>Bildtext</figcaption></figure></div></div>';
            editor.execCommand('mceInsertContent', false, string);
        });

        // Add Button to Visual Editor Toolbar
        editor.addButton('t3t_tinymce_class7', {
            title: 'Insert image- and text-column',
            cmd: 't3t_tinymce_class7',
            image: url + '/tinymce-icons/tinymce-img-txt.svg',
        });
        // Add Img-Content-Block to Content
        editor.addCommand('t3t_tinymce_class7', function() {
            var string = '<div class="t3t-block-content__img row align-left"><div class="columns small-12 medium-6 large-6"><figure><img class="alignnone size-full wp-image-2130" src="http://solvion.domainfactory-kunde.at/wp-content/uploads/2017/08/ChristophKubesch_Solvion.jpg" alt="Hier Alt-Text eingeben" /><figcaption>Text sidzgc gs zuc gus dgcuzgscsdc</figcaption></figure></div><div class="columns small-12 medium-6 large-6"><h3>Überschrift</h3><p>Hier steht etwas Text.</p></div></div>';
            editor.execCommand('mceInsertContent', false, string);
        });

    });
})();
