/* eslint-disable */
import { Foundation } from 'foundation-sites/js/foundation.core';
import { SmoothScroll }  from 'foundation-sites/js/foundation.smoothScroll';
import { Tabs }  from 'foundation-sites/js/foundation.tabs';
import { Abide }  from 'foundation-sites/js/foundation.abide';
import { MediaQuery } from 'foundation-sites/js/foundation.util.mediaQuery';

Foundation.addToJquery($);
Foundation.plugin(Tabs, 'Tabs');
Foundation.plugin(SmoothScroll, 'SmoothScroll');
Foundation.plugin(Abide, 'Abide');
$(document).foundation();

export default {
  init() {
    if( $('.b-cards--filter li').length ) {
      $('.c-list--filter li a').on( 'click', function(e) {
        e.preventDefault();
        $('.c-list--filter a').removeClass('active');
        $(this).toggleClass('active');
        var filterValue = $(this).attr('data-filter');
        var res = filterValue.split(' ');
        var classes = '';
        for(var i=0;i<res.length;i++){
          classes += '.'+res[i];
        }
        var visibleEl = $('.b-cards--filter li'+classes);
        var hiddenEl = $('.b-cards--filter li:not('+classes+')');

        visibleEl.show('300').addClass('fadeIn');
        hiddenEl.hide('300').removeClass('fadeIn');
      });
    }
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    var nav = $('header.banner nav');
    var hheight = $('main > header').outerHeight();
    var prev = 134;
    var header = $('header.banner');

    // APP
    var app = {
      init: function() {
        $('.c-fe-side').hide();
        if($(window).scrollTop() > 110) {
          nav.addClass('nav-fixed');
        }
        if($('.smooth-scroll').length) {
          var scroll_elm = new SmoothScroll($('.smooth-scroll'), {
            animationDuration : 1000,
            animationEasing: 'swing',
          });
        }
      },
      eSend: function(data, tld, dom) {
        var ep2, ep3;
        dom != '' ? ep2 = dom : ep2 = 'leeb';
        tld != '' ? ep3 = tld : ep3 = 'at';
        var uri = 'mailto:'+data+'@'+ep2+'.'+ep3;
        console.log(uri);
        window.location = uri;
      },
      setMegaTabOffset: function() {
        var vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
        var posTab = ((vw - 1220) / 2);
        var posContent = posTab + 300;
        if(vw > 1220) {
          console.log('vpw: ' + vw);
          console.log('posTab: ' + posTab);
          $('.mega-fw-tabbed > a').css({ left: posTab });
          $('.mega-fw-tabbed > ul').css({ left: posContent });
        }
      }
    }
    app.init();

    $().ready(() => {
      console.log('geladen !!!');
      var url = window.location.href;
      var links = document.querySelectorAll('.t3-mmm-item a');
      links.forEach((link) => {
        //if (url.indexOf(link.getAttribute('href')) > -1) {
        if (link.getAttribute('href') == url && window.location.pathname != '') {
          console.log('Unterpunkt');
          link.parentNode.classList.add('is-active');
          link.classList.add('is-active');
        }
      });
    });

    /** onLoad / Filter */
    //$(window).on('load', function(){
    $(".esend").on('click',function(e) {
      e.preventDefault();
      app.eSend($(this).data("esend"), $(this).data("etld"), $(this).data("edname"));
    });


    /** On scrolling - Settings for header and nav */
    $(window).on('scroll', function(){

      console.log('hheight:' + hheight);
      var scrollTop = $(window).scrollTop(); // optional
      if($('header.nav-type--sh').length || $('header.nav-type--fixed').length) {
        $(window).scrollTop() > 110 ? $('header nav').addClass('add-nav-bg') : $('nav').removeClass('add-nav-bg');
        //console.log('scrollTop ' + scrollTop + ' - hheight: ' + hheight);
      }

      //var scrollTop = $(window).scrollTop();
      scrollTop > 100 ? $('.back-to-top').addClass('show') : $('.back-to-top').removeClass('show');
      if(scrollTop > 300) {
        $('.c-fe-side').show();
        $('.c-fe-side').addClass('show');
        $('.c-fe-postnav').addClass('show');
      }
      else {
        $('.c-fe-side').removeClass('show');
        $('.c-fe-postnav').removeClass('show');
        $('.c-fe-side').delay(1000).hide();
      }

      var header_height = 134;
      if(MediaQuery.is('medium')) {
        if(document.documentElement.clientWidth >= 768) {
          if($('header.nav-type--sh').length) {
            if(prev > hheight) { // Change value to header height if nav is not transparent ---
              header.addClass('is-fixed', scrollTop > prev);
            } else {
              header.removeClass('is-fixed', scrollTop < prev);
            }
            if(prev > header_height) {
              header.toggleClass('is-hidden', scrollTop > prev);
              header.toggleClass('add-nav-bg', scrollTop > hheight);
            }
          }
          if($('header.nav-type--fixed').length) {
            if(prev > header_height) {
              header.addClass('is-fixed', scrollTop > prev);
            }
            if(scrollTop <= 1) {
              header.removeClass('is-fixed');
            }
          }
          prev = scrollTop;
        }
      }
    });

  },
};
