@php
  isset($block) ? $fh = $block['data'] : $fh = $page_header;
  // if(array_key_exists('header_show_content', $fh)):
  //   $show_content = $fh['header_show_content'];
  // else:
  //   $show_content = false;
  // endif;
@endphp
<div class="grid-container">
  <div class="grid-x">
    <div class="cell small-12 medium-9 medium-offset-1">
      <div class="c-fh__wrapper">
        {!!$fh['header_content']!!}
        @if($fh['header_breadcrumb'])
          {!! App::breadcrumbNav() !!}
        @endif
      </div>
    </div>
  </div>
</div>
