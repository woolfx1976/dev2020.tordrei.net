<?php
/**
* Product custom post type
*/
add_action('init', function () {
    $labels = array(
        'name'                => _x( 'Produkte', 'Post Type General Name', 'foundationpress' ),
        'singular_name'       => _x( 'Produkt', 'Post Type Singular Name', 'foundationpress' ),
        'menu_name'           => __( 'Produkte', 'foundationpress' ),
        'parent_item_colon'   => __( 'Eltern-Produkt', 'foundationpress' ),
        'all_items'           => __( 'Alle Produkte', 'foundationpress' ),
        'view_item'           => __( 'Produkt zeigen', 'foundationpress' ),
        'add_new_item'        => __( 'Neues Produkt erstellen', 'foundationpress' ),
        'add_new'             => __( 'Neues Produkt', 'foundationpress' ),
        'edit_item'           => __( 'Produkt bearbeiten', 'foundationpress' ),
        'update_item'         => __( 'Produkt aktualisieren', 'foundationpress' ),
        'search_items'        => __( 'Produkt suchen', 'foundationpress' ),
        'not_found'           => __( 'Nicht gefunden', 'foundationpress' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'foundationpress' ),
    );
    register_extended_post_type('product', [
        'labels' => $labels,
        'menu_icon' => 'dashicons-screenoptions',
        'show_in_rest' => true,
        'supports' => array('title','thumbnail','catgegories'),
        'has_archive' => false,
        'rewrite' => true,
    ], [
        'singular' => 'Produkt',
        'plural' => 'Produkte',
        'slug' => 'produkte',
    ]);
    /**
    *   To Activate Custom Post Type Single page
    *   @see http://en.bainternet.info/2011/custom-post-type-getting-404-on-permalinks
    */
    $set = get_option('post_type_rules_flased_authors');
    if ($set !== true){
        flush_rewrite_rules(false);
        update_option('post_type_rules_flased_authors',true);
    }

    /**
    * Update order field if posts exists (w/o order value)
    */

    // $args = array(
    //     'post_type' => 'product', /* product post type */
    //     'posts_per_page' => -1 /* all products */
    // );
    //
    // $products = new WP_Query( $args );
    //
    // // check if products exists
    // if( $products->have_posts() ) {
    //
    //     // loop products
    //     while( $products->have_posts() ): $products->the_post();
    //
    //         // check if field exists
    //         $new_value = '999';
    //         if( !get_field( 'product_order' ) ) {
    //             if ( $new_value !== get_field( 'product_order' ) ) {
    //                 update_field( 'product_order', $new_value ); /* update field */
    //             }
    //         }
    //         elseif( empty(get_field( 'product_order' )) ) {
    //             update_field( 'product_order', $new_value ); /* update field */
    //         }
    //
    //     endwhile;
    //
    //     // reset query to default query
    //     wp_reset_postdata();
    // }

});

/**
* Product taxonomies
*/
add_action('init', function () {
    register_extended_taxonomy('product_category', 'product', [
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array( 'slug' => 'product_category', 'with_front' => false ),
    ], [
        'singular' => 'Produkt Kategorie',
        'plural' => 'Produkt Kategorien',
    ]);
});

// Or via a filter:
// add_filter( 'query_vars', 'wpse12965_query_vars' );
// function wpse12965_query_vars( $query_vars )
// {
//     $query_vars[] = 'catslug';
//     return $query_vars;
// }


//
// function change_link( $permalink, $post ) {
//
//     if( $post->post_type == 'students' ) {
//         $resource_terms = get_the_terms( $post, 'student_types' );
//         $term_slug = '';
//         if( ! empty( $resource_terms ) ) {
//             foreach ( $resource_terms as $term ) {
//                 // The featured resource will have another category which is the main one
//
//                 $term_slug = $term->slug;
//                 break;
//             }
//         }
//         $permalink = apply_filters( 'wpml_permalink', trailingslashit( get_home_url() ) . $term_slug . '/' . $post->post_name );
//     }
//     return $permalink;
// }
// add_filter('post_type_link',"change_link",10,2);



function product_post_link( $post_link, $id = 0 ){

    $post = get_post( $id );

    if ( is_object( $post ) ){
      // Get YOAST Primary Category
      // if ( class_exists('WPSEO_Primary_Term') ) {
      //   // Show the post's 'Primary' category, if this Yoast feature is available, & one is set. category can be replaced with custom terms
    
      //   $wpseo_primary_term = new WPSEO_Primary_Term( 'product_category', $post->ID );
    
      //   $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
      //   $term = get_term( $wpseo_primary_term );
      //   if (is_wp_error($term)) {
      //     //var_dump($term);
      //     $categories = wp_get_post_terms($post->ID, 'product_category');
      //     if(is_array($categories) && !empty($array[0])):
      //       $cat_name = $categories[0]->name;
      //     endif;
      //   } else {
      //     $cat_name = $term->name;
      //   }
      //   //echo '->' . $cat_name;
      // }

        $terms = wp_get_post_terms( $post->ID, 'product_category' );

        $linkHasCat = false;
        $parentCatSlug = '';
        if(defined('ICL_LANGUAGE_CODE')): $lang = ICL_LANGUAGE_CODE; else: $lang = 'de'; endif;

        $prodSlugs = array(
          'de' => 'produkte',
          'at' => 'produkte',
          'ch' => 'produkte',
          'it' => 'prodotti',
          'sl' => 'produkti',
        );

        if($linkHasCat === false) {
            foreach($terms as $term) {
                /***
                * Product Categories IDs ---
                **/
                $catdIDs = array(
                  'de' => array(
                    'bga' => '3',
                    // 'glb' => '193',
                    'bgh' => '17',
                    'zal' => '18',
                    'zta' => '41',
                    'anb' => '28',
                    'but' => '45',
                  ),
                  'at' => array(
                    'bga' => '24',
                    // 'glb' => '194',
                    'bgh' => '86',
                    'zal' => '72',
                    'zta' => '98',
                    'anb' => '74',
                    'but' => '76',
                  ),
                  'ch' => array(
                    'bga' => '71',
                    // 'glb' => '195',
                    'bgh' => '87',
                    'zal' => '73',
                    'zta' => '69',
                    'anb' => '75',
                    'but' => '77',
                  ),
                  'it' => array(
                    'bga' => '104',
                    // 'glb' => '',
                    'bgh' => '110',
                    'zal' => '101',
                    'zta' => '116',
                    'anb' => '102',
                    'but' => '103',
                  ),
                  'sl' => array(
                    'bga' => '121',
                    // 'glb' => '',
                    'bgh' => '127',
                    'zal' => '118',
                    'zta' => '133',
                    'anb' => '119',
                    'but' => '120',
                  ),
                );

                /** Developement */
                // $catdIDs = array(
                //   'de' => array(
                //     'bga' => '7',
                //     'bgh' => '12',
                //     'zal' => '',
                //     'zta' => '',
                //     'anb' => '',
                //     'but' => '',
                //     'glb' => '',
                //   ),
                //   'at' => array(
                //     'bga' => '7',
                //     'bgh' => '12',
                //     'zal' => '',
                //     'zta' => '',
                //     'anb' => '',
                //     'but' => '',
                //     'glb' => '',
                //   ),
                //   'ch' => array(
                //     'bga' => '7',
                //     'bgh' => '12',
                //     'zal' => '',
                //     'zta' => '',
                //     'anb' => '',
                //     'but' => '',
                //     'glb' => '',
                //   ),
                //   'it' => array(
                //     'bga' => '7',
                //     'bgh' => '33',
                //     'zal' => '',
                //     'zta' => '',
                //     'anb' => '',
                //     'but' => '',
                //     'glb' => '',
                //   ),
                //   'sl' => array(
                //     'bga' => '51',
                //     'bgh' => '56',
                //     'zal' => '',
                //     'zta' => '',
                //     'anb' => '',
                //     'but' => '',
                //     'glb' => '',
                //   ),
                // );

                if(isset($_GET['work'])): echo '(' . $term->term_id . ' - ' . $term->slug . ' - ' . $post->ID; endif;

                if( $term->parent == '0' ) { /** Production */
                    // if( $lang == 'de' ) {
                        // if( $term->term_id == $catdIDs[$lang]['bga'] || $term->term_id == $catdIDs[$lang]['glb'] || $term->term_id == $catdIDs[$lang]['bgh'] || $term->term_id == $catdIDs[$lang]['zal'] || $term->term_id == $catdIDs[$lang]['zta'] || $term->term_id == $catdIDs[$lang]['anb'] || $term->term_id == $catdIDs[$lang]['but'] ) {
                        if( $term->term_id == $catdIDs[$lang]['bga'] || $term->term_id == $catdIDs[$lang]['bgh'] || $term->term_id == $catdIDs[$lang]['zal'] || $term->term_id == $catdIDs[$lang]['zta'] || $term->term_id == $catdIDs[$lang]['anb'] || $term->term_id == $catdIDs[$lang]['but'] ) {
                            $linkHasCat = true;
                            $parentCatSlug  = $term->slug;
                        }
                    // }
                    // elseif( $lang == 'at' ) {
                    //     if( $term->term_id == '24' ) {
                    //         $linkHasCat = true;
                    //         $parentCatSlug  = $term->slug;
                    //     }
                    // }
                }

                // if( $term->parent == '0' ) { /** Developement */
                //     if( $lang == 'de' ) {
                //         if( $term->term_id == '7' || $term->term_id == '12' ) {
                //             $linkHasCat = true;
                //             $parentCatSlug  = $term->slug;
                //         }
                //     }
                // }

                if(isset($_GET['work'])): echo ' - linkHasCat: ' . $linkHasCat . ')<br/>'; endif;
                /** Production */
                //if( ( (  $term->term_id == '7' AND $lang == 'de'  ) || (  $term->term_id == '12' AND $lang == 'de'  ) ) AND $term->parent == '0' ) {
                //return str_replace( "/product" , "/produkte/" . $term->slug , $post_link );
            }
        }
        if( $linkHasCat === true) {
            if(isset($_GET['work'])): echo '(' . $term->term_id . ' - ' . $term->slug . ' - ' . $post->ID .  ')'; endif;
            // $linkHasCat = false;
            //echo str_replace( "/product" , "/produkte/" . $term->slug , $post_link );
            return str_replace( "/product" , "/" . $prodSlugs[$lang] . "/" . $parentCatSlug , $post_link );
            // apply_filters( 'wpml_permalink', trailingslashit( get_home_url() ) . $prodSlugs[$lang] . "/" . $parentCatSlug . '/' . $post->post_name );
        }
        else {
            return str_replace( "/product" , "/" . $prodSlugs[$lang] , $post_link );
        }
    }

    return $post_link;
}

// add_filter('generate_rewrite_rules', function($wp_rewrite) {
//     global $sitepress, $sitepress_settings;
//
//     $linkHasCat = false;
//     $parentCatSlug = '';
//
//     /** Developement */
//     $catdIDs = array(
//       'de' => array(
//         'bga' => '7',
//         'bgh' => '12',
//         'zal' => '',
//         'zta' => '',
//         'anb' => '',
//         'but' => '',
//       ),
//       'at' => array(
//         'bga' => '7',
//         'bgh' => '12',
//         'zal' => '',
//         'zta' => '',
//         'anb' => '',
//         'but' => '',
//       ),
//       'ch' => array(
//         'bga' => '7',
//         'bgh' => '12',
//         'zal' => '',
//         'zta' => '',
//         'anb' => '',
//         'but' => '',
//       ),
//       'it' => array(
//         'bga' => '7',
//         'bgh' => '33',
//         'zal' => '',
//         'zta' => '',
//         'anb' => '',
//         'but' => '',
//       ),
//       'sl' => array(
//         'bga' => '51',
//         'bgh' => '56',
//         'zal' => '',
//         'zta' => '',
//         'anb' => '',
//         'but' => '',
//       ),
//     );
//
//
//
//     $has_filter = remove_filter( 'terms_clauses', array( $sitepress, 'terms_clauses' ) );
//     $auto_adjust_ids = $sitepress_settings['auto_adjust_ids'];
//     $sitepress_settings['auto_adjust_ids'] = 0;
//
//     $rules = array();
//     $terms = get_terms( array(
//         'taxonomy' => 'product_category',
//         'hide_empty' => false,
//     ) );
//
//     $post_type = 'product';
//
//     foreach ($terms as $term) {
//         //$rules[ $term->slug . '/([^/]*)$'] = 'index.php?post_type=' . $post_type. '&name=$matches[1]';
//         if( $term->parent == '0' ) { /** Production */
//             if( $term->term_id == $catdIDs[$lang]['bga'] || $term->term_id == $catdIDs[$lang]['bgh'] || $term->term_id == $catdIDs[$lang]['zal'] || $term->term_id == $catdIDs[$lang]['zta'] || $term->term_id == $catdIDs[$lang]['anb'] || $term->term_id == $catdIDs[$lang]['but'] ) {
//                 $linkHasCat = true;
//                 $parentCatSlug  = $term->slug;
//             }
//         }
//         $rules[ $term->slug . '[/(.+)/?$'] = 'index.php?post_type=' . $post_type. '&name=$matches[1]';
//
//     }
//
//     // merge with global rules
//     // $wp_rewrite->rules = $rules + $wp_rewrite->rules;
//     //
//     // if ( $has_filter ) {
//     //     add_filter( 'terms_clauses', array( $sitepress, 'terms_clauses', 0 ) );
//     // }
//     // $sitepress_settings['auto_adjust_ids'] = $auto_adjust_ids;
//
// });

add_filter( 'post_type_link', 'product_post_link', 1, 3 );

// function product_generate_rewrite_rules( $wp_rewrite ) {
//   $new_rules = array(
//       'produkte/([^/]+)/(.+)/?$' => 'index.php?product=$matches[2]',
//       'produkte/(atrium)/?$' => 'index.php?product=$matches[1]',
//       'produkte/(sichtschutz)/?$' => 'index.php?product=$matches[1]',
//       'produkte/(terrassenueberdachungen)/?$' => 'index.php?product=$matches[1]',
//       'produkte/(sommergaerten)/?$' => 'index.php?product=$matches[1]',
//       'produkte/(balkonueberdachungen)/?$' => 'index.php?product=$matches[1]',
//       'produkte/(carports)/?$' => 'index.php?product=$matches[1]',
//       'produkte/(solar-terrassendach)/?$' => 'index.php?product=$matches[1]',
//       );
//   $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
// }
// add_action( 'generate_rewrite_rules', 'product_generate_rewrite_rules' );

function custom_add_rewrite_rules( $rules ) {

    $new = array();
    $new['produkte/([^/]+)/(.+)/?$'] = 'index.php?product=$matches[2]';
    $new['prodotti/([^/]+)/(.+)/?$'] = 'index.php?product=$matches[2]';
    $new['produkti/([^/]+)/(.+)/?$'] = 'index.php?product=$matches[2]';
    $new['produkte/(atrium)/?$'] = 'index.php?product=$matches[1]';
    $new['produkte/(sichtschutz)/?$'] = 'index.php?product=$matches[1]';
    $new['produkte/(terrassenueberdachung)/?$'] = 'index.php?product=$matches[1]';
    $new['produkte/(sommergarten)/?$'] = 'index.php?product=$matches[1]';
    // $new['produkte/(balkonueberdachungen)/?$'] = 'index.php?product=$matches[1]';
    $new['produkte/(carport)/?$'] = 'index.php?product=$matches[1]';
    $new['produkte/(solar-carport)/?$'] = 'index.php?product=$matches[1]';
    $new['produkte/(solar-terrassendach)/?$'] = 'index.php?product=$matches[1]';
    $new['prodotti/(atrium)/?$'] = 'index.php?product=$matches[1]';
    $new['prodotti/(protezione-dalla-vista)/?$'] = 'index.php?product=$matches[1]';
    $new['prodotti/(tettoie-per-terrazze-e-balconi)/?$'] = 'index.php?product=$matches[1]';
    $new['prodotti/(giardino-estivo)/?$'] = 'index.php?product=$matches[1]';
    // $new['prodotti/(balkonueberdachungen)/?$'] = 'index.php?product=$matches[1]';
    $new['prodotti/(carports)/?$'] = 'index.php?product=$matches[1]';
    $new['prodotti/(tettoia-solare-da-terrazza)/?$'] = 'index.php?product=$matches[1]';
    $new['produkti/(atrium)/?$'] = 'index.php?product=$matches[1]';
    $new['produkti/(ograje-in-zascita-pred-pogledi)/?$'] = 'index.php?product=$matches[1]';
    $new['produkti/(zastresenie-terasy)/?$'] = 'index.php?product=$matches[1]';
    $new['produkti/(terasa-kot-letni-vrt)/?$'] = 'index.php?product=$matches[1]';
    // $new['produkti/(balkonueberdachungen)/?$'] = 'index.php?product=$matches[1]';
    $new['produkti/(carports)/?$'] = 'index.php?product=$matches[1]';
    $new['produkti/(solarna-streha-terase)/?$'] = 'index.php?product=$matches[1]';

    if(!isset($rules)) {
        return $new;
    }
    else {
        return array_merge( $new, $rules ); // Ensure our rules come first
    }
    // return $wp_rewrite->rules = $new + $wp_rewrite->rules;
}
add_filter( 'rewrite_rules_array', 'custom_add_rewrite_rules' );
