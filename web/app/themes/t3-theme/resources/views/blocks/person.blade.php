{{--
  Name: person
  Title: Person
  Description: cards, person, contact
  Category: t3layout
  Icon: admin-users
  Keywords: person card
  Mode: view
  Align: wide
  PostTypes: page post product
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
  $block['layout'] == '4' ? $layout = array('left' => 'small-12 medium-4', 'right' => 'small-12 medium-8') : $layout = array('left' => 'small-12 medium-6', 'right' => 'small-12 medium-6');
@endphp

<div class="c-person grid-x grid-margin-x">
  <div class="c-person__img cell {!!$layout['left']!!}">
      <div class="c-person__img-wrapper">
        {{-- @if(!is_admin())
          <img data-src="{!!$block['imageSmall']!!}" class="lazyload h-rounded-img" alt="{!!$block['alt']!!}" />
        @else --}}
          <img src="{!!$block['imageSmall']!!}" class="h-rounded-img" alt="{!!$block['alt']!!}" width="640" height="432" />
        {{-- @endif --}}
      </div>
  </div>
  <div class="c-person__txt cell {!!$layout['right']!!}">
    <div class="c-person__txt-wrapper h-lc-no-mb">
      @if($block['onlyText'] != '1')
        <h3>
          @if(!empty($block['graduation']))
            {!!$block['graduation']!!}
          @endif
          {!!$block['firstname']!!} {!!$block['lastname']!!}
        </h3>
        @if($block['props']['show_function'] == '1')
          <small>{!!$block['function']!!}</small>
        @endif
        @if($block['props']['show_blockquote'] == '1')
          @if($block['props']['has_format'] != '1')
            <p>{!!$block['blockquote']!!}</p>
          @else
            <p class="blockquote">{!!$block['blockquote']!!}</p>
          @endif
        @endif
      @endif

      @if(!empty($block['txt']))
        <p>{!!$block['txt']!!}</p>
      @endif

      @if($block['button'] != '0')
        {{-- 1 = Button of Link, 2 = Email, 3 = Phone --}}
        @if($block['button'] == '1' && $block['props']['link'])
          <a href="{!!$block['props']['link']['url']!!}" class="button">{!!$block['props']['link']['title']!!}</a>
        @elseif($block['button'] == '2' && !empty($block['email']))
          <a href="#" class="esend button" data-esend="{!!$block['email']!!}" data-etld="{!!$block['emailTLD']!!}" data-edname="{!!$block['emailDomainname']!!}"><?php _e('Email senden', 't3-theme'); ?></a>
        @elseif($block['button'] == '3' && !empty($block['tel']))
          <a href="tel:{!!$block['tel']!!}" class="button"><?php _e('Jetzt anrufen', 't3-theme'); ?></a>
        @endif
      @endif
      @if(!empty($block['props']['has_detail']))
        <a href="#" class="button hollow"><?php _e('Details', 't3-theme'); ?></a>
      @endif
    </div>
  </div>
</div>
