@php
  isset($block) ? $fh = $block['data'] : $fh = $page_header;
  if(array_key_exists('header_show_content', $fh)):
    $show_content = $fh['header_show_content'];
  else:
    $show_content = false;
  endif;
@endphp

<img src="{!! $fh['imgsmall'] !!}" srcset="{!! $fh['srcset'] !!}" alt="{!! $fh['alt'] !!}" width="640" height="375" />

<div class="grid-container">
  <div class="grid-x">
    <div class="cell small-12 medium-6 medium-offset-1">
      <div class="c-fh__wrapper">
        @if($show_content)
          @if(!empty($fh['header_teaser']))
            {!! $fh['header_teaser'] !!}
          @else
            {!! $fh['header_content'] !!}
          @endif
        @endif
      </div>
    </div>
  </div>
</div>
